webpackJsonp([0],[
/* 0 */,
/* 1 */,
/* 2 */,
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return auth; });
var auth = {
    data: function data() {
        return {
            token: localStorage.getItem('token')
        };
    },

    // created () {
    //     this.authentication()
    // },
    methods: {
        authentication: function authentication() {
            // console.log('authentication')
            if (!this.token || this.token != '') {
                // console.log('not login')
                this.$router.push({ 'name': 'PageFeedapi' });
            }
        }
    },
    mounted: function mounted() {
        if (!this.token || this.token === '') {
            // this.token = token
            this.$router.push({ 'name': 'PageLogin' });
        }
    }
};

/***/ }),
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */,
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/img/logo-coba-coba.b155c6c.png";

/***/ }),
/* 9 */,
/* 10 */,
/* 11 */,
/* 12 */,
/* 13 */,
/* 14 */,
/* 15 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_NotFound_vue__ = __webpack_require__(61);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_55829c07_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_NotFound_vue__ = __webpack_require__(163);
function injectStyle (ssrContext) {
  __webpack_require__(161)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-55829c07"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_NotFound_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_55829c07_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_NotFound_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 16 */,
/* 17 */,
/* 18 */,
/* 19 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Licensed to the Software Freedom Conservancy (SFC) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The SFC licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.



/**
 * The base WebDriver error type. This error type is only used directly when a
 * more appropriate category is not defined for the offending error.
 */
class WebDriverError extends Error {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);

    /** @override */
    this.name = this.constructor.name;

    /**
     * A stacktrace reported by the remote webdriver endpoint that initially
     * reported this error. This property will be an empty string if the remote
     * end did not provide a stacktrace.
     * @type {string}
     */
    this.remoteStacktrace = '';
  }
}


/**
 * An attempt was made to select an element that cannot be selected.
 */
class ElementNotSelectableError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * Indicates a command could not be completed because the target element is
 * not pointer or keyboard interactable. This will often occur if an element
 * is present in the DOM, but not rendered (i.e. its CSS style has
 * "display: none").
 */
class ElementNotInteractableError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * An element command could not be completed because the element is not visible
 * on the page.
 *
 * @deprecated Use {@link ElementNotInteractable} instead.
 */
class ElementNotVisibleError extends ElementNotInteractableError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * The arguments passed to a command are either invalid or malformed.
 */
class InvalidArgumentError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * An illegal attempt was made to set a cookie under a different domain than
 * the current page.
 */
class InvalidCookieDomainError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * The coordinates provided to an interactions operation are invalid.
 */
class InvalidElementCoordinatesError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * An element command could not be completed because the element is in an
 * invalid state, e.g. attempting to click an element that is no longer attached
 * to the document.
 */
class InvalidElementStateError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * Argument was an invalid selector.
 */
class InvalidSelectorError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * Occurs when a command is directed to a session that does not exist.
 */
class NoSuchSessionError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * An error occurred while executing JavaScript supplied by the user.
 */
class JavascriptError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * The target for mouse interaction is not in the browser’s viewport and cannot
 * be brought into that viewport.
 */
class MoveTargetOutOfBoundsError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * An attempt was made to operate on a modal dialog when one was not open.
 */
class NoSuchAlertError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * An element could not be located on the page using the given search
 * parameters.
 */
class NoSuchElementError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * A request to switch to a frame could not be satisfied because the frame
 * could not be found.
 */
class NoSuchFrameError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * A request to switch to a window could not be satisfied because the window
 * could not be found.
 */
class NoSuchWindowError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * A script did not complete before its timeout expired.
 */
class ScriptTimeoutError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * A new session could not be created.
 */
class SessionNotCreatedError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}



/**
 * An element command failed because the referenced element is no longer
 * attached to the DOM.
 */
class StaleElementReferenceError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * An operation did not complete before its timeout expired.
 */
class TimeoutError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * A request to set a cookie’s value could not be satisfied.
 */
class UnableToSetCookieError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * A screen capture operation was not possible.
 */
class UnableToCaptureScreenError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * A modal dialog was open, blocking this operation.
 */
class UnexpectedAlertOpenError extends WebDriverError {
  /**
   * @param {string=} opt_error the error message, if any.
   * @param {string=} opt_text the text of the open dialog, if available.
   */
  constructor(opt_error, opt_text) {
    super(opt_error);

    /** @private {(string|undefined)} */
    this.text_ = opt_text;
  }

  /**
   * @return {(string|undefined)} The text displayed with the unhandled alert,
   *     if available.
   */
  getAlertText() {
    return this.text_;
  }
}


/**
 * A command could not be executed because the remote end is not aware of it.
 */
class UnknownCommandError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * The requested command matched a known URL but did not match an method for
 * that URL.
 */
class UnknownMethodError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}


/**
 * Reports an unsupported operation.
 */
class UnsupportedOperationError extends WebDriverError {
  /** @param {string=} opt_error the error message, if any. */
  constructor(opt_error) {
    super(opt_error);
  }
}

// TODO(jleyba): Define UnknownError as an alias of WebDriverError?


/**
 * Enum of legacy error codes.
 * TODO: remove this when all code paths have been switched to the new error
 * types.
 * @deprecated
 * @enum {number}
 */
const ErrorCode = {
  SUCCESS: 0,
  NO_SUCH_ELEMENT: 7,
  NO_SUCH_FRAME: 8,
  UNKNOWN_COMMAND: 9,
  UNSUPPORTED_OPERATION: 9,
  STALE_ELEMENT_REFERENCE: 10,
  ELEMENT_NOT_VISIBLE: 11,
  INVALID_ELEMENT_STATE: 12,
  UNKNOWN_ERROR: 13,
  ELEMENT_NOT_SELECTABLE: 15,
  JAVASCRIPT_ERROR: 17,
  XPATH_LOOKUP_ERROR: 19,
  TIMEOUT: 21,
  NO_SUCH_WINDOW: 23,
  INVALID_COOKIE_DOMAIN: 24,
  UNABLE_TO_SET_COOKIE: 25,
  UNEXPECTED_ALERT_OPEN: 26,
  NO_SUCH_ALERT: 27,
  SCRIPT_TIMEOUT: 28,
  INVALID_ELEMENT_COORDINATES: 29,
  IME_NOT_AVAILABLE: 30,
  IME_ENGINE_ACTIVATION_FAILED: 31,
  INVALID_SELECTOR_ERROR: 32,
  SESSION_NOT_CREATED: 33,
  MOVE_TARGET_OUT_OF_BOUNDS: 34,
  SQL_DATABASE_ERROR: 35,
  INVALID_XPATH_SELECTOR: 51,
  INVALID_XPATH_SELECTOR_RETURN_TYPE: 52,
  METHOD_NOT_ALLOWED: 405
};


const LEGACY_ERROR_CODE_TO_TYPE = new Map([
    [ErrorCode.NO_SUCH_ELEMENT, NoSuchElementError],
    [ErrorCode.NO_SUCH_FRAME, NoSuchFrameError],
    [ErrorCode.UNSUPPORTED_OPERATION, UnsupportedOperationError],
    [ErrorCode.STALE_ELEMENT_REFERENCE, StaleElementReferenceError],
    [ErrorCode.ELEMENT_NOT_VISIBLE, ElementNotVisibleError],
    [ErrorCode.INVALID_ELEMENT_STATE, InvalidElementStateError],
    [ErrorCode.UNKNOWN_ERROR, WebDriverError],
    [ErrorCode.ELEMENT_NOT_SELECTABLE, ElementNotSelectableError],
    [ErrorCode.JAVASCRIPT_ERROR, JavascriptError],
    [ErrorCode.XPATH_LOOKUP_ERROR, InvalidSelectorError],
    [ErrorCode.TIMEOUT, TimeoutError],
    [ErrorCode.NO_SUCH_WINDOW, NoSuchWindowError],
    [ErrorCode.INVALID_COOKIE_DOMAIN, InvalidCookieDomainError],
    [ErrorCode.UNABLE_TO_SET_COOKIE, UnableToSetCookieError],
    [ErrorCode.UNEXPECTED_ALERT_OPEN, UnexpectedAlertOpenError],
    [ErrorCode.NO_SUCH_ALERT, NoSuchAlertError],
    [ErrorCode.SCRIPT_TIMEOUT, ScriptTimeoutError],
    [ErrorCode.INVALID_ELEMENT_COORDINATES, InvalidElementCoordinatesError],
    [ErrorCode.INVALID_SELECTOR_ERROR, InvalidSelectorError],
    [ErrorCode.SESSION_NOT_CREATED, SessionNotCreatedError],
    [ErrorCode.MOVE_TARGET_OUT_OF_BOUNDS, MoveTargetOutOfBoundsError],
    [ErrorCode.INVALID_XPATH_SELECTOR, InvalidSelectorError],
    [ErrorCode.INVALID_XPATH_SELECTOR_RETURN_TYPE, InvalidSelectorError],
    [ErrorCode.METHOD_NOT_ALLOWED, UnsupportedOperationError]]);


const ERROR_CODE_TO_TYPE = new Map([
    ['unknown error', WebDriverError],
    ['element not interactable', ElementNotInteractableError],
    ['element not selectable', ElementNotSelectableError],
    ['element not visible', ElementNotVisibleError],
    ['invalid argument', InvalidArgumentError],
    ['invalid cookie domain', InvalidCookieDomainError],
    ['invalid element coordinates', InvalidElementCoordinatesError],
    ['invalid element state', InvalidElementStateError],
    ['invalid selector', InvalidSelectorError],
    ['invalid session id', NoSuchSessionError],
    ['javascript error', JavascriptError],
    ['move target out of bounds', MoveTargetOutOfBoundsError],
    ['no such alert', NoSuchAlertError],
    ['no such element', NoSuchElementError],
    ['no such frame', NoSuchFrameError],
    ['no such window', NoSuchWindowError],
    ['script timeout', ScriptTimeoutError],
    ['session not created', SessionNotCreatedError],
    ['stale element reference', StaleElementReferenceError],
    ['timeout', TimeoutError],
    ['unable to set cookie', UnableToSetCookieError],
    ['unable to capture screen', UnableToCaptureScreenError],
    ['unexpected alert open', UnexpectedAlertOpenError],
    ['unknown command', UnknownCommandError],
    ['unknown method', UnknownMethodError],
    ['unsupported operation', UnsupportedOperationError]]);


const TYPE_TO_ERROR_CODE = new Map;
ERROR_CODE_TO_TYPE.forEach((value, key) => {
  TYPE_TO_ERROR_CODE.set(value, key);
});



/**
 * @param {*} err The error to encode.
 * @return {{error: string, message: string}} the encoded error.
 */
function encodeError(err) {
  let type = WebDriverError;
  if (err instanceof WebDriverError
      && TYPE_TO_ERROR_CODE.has(err.constructor)) {
    type = err.constructor;
  }

  let message = err instanceof Error
      ? err.message
      : err + '';

  let code = /** @type {string} */(TYPE_TO_ERROR_CODE.get(type));
  return {'error': code, 'message': message};
}


/**
 * Checks a response object from a server that adheres to the W3C WebDriver
 * protocol.
 * @param {*} data The response data to check.
 * @return {*} The response data if it was not an encoded error.
 * @throws {WebDriverError} the decoded error, if present in the data object.
 * @deprecated Use {@link #throwDecodedError(data)} instead.
 * @see https://w3c.github.io/webdriver/webdriver-spec.html#protocol
 */
function checkResponse(data) {
  if (data && typeof data.error === 'string') {
    let ctor = ERROR_CODE_TO_TYPE.get(data.error) || WebDriverError;
    throw new ctor(data.message);
  }
  return data;
}

/**
 * Tests if the given value is a valid error response object according to the
 * W3C WebDriver spec.
 *
 * @param {?} data The value to test.
 * @return {boolean} Whether the given value data object is a valid error
 *     response.
 * @see https://w3c.github.io/webdriver/webdriver-spec.html#protocol
 */
function isErrorResponse(data) {
  return data && typeof data === 'object' && typeof data.error === 'string';
}

/**
 * Throws an error coded from the W3C protocol. A generic error will be thrown
 * if the provided `data` is not a valid encoded error.
 *
 * @param {{error: string, message: string}} data The error data to decode.
 * @throws {WebDriverError} the decoded error.
 * @see https://w3c.github.io/webdriver/webdriver-spec.html#protocol
 */
function throwDecodedError(data) {
  if (isErrorResponse(data)) {
    let ctor = ERROR_CODE_TO_TYPE.get(data.error) || WebDriverError;
    let err = new ctor(data.message);
    if (typeof data.stacktrace === 'string') {
      err.remoteStacktrace = data.stacktrace;
    }
    throw err;
  }
  throw new WebDriverError('Unknown error: ' + JSON.stringify(data));
}


/**
 * Checks a legacy response from the Selenium 2.0 wire protocol for an error.
 * @param {*} responseObj the response object to check.
 * @return {*} responseObj the original response if it does not define an error.
 * @throws {WebDriverError} if the response object defines an error.
 */
function checkLegacyResponse(responseObj) {
  // Handle the legacy Selenium error response format.
  if (responseObj
      && typeof responseObj === 'object'
      && typeof responseObj['status'] === 'number'
      && responseObj['status'] !== 0) {
    let status = responseObj['status'];
    let ctor = LEGACY_ERROR_CODE_TO_TYPE.get(status) || WebDriverError;

    let value = responseObj['value'];

    if (!value || typeof value !== 'object') {
      throw new ctor(value + '');
    } else {
      let message = value['message'] + '';
      if (ctor !== UnexpectedAlertOpenError) {
        throw new ctor(message);
      }

      let text = '';
      if (value['alert'] && typeof value['alert']['text'] === 'string') {
        text = value['alert']['text'];
      }
      throw new UnexpectedAlertOpenError(message, text);
    }
  }
  return responseObj;
}


// PUBLIC API


module.exports = {
  ErrorCode: ErrorCode,

  WebDriverError: WebDriverError,
  ElementNotInteractableError: ElementNotInteractableError,
  ElementNotSelectableError: ElementNotSelectableError,
  ElementNotVisibleError: ElementNotVisibleError,
  InvalidArgumentError: InvalidArgumentError,
  InvalidCookieDomainError: InvalidCookieDomainError,
  InvalidElementCoordinatesError: InvalidElementCoordinatesError,
  InvalidElementStateError: InvalidElementStateError,
  InvalidSelectorError: InvalidSelectorError,
  JavascriptError: JavascriptError,
  MoveTargetOutOfBoundsError: MoveTargetOutOfBoundsError,
  NoSuchAlertError: NoSuchAlertError,
  NoSuchElementError: NoSuchElementError,
  NoSuchFrameError: NoSuchFrameError,
  NoSuchSessionError: NoSuchSessionError,
  NoSuchWindowError: NoSuchWindowError,
  ScriptTimeoutError: ScriptTimeoutError,
  SessionNotCreatedError: SessionNotCreatedError,
  StaleElementReferenceError: StaleElementReferenceError,
  TimeoutError: TimeoutError,
  UnableToSetCookieError: UnableToSetCookieError,
  UnableToCaptureScreenError: UnableToCaptureScreenError,
  UnexpectedAlertOpenError: UnexpectedAlertOpenError,
  UnknownCommandError: UnknownCommandError,
  UnknownMethodError: UnknownMethodError,
  UnsupportedOperationError: UnsupportedOperationError,

  checkResponse: checkResponse,
  checkLegacyResponse: checkLegacyResponse,
  encodeError: encodeError,
  isErrorResponse: isErrorResponse,
  throwDecodedError: throwDecodedError,
};


/***/ }),
/* 20 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_LoadingActivity_vue__ = __webpack_require__(68);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_26b0f6dc_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_LoadingActivity_vue__ = __webpack_require__(179);
function injectStyle (ssrContext) {
  __webpack_require__(178)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_LoadingActivity_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_26b0f6dc_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_LoadingActivity_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 21 */,
/* 22 */,
/* 23 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Licensed to the Software Freedom Conservancy (SFC) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The SFC licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

/**
 * @fileoverview Contains several classes for handling commands.
 */



/**
 * Describes a command to execute.
 * @final
 */
class Command {
  /** @param {string} name The name of this command. */
  constructor(name) {
    /** @private {string} */
    this.name_ = name;

    /** @private {!Object<*>} */
    this.parameters_ = {};
  }

  /** @return {string} This command's name. */
  getName() {
    return this.name_;
  }

  /**
   * Sets a parameter to send with this command.
   * @param {string} name The parameter name.
   * @param {*} value The parameter value.
   * @return {!Command} A self reference.
   */
  setParameter(name, value) {
    this.parameters_[name] = value;
    return this;
  }

  /**
   * Sets the parameters for this command.
   * @param {!Object<*>} parameters The command parameters.
   * @return {!Command} A self reference.
   */
  setParameters(parameters) {
    this.parameters_ = parameters;
    return this;
  }

  /**
   * Returns a named command parameter.
   * @param {string} key The parameter key to look up.
   * @return {*} The parameter value, or undefined if it has not been set.
   */
  getParameter(key) {
    return this.parameters_[key];
  }

  /**
   * @return {!Object<*>} The parameters to send with this command.
   */
  getParameters() {
    return this.parameters_;
  }
}


/**
 * Enumeration of predefined names command names that all command processors
 * will support.
 * @enum {string}
 */
// TODO: Delete obsolete command names.
const Name = {
  GET_SERVER_STATUS: 'getStatus',

  NEW_SESSION: 'newSession',
  GET_SESSIONS: 'getSessions',
  DESCRIBE_SESSION: 'getSessionCapabilities',

  CLOSE: 'close',
  QUIT: 'quit',

  GET_CURRENT_URL: 'getCurrentUrl',
  GET: 'get',
  GO_BACK: 'goBack',
  GO_FORWARD: 'goForward',
  REFRESH: 'refresh',

  ADD_COOKIE: 'addCookie',
  GET_COOKIE: 'getCookie',
  GET_ALL_COOKIES: 'getCookies',
  DELETE_COOKIE: 'deleteCookie',
  DELETE_ALL_COOKIES: 'deleteAllCookies',

  GET_ACTIVE_ELEMENT: 'getActiveElement',
  FIND_ELEMENT: 'findElement',
  FIND_ELEMENTS: 'findElements',
  FIND_CHILD_ELEMENT: 'findChildElement',
  FIND_CHILD_ELEMENTS: 'findChildElements',

  CLEAR_ELEMENT: 'clearElement',
  CLICK_ELEMENT: 'clickElement',
  SEND_KEYS_TO_ELEMENT: 'sendKeysToElement',
  SUBMIT_ELEMENT: 'submitElement',

  GET_CURRENT_WINDOW_HANDLE: 'getCurrentWindowHandle',
  GET_WINDOW_HANDLES: 'getWindowHandles',
  GET_WINDOW_POSITION: 'getWindowPosition',
  SET_WINDOW_POSITION: 'setWindowPosition',
  GET_WINDOW_SIZE: 'getWindowSize',
  SET_WINDOW_SIZE: 'setWindowSize',
  MAXIMIZE_WINDOW: 'maximizeWindow',

  SWITCH_TO_WINDOW: 'switchToWindow',
  SWITCH_TO_FRAME: 'switchToFrame',
  GET_PAGE_SOURCE: 'getPageSource',
  GET_TITLE: 'getTitle',

  EXECUTE_SCRIPT: 'executeScript',
  EXECUTE_ASYNC_SCRIPT: 'executeAsyncScript',

  GET_ELEMENT_TEXT: 'getElementText',
  GET_ELEMENT_TAG_NAME: 'getElementTagName',
  IS_ELEMENT_SELECTED: 'isElementSelected',
  IS_ELEMENT_ENABLED: 'isElementEnabled',
  IS_ELEMENT_DISPLAYED: 'isElementDisplayed',
  GET_ELEMENT_LOCATION: 'getElementLocation',
  GET_ELEMENT_LOCATION_IN_VIEW: 'getElementLocationOnceScrolledIntoView',
  GET_ELEMENT_SIZE: 'getElementSize',
  GET_ELEMENT_ATTRIBUTE: 'getElementAttribute',
  GET_ELEMENT_VALUE_OF_CSS_PROPERTY: 'getElementValueOfCssProperty',
  ELEMENT_EQUALS: 'elementEquals',

  SCREENSHOT: 'screenshot',
  TAKE_ELEMENT_SCREENSHOT: 'takeElementScreenshot',
  IMPLICITLY_WAIT: 'implicitlyWait',
  SET_SCRIPT_TIMEOUT: 'setScriptTimeout',

  GET_TIMEOUT: 'getTimeout',
  SET_TIMEOUT: 'setTimeout',

  ACCEPT_ALERT: 'acceptAlert',
  DISMISS_ALERT: 'dismissAlert',
  GET_ALERT_TEXT: 'getAlertText',
  SET_ALERT_TEXT: 'setAlertValue',
  SET_ALERT_CREDENTIALS: 'setAlertCredentials',

  EXECUTE_SQL: 'executeSQL',
  GET_LOCATION: 'getLocation',
  SET_LOCATION: 'setLocation',
  GET_APP_CACHE: 'getAppCache',
  GET_APP_CACHE_STATUS: 'getStatus',
  CLEAR_APP_CACHE: 'clearAppCache',
  IS_BROWSER_ONLINE: 'isBrowserOnline',
  SET_BROWSER_ONLINE: 'setBrowserOnline',

  GET_LOCAL_STORAGE_ITEM: 'getLocalStorageItem',
  GET_LOCAL_STORAGE_KEYS: 'getLocalStorageKeys',
  SET_LOCAL_STORAGE_ITEM: 'setLocalStorageItem',
  REMOVE_LOCAL_STORAGE_ITEM: 'removeLocalStorageItem',
  CLEAR_LOCAL_STORAGE: 'clearLocalStorage',
  GET_LOCAL_STORAGE_SIZE: 'getLocalStorageSize',

  GET_SESSION_STORAGE_ITEM: 'getSessionStorageItem',
  GET_SESSION_STORAGE_KEYS: 'getSessionStorageKey',
  SET_SESSION_STORAGE_ITEM: 'setSessionStorageItem',
  REMOVE_SESSION_STORAGE_ITEM: 'removeSessionStorageItem',
  CLEAR_SESSION_STORAGE: 'clearSessionStorage',
  GET_SESSION_STORAGE_SIZE: 'getSessionStorageSize',

  SET_SCREEN_ORIENTATION: 'setScreenOrientation',
  GET_SCREEN_ORIENTATION: 'getScreenOrientation',

  // These belong to the Advanced user interactions - an element is
  // optional for these commands.
  CLICK: 'mouseClick',
  DOUBLE_CLICK: 'mouseDoubleClick',
  MOUSE_DOWN: 'mouseButtonDown',
  MOUSE_UP: 'mouseButtonUp',
  MOVE_TO: 'mouseMoveTo',
  SEND_KEYS_TO_ACTIVE_ELEMENT: 'sendKeysToActiveElement',

  // These belong to the Advanced Touch API
  TOUCH_SINGLE_TAP: 'touchSingleTap',
  TOUCH_DOWN: 'touchDown',
  TOUCH_UP: 'touchUp',
  TOUCH_MOVE: 'touchMove',
  TOUCH_SCROLL: 'touchScroll',
  TOUCH_DOUBLE_TAP: 'touchDoubleTap',
  TOUCH_LONG_PRESS: 'touchLongPress',
  TOUCH_FLICK: 'touchFlick',

  GET_AVAILABLE_LOG_TYPES: 'getAvailableLogTypes',
  GET_LOG: 'getLog',
  GET_SESSION_LOGS: 'getSessionLogs',

  // Non-standard commands used by the standalone Selenium server.
  UPLOAD_FILE: 'uploadFile'
};



/**
 * Handles the execution of WebDriver {@link Command commands}.
 * @interface
 */
class Executor {
  /**
   * Executes the given {@code command}. If there is an error executing the
   * command, the provided callback will be invoked with the offending error.
   * Otherwise, the callback will be invoked with a null Error and non-null
   * response object.
   *
   * @param {!Command} command The command to execute.
   * @return {!Promise<?>} A promise that will be fulfilled with the command
   *     result.
   */
  execute(command) {}
}



// PUBLIC API


module.exports = {
  Command: Command,
  Name: Name,
  Executor: Executor
};


/***/ }),
/* 24 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Licensed to the Software Freedom Conservancy (SFC) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The SFC licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.



/**
 * @fileoverview Defines WebDriver's logging system. The logging system is
 * broken into major components: local and remote logging.
 *
 * The local logging API, which is anchored by the {@linkplain Logger} class is
 * similar to Java's logging API. Loggers, retrieved by
 * {@linkplain #getLogger getLogger(name)}, use hierarchical, dot-delimited
 * namespaces (e.g. "" > "webdriver" > "webdriver.logging"). Recorded log
 * messages are represented by the {@linkplain Entry} class. You can capture log
 * records by {@linkplain Logger#addHandler attaching} a handler function to the
 * desired logger. For convenience, you can quickly enable logging to the
 * console by simply calling {@linkplain #installConsoleHandler
 * installConsoleHandler}.
 *
 * The [remote logging API](https://github.com/SeleniumHQ/selenium/wiki/Logging)
 * allows you to retrieve logs from a remote WebDriver server. This API uses the
 * {@link Preferences} class to define desired log levels prior to creating
 * a WebDriver session:
 *
 *     var prefs = new logging.Preferences();
 *     prefs.setLevel(logging.Type.BROWSER, logging.Level.DEBUG);
 *
 *     var caps = Capabilities.chrome();
 *     caps.setLoggingPrefs(prefs);
 *     // ...
 *
 * Remote log entries, also represented by the {@link Entry} class, may be
 * retrieved via {@link webdriver.WebDriver.Logs}:
 *
 *     driver.manage().logs().get(logging.Type.BROWSER)
 *         .then(function(entries) {
 *            entries.forEach(function(entry) {
 *              console.log('[%s] %s', entry.level.name, entry.message);
 *            });
 *         });
 *
 * **NOTE:** Only a few browsers support the remote logging API (notably
 * Firefox and Chrome). Firefox supports basic logging functionality, while
 * Chrome exposes robust
 * [performance logging](https://sites.google.com/a/chromium.org/chromedriver/logging)
 * options. Remote logging is still considered a non-standard feature, and the
 * APIs exposed by this module for it are non-frozen. This module will be
 * updated, possibly breaking backwards-compatibility, once logging is
 * officially defined by the
 * [W3C WebDriver spec](http://www.w3.org/TR/webdriver/).
 */

/**
 * Defines a message level that may be used to control logging output.
 *
 * @final
 */
class Level {
  /**
   * @param {string} name the level's name.
   * @param {number} level the level's numeric value.
   */
  constructor(name, level) {
    if (level < 0) {
      throw new TypeError('Level must be >= 0');
    }

    /** @private {string} */
    this.name_ = name;

    /** @private {number} */
    this.value_ = level;
  }

  /** This logger's name. */
  get name() {
    return this.name_;
  }

  /** The numeric log level. */
  get value() {
    return this.value_;
  }

  /** @override */
  toString() {
    return this.name;
  }
}

/**
 * Indicates no log messages should be recorded.
 * @const
 */
Level.OFF = new Level('OFF', Infinity);


/**
 * Log messages with a level of `1000` or higher.
 * @const
 */
Level.SEVERE = new Level('SEVERE', 1000);


/**
 * Log messages with a level of `900` or higher.
 * @const
 */
Level.WARNING = new Level('WARNING', 900);


/**
 * Log messages with a level of `800` or higher.
 * @const
 */
Level.INFO = new Level('INFO', 800);


/**
 * Log messages with a level of `700` or higher.
 * @const
 */
Level.DEBUG = new Level('DEBUG', 700);


/**
 * Log messages with a level of `500` or higher.
 * @const
 */
Level.FINE = new Level('FINE', 500);


/**
 * Log messages with a level of `400` or higher.
 * @const
 */
Level.FINER = new Level('FINER', 400);


/**
 * Log messages with a level of `300` or higher.
 * @const
 */
Level.FINEST = new Level('FINEST', 300);


/**
 * Indicates all log messages should be recorded.
 * @const
 */
Level.ALL = new Level('ALL', 0);


const ALL_LEVELS = /** !Set<Level> */new Set([
  Level.OFF,
  Level.SEVERE,
  Level.WARNING,
  Level.INFO,
  Level.DEBUG,
  Level.FINE,
  Level.FINER,
  Level.FINEST,
  Level.ALL
]);


const LEVELS_BY_NAME = /** !Map<string, !Level> */ new Map([
  [Level.OFF.name, Level.OFF],
  [Level.SEVERE.name, Level.SEVERE],
  [Level.WARNING.name, Level.WARNING],
  [Level.INFO.name, Level.INFO],
  [Level.DEBUG.name, Level.DEBUG],
  [Level.FINE.name, Level.FINE],
  [Level.FINER.name, Level.FINER],
  [Level.FINEST.name, Level.FINEST],
  [Level.ALL.name, Level.ALL]
]);


/**
 * Converts a level name or value to a {@link Level} value. If the name/value
 * is not recognized, {@link Level.ALL} will be returned.
 *
 * @param {(number|string)} nameOrValue The log level name, or value, to
 *     convert.
 * @return {!Level} The converted level.
 */
function getLevel(nameOrValue) {
  if (typeof nameOrValue === 'string') {
    return LEVELS_BY_NAME.get(nameOrValue) || Level.ALL;
  }
  if (typeof nameOrValue !== 'number') {
    throw new TypeError('not a string or number');
  }
  for (let level of ALL_LEVELS) {
    if (nameOrValue >= level.value) {
      return level;
    }
  }
  return Level.ALL;
}


/**
 * Describes a single log entry.
 *
 * @final
 */
class Entry {
  /**
   * @param {(!Level|string|number)} level The entry level.
   * @param {string} message The log message.
   * @param {number=} opt_timestamp The time this entry was generated, in
   *     milliseconds since 0:00:00, January 1, 1970 UTC. If omitted, the
   *     current time will be used.
   * @param {string=} opt_type The log type, if known.
   */
  constructor(level, message, opt_timestamp, opt_type) {
    this.level = level instanceof Level ? level : getLevel(level);
    this.message = message;
    this.timestamp =
        typeof opt_timestamp === 'number' ? opt_timestamp : Date.now();
    this.type = opt_type || '';
  }

  /**
   * @return {{level: string, message: string, timestamp: number,
   *           type: string}} The JSON representation of this entry.
   */
  toJSON() {
    return {
      'level': this.level.name,
      'message': this.message,
      'timestamp': this.timestamp,
      'type': this.type
    };
  }
}


/** @typedef {(string|function(): string)} */
let Loggable;


/**
 * An object used to log debugging messages. Loggers use a hierarchical,
 * dot-separated naming scheme. For instance, "foo" is considered the parent of
 * the "foo.bar" and an ancestor of "foo.bar.baz".
 *
 * Each logger may be assigned a {@linkplain #setLevel log level}, which
 * controls which level of messages will be reported to the
 * {@linkplain #addHandler handlers} attached to this instance. If a log level
 * is not explicitly set on a logger, it will inherit its parent.
 *
 * This class should never be directly instantiated. Instead, users should
 * obtain logger references using the {@linkplain ./logging.getLogger()
 * getLogger()} function.
 *
 * @final
 */
class Logger {
  /**
   * @param {string} name the name of this logger.
   * @param {Level=} opt_level the initial level for this logger.
   */
  constructor(name, opt_level) {
    /** @private {string} */
    this.name_ = name;

    /** @private {Level} */
    this.level_ = opt_level || null;

    /** @private {Logger} */
    this.parent_ = null;

    /** @private {Set<function(!Entry)>} */
    this.handlers_ = null;
  }

  /** @return {string} the name of this logger. */
  getName() {
    return this.name_;
  }

  /**
   * @param {Level} level the new level for this logger, or `null` if the logger
   *     should inherit its level from its parent logger.
   */
  setLevel(level) {
    this.level_ = level;
  }

  /** @return {Level} the log level for this logger. */
  getLevel() {
    return this.level_;
  }

  /**
   * @return {!Level} the effective level for this logger.
   */
  getEffectiveLevel() {
    let logger = this;
    let level;
    do {
      level = logger.level_;
      logger = logger.parent_;
    } while (logger && !level);
    return level || Level.OFF;
  }

  /**
   * @param {!Level} level the level to check.
   * @return {boolean} whether messages recorded at the given level are loggable
   *     by this instance.
   */
  isLoggable(level) {
    return level.value !== Level.OFF.value
        && level.value >= this.getEffectiveLevel().value;
  }

  /**
   * Adds a handler to this logger. The handler will be invoked for each message
   * logged with this instance, or any of its descendants.
   *
   * @param {function(!Entry)} handler the handler to add.
   */
  addHandler(handler) {
    if (!this.handlers_) {
      this.handlers_ = new Set;
    }
    this.handlers_.add(handler);
  }

  /**
   * Removes a handler from this logger.
   *
   * @param {function(!Entry)} handler the handler to remove.
   * @return {boolean} whether a handler was successfully removed.
   */
  removeHandler(handler) {
    if (!this.handlers_) {
      return false;
    }
    return this.handlers_.delete(handler);
  }

  /**
   * Logs a message at the given level. The message may be defined as a string
   * or as a function that will return the message. If a function is provided,
   * it will only be invoked if this logger's
   * {@linkplain #getEffectiveLevel() effective log level} includes the given
   * `level`.
   *
   * @param {!Level} level the level at which to log the message.
   * @param {(string|function(): string)} loggable the message to log, or a
   *     function that will return the message.
   */
  log(level, loggable) {
    if (!this.isLoggable(level)) {
      return;
    }
    let message = '[' + this.name_ + '] '
        + (typeof loggable === 'function' ? loggable() : loggable);
    let entry = new Entry(level, message, Date.now());
    for (let logger = this; !!logger; logger = logger.parent_) {
      if (logger.handlers_) {
        for (let handler of logger.handlers_) {
          handler(entry);
        }
      }
    }
  }

  /**
   * Logs a message at the {@link Level.SEVERE} log level.
   * @param {(string|function(): string)} loggable the message to log, or a
   *     function that will return the message.
   */
  severe(loggable) {
    this.log(Level.SEVERE, loggable);
  }

  /**
   * Logs a message at the {@link Level.WARNING} log level.
   * @param {(string|function(): string)} loggable the message to log, or a
   *     function that will return the message.
   */
  warning(loggable) {
    this.log(Level.WARNING, loggable);
  }

  /**
   * Logs a message at the {@link Level.INFO} log level.
   * @param {(string|function(): string)} loggable the message to log, or a
   *     function that will return the message.
   */
  info(loggable) {
    this.log(Level.INFO, loggable);
  }

  /**
   * Logs a message at the {@link Level.DEBUG} log level.
   * @param {(string|function(): string)} loggable the message to log, or a
   *     function that will return the message.
   */
  debug(loggable) {
    this.log(Level.DEBUG, loggable);
  }

  /**
   * Logs a message at the {@link Level.FINE} log level.
   * @param {(string|function(): string)} loggable the message to log, or a
   *     function that will return the message.
   */
  fine(loggable) {
    this.log(Level.FINE, loggable);
  }

  /**
   * Logs a message at the {@link Level.FINER} log level.
   * @param {(string|function(): string)} loggable the message to log, or a
   *     function that will return the message.
   */
  finer(loggable) {
    this.log(Level.FINER, loggable);
  }

  /**
   * Logs a message at the {@link Level.FINEST} log level.
   * @param {(string|function(): string)} loggable the message to log, or a
   *     function that will return the message.
   */
  finest(loggable) {
    this.log(Level.FINEST, loggable);
  }
}


/**
 * Maintains a collection of loggers.
 *
 * @final
 */
class LogManager {
  constructor() {
    /** @private {!Map<string, !Logger>} */
    this.loggers_ = new Map;
    this.root_ = new Logger('', Level.OFF);
  }

  /**
   * Retrieves a named logger, creating it in the process. This function will
   * implicitly create the requested logger, and any of its parents, if they
   * do not yet exist.
   *
   * @param {string} name the logger's name.
   * @return {!Logger} the requested logger.
   */
  getLogger(name) {
    if (!name) {
      return this.root_;
    }
    let parent = this.root_;
    for (let i = name.indexOf('.'); i != -1; i = name.indexOf('.', i + 1)) {
      let parentName = name.substr(0, i);
      parent = this.createLogger_(parentName, parent);
    }
    return this.createLogger_(name, parent);
  }

  /**
   * Creates a new logger.
   *
   * @param {string} name the logger's name.
   * @param {!Logger} parent the logger's parent.
   * @return {!Logger} the new logger.
   * @private
   */
  createLogger_(name, parent) {
    if (this.loggers_.has(name)) {
      return /** @type {!Logger} */(this.loggers_.get(name));
    }
    let logger = new Logger(name, null);
    logger.parent_ = parent;
    this.loggers_.set(name, logger);
    return logger;
  }
}


const logManager = new LogManager;


/**
 * Retrieves a named logger, creating it in the process. This function will
 * implicitly create the requested logger, and any of its parents, if they
 * do not yet exist.
 *
 * The log level will be unspecified for newly created loggers. Use
 * {@link Logger#setLevel(level)} to explicitly set a level.
 *
 * @param {string} name the logger's name.
 * @return {!Logger} the requested logger.
 */
function getLogger(name) {
  return logManager.getLogger(name);
}


/**
 * Pads a number to ensure it has a minimum of two digits.
 * 
 * @param {number} n the number to be padded.
 * @return {string} the padded number.
 */
function pad(n) {
  if (n >= 10) {
    return '' + n;
  } else {
    return '0' + n;
  }
}


/**
 * Logs all messages to the Console API.
 * @param {!Entry} entry the entry to log.
 */
function consoleHandler(entry) {
  if (typeof console === 'undefined' || !console) {
    return;
  }

  var timestamp = new Date(entry.timestamp);
  var msg =
      '[' + timestamp.getUTCFullYear() + '-' +
      pad(timestamp.getUTCMonth() + 1) + '-' +
      pad(timestamp.getUTCDate()) + 'T' +
      pad(timestamp.getUTCHours()) + ':' +
      pad(timestamp.getUTCMinutes()) + ':' +
      pad(timestamp.getUTCSeconds()) + 'Z] ' +
      '[' + entry.level.name + '] ' +
      entry.message;

  var level = entry.level.value;
  if (level >= Level.SEVERE.value) {
    console.error(msg);
  } else if (level >= Level.WARNING.value) {
    console.warn(msg);
  } else {
    console.log(msg);
  }
}


/**
 * Adds the console handler to the given logger. The console handler will log
 * all messages using the JavaScript Console API.
 *
 * @param {Logger=} opt_logger The logger to add the handler to; defaults
 *     to the root logger.
 */
function addConsoleHandler(opt_logger) {
  let logger = opt_logger || logManager.root_;
  logger.addHandler(consoleHandler);
}


/**
 * Removes the console log handler from the given logger.
 *
 * @param {Logger=} opt_logger The logger to remove the handler from; defaults
 *     to the root logger.
 * @see exports.addConsoleHandler
 */
function removeConsoleHandler(opt_logger) {
  let logger = opt_logger || logManager.root_;
  logger.removeHandler(consoleHandler);
}


/**
 * Installs the console log handler on the root logger.
 */
function installConsoleHandler() {
  addConsoleHandler(logManager.root_);
}


/**
 * Common log types.
 * @enum {string}
 */
const Type = {
  /** Logs originating from the browser. */
  BROWSER: 'browser',
  /** Logs from a WebDriver client. */
  CLIENT: 'client',
  /** Logs from a WebDriver implementation. */
  DRIVER: 'driver',
  /** Logs related to performance. */
  PERFORMANCE: 'performance',
  /** Logs from the remote server. */
  SERVER: 'server'
};


/**
 * Describes the log preferences for a WebDriver session.
 *
 * @final
 */
class Preferences {
  constructor() {
    /** @private {!Map<string, !Level>} */
    this.prefs_ = new Map;
  }

  /**
   * Sets the desired logging level for a particular log type.
   * @param {(string|Type)} type The log type.
   * @param {(!Level|string|number)} level The desired log level.
   * @throws {TypeError} if `type` is not a `string`.
   */
  setLevel(type, level) {
    if (typeof type !== 'string') {
      throw TypeError('specified log type is not a string: ' + typeof type);
    }
    this.prefs_.set(type, level instanceof Level ? level : getLevel(level));
  }

  /**
   * Converts this instance to its JSON representation.
   * @return {!Object<string, string>} The JSON representation of this set of
   *     preferences.
   */
  toJSON() {
    let json = {};
    for (let key of this.prefs_.keys()) {
      json[key] = this.prefs_.get(key).name;
    }
    return json;
  }
}


// PUBLIC API


module.exports = {
  Entry: Entry,
  Level: Level,
  LogManager: LogManager,
  Logger: Logger,
  Preferences: Preferences,
  Type: Type,
  addConsoleHandler: addConsoleHandler,
  getLevel: getLevel,
  getLogger: getLogger,
  installConsoleHandler: installConsoleHandler,
  removeConsoleHandler: removeConsoleHandler
};


/***/ }),
/* 25 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/img/profile-2.fd00a15.jpg";

/***/ }),
/* 26 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
  name: 'app'
});

/***/ }),
/* 27 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ContentAplikasi_vue__ = __webpack_require__(88);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mixin_auth_js__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["a"] = ({
  mixins: [__WEBPACK_IMPORTED_MODULE_1__mixin_auth_js__["a" /* auth */]],
  data: function data() {
    return {
      authenticated: true,
      mockAccount: {
        nik: "nik",
        password: "password"
      }
    };
  },

  components: {
    ContentAplikasi: __WEBPACK_IMPORTED_MODULE_0__ContentAplikasi_vue__["a" /* default */]
  },
  created: function created() {
    console.log(this.authenticated);
  },
  mounted: function mounted() {
    if (!this.authenticated) {
      this.$router.replace({ name: 'PageLogin' });
    }
  },

  methods: {
    // setAuthenticated(status) {
    //   this.authenticated = status;
    // },
    setAuthenticated: function setAuthenticated() {
      this.authenticated = true;
    },
    logout: function logout() {
      this.authenticated = false;
    }
  }
});

/***/ }),
/* 28 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_img_picture_svg__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_img_picture_svg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__assets_img_picture_svg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_LoadingFeed_vue__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__assets_img_feed_jpeg__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__assets_img_feed_jpeg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__assets_img_feed_jpeg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_vue_socialmedia_share__ = __webpack_require__(113);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_vue_socialmedia_share___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_vue_socialmedia_share__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["a"] = ({
  data: function data() {
    return {
      imagekosong: __WEBPACK_IMPORTED_MODULE_0__assets_img_picture_svg___default.a,
      posts: [],
      errors: [],
      loading: false,
      avatar: __WEBPACK_IMPORTED_MODULE_3__assets_img_feed_jpeg___default.a
    };
  },

  components: {
    LoadingFeed: __WEBPACK_IMPORTED_MODULE_2__components_LoadingFeed_vue__["a" /* default */], Facebook: __WEBPACK_IMPORTED_MODULE_4_vue_socialmedia_share__["Facebook"], Twitter: __WEBPACK_IMPORTED_MODULE_4_vue_socialmedia_share__["Twitter"]
  },
  created: function created() {
    var _this = this;

    this.loading = true;
    __WEBPACK_IMPORTED_MODULE_1_axios___default.a.get('https://newsapi.org/v2/top-headlines?country=id&apiKey=e9f7a9e849fd4f90853d7e8d4d7f1d56').then(function (response) {
      _this.posts = response.data.articles;
      _this.loading = false;
    }).catch(function (e) {
      _this.error.push(e);
    });
  }
});

/***/ }),
/* 29 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIj8+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDUxMiA1MTIiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMiA1MTI7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiIGNsYXNzPSIiPjxnPjxnPgoJPGc+CgkJPHBhdGggZD0iTTM3NC4wMDcsMTA3Ljk5MUgxNTQuOTA1bDE1MC4wOTgtODYuNjU5YzQuNzcyLTIuNzU4LDEwLjkwMi0xLjExNSwxMy42NiwzLjY2MWwzMy44NjgsNTguNjYxICAgIGMyLjc2Myw0Ljc4Myw4Ljg3OSw2LjQyMiwxMy42NiwzLjY2YzQuNzgzLTIuNzYxLDYuNDIyLTguODc3LDMuNjYtMTMuNjZsLTMzLjg2OC01OC42NjFjLTguMjcxLTE0LjMyNS0yNi42NTYtMTkuMjUtNDAuOTgxLTEwLjk4ICAgIEwxMTUuMTE5LDEwNy44NjljLTAuMDY3LDAuMDM5LTAuMTI2LDAuMDg0LTAuMTkyLDAuMTI0SDMwLjAwMUMxMy40NTgsMTA3Ljk5MywwLDEyMS40NTEsMCwxMzcuOTkzdjM0NC4wMDYgICAgQzAsNDk4LjU0MiwxMy40NTgsNTEyLDMwLjAwMSw1MTJoMzQ0LjAwNmMxNi41NDIsMCwzMC4wMDEtMTMuNDU4LDMwLjAwMS0zMC4wMDFWMTM3Ljk5MyAgICBDNDA0LjAwNywxMjEuNDUsMzkwLjU0OSwxMDcuOTkxLDM3NC4wMDcsMTA3Ljk5MXogTTM4NC4wMDcsNDgxLjk5OGMwLDUuNTE0LTQuNDg2LDEwLTEwLDEwSDMwLjAwMWMtNS41MTQsMC0xMC00LjQ4Ni0xMC0xMCAgICBWMTM3Ljk5MmMwLTUuNTE0LDQuNDg2LTEwLDEwLTEwaDM0NC4wMDZjNS41MTQsMCwxMCw0LjQ4NiwxMCwxMFY0ODEuOTk4eiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgY2xhc3M9ImFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iI2Q5ZDlkOSIgZmlsbD0iI2Q5ZDlkOSIvPgoJPC9nPgo8L2c+PGc+Cgk8Zz4KCQk8cGF0aCBkPSJNNDQ3LjMwNywyMDguNTA0Yy0xLjg2MS0xLjg2LTQuNDQtMi45My03LjA3LTIuOTNzLTUuMjEsMS4wNy03LjA2OSwyLjkzYy0xLjg2LDEuODYtMi45MzEsNC40NC0yLjkzMSw3LjA3ICAgIGMwLDIuNjMsMS4wNyw1LjIxLDIuOTMxLDcuMDdjMS44NiwxLjg1OSw0LjQ0LDIuOTMsNy4wNjksMi45M2MyLjYzLDAsNS4yMS0xLjA3LDcuMDctMi45M2MxLjg2LTEuODYsMi45My00LjQ0LDIuOTMtNy4wNyAgICBDNDUwLjIzNywyMTIuOTQ0LDQ0OS4xNjgsMjEwLjM2NCw0NDcuMzA3LDIwOC41MDR6IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iYWN0aXZlLXBhdGgiIGRhdGEtb2xkX2NvbG9yPSIjZDlkOWQ5IiBmaWxsPSIjZDlkOWQ5Ii8+Cgk8L2c+CjwvZz48Zz4KCTxnPgoJCTxwYXRoIGQ9Ik01MDcuOTg3LDMxMi45MTFsLTQxLjM3Ni03MS42NjVjLTIuNzYyLTQuNzgzLTguODc2LTYuNDIxLTEzLjY2LTMuNjZjLTQuNzgzLDIuNzYxLTYuNDIyLDguODc3LTMuNjYsMTMuNjZsNDEuMzc2LDcxLjY2NCAgICBjMi43NTcsNC43NzYsMS4xMTUsMTAuOTA0LTMuNjYsMTMuNjYxbC02NC4zMTIsMzcuMTNjLTQuNzgzLDIuNzYxLTYuNDIyLDguODc3LTMuNjYsMTMuNjZjMS44NTMsMy4yMDgsNS4yMTMsNS4wMDEsOC42Nyw1LjAwMSAgICBjMS42OTYsMCwzLjQxNi0wLjQzMiw0Ljk5LTEuMzQxbDY0LjMxMi0zNy4xM0M1MTEuMzMxLDM0NS42MjEsNTE2LjI1NywzMjcuMjM4LDUwNy45ODcsMzEyLjkxMXoiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiNkOWQ5ZDkiIGZpbGw9IiNkOWQ5ZDkiLz4KCTwvZz4KPC9nPjxnPgoJPGc+CgkJPHBhdGggZD0iTTI2My4wNzQsMTU4LjkyMmMtMS44NTktMS44Ni00LjQzOS0yLjkzLTcuMDY5LTIuOTNjLTIuNjMsMC01LjIxLDEuMDctNy4wNywyLjkzYy0xLjg2LDEuODYtMi45Myw0LjQ0LTIuOTMsNy4wNyAgICBzMS4wNjksNS4yMSwyLjkzLDcuMDdzNC40NCwyLjkzLDcuMDcsMi45M2MyLjYzLDAsNS4yMS0xLjA3LDcuMDY5LTIuOTNjMS44Ni0xLjg2LDIuOTMxLTQuNDQsMi45MzEtNy4wNyAgICBTMjY0LjkzNSwxNjAuNzgyLDI2My4wNzQsMTU4LjkyMnoiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiNkOWQ5ZDkiIGZpbGw9IiNkOWQ5ZDkiLz4KCTwvZz4KPC9nPjxnPgoJPGc+CgkJPHBhdGggZD0iTTM0Ni4wMDYsMTU1Ljk5MmgtMzkuMDAxYy01LjUyMiwwLTEwLDQuNDc3LTEwLDEwYzAsNS41MjMsNC40NzgsMTAsMTAsMTBoMjl2MTQyLjU3M2wtNzkuMzkxLTc5LjM5ICAgIGMtMS44NzYtMS44NzUtNC40MTktMi45MjktNy4wNzEtMi45MjlzLTUuMTk1LDEuMDU0LTcuMDcxLDIuOTI5bC01Mi45NzgsNTIuOTc4Yy0zLjkwNSwzLjkwNS0zLjkwNSwxMC4yMzcsMCwxNC4xNDIgICAgYzMuOTA2LDMuOTA1LDEwLjIzNiwzLjkwNSwxNC4xNDMsMGw0NS45MDYtNDUuOTA3bDg2LjM5Miw4Ni4zOTJjMC4wMjMsMC4wMjMsMC4wNDgsMC4wNDIsMC4wNzEsMC4wNjR2NDkuMTUySDgyLjE0NSAgICBsNzIuNTk3LTcyLjU5N2w1My45Myw1My45M2MzLjkwNiwzLjkwNSwxMC4yMzYsMy45MDUsMTQuMTQzLDBjMy45MDUtMy45MDUsMy45MDUtMTAuMjM3LDAtMTQuMTQzbC02MS4wMDEtNjEuMDAxICAgIGMtMS44NzYtMS44NzUtNC40MTktMi45MjktNy4wNzEtMi45MjlzLTUuMTk1LDEuMDU0LTcuMDcxLDIuOTI5bC03OS42Nyw3OS42N1YxNzUuOTkyaDEzNC4wMDJjNS41MjIsMCwxMC00LjQ3NywxMC0xMCAgICBjMC01LjUyMy00LjQ3OC0xMC0xMC0xMEg1OC4wMDFjLTUuNTIyLDAtMTAsNC40NzctMTAsMTB2MjQwLjAwNGMwLDUuNTIzLDQuNDc4LDEwLDEwLDEwaDI4OC4wMDVjNS41MjIsMCwxMC00LjQ3NywxMC0xMFYxNjUuOTkyICAgIEMzNTYuMDA2LDE2MC40NywzNTEuNTI4LDE1NS45OTIsMzQ2LjAwNiwxNTUuOTkyeiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgY2xhc3M9ImFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iI2Q5ZDlkOSIgZmlsbD0iI2Q5ZDlkOSIvPgoJPC9nPgo8L2c+PGc+Cgk8Zz4KCQk8cGF0aCBkPSJNMTQ1LjY3LDE5OC45OTJjLTE3LjY0NSwwLTMyLjAwMSwxNC4zNTUtMzIuMDAxLDMyLjAwMXMxNC4zNTUsMzIuMDAxLDMyLjAwMSwzMi4wMDEgICAgYzE3LjY0NSwwLDMyLjAwMS0xNC4zNTUsMzIuMDAxLTMyLjAwMVMxNjMuMzE1LDE5OC45OTIsMTQ1LjY3LDE5OC45OTJ6IE0xNDUuNjcsMjQyLjk5M2MtNi42MTcsMC0xMi01LjM4My0xMi0xMnM1LjM4My0xMiwxMi0xMiAgICBzMTIsNS4zODQsMTIsMTJDMTU3LjY3LDIzNy42MSwxNTIuMjg3LDI0Mi45OTMsMTQ1LjY3LDI0Mi45OTN6IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iYWN0aXZlLXBhdGgiIGRhdGEtb2xkX2NvbG9yPSIjZDlkOWQ5IiBmaWxsPSIjZDlkOWQ5Ii8+Cgk8L2c+CjwvZz48L2c+IDwvc3ZnPgo="

/***/ }),
/* 30 */,
/* 31 */,
/* 32 */,
/* 33 */,
/* 34 */,
/* 35 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_LoadingFeed_vue__ = __webpack_require__(36);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_3359dc78_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_LoadingFeed_vue__ = __webpack_require__(112);
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_LoadingFeed_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_3359dc78_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_LoadingFeed_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 36 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_content_loader__ = __webpack_require__(12);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["a"] = ({
    components: { ContentLoader: __WEBPACK_IMPORTED_MODULE_0_vue_content_loader__["a" /* ContentLoader */] }
});

/***/ }),
/* 37 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/img/feed.e9c1c45.jpeg";

/***/ }),
/* 38 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_img_picture_svg__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_img_picture_svg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__assets_img_picture_svg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__assets_img_logo_coba_coba_png__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__assets_img_logo_coba_coba_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__assets_img_logo_coba_coba_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_LoadingFeed_vue__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__components_OneLoading_vue__ = __webpack_require__(117);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__assets_img_feed_jpeg__ = __webpack_require__(37);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__assets_img_feed_jpeg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__assets_img_feed_jpeg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_link_prevue__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_link_prevue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_link_prevue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_sweetalert__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__mixin_auth_js__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//










// console.log(auth)
var count = 0;
/* harmony default export */ __webpack_exports__["a"] = ({
  mixins: [__WEBPACK_IMPORTED_MODULE_8__mixin_auth_js__["a" /* auth */]],
  data: function data() {
    return {
      imagekosong: __WEBPACK_IMPORTED_MODULE_0__assets_img_picture_svg___default.a,
      posts: [],
      errors: [],
      loading: false,
      avatar: __WEBPACK_IMPORTED_MODULE_5__assets_img_feed_jpeg___default.a,
      logocoba: __WEBPACK_IMPORTED_MODULE_1__assets_img_logo_coba_coba_png___default.a,
      share_feed: [],
      overriddenNetworks: {
        "fb": {
          "sharer": "https://www.facebook.com/sharer/sharer.php?u=@url&title=@title&description=@description&quote=@quote",
          "type": "popup",
          "id": "1"
        }
      },
      arrPage: 1,
      datanya: [],
      busy: false,
      share_twitter: []

    };
  },

  components: {
    LoadingFeed: __WEBPACK_IMPORTED_MODULE_3__components_LoadingFeed_vue__["a" /* default */],
    LinkPrevue: __WEBPACK_IMPORTED_MODULE_6_link_prevue___default.a,
    OneLoading: __WEBPACK_IMPORTED_MODULE_4__components_OneLoading_vue__["a" /* default */]
  },
  created: function created() {
    var _this = this;

    //http://103.49.223.93/advokasi/api/feed_all
    __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get('http://103.49.223.93/advokasi/api/feed_all').then(function (response) {
      _this.posts = response.data;
    }).catch(function (e) {
      _this.error.push(e);
    });
  },

  methods: {
    shareFeed: function shareFeed(id_feed, point) {
      var _this2 = this;

      var id_sosmedf = 1;
      var pointnya = point;
      __WEBPACK_IMPORTED_MODULE_2_axios___default.a.post('http://103.49.223.93/advokasi/api/share_feed', {
        id_feed: id_feed,
        id_sosmed: id_sosmedf
      }, {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('token')
        }
      }).then(function (response) {
        _this2.share = response.data;
        if (_this2.share.status === true) {
          __WEBPACK_IMPORTED_MODULE_7_sweetalert___default()("Selamat!, Kamu berhasil mendapatkan " + pointnya + " Rewards Points");
          console.log(_this2.share);
        } else if (_this2.share.status === false) {
          __WEBPACK_IMPORTED_MODULE_7_sweetalert___default()("Kamu sudah membagikan konten ini ke facebook");
        }
      }).catch(function (e) {
        _this2.error.push(e);
      });
    },
    sharetw: function sharetw(id_feed, point, url) {
      var _this3 = this;

      var id_account = localStorage.getItem('id_account');
      if (id_account === null) {
        __WEBPACK_IMPORTED_MODULE_7_sweetalert___default()({
          text: "Silahkan login ke twitter terlebih dahulu",
          buttons: true,
          dangerMode: false
        }).then(function (willLogin) {
          if (willLogin) {
            location.href = "http://api-engagement.tms.id/api/login-twitter";
          } else {
            __WEBPACK_IMPORTED_MODULE_7_sweetalert___default()("Kamu membatalkan login ke twitter");
          }
        });
      } else {
        __WEBPACK_IMPORTED_MODULE_2_axios___default.a.post('https://api-engagement.tms.id/api/twitter/tweet', {
          id_account: id_account,
          message: url
        }).then(function (response) {
          _this3.share_twitter = response.data;
          console.log(_this3.share_twitter);
          if (_this3.share_twitter.status === 'OK') {
            var id_sosmedt = 2;
            var pointnya = point;
            __WEBPACK_IMPORTED_MODULE_2_axios___default.a.post('http://103.49.223.93/advokasi/api/share_feed', {
              id_feed: id_feed,
              id_sosmed: id_sosmedt
            }, {
              headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
              }
            }).then(function (response) {
              _this3.share = response.data;
              if (_this3.share.status === true) {
                __WEBPACK_IMPORTED_MODULE_7_sweetalert___default()("Selamat!, Kamu berhasil mendapatkan " + pointnya + " poin tambahan");
              } else if (_this3.share.status === false) {
                __WEBPACK_IMPORTED_MODULE_7_sweetalert___default()("Kamu sudah membagikan konten ini ke twitter");
              }
            }).catch(function (e) {
              _this3.error.push(e);
            });
          } else {
            __WEBPACK_IMPORTED_MODULE_7_sweetalert___default()("Kamu sudah membagikan konten ini ke twitter");
          }
        }).catch(function (e) {
          _this3.error.push(e);
        });
      }
    },
    sharein: function sharein(id_feed, point) {
      var id_linkedin = localStorage.getItem('token_linked');
      if (id_linkedin === null) {
        __WEBPACK_IMPORTED_MODULE_7_sweetalert___default()({
          text: "Silahkan login ke linkedin terlebih dahulu",
          buttons: true,
          dangerMode: false
        }).then(function (willLogin) {
          if (willLogin) {
            location.href = "http://103.49.223.93/advokasi/api/login-linkedin";
          } else {
            __WEBPACK_IMPORTED_MODULE_7_sweetalert___default()("Kamu membatalkan login ke linkedin");
          }
        });
      } else {
        __WEBPACK_IMPORTED_MODULE_7_sweetalert___default()("Mohon maaf fitur ini sedang dalam pengembangan");
      }

      // const id_sosmedn = 3
      // const pointnya = point
      // axios.post('http://103.49.223.93/advokasi/api/share_feed',
      // {
      //   id_feed: id_feed,
      //   id_sosmed: id_sosmedn
      // },
      // {
      //   headers:{
      //     Authorization:'Bearer '+localStorage.getItem('token') 
      //   }
      // }
      // ).then(response => {
      //   this.share = response.data
      //   if(this.share.status === true){
      //     swal("Selamat!, Kamu berhasil mendapatkan "+pointnya+" Rewards Points")
      //   }else if(this.share.status === false){
      //     swal("Kamu sudah membagikan konten ini ke linkedin")
      //   }
      // }).catch(e => {
      //   this.error.push(e)
      // })
    },
    loadMore: function loadMore() {
      var _this4 = this;

      this.arrPage += 1;
      this.busy = true;
      if (this.arrPage === this.posts.last_page) {
        this.busy = true;
      } else {
        __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get('http://103.49.223.93/advokasi/api/feed_all?page=' + this.arrPage).then(function (response) {
          // this.datanya = response.data.data
          _this4.datanya.push(response.data.data[0]);
          _this4.busy = false;
          // this.loading = false
        }).catch(function (e) {
          _this4.error.push(e);
        });
      }
    }
  }
});

/***/ }),
/* 39 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_content_loader__ = __webpack_require__(12);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["a"] = ({ components: { ContentLoader: __WEBPACK_IMPORTED_MODULE_0_vue_content_loader__["a" /* ContentLoader */] }
});

/***/ }),
/* 40 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_LoadingRewards_vue__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__assets_img_logo_coba_coba_png__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__assets_img_logo_coba_coba_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__assets_img_logo_coba_coba_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_LoadingReedem_vue__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_vue_spinner_src_BeatLoader_vue__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__assets_img_cutlery_svg__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__assets_img_cutlery_svg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__assets_img_cutlery_svg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__assets_img_smartphone_svg__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__assets_img_smartphone_svg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__assets_img_smartphone_svg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_img_shopping_cart_svg__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_img_shopping_cart_svg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__assets_img_shopping_cart_svg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__assets_img_movie_tickets_svg__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__assets_img_movie_tickets_svg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__assets_img_movie_tickets_svg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_axios__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_sweetalert__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_selenium_webdriver_http__ = __webpack_require__(135);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_selenium_webdriver_http___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_selenium_webdriver_http__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__components_NotFound__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__mixin_auth_js__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//















/* harmony default export */ __webpack_exports__["a"] = ({
    mixins: [__WEBPACK_IMPORTED_MODULE_12__mixin_auth_js__["a" /* auth */]],
    data: function data() {
        return {
            active: 0,
            icon1: __WEBPACK_IMPORTED_MODULE_4__assets_img_cutlery_svg___default.a,
            icon2: __WEBPACK_IMPORTED_MODULE_5__assets_img_smartphone_svg___default.a,
            icon3: __WEBPACK_IMPORTED_MODULE_6__assets_img_shopping_cart_svg___default.a,
            icon4: __WEBPACK_IMPORTED_MODULE_7__assets_img_movie_tickets_svg___default.a,
            loading_one: true,
            loading_two: true,
            loading_thre: true,
            size: '8px',
            color: '#6c757d',
            logocoba: __WEBPACK_IMPORTED_MODULE_1__assets_img_logo_coba_coba_png___default.a,
            voucher_reedem: null,
            point: [],
            redeem: [],
            hitung: [],
            redeemhitung: [],
            details: []
        };
    },

    components: {
        LoadingRewards: __WEBPACK_IMPORTED_MODULE_0__components_LoadingRewards_vue__["a" /* default */], LoadingReedem: __WEBPACK_IMPORTED_MODULE_2__components_LoadingReedem_vue__["a" /* default */], BeatLoader: __WEBPACK_IMPORTED_MODULE_3_vue_spinner_src_BeatLoader_vue__["a" /* default */], NotFound: __WEBPACK_IMPORTED_MODULE_11__components_NotFound__["a" /* default */]
    },
    created: function created() {
        var _this = this;

        this.loding_one = true;
        __WEBPACK_IMPORTED_MODULE_8_axios___default.a.get('http://103.49.223.93/advokasi/api/voucher_all', {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        }).then(function (response) {
            _this.posts = response.data;
            _this.maPoint();
            _this.loading_one = false;
        }).catch(function (e) {
            _this.error.push(e);
        });
    },

    methods: {
        maPoint: function maPoint() {
            var _this2 = this;

            this.loading_thre = true;
            __WEBPACK_IMPORTED_MODULE_8_axios___default.a.get('http://103.49.223.93/advokasi/api/userpoint', {
                headers: {
                    Authorization: 'Bearer ' + localStorage.getItem('token')
                }
            }).then(function (response) {
                _this2.point = response.data;
                _this2.hitung = response.data.length;
                console.log(_this2.hitung);
                _this2.loading_thre = false;
            }).catch(function (e) {
                _this2.error.push(e);
            });
        },
        redemtions: function redemtions() {
            var _this3 = this;

            this.loding_two = true;
            __WEBPACK_IMPORTED_MODULE_8_axios___default.a.get('http://103.49.223.93/advokasi/api/reedemku', {
                headers: {
                    Authorization: 'Bearer ' + localStorage.getItem('token')
                }
            }).then(function (response) {
                _this3.redeem = response.data.data;
                _this3.redeemhitung = response.data.data.length;
                _this3.loading_two = false;
            }).catch(function (e) {
                _this3.error.push(e);
            });
        },
        detail: function detail(id_reedem) {
            var _this4 = this;

            __WEBPACK_IMPORTED_MODULE_8_axios___default.a.post('http://103.49.223.93/advokasi/api/kodeku', {
                id_reedem: id_reedem
            }, {
                headers: {
                    Authorization: 'Bearer ' + localStorage.getItem('token')
                }
            }).then(function (response) {
                _this4.details = response.data[0].kodevoucher[0];
                __WEBPACK_IMPORTED_MODULE_9_sweetalert___default()("Kode unik: " + _this4.details.kode_unik);
            }).catch(function (e) {
                _this4.error.push(e);
            });
        }
    }
});

/***/ }),
/* 41 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_content_loader__ = __webpack_require__(12);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["a"] = ({ components: { ContentLoader: __WEBPACK_IMPORTED_MODULE_0_vue_content_loader__["a" /* ContentLoader */] }
});

/***/ }),
/* 42 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_content_loader__ = __webpack_require__(12);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["a"] = ({ components: { ContentLoader: __WEBPACK_IMPORTED_MODULE_0_vue_content_loader__["a" /* ContentLoader */] }
});

/***/ }),
/* 43 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({

  name: 'BeatLoader',

  props: {
    loading: {
      type: Boolean,
      default: true
    },
    color: {
      type: String,
      default: '#5dc596'
    },
    size: {
      type: String,
      default: '15px'
    },
    margin: {
      type: String,
      default: '2px'
    },
    radius: {
      type: String,
      default: '100%'
    }
  },
  data: function data() {
    return {
      spinnerStyle: {
        backgroundColor: this.color,
        height: this.size,
        width: this.size,
        margin: this.margin,
        borderRadius: this.radius
      }
    };
  }
});

/***/ }),
/* 44 */,
/* 45 */,
/* 46 */,
/* 47 */,
/* 48 */,
/* 49 */,
/* 50 */,
/* 51 */,
/* 52 */,
/* 53 */,
/* 54 */,
/* 55 */,
/* 56 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Licensed to the Software Freedom Conservancy (SFC) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The SFC licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

/**
 * @fileoverview
 *
 * > ### IMPORTANT NOTICE
 * >
 * > The promise manager contained in this module is in the process of being
 * > phased out in favor of native JavaScript promises. This will be a long
 * > process and will not be completed until there have been two major LTS Node
 * > releases (approx. Node v10.0) that support
 * > [async functions](https://tc39.github.io/ecmascript-asyncawait/).
 * >
 * > At this time, the promise manager can be disabled by setting an environment
 * > variable, `SELENIUM_PROMISE_MANAGER=0`. In the absence of async functions,
 * > users may use generators with the
 * > {@link ./promise.consume promise.consume()} function to write "synchronous"
 * > style tests:
 * >
 * > ```js
 * > const {Builder, By, Key, promise, until} = require('selenium-webdriver');
 * >
 * > let result = promise.consume(function* doGoogleSearch() {
 * >   let driver = new Builder().forBrowser('firefox').build();
 * >   yield driver.get('http://www.google.com/ncr');
 * >   yield driver.findElement(By.name('q')).sendKeys('webdriver', Key.RETURN);
 * >   yield driver.wait(until.titleIs('webdriver - Google Search'), 1000);
 * >   yield driver.quit();
 * > });
 * >
 * > result.then(_ => console.log('SUCCESS!'),
 * >             e => console.error('FAILURE: ' + e));
 * > ```
 * >
 * > The motivation behind this change and full deprecation plan are documented
 * > in [issue 2969](https://github.com/SeleniumHQ/selenium/issues/2969).
 * >
 * >
 *
 * The promise module is centered around the {@linkplain ControlFlow}, a class
 * that coordinates the execution of asynchronous tasks. The ControlFlow allows
 * users to focus on the imperative commands for their script without worrying
 * about chaining together every single asynchronous action, which can be
 * tedious and verbose. APIs may be layered on top of the control flow to read
 * as if they were synchronous. For instance, the core
 * {@linkplain ./webdriver.WebDriver WebDriver} API is built on top of the
 * control flow, allowing users to write
 *
 *     driver.get('http://www.google.com/ncr');
 *     driver.findElement({name: 'q'}).sendKeys('webdriver', Key.RETURN);
 *
 * instead of
 *
 *     driver.get('http://www.google.com/ncr')
 *     .then(function() {
 *       return driver.findElement({name: 'q'});
 *     })
 *     .then(function(q) {
 *       return q.sendKeys('webdriver', Key.RETURN);
 *     });
 *
 * ## Tasks and Task Queues
 *
 * The control flow is based on the concept of tasks and task queues. Tasks are
 * functions that define the basic unit of work for the control flow to execute.
 * Each task is scheduled via {@link ControlFlow#execute()}, which will return
 * a {@link ManagedPromise} that will be resolved with the task's result.
 *
 * A task queue contains all of the tasks scheduled within a single turn of the
 * [JavaScript event loop][JSEL]. The control flow will create a new task queue
 * the first time a task is scheduled within an event loop.
 *
 *     var flow = promise.controlFlow();
 *     flow.execute(foo);       // Creates a new task queue and inserts foo.
 *     flow.execute(bar);       // Inserts bar into the same queue as foo.
 *     setTimeout(function() {
 *       flow.execute(baz);     // Creates a new task queue and inserts baz.
 *     }, 0);
 *
 * Whenever the control flow creates a new task queue, it will automatically
 * begin executing tasks in the next available turn of the event loop. This
 * execution is [scheduled as a microtask][MicrotasksArticle] like e.g. a
 * (native) `Promise.then()` callback.
 *
 *     setTimeout(() => console.log('a'));
 *     Promise.resolve().then(() => console.log('b'));  // A native promise.
 *     flow.execute(() => console.log('c'));
 *     Promise.resolve().then(() => console.log('d'));
 *     setTimeout(() => console.log('fin'));
 *     // b
 *     // c
 *     // d
 *     // a
 *     // fin
 *
 * In the example above, b/c/d is logged before a/fin because native promises
 * and this module use "microtask" timers, which have a higher priority than
 * "macrotasks" like `setTimeout`.
 *
 * ## Task Execution
 *
 * Upon creating a task queue, and whenever an existing queue completes a task,
 * the control flow will schedule a microtask timer to process any scheduled
 * tasks. This ensures no task is ever started within the same turn of the
 * JavaScript event loop in which it was scheduled, nor is a task ever started
 * within the same turn that another finishes.
 *
 * When the execution timer fires, a single task will be dequeued and executed.
 * There are several important events that may occur while executing a task
 * function:
 *
 * 1. A new task queue is created by a call to {@link ControlFlow#execute()}.
 *    Any tasks scheduled within this task queue are considered subtasks of the
 *    current task.
 * 2. The task function throws an error. Any scheduled tasks are immediately
 *    discarded and the task's promised result (previously returned by
 *    {@link ControlFlow#execute()}) is immediately rejected with the thrown
 *    error.
 * 3. The task function returns successfully.
 *
 * If a task function created a new task queue, the control flow will wait for
 * that queue to complete before processing the task result. If the queue
 * completes without error, the flow will settle the task's promise with the
 * value originally returned by the task function. On the other hand, if the task
 * queue terminates with an error, the task's promise will be rejected with that
 * error.
 *
 *     flow.execute(function() {
 *       flow.execute(() => console.log('a'));
 *       flow.execute(() => console.log('b'));
 *     });
 *     flow.execute(() => console.log('c'));
 *     // a
 *     // b
 *     // c
 *
 * ## ManagedPromise Integration
 *
 * In addition to the {@link ControlFlow} class, the promise module also exports
 * a [Promises/A+] {@linkplain ManagedPromise implementation} that is deeply
 * integrated with the ControlFlow. First and foremost, each promise
 * {@linkplain ManagedPromise#then() callback} is scheduled with the
 * control flow as a task. As a result, each callback is invoked in its own turn
 * of the JavaScript event loop with its own task queue. If any tasks are
 * scheduled within a callback, the callback's promised result will not be
 * settled until the task queue has completed.
 *
 *     promise.fulfilled().then(function() {
 *       flow.execute(function() {
 *         console.log('b');
 *       });
 *     }).then(() => console.log('a'));
 *     // b
 *     // a
 *
 * ### Scheduling ManagedPromise Callbacks <a id="scheduling_callbacks"></a>
 *
 * How callbacks are scheduled in the control flow depends on when they are
 * attached to the promise. Callbacks attached to a _previously_ resolved
 * promise are immediately enqueued as subtasks of the currently running task.
 *
 *     var p = promise.fulfilled();
 *     flow.execute(function() {
 *       flow.execute(() => console.log('A'));
 *       p.then(      () => console.log('B'));
 *       flow.execute(() => console.log('C'));
 *       p.then(      () => console.log('D'));
 *     }).then(function() {
 *       console.log('fin');
 *     });
 *     // A
 *     // B
 *     // C
 *     // D
 *     // fin
 *
 * When a promise is resolved while a task function is on the call stack, any
 * callbacks also registered in that stack frame are scheduled as if the promise
 * were already resolved:
 *
 *     var d = promise.defer();
 *     flow.execute(function() {
 *       flow.execute(  () => console.log('A'));
 *       d.promise.then(() => console.log('B'));
 *       flow.execute(  () => console.log('C'));
 *       d.promise.then(() => console.log('D'));
 *
 *       d.fulfill();
 *     }).then(function() {
 *       console.log('fin');
 *     });
 *     // A
 *     // B
 *     // C
 *     // D
 *     // fin
 *
 * Callbacks attached to an _unresolved_ promise within a task function are
 * only weakly scheduled as subtasks and will be dropped if they reach the
 * front of the queue before the promise is resolved. In the example below, the
 * callbacks for `B` & `D` are dropped as sub-tasks since they are attached to
 * an unresolved promise when they reach the front of the task queue.
 *
 *     var d = promise.defer();
 *     flow.execute(function() {
 *       flow.execute(  () => console.log('A'));
 *       d.promise.then(() => console.log('B'));
 *       flow.execute(  () => console.log('C'));
 *       d.promise.then(() => console.log('D'));
 *
 *       setTimeout(d.fulfill, 20);
 *     }).then(function() {
 *       console.log('fin')
 *     });
 *     // A
 *     // C
 *     // fin
 *     // B
 *     // D
 *
 * If a promise is resolved while a task function is on the call stack, any
 * previously registered and unqueued callbacks (i.e. either attached while no
 * task was on the call stack, or previously dropped as described above) act as
 * _interrupts_ and are inserted at the front of the task queue. If multiple
 * promises are fulfilled, their interrupts are enqueued in the order the
 * promises are resolved.
 *
 *     var d1 = promise.defer();
 *     d1.promise.then(() => console.log('A'));
 *
 *     var d2 = promise.defer();
 *     d2.promise.then(() => console.log('B'));
 *
 *     flow.execute(function() {
 *       d1.promise.then(() => console.log('C'));
 *       flow.execute(() => console.log('D'));
 *     });
 *     flow.execute(function() {
 *       flow.execute(() => console.log('E'));
 *       flow.execute(() => console.log('F'));
 *       d1.fulfill();
 *       d2.fulfill();
 *     }).then(function() {
 *       console.log('fin');
 *     });
 *     // D
 *     // A
 *     // C
 *     // B
 *     // E
 *     // F
 *     // fin
 *
 * Within a task function (or callback), each step of a promise chain acts as
 * an interrupt on the task queue:
 *
 *     var d = promise.defer();
 *     flow.execute(function() {
 *       d.promise.
 *           then(() => console.log('A')).
 *           then(() => console.log('B')).
 *           then(() => console.log('C')).
 *           then(() => console.log('D'));
 *
 *       flow.execute(() => console.log('E'));
 *       d.fulfill();
 *     }).then(function() {
 *       console.log('fin');
 *     });
 *     // A
 *     // B
 *     // C
 *     // D
 *     // E
 *     // fin
 *
 * If there are multiple promise chains derived from a single promise, they are
 * processed in the order created:
 *
 *     var d = promise.defer();
 *     flow.execute(function() {
 *       var chain = d.promise.then(() => console.log('A'));
 *
 *       chain.then(() => console.log('B')).
 *           then(() => console.log('C'));
 *
 *       chain.then(() => console.log('D')).
 *           then(() => console.log('E'));
 *
 *       flow.execute(() => console.log('F'));
 *
 *       d.fulfill();
 *     }).then(function() {
 *       console.log('fin');
 *     });
 *     // A
 *     // B
 *     // C
 *     // D
 *     // E
 *     // F
 *     // fin
 *
 * Even though a subtask's promised result will never resolve while the task
 * function is on the stack, it will be treated as a promise resolved within the
 * task. In all other scenarios, a task's promise behaves just like a normal
 * promise. In the sample below, `C/D` is logged before `B` because the
 * resolution of `subtask1` interrupts the flow of the enclosing task. Within
 * the final subtask, `E/F` is logged in order because `subtask1` is a resolved
 * promise when that task runs.
 *
 *     flow.execute(function() {
 *       var subtask1 = flow.execute(() => console.log('A'));
 *       var subtask2 = flow.execute(() => console.log('B'));
 *
 *       subtask1.then(() => console.log('C'));
 *       subtask1.then(() => console.log('D'));
 *
 *       flow.execute(function() {
 *         flow.execute(() => console.log('E'));
 *         subtask1.then(() => console.log('F'));
 *       });
 *     }).then(function() {
 *       console.log('fin');
 *     });
 *     // A
 *     // C
 *     // D
 *     // B
 *     // E
 *     // F
 *     // fin
 *
 * Finally, consider the following:
 *
 *     var d = promise.defer();
 *     d.promise.then(() => console.log('A'));
 *     d.promise.then(() => console.log('B'));
 *
 *     flow.execute(function() {
 *       flow.execute(  () => console.log('C'));
 *       d.promise.then(() => console.log('D'));
 *
 *       flow.execute(  () => console.log('E'));
 *       d.promise.then(() => console.log('F'));
 *
 *       d.fulfill();
 *
 *       flow.execute(  () => console.log('G'));
 *       d.promise.then(() => console.log('H'));
 *     }).then(function() {
 *       console.log('fin');
 *     });
 *     // A
 *     // B
 *     // C
 *     // D
 *     // E
 *     // F
 *     // G
 *     // H
 *     // fin
 *
 * In this example, callbacks are registered on `d.promise` both before and
 * during the invocation of the task function. When `d.fulfill()` is called,
 * the callbacks registered before the task (`A` & `B`) are registered as
 * interrupts. The remaining callbacks were all attached within the task and
 * are scheduled in the flow as standard tasks.
 *
 * ## Generator Support
 *
 * [Generators][GF] may be scheduled as tasks within a control flow or attached
 * as callbacks to a promise. Each time the generator yields a promise, the
 * control flow will wait for that promise to settle before executing the next
 * iteration of the generator. The yielded promise's fulfilled value will be
 * passed back into the generator:
 *
 *     flow.execute(function* () {
 *       var d = promise.defer();
 *
 *       setTimeout(() => console.log('...waiting...'), 25);
 *       setTimeout(() => d.fulfill(123), 50);
 *
 *       console.log('start: ' + Date.now());
 *
 *       var value = yield d.promise;
 *       console.log('mid: %d; value = %d', Date.now(), value);
 *
 *       yield promise.delayed(10);
 *       console.log('end: ' + Date.now());
 *     }).then(function() {
 *       console.log('fin');
 *     });
 *     // start: 0
 *     // ...waiting...
 *     // mid: 50; value = 123
 *     // end: 60
 *     // fin
 *
 * Yielding the result of a promise chain will wait for the entire chain to
 * complete:
 *
 *     promise.fulfilled().then(function* () {
 *       console.log('start: ' + Date.now());
 *
 *       var value = yield flow.
 *           execute(() => console.log('A')).
 *           then(   () => console.log('B')).
 *           then(   () => 123);
 *
 *       console.log('mid: %s; value = %d', Date.now(), value);
 *
 *       yield flow.execute(() => console.log('C'));
 *     }).then(function() {
 *       console.log('fin');
 *     });
 *     // start: 0
 *     // A
 *     // B
 *     // mid: 2; value = 123
 *     // C
 *     // fin
 *
 * Yielding a _rejected_ promise will cause the rejected value to be thrown
 * within the generator function:
 *
 *     flow.execute(function* () {
 *       console.log('start: ' + Date.now());
 *       try {
 *         yield promise.delayed(10).then(function() {
 *           throw Error('boom');
 *         });
 *       } catch (ex) {
 *         console.log('caught time: ' + Date.now());
 *         console.log(ex.message);
 *       }
 *     });
 *     // start: 0
 *     // caught time: 10
 *     // boom
 *
 * # Error Handling
 *
 * ES6 promises do not require users to handle a promise rejections. This can
 * result in subtle bugs as the rejections are silently "swallowed" by the
 * Promise class.
 *
 *     Promise.reject(Error('boom'));
 *     // ... *crickets* ...
 *
 * Selenium's promise module, on the other hand, requires that every rejection
 * be explicitly handled. When a {@linkplain ManagedPromise ManagedPromise} is
 * rejected and no callbacks are defined on that promise, it is considered an
 * _unhandled rejection_ and reported to the active task queue. If the rejection
 * remains unhandled after a single turn of the [event loop][JSEL] (scheduled
 * with a microtask), it will propagate up the stack.
 *
 * ## Error Propagation
 *
 * If an unhandled rejection occurs within a task function, that task's promised
 * result is rejected and all remaining subtasks are discarded:
 *
 *     flow.execute(function() {
 *       // No callbacks registered on promise -> unhandled rejection
 *       promise.rejected(Error('boom'));
 *       flow.execute(function() { console.log('this will never run'); });
 *     }).catch(function(e) {
 *       console.log(e.message);
 *     });
 *     // boom
 *
 * The promised results for discarded tasks are silently rejected with a
 * cancellation error and existing callback chains will never fire.
 *
 *     flow.execute(function() {
 *       promise.rejected(Error('boom'));
 *       flow.execute(function() { console.log('a'); }).
 *           then(function() { console.log('b'); });
 *     }).catch(function(e) {
 *       console.log(e.message);
 *     });
 *     // boom
 *
 * An unhandled rejection takes precedence over a task function's returned
 * result, even if that value is another promise:
 *
 *     flow.execute(function() {
 *       promise.rejected(Error('boom'));
 *       return flow.execute(someOtherTask);
 *     }).catch(function(e) {
 *       console.log(e.message);
 *     });
 *     // boom
 *
 * If there are multiple unhandled rejections within a task, they are packaged
 * in a {@link MultipleUnhandledRejectionError}, which has an `errors` property
 * that is a `Set` of the recorded unhandled rejections:
 *
 *     flow.execute(function() {
 *       promise.rejected(Error('boom1'));
 *       promise.rejected(Error('boom2'));
 *     }).catch(function(ex) {
 *       console.log(ex instanceof MultipleUnhandledRejectionError);
 *       for (var e of ex.errors) {
 *         console.log(e.message);
 *       }
 *     });
 *     // boom1
 *     // boom2
 *
 * When a subtask is discarded due to an unreported rejection in its parent
 * frame, the existing callbacks on that task will never settle and the
 * callbacks will not be invoked. If a new callback is attached to the subtask
 * _after_ it has been discarded, it is handled the same as adding a callback
 * to a cancelled promise: the error-callback path is invoked. This behavior is
 * intended to handle cases where the user saves a reference to a task promise,
 * as illustrated below.
 *
 *     var subTask;
 *     flow.execute(function() {
 *       promise.rejected(Error('boom'));
 *       subTask = flow.execute(function() {});
 *     }).catch(function(e) {
 *       console.log(e.message);
 *     }).then(function() {
 *       return subTask.then(
 *           () => console.log('subtask success!'),
 *           (e) => console.log('subtask failed:\n' + e));
 *     });
 *     // boom
 *     // subtask failed:
 *     // DiscardedTaskError: Task was discarded due to a previous failure: boom
 *
 * When a subtask fails, its promised result is treated the same as any other
 * promise: it must be handled within one turn of the rejection or the unhandled
 * rejection is propagated to the parent task. This means users can catch errors
 * from complex flows from the top level task:
 *
 *     flow.execute(function() {
 *       flow.execute(function() {
 *         flow.execute(function() {
 *           throw Error('fail!');
 *         });
 *       });
 *     }).catch(function(e) {
 *       console.log(e.message);
 *     });
 *     // fail!
 *
 * ## Unhandled Rejection Events
 *
 * When an unhandled rejection propagates to the root of the control flow, the
 * flow will emit an __uncaughtException__ event. If no listeners are registered
 * on the flow, the error will be rethrown to the global error handler: an
 * __uncaughtException__ event from the
 * [`process`](https://nodejs.org/api/process.html) object in node, or
 * `window.onerror` when running in a browser.
 *
 * Bottom line: you __*must*__ handle rejected promises.
 *
 * # Promises/A+ Compatibility
 *
 * This `promise` module is compliant with the [Promises/A+] specification
 * except for sections `2.2.6.1` and `2.2.6.2`:
 *
 * >
 * > - `then` may be called multiple times on the same promise.
 * >    - If/when `promise` is fulfilled, all respective `onFulfilled` callbacks
 * >      must execute in the order of their originating calls to `then`.
 * >    - If/when `promise` is rejected, all respective `onRejected` callbacks
 * >      must execute in the order of their originating calls to `then`.
 * >
 *
 * Specifically, the conformance tests contain the following scenario (for
 * brevity, only the fulfillment version is shown):
 *
 *     var p1 = Promise.resolve();
 *     p1.then(function() {
 *       console.log('A');
 *       p1.then(() => console.log('B'));
 *     });
 *     p1.then(() => console.log('C'));
 *     // A
 *     // C
 *     // B
 *
 * Since the [ControlFlow](#scheduling_callbacks) executes promise callbacks as
 * tasks, with this module, the result would be:
 *
 *     var p2 = promise.fulfilled();
 *     p2.then(function() {
 *       console.log('A');
 *       p2.then(() => console.log('B');
 *     });
 *     p2.then(() => console.log('C'));
 *     // A
 *     // B
 *     // C
 *
 * [JSEL]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/EventLoop
 * [GF]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/function*
 * [Promises/A+]: https://promisesaplus.com/
 * [MicrotasksArticle]: https://jakearchibald.com/2015/tasks-microtasks-queues-and-schedules/
 */



const error = __webpack_require__(19);
const events = __webpack_require__(155);
const logging = __webpack_require__(24);


/**
 * Alias to help with readability and differentiate types.
 * @const
 */
const NativePromise = Promise;


/**
 * Whether to append traces of `then` to rejection errors.
 * @type {boolean}
 */
var LONG_STACK_TRACES = false;  // TODO: this should not be CONSTANT_CASE


/** @const */
const LOG = logging.getLogger('promise');


const UNIQUE_IDS = new WeakMap;
let nextId = 1;


function getUid(obj) {
  let id = UNIQUE_IDS.get(obj);
  if (!id) {
    id = nextId;
    nextId += 1;
    UNIQUE_IDS.set(obj, id);
  }
  return id;
}


/**
 * Runs the given function after a microtask yield.
 * @param {function()} fn The function to run.
 */
function asyncRun(fn) {
  NativePromise.resolve().then(function() {
    try {
      fn();
    } catch (ignored) {
      // Do nothing.
    }
  });
}

/**
 * @param {number} level What level of verbosity to log with.
 * @param {(string|function(this: T): string)} loggable The message to log.
 * @param {T=} opt_self The object in whose context to run the loggable
 *     function.
 * @template T
 */
function vlog(level, loggable, opt_self) {
  var logLevel = logging.Level.FINE;
  if (level > 1) {
    logLevel = logging.Level.FINEST;
  } else if (level > 0) {
    logLevel = logging.Level.FINER;
  }

  if (typeof loggable === 'function') {
    loggable = loggable.bind(opt_self);
  }

  LOG.log(logLevel, loggable);
}


/**
 * Generates an error to capture the current stack trace.
 * @param {string} name Error name for this stack trace.
 * @param {string} msg Message to record.
 * @param {Function=} opt_topFn The function that should appear at the top of
 *     the stack; only applicable in V8.
 * @return {!Error} The generated error.
 */
function captureStackTrace(name, msg, opt_topFn) {
  var e = Error(msg);
  e.name = name;
  if (Error.captureStackTrace) {
    Error.captureStackTrace(e, opt_topFn);
  } else {
    var stack = Error().stack;
    if (stack) {
      e.stack = e.toString();
      e.stack += '\n' + stack;
    }
  }
  return e;
}


/**
 * Error used when the computation of a promise is cancelled.
 */
class CancellationError extends Error {
  /**
   * @param {string=} opt_msg The cancellation message.
   */
  constructor(opt_msg) {
    super(opt_msg);

    /** @override */
    this.name = this.constructor.name;

    /** @private {boolean} */
    this.silent_ = false;
  }

  /**
   * Wraps the given error in a CancellationError.
   *
   * @param {*} error The error to wrap.
   * @param {string=} opt_msg The prefix message to use.
   * @return {!CancellationError} A cancellation error.
   */
  static wrap(error, opt_msg) {
    var message;
    if (error instanceof CancellationError) {
      return new CancellationError(
          opt_msg ? (opt_msg + ': ' + error.message) : error.message);
    } else if (opt_msg) {
      message = opt_msg;
      if (error) {
        message += ': ' + error;
      }
      return new CancellationError(message);
    }
    if (error) {
      message = error + '';
    }
    return new CancellationError(message);
  }
}


/**
 * Error used to cancel tasks when a control flow is reset.
 * @final
 */
class FlowResetError extends CancellationError {
  constructor() {
    super('ControlFlow was reset');
    this.silent_ = true;
  }
}


/**
 * Error used to cancel tasks that have been discarded due to an uncaught error
 * reported earlier in the control flow.
 * @final
 */
class DiscardedTaskError extends CancellationError {
  /** @param {*} error The original error. */
  constructor(error) {
    if (error instanceof DiscardedTaskError) {
      return /** @type {!DiscardedTaskError} */(error);
    }

    var msg = '';
    if (error) {
      msg = ': ' + (
          typeof error.message === 'string' ? error.message : error);
    }

    super('Task was discarded due to a previous failure' + msg);
    this.silent_ = true;
  }
}


/**
 * Error used when there are multiple unhandled promise rejections detected
 * within a task or callback.
 *
 * @final
 */
class MultipleUnhandledRejectionError extends Error {
  /**
   * @param {!(Set<*>)} errors The errors to report.
   */
  constructor(errors) {
    super('Multiple unhandled promise rejections reported');

    /** @override */
    this.name = this.constructor.name;

    /** @type {!Set<*>} */
    this.errors = errors;
  }
}


/**
 * Property used to flag constructor's as implementing the Thenable interface
 * for runtime type checking.
 * @const
 */
const IMPLEMENTED_BY_SYMBOL = Symbol('promise.Thenable');
const CANCELLABLE_SYMBOL = Symbol('promise.CancellableThenable');


/**
 * @param {function(new: ?)} ctor
 * @param {!Object} symbol
 */
function addMarkerSymbol(ctor, symbol) {
  try {
    ctor.prototype[symbol] = true;
  } catch (ignored) {
    // Property access denied?
  }
}


/**
 * @param {*} object
 * @param {!Object} symbol
 * @return {boolean}
 */
function hasMarkerSymbol(object, symbol) {
  if (!object) {
    return false;
  }
  try {
    return !!object[symbol];
  } catch (e) {
    return false;  // Property access seems to be forbidden.
  }
}


/**
 * Thenable is a promise-like object with a {@code then} method which may be
 * used to schedule callbacks on a promised value.
 *
 * @record
 * @extends {IThenable<T>}
 * @template T
 */
class Thenable {
  /**
   * Adds a property to a class prototype to allow runtime checks of whether
   * instances of that class implement the Thenable interface.
   * @param {function(new: Thenable, ...?)} ctor The
   *     constructor whose prototype to modify.
   */
  static addImplementation(ctor) {
    addMarkerSymbol(ctor, IMPLEMENTED_BY_SYMBOL);
  }

  /**
   * Checks if an object has been tagged for implementing the Thenable
   * interface as defined by {@link Thenable.addImplementation}.
   * @param {*} object The object to test.
   * @return {boolean} Whether the object is an implementation of the Thenable
   *     interface.
   */
  static isImplementation(object) {
    return hasMarkerSymbol(object, IMPLEMENTED_BY_SYMBOL);
  }

  /**
   * Registers listeners for when this instance is resolved.
   *
   * @param {?(function(T): (R|IThenable<R>))=} opt_callback The
   *     function to call if this promise is successfully resolved. The function
   *     should expect a single argument: the promise's resolved value.
   * @param {?(function(*): (R|IThenable<R>))=} opt_errback
   *     The function to call if this promise is rejected. The function should
   *     expect a single argument: the rejection reason.
   * @return {!Thenable<R>} A new promise which will be resolved with the result
   *     of the invoked callback.
   * @template R
   */
  then(opt_callback, opt_errback) {}

  /**
   * Registers a listener for when this promise is rejected. This is synonymous
   * with the {@code catch} clause in a synchronous API:
   *
   *     // Synchronous API:
   *     try {
   *       doSynchronousWork();
   *     } catch (ex) {
   *       console.error(ex);
   *     }
   *
   *     // Asynchronous promise API:
   *     doAsynchronousWork().catch(function(ex) {
   *       console.error(ex);
   *     });
   *
   * @param {function(*): (R|IThenable<R>)} errback The
   *     function to call if this promise is rejected. The function should
   *     expect a single argument: the rejection reason.
   * @return {!Thenable<R>} A new promise which will be resolved with the result
   *     of the invoked callback.
   * @template R
   */
  catch(errback) {}
}


/**
 * Marker interface for objects that allow consumers to request the cancellation
 * of a promise-based operation. A cancelled promise will be rejected with a
 * {@link CancellationError}.
 *
 * This interface is considered package-private and should not be used outside
 * of selenium-webdriver.
 *
 * @interface
 * @extends {Thenable<T>}
 * @template T
 * @package
 */
class CancellableThenable {
  /**
   * @param {function(new: CancellableThenable, ...?)} ctor
   */
  static addImplementation(ctor) {
    Thenable.addImplementation(ctor);
    addMarkerSymbol(ctor, CANCELLABLE_SYMBOL);
  }

  /**
   * @param {*} object
   * @return {boolean}
   */
  static isImplementation(object) {
    return hasMarkerSymbol(object, CANCELLABLE_SYMBOL);
  }

  /**
   * Requests the cancellation of the computation of this promise's value,
   * rejecting the promise in the process. This method is a no-op if the promise
   * has already been resolved.
   *
   * @param {(string|Error)=} opt_reason The reason this promise is being
   *     cancelled. This value will be wrapped in a {@link CancellationError}.
   */
  cancel(opt_reason) {}
}


/**
 * @enum {string}
 */
const PromiseState = {
  PENDING: 'pending',
  BLOCKED: 'blocked',
  REJECTED: 'rejected',
  FULFILLED: 'fulfilled'
};


/**
 * Internal map used to store cancellation handlers for {@link ManagedPromise}
 * objects. This is an internal implementation detail used by the
 * {@link TaskQueue} class to monitor for when a promise is cancelled without
 * generating an extra promise via then().
 *
 * @const {!WeakMap<!ManagedPromise, function(!CancellationError)>}
 */
const ON_CANCEL_HANDLER = new WeakMap;

const SKIP_LOG = Symbol('skip-log');
const FLOW_LOG = logging.getLogger('promise.ControlFlow');


/**
 * Represents the eventual value of a completed operation. Each promise may be
 * in one of three states: pending, fulfilled, or rejected. Each promise starts
 * in the pending state and may make a single transition to either a
 * fulfilled or rejected state, at which point the promise is considered
 * resolved.
 *
 * @implements {CancellableThenable<T>}
 * @template T
 * @see http://promises-aplus.github.io/promises-spec/
 */
class ManagedPromise {
  /**
   * @param {function(
   *           function((T|IThenable<T>|Thenable)=),
   *           function(*=))} resolver
   *     Function that is invoked immediately to begin computation of this
   *     promise's value. The function should accept a pair of callback
   *     functions, one for fulfilling the promise and another for rejecting it.
   * @param {ControlFlow=} opt_flow The control flow
   *     this instance was created under. Defaults to the currently active flow.
   * @param {?=} opt_skipLog An internal parameter used to skip logging the
   *     creation of this promise. This parameter has no effect unless it is
   *     strictly equal to an internal symbol. In other words, this parameter
   *     is always ignored for external code.
   */
  constructor(resolver, opt_flow, opt_skipLog) {
    if (!usePromiseManager()) {
      throw TypeError(
        'Unable to create a managed promise instance: the promise manager has'
            + ' been disabled by the SELENIUM_PROMISE_MANAGER environment'
            + ' variable: ' + Object({"NODE_ENV":"production"})['SELENIUM_PROMISE_MANAGER']);
    } else if (opt_skipLog !== SKIP_LOG) {
      FLOW_LOG.warning(() => {
        let e =
            captureStackTrace(
                'ManagedPromiseError',
                'Creating a new managed Promise. This call will fail when the'
                    + ' promise manager is disabled',
            ManagedPromise)
        return e.stack;
      });
    }

    getUid(this);

    /** @private {!ControlFlow} */
    this.flow_ = opt_flow || controlFlow();

    /** @private {Error} */
    this.stack_ = null;
    if (LONG_STACK_TRACES) {
      this.stack_ = captureStackTrace('ManagedPromise', 'new', this.constructor);
    }

    /** @private {Thenable<?>} */
    this.parent_ = null;

    /** @private {Array<!Task>} */
    this.callbacks_ = null;

    /** @private {PromiseState} */
    this.state_ = PromiseState.PENDING;

    /** @private {boolean} */
    this.handled_ = false;

    /** @private {*} */
    this.value_ = undefined;

    /** @private {TaskQueue} */
    this.queue_ = null;

    try {
      var self = this;
      resolver(function(value) {
        self.resolve_(PromiseState.FULFILLED, value);
      }, function(reason) {
        self.resolve_(PromiseState.REJECTED, reason);
      });
    } catch (ex) {
      this.resolve_(PromiseState.REJECTED, ex);
    }
  }

  /**
   * Creates a promise that is immediately resolved with the given value.
   *
   * @param {T=} opt_value The value to resolve.
   * @return {!ManagedPromise<T>} A promise resolved with the given value.
   * @template T
   */
  static resolve(opt_value) {
    if (opt_value instanceof ManagedPromise) {
      return opt_value;
    }
    return new ManagedPromise(resolve => resolve(opt_value));
  }

  /**
   * Creates a promise that is immediately rejected with the given reason.
   *
   * @param {*=} opt_reason The rejection reason.
   * @return {!ManagedPromise<?>} A new rejected promise.
   */
  static reject(opt_reason) {
    return new ManagedPromise((_, reject) => reject(opt_reason));
  }

  /** @override */
  toString() {
    return 'ManagedPromise::' + getUid(this) +
      ' {[[PromiseStatus]]: "' + this.state_ + '"}';
  }

  /**
   * Resolves this promise. If the new value is itself a promise, this function
   * will wait for it to be resolved before notifying the registered listeners.
   * @param {PromiseState} newState The promise's new state.
   * @param {*} newValue The promise's new value.
   * @throws {TypeError} If {@code newValue === this}.
   * @private
   */
  resolve_(newState, newValue) {
    if (PromiseState.PENDING !== this.state_) {
      return;
    }

    if (newValue === this) {
      // See promise a+, 2.3.1
      // http://promises-aplus.github.io/promises-spec/#point-48
      newValue = new TypeError('A promise may not resolve to itself');
      newState = PromiseState.REJECTED;
    }

    this.parent_ = null;
    this.state_ = PromiseState.BLOCKED;

    if (newState !== PromiseState.REJECTED) {
      if (Thenable.isImplementation(newValue)) {
        // 2.3.2
        newValue = /** @type {!Thenable} */(newValue);
        this.parent_ = newValue;
        newValue.then(
            this.unblockAndResolve_.bind(this, PromiseState.FULFILLED),
            this.unblockAndResolve_.bind(this, PromiseState.REJECTED));
        return;

      } else if (newValue
          && (typeof newValue === 'object' || typeof newValue === 'function')) {
        // 2.3.3

        try {
          // 2.3.3.1
          var then = newValue['then'];
        } catch (e) {
          // 2.3.3.2
          this.state_ = PromiseState.REJECTED;
          this.value_ = e;
          this.scheduleNotifications_();
          return;
        }

        if (typeof then === 'function') {
          // 2.3.3.3
          this.invokeThen_(/** @type {!Object} */(newValue), then);
          return;
        }
      }
    }

    if (newState === PromiseState.REJECTED &&
        isError(newValue) && newValue.stack && this.stack_) {
      newValue.stack += '\nFrom: ' + (this.stack_.stack || this.stack_);
    }

    // 2.3.3.4 and 2.3.4
    this.state_ = newState;
    this.value_ = newValue;
    this.scheduleNotifications_();
  }

  /**
   * Invokes a thenable's "then" method according to 2.3.3.3 of the promise
   * A+ spec.
   * @param {!Object} x The thenable object.
   * @param {!Function} then The "then" function to invoke.
   * @private
   */
  invokeThen_(x, then) {
    var called = false;
    var self = this;

    var resolvePromise = function(value) {
      if (!called) {  // 2.3.3.3.3
        called = true;
        // 2.3.3.3.1
        self.unblockAndResolve_(PromiseState.FULFILLED, value);
      }
    };

    var rejectPromise = function(reason) {
      if (!called) {  // 2.3.3.3.3
        called = true;
        // 2.3.3.3.2
        self.unblockAndResolve_(PromiseState.REJECTED, reason);
      }
    };

    try {
      // 2.3.3.3
      then.call(x, resolvePromise, rejectPromise);
    } catch (e) {
      // 2.3.3.3.4.2
      rejectPromise(e);
    }
  }

  /**
   * @param {PromiseState} newState The promise's new state.
   * @param {*} newValue The promise's new value.
   * @private
   */
  unblockAndResolve_(newState, newValue) {
    if (this.state_ === PromiseState.BLOCKED) {
      this.state_ = PromiseState.PENDING;
      this.resolve_(newState, newValue);
    }
  }

  /**
   * @private
   */
  scheduleNotifications_() {
    vlog(2, () => this + ' scheduling notifications', this);

    ON_CANCEL_HANDLER.delete(this);
    if (this.value_ instanceof CancellationError
        && this.value_.silent_) {
      this.callbacks_ = null;
    }

    if (!this.queue_) {
      this.queue_ = this.flow_.getActiveQueue_();
    }

    if (!this.handled_ &&
        this.state_ === PromiseState.REJECTED &&
        !(this.value_ instanceof CancellationError)) {
      this.queue_.addUnhandledRejection(this);
    }
    this.queue_.scheduleCallbacks(this);
  }

  /** @override */
  cancel(opt_reason) {
    if (!canCancel(this)) {
      return;
    }

    if (this.parent_ && canCancel(this.parent_)) {
      /** @type {!CancellableThenable} */(this.parent_).cancel(opt_reason);
    } else {
      var reason = CancellationError.wrap(opt_reason);
      let onCancel = ON_CANCEL_HANDLER.get(this);
      if (onCancel) {
        onCancel(reason);
        ON_CANCEL_HANDLER.delete(this);
      }

      if (this.state_ === PromiseState.BLOCKED) {
        this.unblockAndResolve_(PromiseState.REJECTED, reason);
      } else {
        this.resolve_(PromiseState.REJECTED, reason);
      }
    }

    function canCancel(promise) {
      if (!(promise instanceof ManagedPromise)) {
        return CancellableThenable.isImplementation(promise);
      }
      return promise.state_ === PromiseState.PENDING
          || promise.state_ === PromiseState.BLOCKED;
    }
  }

  /** @override */
  then(opt_callback, opt_errback) {
    return this.addCallback_(
        opt_callback, opt_errback, 'then', ManagedPromise.prototype.then);
  }

  /** @override */
  catch(errback) {
    return this.addCallback_(
        null, errback, 'catch', ManagedPromise.prototype.catch);
  }

  /**
   * @param {function(): (R|IThenable<R>)} callback
   * @return {!ManagedPromise<R>}
   * @template R
   * @see ./promise.finally()
   */
  finally(callback) {
    let result = thenFinally(this, callback);
    return /** @type {!ManagedPromise} */(result);
  }

  /**
   * Registers a new callback with this promise
   * @param {(function(T): (R|IThenable<R>)|null|undefined)} callback The
   *    fulfillment callback.
   * @param {(function(*): (R|IThenable<R>)|null|undefined)} errback The
   *    rejection callback.
   * @param {string} name The callback name.
   * @param {!Function} fn The function to use as the top of the stack when
   *     recording the callback's creation point.
   * @return {!ManagedPromise<R>} A new promise which will be resolved with the
   *     result of the invoked callback.
   * @template R
   * @private
   */
  addCallback_(callback, errback, name, fn) {
    if (typeof callback !== 'function' && typeof errback !== 'function') {
      return this;
    }

    this.handled_ = true;
    if (this.queue_) {
      this.queue_.clearUnhandledRejection(this);
    }

    var cb = new Task(
        this.flow_,
        this.invokeCallback_.bind(this, callback, errback),
        name,
        LONG_STACK_TRACES ? {name: 'Promise', top: fn} : undefined);
    cb.promise.parent_ = this;

    if (this.state_ !== PromiseState.PENDING &&
        this.state_ !== PromiseState.BLOCKED) {
      this.flow_.getActiveQueue_().enqueue(cb);
    } else {
      if (!this.callbacks_) {
        this.callbacks_ = [];
      }
      this.callbacks_.push(cb);
      cb.blocked = true;
      this.flow_.getActiveQueue_().enqueue(cb);
    }

    return cb.promise;
  }

  /**
   * Invokes a callback function attached to this promise.
   * @param {(function(T): (R|IThenable<R>)|null|undefined)} callback The
   *    fulfillment callback.
   * @param {(function(*): (R|IThenable<R>)|null|undefined)} errback The
   *    rejection callback.
   * @template R
   * @private
   */
  invokeCallback_(callback, errback) {
    var callbackFn = callback;
    if (this.state_ === PromiseState.REJECTED) {
      callbackFn = errback;
    }

    if (typeof callbackFn === 'function') {
      if (isGenerator(callbackFn)) {
        return consume(callbackFn, null, this.value_);
      }
      return callbackFn(this.value_);
    } else if (this.state_ === PromiseState.REJECTED) {
      throw this.value_;
    } else {
      return this.value_;
    }
  }
}
CancellableThenable.addImplementation(ManagedPromise);


/**
 * @param {!ManagedPromise} promise
 * @return {boolean}
 */
function isPending(promise) {
  return promise.state_ === PromiseState.PENDING;
}


/**
 * Structural interface for a deferred promise resolver.
 * @record
 * @template T
 */
function Resolver() {}


/**
 * The promised value for this resolver.
 * @type {!Thenable<T>}
 */
Resolver.prototype.promise;


/**
 * Resolves the promised value with the given `value`.
 * @param {T|Thenable<T>} value
 * @return {void}
 */
Resolver.prototype.resolve;


/**
 * Rejects the promised value with the given `reason`.
 * @param {*} reason
 * @return {void}
 */
Resolver.prototype.reject;


/**
 * Represents a value that will be resolved at some point in the future. This
 * class represents the protected "producer" half of a ManagedPromise - each Deferred
 * has a {@code promise} property that may be returned to consumers for
 * registering callbacks, reserving the ability to resolve the deferred to the
 * producer.
 *
 * If this Deferred is rejected and there are no listeners registered before
 * the next turn of the event loop, the rejection will be passed to the
 * {@link ControlFlow} as an unhandled failure.
 *
 * @template T
 * @implements {Resolver<T>}
 */
class Deferred {
  /**
   * @param {ControlFlow=} opt_flow The control flow this instance was
   *     created under. This should only be provided during unit tests.
   * @param {?=} opt_skipLog An internal parameter used to skip logging the
   *     creation of this promise. This parameter has no effect unless it is
   *     strictly equal to an internal symbol. In other words, this parameter
   *     is always ignored for external code.
   */
  constructor(opt_flow, opt_skipLog) {
    var fulfill, reject;

    /** @type {!ManagedPromise<T>} */
    this.promise = new ManagedPromise(function(f, r) {
      fulfill = f;
      reject = r;
    }, opt_flow, opt_skipLog);

    var self = this;
    var checkNotSelf = function(value) {
      if (value === self) {
        throw new TypeError('May not resolve a Deferred with itself');
      }
    };

    /**
     * Resolves this deferred with the given value. It is safe to call this as a
     * normal function (with no bound "this").
     * @param {(T|IThenable<T>|Thenable)=} opt_value The fulfilled value.
     * @const
     */
    this.resolve = function(opt_value) {
      checkNotSelf(opt_value);
      fulfill(opt_value);
    };

    /**
     * An alias for {@link #resolve}.
     * @const
     */
    this.fulfill = this.resolve;

    /**
     * Rejects this promise with the given reason. It is safe to call this as a
     * normal function (with no bound "this").
     * @param {*=} opt_reason The rejection reason.
     * @const
     */
    this.reject = function(opt_reason) {
      checkNotSelf(opt_reason);
      reject(opt_reason);
    };
  }
}


/**
 * Tests if a value is an Error-like object. This is more than an straight
 * instanceof check since the value may originate from another context.
 * @param {*} value The value to test.
 * @return {boolean} Whether the value is an error.
 */
function isError(value) {
  return value instanceof Error ||
      (!!value && typeof value === 'object'
          && typeof value.message === 'string');
}


/**
 * Determines whether a {@code value} should be treated as a promise.
 * Any object whose "then" property is a function will be considered a promise.
 *
 * @param {?} value The value to test.
 * @return {boolean} Whether the value is a promise.
 */
function isPromise(value) {
  try {
    // Use array notation so the Closure compiler does not obfuscate away our
    // contract.
    return value
        && (typeof value === 'object' || typeof value === 'function')
        && typeof value['then'] === 'function';
  } catch (ex) {
    return false;
  }
}


/**
 * Creates a promise that will be resolved at a set time in the future.
 * @param {number} ms The amount of time, in milliseconds, to wait before
 *     resolving the promise.
 * @return {!Thenable} The promise.
 */
function delayed(ms) {
  return createPromise(resolve => {
    setTimeout(() => resolve(), ms);
  });
}


/**
 * Creates a new deferred resolver.
 *
 * If the promise manager is currently enabled, this function will return a
 * {@link Deferred} instance. Otherwise, it will return a resolver for a
 * {@linkplain NativePromise native promise}.
 *
 * @return {!Resolver<T>} A new deferred resolver.
 * @template T
 */
function defer() {
  if (usePromiseManager()) {
    return new Deferred();
  }
  let resolve, reject;
  let promise = new NativePromise((_resolve, _reject) => {
    resolve = _resolve;
    reject = _reject;
  });
  return {promise, resolve, reject};
}


/**
 * Creates a promise that has been resolved with the given value.
 *
 * If the promise manager is currently enabled, this function will return a
 * {@linkplain ManagedPromise managed promise}. Otherwise, it will return a
 * {@linkplain NativePromise native promise}.
 *
 * @param {T=} opt_value The resolved value.
 * @return {!Thenable<T>} The resolved promise.
 * @template T
 */
function fulfilled(opt_value) {
  let ctor = usePromiseManager() ? ManagedPromise : NativePromise;
  if (opt_value instanceof ctor) {
    return /** @type {!Thenable} */(opt_value);
  }

  if (usePromiseManager()) {
    // We can skip logging warnings about creating a managed promise because
    // this function will automatically switch to use a native promise when
    // the promise manager is disabled.
    return new ManagedPromise(
        resolve => resolve(opt_value), undefined, SKIP_LOG);
  }
  return NativePromise.resolve(opt_value);
}


/**
 * Creates a promise that has been rejected with the given reason.
 *
 * If the promise manager is currently enabled, this function will return a
 * {@linkplain ManagedPromise managed promise}. Otherwise, it will return a
 * {@linkplain NativePromise native promise}.
 *
 * @param {*=} opt_reason The rejection reason; may be any value, but is
 *     usually an Error or a string.
 * @return {!Thenable<?>} The rejected promise.
 */
function rejected(opt_reason) {
  if (usePromiseManager()) {
    // We can skip logging warnings about creating a managed promise because
    // this function will automatically switch to use a native promise when
    // the promise manager is disabled.
    return new ManagedPromise(
        (_, reject) => reject(opt_reason), undefined, SKIP_LOG);
  }
  return NativePromise.reject(opt_reason);
}


/**
 * Wraps a function that expects a node-style callback as its final
 * argument. This callback expects two arguments: an error value (which will be
 * null if the call succeeded), and the success value as the second argument.
 * The callback will the resolve or reject the returned promise, based on its
 * arguments.
 * @param {!Function} fn The function to wrap.
 * @param {...?} var_args The arguments to apply to the function, excluding the
 *     final callback.
 * @return {!Thenable} A promise that will be resolved with the
 *     result of the provided function's callback.
 */
function checkedNodeCall(fn, var_args) {
  let args = Array.prototype.slice.call(arguments, 1);
  return createPromise(function(fulfill, reject) {
    try {
      args.push(function(error, value) {
        error ? reject(error) : fulfill(value);
      });
      fn.apply(undefined, args);
    } catch (ex) {
      reject(ex);
    }
  });
}

/**
 * Registers a listener to invoke when a promise is resolved, regardless
 * of whether the promise's value was successfully computed. This function
 * is synonymous with the {@code finally} clause in a synchronous API:
 *
 *     // Synchronous API:
 *     try {
 *       doSynchronousWork();
 *     } finally {
 *       cleanUp();
 *     }
 *
 *     // Asynchronous promise API:
 *     doAsynchronousWork().finally(cleanUp);
 *
 * __Note:__ similar to the {@code finally} clause, if the registered
 * callback returns a rejected promise or throws an error, it will silently
 * replace the rejection error (if any) from this promise:
 *
 *     try {
 *       throw Error('one');
 *     } finally {
 *       throw Error('two');  // Hides Error: one
 *     }
 *
 *     let p = Promise.reject(Error('one'));
 *     promise.finally(p, function() {
 *       throw Error('two');  // Hides Error: one
 *     });
 *
 * @param {!IThenable<?>} promise The promise to add the listener to.
 * @param {function(): (R|IThenable<R>)} callback The function to call when
 *     the promise is resolved.
 * @return {!IThenable<R>} A promise that will be resolved with the callback
 *     result.
 * @template R
 */
function thenFinally(promise, callback) {
  let error;
  let mustThrow = false;
  return promise.then(function() {
    return callback();
  }, function(err) {
    error = err;
    mustThrow = true;
    return callback();
  }).then(function() {
    if (mustThrow) {
      throw error;
    }
  });
}


/**
 * Registers an observer on a promised {@code value}, returning a new promise
 * that will be resolved when the value is. If {@code value} is not a promise,
 * then the return promise will be immediately resolved.
 * @param {*} value The value to observe.
 * @param {Function=} opt_callback The function to call when the value is
 *     resolved successfully.
 * @param {Function=} opt_errback The function to call when the value is
 *     rejected.
 * @return {!Thenable} A new promise.
 * @deprecated Use `promise.fulfilled(value).then(opt_callback, opt_errback)`
 */
function when(value, opt_callback, opt_errback) {
  return fulfilled(value).then(opt_callback, opt_errback);
}


/**
 * Invokes the appropriate callback function as soon as a promised `value` is
 * resolved.
 *
 * @param {*} value The value to observe.
 * @param {Function} callback The function to call when the value is
 *     resolved successfully.
 * @param {Function=} opt_errback The function to call when the value is
 *     rejected.
 */
function asap(value, callback, opt_errback) {
  if (isPromise(value)) {
    value.then(callback, opt_errback);

  } else if (callback) {
    callback(value);
  }
}


/**
 * Given an array of promises, will return a promise that will be fulfilled
 * with the fulfillment values of the input array's values. If any of the
 * input array's promises are rejected, the returned promise will be rejected
 * with the same reason.
 *
 * @param {!Array<(T|!ManagedPromise<T>)>} arr An array of
 *     promises to wait on.
 * @return {!Thenable<!Array<T>>} A promise that is
 *     fulfilled with an array containing the fulfilled values of the
 *     input array, or rejected with the same reason as the first
 *     rejected value.
 * @template T
 */
function all(arr) {
  return createPromise(function(fulfill, reject) {
    var n = arr.length;
    var values = [];

    if (!n) {
      fulfill(values);
      return;
    }

    var toFulfill = n;
    var onFulfilled = function(index, value) {
      values[index] = value;
      toFulfill--;
      if (toFulfill == 0) {
        fulfill(values);
      }
    };

    function processPromise(index) {
      asap(arr[index], function(value) {
        onFulfilled(index, value);
      }, reject);
    }

    for (var i = 0; i < n; ++i) {
      processPromise(i);
    }
  });
}


/**
 * Calls a function for each element in an array and inserts the result into a
 * new array, which is used as the fulfillment value of the promise returned
 * by this function.
 *
 * If the return value of the mapping function is a promise, this function
 * will wait for it to be fulfilled before inserting it into the new array.
 *
 * If the mapping function throws or returns a rejected promise, the
 * promise returned by this function will be rejected with the same reason.
 * Only the first failure will be reported; all subsequent errors will be
 * silently ignored.
 *
 * @param {!(Array<TYPE>|ManagedPromise<!Array<TYPE>>)} arr The
 *     array to iterator over, or a promise that will resolve to said array.
 * @param {function(this: SELF, TYPE, number, !Array<TYPE>): ?} fn The
 *     function to call for each element in the array. This function should
 *     expect three arguments (the element, the index, and the array itself.
 * @param {SELF=} opt_self The object to be used as the value of 'this' within
 *     {@code fn}.
 * @template TYPE, SELF
 */
function map(arr, fn, opt_self) {
  return createPromise(resolve => resolve(arr)).then(v => {
    if (!Array.isArray(v)) {
      throw TypeError('not an array');
    }
    var arr = /** @type {!Array} */(v);
    return createPromise(function(fulfill, reject) {
      var n = arr.length;
      var values = new Array(n);
      (function processNext(i) {
        for (; i < n; i++) {
          if (i in arr) {
            break;
          }
        }
        if (i >= n) {
          fulfill(values);
          return;
        }
        try {
          asap(
              fn.call(opt_self, arr[i], i, /** @type {!Array} */(arr)),
              function(value) {
                values[i] = value;
                processNext(i + 1);
              },
              reject);
        } catch (ex) {
          reject(ex);
        }
      })(0);
    });
  });
}


/**
 * Calls a function for each element in an array, and if the function returns
 * true adds the element to a new array.
 *
 * If the return value of the filter function is a promise, this function
 * will wait for it to be fulfilled before determining whether to insert the
 * element into the new array.
 *
 * If the filter function throws or returns a rejected promise, the promise
 * returned by this function will be rejected with the same reason. Only the
 * first failure will be reported; all subsequent errors will be silently
 * ignored.
 *
 * @param {!(Array<TYPE>|ManagedPromise<!Array<TYPE>>)} arr The
 *     array to iterator over, or a promise that will resolve to said array.
 * @param {function(this: SELF, TYPE, number, !Array<TYPE>): (
 *             boolean|ManagedPromise<boolean>)} fn The function
 *     to call for each element in the array.
 * @param {SELF=} opt_self The object to be used as the value of 'this' within
 *     {@code fn}.
 * @template TYPE, SELF
 */
function filter(arr, fn, opt_self) {
  return createPromise(resolve => resolve(arr)).then(v => {
    if (!Array.isArray(v)) {
      throw TypeError('not an array');
    }
    var arr = /** @type {!Array} */(v);
    return createPromise(function(fulfill, reject) {
      var n = arr.length;
      var values = [];
      var valuesLength = 0;
      (function processNext(i) {
        for (; i < n; i++) {
          if (i in arr) {
            break;
          }
        }
        if (i >= n) {
          fulfill(values);
          return;
        }
        try {
          var value = arr[i];
          var include = fn.call(opt_self, value, i, /** @type {!Array} */(arr));
          asap(include, function(include) {
            if (include) {
              values[valuesLength++] = value;
            }
            processNext(i + 1);
            }, reject);
        } catch (ex) {
          reject(ex);
        }
      })(0);
    });
  });
}


/**
 * Returns a promise that will be resolved with the input value in a
 * fully-resolved state. If the value is an array, each element will be fully
 * resolved. Likewise, if the value is an object, all keys will be fully
 * resolved. In both cases, all nested arrays and objects will also be
 * fully resolved.  All fields are resolved in place; the returned promise will
 * resolve on {@code value} and not a copy.
 *
 * Warning: This function makes no checks against objects that contain
 * cyclical references:
 *
 *     var value = {};
 *     value['self'] = value;
 *     promise.fullyResolved(value);  // Stack overflow.
 *
 * @param {*} value The value to fully resolve.
 * @return {!Thenable} A promise for a fully resolved version
 *     of the input value.
 */
function fullyResolved(value) {
  if (isPromise(value)) {
    return fulfilled(value).then(fullyResolveValue);
  }
  return fullyResolveValue(value);
}


/**
 * @param {*} value The value to fully resolve. If a promise, assumed to
 *     already be resolved.
 * @return {!Thenable} A promise for a fully resolved version
 *     of the input value.
 */
function fullyResolveValue(value) {
  if (Array.isArray(value)) {
    return fullyResolveKeys(/** @type {!Array} */ (value));
  }

  if (isPromise(value)) {
    if (isPromise(value)) {
      // We get here when the original input value is a promise that
      // resolves to itself. When the user provides us with such a promise,
      // trust that it counts as a "fully resolved" value and return it.
      // Of course, since it's already a promise, we can just return it
      // to the user instead of wrapping it in another promise.
      return /** @type {!ManagedPromise} */ (value);
    }
  }

  if (value && typeof value === 'object') {
    return fullyResolveKeys(/** @type {!Object} */ (value));
  }

  if (typeof value === 'function') {
    return fullyResolveKeys(/** @type {!Object} */ (value));
  }

  return createPromise(resolve => resolve(value));
}


/**
 * @param {!(Array|Object)} obj the object to resolve.
 * @return {!Thenable} A promise that will be resolved with the
 *     input object once all of its values have been fully resolved.
 */
function fullyResolveKeys(obj) {
  var isArray = Array.isArray(obj);
  var numKeys = isArray ? obj.length : (function() {
    let n = 0;
    for (let key in obj) {
      n += 1;
    }
    return n;
  })();

  if (!numKeys) {
    return createPromise(resolve => resolve(obj));
  }

  function forEachProperty(obj, fn) {
    for (let key in obj) {
      fn.call(null, obj[key], key, obj);
    }
  }

  function forEachElement(arr, fn) {
    arr.forEach(fn);
  }

  var numResolved = 0;
  return createPromise(function(fulfill, reject) {
    var forEachKey = isArray ? forEachElement: forEachProperty;

    forEachKey(obj, function(partialValue, key) {
      if (!Array.isArray(partialValue)
          && (!partialValue || typeof partialValue !== 'object')) {
        maybeResolveValue();
        return;
      }

      fullyResolved(partialValue).then(
          function(resolvedValue) {
            obj[key] = resolvedValue;
            maybeResolveValue();
          },
          reject);
    });

    function maybeResolveValue() {
      if (++numResolved == numKeys) {
        fulfill(obj);
      }
    }
  });
}


//////////////////////////////////////////////////////////////////////////////
//
//  ControlFlow
//
//////////////////////////////////////////////////////////////////////////////


/**
 * Defines methods for coordinating the execution of asynchronous tasks.
 * @record
 */
class Scheduler {
  /**
   * Schedules a task for execution. If the task function is a generator, the
   * task will be executed using {@link ./promise.consume consume()}.
   *
   * @param {function(): (T|IThenable<T>)} fn The function to call to start the
   *     task.
   * @param {string=} opt_description A description of the task for debugging
   *     purposes.
   * @return {!Thenable<T>} A promise that will be resolved with the task
   *     result.
   * @template T
   */
  execute(fn, opt_description) {}

  /**
   * Creates a new promise using the given resolver function.
   *
   * @param {function(
   *             function((T|IThenable<T>|Thenable|null)=),
   *             function(*=))} resolver
   * @return {!Thenable<T>}
   * @template T
   */
  promise(resolver) {}

  /**
   * Schedules a `setTimeout` call.
   *
   * @param {number} ms The timeout delay, in milliseconds.
   * @param {string=} opt_description A description to accompany the timeout.
   * @return {!Thenable<void>} A promise that will be resolved when the timeout
   *     fires.
   */
  timeout(ms, opt_description) {}

  /**
   * Schedules a task to wait for a condition to hold.
   *
   * If the condition is defined as a function, it may return any value. Promise
   * will be resolved before testing if the condition holds (resolution time
   * counts towards the timeout). Once resolved, values are always evaluated as
   * booleans.
   *
   * If the condition function throws, or returns a rejected promise, the
   * wait task will fail.
   *
   * If the condition is defined as a promise, the scheduler will wait for it to
   * settle. If the timeout expires before the promise settles, the promise
   * returned by this function will be rejected.
   *
   * If this function is invoked with `timeout === 0`, or the timeout is
   * omitted, this scheduler will wait indefinitely for the condition to be
   * satisfied.
   *
   * @param {(!IThenable<T>|function())} condition The condition to poll,
   *     or a promise to wait on.
   * @param {number=} opt_timeout How long to wait, in milliseconds, for the
   *     condition to hold before timing out. If omitted, the flow will wait
   *     indefinitely.
   * @param {string=} opt_message An optional error message to include if the
   *     wait times out; defaults to the empty string.
   * @return {!Thenable<T>} A promise that will be fulfilled
   *     when the condition has been satisfied. The promise shall be rejected
   *     if the wait times out waiting for the condition.
   * @throws {TypeError} If condition is not a function or promise or if timeout
   *     is not a number >= 0.
   * @template T
   */
  wait(condition, opt_timeout, opt_message) {}
}


let USE_PROMISE_MANAGER;
function usePromiseManager() {
  if (typeof USE_PROMISE_MANAGER !== 'undefined') {
    return !!USE_PROMISE_MANAGER;
  }
  return Object({"NODE_ENV":"production"})['SELENIUM_PROMISE_MANAGER'] === undefined
      || !/^0|false$/i.test(Object({"NODE_ENV":"production"})['SELENIUM_PROMISE_MANAGER']);
}


/**
 * Creates a new promise with the given `resolver` function. If the promise
 * manager is currently enabled, the returned promise will be a
 * {@linkplain ManagedPromise} instance. Otherwise, it will be a native promise.
 *
 * @param {function(
 *             function((T|IThenable<T>|Thenable|null)=),
 *             function(*=))} resolver
 * @return {!Thenable<T>}
 * @template T
 */
function createPromise(resolver) {
  let ctor = usePromiseManager() ? ManagedPromise : NativePromise;
  return new ctor(resolver);
}


/**
 * @param {!Scheduler} scheduler The scheduler to use.
 * @param {(!IThenable<T>|function())} condition The condition to poll,
 *     or a promise to wait on.
 * @param {number=} opt_timeout How long to wait, in milliseconds, for the
 *     condition to hold before timing out. If omitted, the flow will wait
 *     indefinitely.
 * @param {string=} opt_message An optional error message to include if the
 *     wait times out; defaults to the empty string.
 * @return {!Thenable<T>} A promise that will be fulfilled
 *     when the condition has been satisfied. The promise shall be rejected
 *     if the wait times out waiting for the condition.
 * @throws {TypeError} If condition is not a function or promise or if timeout
 *     is not a number >= 0.
 * @template T
 */
function scheduleWait(scheduler, condition, opt_timeout, opt_message) {
  let timeout = opt_timeout || 0;
  if (typeof timeout !== 'number' || timeout < 0) {
    throw TypeError('timeout must be a number >= 0: ' + timeout);
  }

  if (isPromise(condition)) {
    return scheduler.execute(function() {
      if (!timeout) {
        return condition;
      }
      return scheduler.promise(function(fulfill, reject) {
        let start = Date.now();
        let timer = setTimeout(function() {
          timer = null;
          reject(
              new error.TimeoutError(
                  (opt_message ? opt_message + '\n' : '')
                      + 'Timed out waiting for promise to resolve after '
                      + (Date.now() - start) + 'ms'));
        }, timeout);

        /** @type {Thenable} */(condition).then(
          function(value) {
            timer && clearTimeout(timer);
            fulfill(value);
          },
          function(error) {
            timer && clearTimeout(timer);
            reject(error);
          });
      });
    }, opt_message || '<anonymous wait: promise resolution>');
  }

  if (typeof condition !== 'function') {
    throw TypeError('Invalid condition; must be a function or promise: ' +
        typeof condition);
  }

  if (isGenerator(condition)) {
    let original = condition;
    condition = () => consume(original);
  }

  return scheduler.execute(function() {
    var startTime = Date.now();
    return scheduler.promise(function(fulfill, reject) {
      pollCondition();

      function pollCondition() {
        var conditionFn = /** @type {function()} */(condition);
        scheduler.execute(conditionFn).then(function(value) {
          var elapsed = Date.now() - startTime;
          if (!!value) {
            fulfill(value);
          } else if (timeout && elapsed >= timeout) {
            reject(
                new error.TimeoutError(
                    (opt_message ? opt_message + '\n' : '')
                        + `Wait timed out after ${elapsed}ms`));
          } else {
            // Do not use asyncRun here because we need a non-micro yield
            // here so the UI thread is given a chance when running in a
            // browser.
            setTimeout(pollCondition, 0);
          }
        }, reject);
      }
    });
  }, opt_message || '<anonymous wait>');
}


/**
 * A scheduler that executes all tasks immediately, with no coordination. This
 * class is an event emitter for API compatibility with the {@link ControlFlow},
 * however, it emits no events.
 *
 * @implements {Scheduler}
 */
class SimpleScheduler extends events.EventEmitter {
  /** @override */
  execute(fn) {
    return this.promise((resolve, reject) => {
      try {
        if (isGenerator(fn)) {
          consume(fn).then(resolve, reject);
        } else {
          resolve(fn.call(undefined));
        }
      } catch (ex) {
        reject(ex);
      }
    });
  }

  /** @override */
  promise(resolver) {
    return new NativePromise(resolver);
  }

  /** @override */
  timeout(ms) {
    return this.promise(resolve => setTimeout(_ => resolve(), ms));
  }

  /** @override */
  wait(condition, opt_timeout, opt_message) {
    return scheduleWait(this, condition, opt_timeout, opt_message);
  }
}
const SIMPLE_SCHEDULER = new SimpleScheduler;


/**
 * Handles the execution of scheduled tasks, each of which may be an
 * asynchronous operation. The control flow will ensure tasks are executed in
 * the order scheduled, starting each task only once those before it have
 * completed.
 *
 * Each task scheduled within this flow may return a {@link ManagedPromise} to
 * indicate it is an asynchronous operation. The ControlFlow will wait for such
 * promises to be resolved before marking the task as completed.
 *
 * Tasks and each callback registered on a {@link ManagedPromise} will be run
 * in their own ControlFlow frame. Any tasks scheduled within a frame will take
 * priority over previously scheduled tasks. Furthermore, if any of the tasks in
 * the frame fail, the remainder of the tasks in that frame will be discarded
 * and the failure will be propagated to the user through the callback/task's
 * promised result.
 *
 * Each time a ControlFlow empties its task queue, it will fire an
 * {@link ControlFlow.EventType.IDLE IDLE} event. Conversely, whenever
 * the flow terminates due to an unhandled error, it will remove all
 * remaining tasks in its queue and fire an
 * {@link ControlFlow.EventType.UNCAUGHT_EXCEPTION UNCAUGHT_EXCEPTION} event.
 * If there are no listeners registered with the flow, the error will be
 * rethrown to the global error handler.
 *
 * Refer to the {@link ./promise} module documentation for a detailed
 * explanation of how the ControlFlow coordinates task execution.
 *
 * @implements {Scheduler}
 * @final
 */
class ControlFlow extends events.EventEmitter {
  constructor() {
    if (!usePromiseManager()) {
      throw TypeError(
          'Cannot instantiate control flow when the promise manager has'
              + ' been disabled');
    }

    super();

    /** @private {boolean} */
    this.propagateUnhandledRejections_ = true;

    /** @private {TaskQueue} */
    this.activeQueue_ = null;

    /** @private {Set<TaskQueue>} */
    this.taskQueues_ = null;

    /**
     * Microtask that controls shutting down the control flow. Upon shut down,
     * the flow will emit an
     * {@link ControlFlow.EventType.IDLE} event. Idle events
     * always follow a brief timeout in order to catch latent errors from the
     * last completed task. If this task had a callback registered, but no
     * errback, and the task fails, the unhandled failure would not be reported
     * by the promise system until the next turn of the event loop:
     *
     *   // Schedule 1 task that fails.
     *   var result = promise.controlFlow().execute(
     *       () => promise.rejected('failed'), 'example');
     *   // Set a callback on the result. This delays reporting the unhandled
     *   // failure for 1 turn of the event loop.
     *   result.then(function() {});
     *
     * @private {MicroTask}
     */
    this.shutdownTask_ = null;

    /**
     * ID for a long running interval used to keep a Node.js process running
     * while a control flow's event loop is still working. This is a cheap hack
     * required since JS events are only scheduled to run when there is
     * _actually_ something to run. When a control flow is waiting on a task,
     * there will be nothing in the JS event loop and the process would
     * terminate without this.
     * @private
     */
    this.hold_ = null;
  }

  /**
   * Returns a string representation of this control flow, which is its current
   * {@linkplain #getSchedule() schedule}, sans task stack traces.
   * @return {string} The string representation of this control flow.
   * @override
   */
  toString() {
    return this.getSchedule();
  }

  /**
   * Sets whether any unhandled rejections should propagate up through the
   * control flow stack and cause rejections within parent tasks. If error
   * propagation is disabled, tasks will not be aborted when an unhandled
   * promise rejection is detected, but the rejection _will_ trigger an
   * {@link ControlFlow.EventType.UNCAUGHT_EXCEPTION} event.
   *
   * The default behavior is to propagate all unhandled rejections. _The use
   * of this option is highly discouraged._
   *
   * @param {boolean} propagate whether to propagate errors.
   */
  setPropagateUnhandledRejections(propagate) {
    this.propagateUnhandledRejections_ = propagate;
  }

  /**
   * @return {boolean} Whether this flow is currently idle.
   */
  isIdle() {
    return !this.shutdownTask_ && (!this.taskQueues_ || !this.taskQueues_.size);
  }

  /**
   * Resets this instance, clearing its queue and removing all event listeners.
   */
  reset() {
    this.cancelQueues_(new FlowResetError);
    this.emit(ControlFlow.EventType.RESET);
    this.removeAllListeners();
    this.cancelShutdown_();
  }

  /**
   * Generates an annotated string describing the internal state of this control
   * flow, including the currently executing as well as pending tasks. If
   * {@code opt_includeStackTraces === true}, the string will include the
   * stack trace from when each task was scheduled.
   * @param {string=} opt_includeStackTraces Whether to include the stack traces
   * from when each task was scheduled. Defaults to false.
   * @return {string} String representation of this flow's internal state.
   */
  getSchedule(opt_includeStackTraces) {
    var ret = 'ControlFlow::' + getUid(this);
    var activeQueue = this.activeQueue_;
    if (!this.taskQueues_ || !this.taskQueues_.size) {
      return ret;
    }
    var childIndent = '| ';
    for (var q of this.taskQueues_) {
      ret += '\n' + printQ(q, childIndent);
    }
    return ret;

    function printQ(q, indent) {
      var ret = q.toString();
      if (q === activeQueue) {
        ret = '(active) ' + ret;
      }
      var prefix = indent + childIndent;
      if (q.pending_) {
        if (q.pending_.q.state_ !== TaskQueueState.FINISHED) {
          ret += '\n' + prefix + '(pending) ' + q.pending_.task;
          ret += '\n' + printQ(q.pending_.q, prefix + childIndent);
        } else {
          ret += '\n' + prefix + '(blocked) ' + q.pending_.task;
        }
      }
      if (q.interrupts_) {
        q.interrupts_.forEach((task) => {
          ret += '\n' + prefix + task;
        });
      }
      if (q.tasks_) {
        q.tasks_.forEach((task) => ret += printTask(task, '\n' + prefix));
      }
      return indent + ret;
    }

    function printTask(task, prefix) {
      var ret = prefix + task;
      if (opt_includeStackTraces && task.promise.stack_) {
        ret += prefix + childIndent
            + (task.promise.stack_.stack || task.promise.stack_)
                  .replace(/\n/g, prefix);
      }
      return ret;
    }
  }

  /**
   * Returns the currently active task queue for this flow. If there is no
   * active queue, one will be created.
   * @return {!TaskQueue} the currently active task queue for this flow.
   * @private
   */
  getActiveQueue_() {
    if (this.activeQueue_) {
      return this.activeQueue_;
    }

    this.activeQueue_ = new TaskQueue(this);
    if (!this.taskQueues_) {
      this.taskQueues_ = new Set();
    }
    this.taskQueues_.add(this.activeQueue_);
    this.activeQueue_
        .once('end', this.onQueueEnd_, this)
        .once('error', this.onQueueError_, this);

    asyncRun(() => this.activeQueue_ = null);
    this.activeQueue_.start();
    return this.activeQueue_;
  }

  /** @override */
  execute(fn, opt_description) {
    if (isGenerator(fn)) {
      let original = fn;
      fn = () => consume(original);
    }

    if (!this.hold_) {
      let holdIntervalMs = 2147483647;  // 2^31-1; max timer length for Node.js
      this.hold_ = setInterval(function() {}, holdIntervalMs);
    }

    let task = new Task(
        this, fn, opt_description || '<anonymous>',
        {name: 'Task', top: ControlFlow.prototype.execute},
        true);

    let q = this.getActiveQueue_();

    for (let i = q.tasks_.length; i > 0; i--) {
      let previousTask = q.tasks_[i - 1];
      if (previousTask.userTask_) {
        FLOW_LOG.warning(() => {
          return `Detected scheduling of an unchained task.
When the promise manager is disabled, unchained tasks will not wait for
previously scheduled tasks to finish before starting to execute.
New task: ${task.promise.stack_.stack}
Previous task: ${previousTask.promise.stack_.stack}`.split(/\n/).join('\n    ');
        });
        break;
      }
    }

    q.enqueue(task);
    this.emit(ControlFlow.EventType.SCHEDULE_TASK, task.description);
    return task.promise;
  }

  /** @override */
  promise(resolver) {
    return new ManagedPromise(resolver, this, SKIP_LOG);
  }

  /** @override */
  timeout(ms, opt_description) {
    return this.execute(() => {
      return this.promise(resolve => setTimeout(() => resolve(), ms));
    }, opt_description);
  }

  /** @override */
  wait(condition, opt_timeout, opt_message) {
    return scheduleWait(this, condition, opt_timeout, opt_message);
  }

  /**
   * Executes a function in the next available turn of the JavaScript event
   * loop. This ensures the function runs with its own task queue and any
   * scheduled tasks will run in "parallel" to those scheduled in the current
   * function.
   *
   *     flow.execute(() => console.log('a'));
   *     flow.execute(() => console.log('b'));
   *     flow.execute(() => console.log('c'));
   *     flow.async(() => {
   *        flow.execute(() => console.log('d'));
   *        flow.execute(() => console.log('e'));
   *     });
   *     flow.async(() => {
   *        flow.execute(() => console.log('f'));
   *        flow.execute(() => console.log('g'));
   *     });
   *     flow.once('idle', () => console.log('fin'));
   *     // a
   *     // d
   *     // f
   *     // b
   *     // e
   *     // g
   *     // c
   *     // fin
   *
   * If the function itself throws, the error will be treated the same as an
   * unhandled rejection within the control flow.
   *
   * __NOTE__: This function is considered _unstable_.
   *
   * @param {!Function} fn The function to execute.
   * @param {Object=} opt_self The object in whose context to run the function.
   * @param {...*} var_args Any arguments to pass to the function.
   */
  async(fn, opt_self, var_args) {
    asyncRun(() => {
      // Clear any lingering queues, forces getActiveQueue_ to create a new one.
      this.activeQueue_ = null;
      var q = this.getActiveQueue_();
      try {
        q.execute_(fn.bind(opt_self, var_args));
      } catch (ex) {
        var cancellationError = CancellationError.wrap(ex,
            'Function passed to ControlFlow.async() threw');
        cancellationError.silent_ = true;
        q.abort_(cancellationError);
      } finally {
        this.activeQueue_ = null;
      }
    });
  }

  /**
   * Event handler for when a task queue is exhausted. This starts the shutdown
   * sequence for this instance if there are no remaining task queues: after
   * one turn of the event loop, this object will emit the
   * {@link ControlFlow.EventType.IDLE IDLE} event to signal
   * listeners that it has completed. During this wait, if another task is
   * scheduled, the shutdown will be aborted.
   *
   * @param {!TaskQueue} q the completed task queue.
   * @private
   */
  onQueueEnd_(q) {
    if (!this.taskQueues_) {
      return;
    }
    this.taskQueues_.delete(q);

    vlog(1, () => q + ' has finished');
    vlog(1, () => this.taskQueues_.size + ' queues remain\n' + this, this);

    if (!this.taskQueues_.size) {
      if (this.shutdownTask_) {
        throw Error('Already have a shutdown task??');
      }
      vlog(1, () => 'Scheduling shutdown\n' + this);
      this.shutdownTask_ = new MicroTask(() => this.shutdown_());
    }
  }

  /**
   * Event handler for when a task queue terminates with an error. This triggers
   * the cancellation of all other task queues and a
   * {@link ControlFlow.EventType.UNCAUGHT_EXCEPTION} event.
   * If there are no error event listeners registered with this instance, the
   * error will be rethrown to the global error handler.
   *
   * @param {*} error the error that caused the task queue to terminate.
   * @param {!TaskQueue} q the task queue.
   * @private
   */
  onQueueError_(error, q) {
    if (this.taskQueues_) {
      this.taskQueues_.delete(q);
    }
    this.cancelQueues_(CancellationError.wrap(
        error, 'There was an uncaught error in the control flow'));
    this.cancelShutdown_();
    this.cancelHold_();

    setTimeout(() => {
      let listeners = this.listeners(ControlFlow.EventType.UNCAUGHT_EXCEPTION);
      if (!listeners.size) {
        throw error;
      } else {
        this.reportUncaughtException_(error);
      }
    }, 0);
  }

  /**
   * Cancels all remaining task queues.
   * @param {!CancellationError} reason The cancellation reason.
   * @private
   */
  cancelQueues_(reason) {
    reason.silent_ = true;
    if (this.taskQueues_) {
      for (var q of this.taskQueues_) {
        q.removeAllListeners();
        q.abort_(reason);
      }
      this.taskQueues_.clear();
      this.taskQueues_ = null;
    }
  }

  /**
   * Reports an uncaught exception using a
   * {@link ControlFlow.EventType.UNCAUGHT_EXCEPTION} event.
   *
   * @param {*} e the error to report.
   * @private
   */
  reportUncaughtException_(e) {
    this.emit(ControlFlow.EventType.UNCAUGHT_EXCEPTION, e);
  }

  /** @private */
  cancelHold_() {
    if (this.hold_) {
      clearInterval(this.hold_);
      this.hold_ = null;
    }
  }

  /** @private */
  shutdown_() {
    vlog(1, () => 'Going idle: ' + this);
    this.cancelHold_();
    this.shutdownTask_ = null;
    this.emit(ControlFlow.EventType.IDLE);
  }

  /**
   * Cancels the shutdown sequence if it is currently scheduled.
   * @private
   */
  cancelShutdown_() {
    if (this.shutdownTask_) {
      this.shutdownTask_.cancel();
      this.shutdownTask_ = null;
    }
  }
}


/**
 * Events that may be emitted by an {@link ControlFlow}.
 * @enum {string}
 */
ControlFlow.EventType = {

  /** Emitted when all tasks have been successfully executed. */
  IDLE: 'idle',

  /** Emitted when a ControlFlow has been reset. */
  RESET: 'reset',

  /** Emitted whenever a new task has been scheduled. */
  SCHEDULE_TASK: 'scheduleTask',

  /**
   * Emitted whenever a control flow aborts due to an unhandled promise
   * rejection. This event will be emitted along with the offending rejection
   * reason. Upon emitting this event, the control flow will empty its task
   * queue and revert to its initial state.
   */
  UNCAUGHT_EXCEPTION: 'uncaughtException'
};


/**
 * Wraps a function to execute as a cancellable micro task.
 * @final
 */
class MicroTask {
  /**
   * @param {function()} fn The function to run as a micro task.
   */
  constructor(fn) {
    /** @private {boolean} */
    this.cancelled_ = false;
    asyncRun(() => {
      if (!this.cancelled_) {
        fn();
      }
    });
  }

  /**
   * Runs the given function after a microtask yield.
   * @param {function()} fn The function to run.
   */
  static run(fn) {
    NativePromise.resolve().then(function() {
      try {
        fn();
      } catch (ignored) {
        // Do nothing.
      }
    });
  }

  /**
   * Cancels the execution of this task. Note: this will not prevent the task
   * timer from firing, just the invocation of the wrapped function.
   */
  cancel() {
    this.cancelled_ = true;
  }
}


/**
 * A task to be executed by a {@link ControlFlow}.
 *
 * @template T
 * @final
 */
class Task extends Deferred {
  /**
   * @param {!ControlFlow} flow The flow this instances belongs
   *     to.
   * @param {function(): (T|!ManagedPromise<T>)} fn The function to
   *     call when the task executes. If it returns a
   *     {@link ManagedPromise}, the flow will wait for it to be
   *     resolved before starting the next task.
   * @param {string} description A description of the task for debugging.
   * @param {{name: string, top: !Function}=} opt_stackOptions Options to use
   *     when capturing the stacktrace for when this task was created.
   * @param {boolean=} opt_isUserTask Whether this task was explicitly scheduled
   *     by the use of the promise manager.
   */
  constructor(flow, fn, description, opt_stackOptions, opt_isUserTask) {
    super(flow, SKIP_LOG);
    getUid(this);

    /** @type {function(): (T|!ManagedPromise<T>)} */
    this.execute = fn;

    /** @type {string} */
    this.description = description;

    /** @type {TaskQueue} */
    this.queue = null;

    /** @private @const {boolean} */
    this.userTask_ = !!opt_isUserTask;

    /**
     * Whether this task is considered block. A blocked task may be registered
     * in a task queue, but will be dropped if it is still blocked when it
     * reaches the front of the queue. A dropped task may always be rescheduled.
     *
     * Blocked tasks are used when a callback is attached to an unsettled
     * promise to reserve a spot in line (in a manner of speaking). If the
     * promise is not settled before the callback reaches the front of the
     * of the queue, it will be dropped. Once the promise is settled, the
     * dropped task will be rescheduled as an interrupt on the currently task
     * queue.
     *
     * @type {boolean}
     */
    this.blocked = false;

    if (opt_stackOptions) {
      this.promise.stack_ = captureStackTrace(
          opt_stackOptions.name, this.description, opt_stackOptions.top);
    }
  }

  /** @override */
  toString() {
    return 'Task::' + getUid(this) + '<' + this.description + '>';
  }
}


/** @enum {string} */
const TaskQueueState = {
  NEW: 'new',
  STARTED: 'started',
  FINISHED: 'finished'
};


/**
 * @final
 */
class TaskQueue extends events.EventEmitter {
  /** @param {!ControlFlow} flow . */
  constructor(flow) {
    super();

    /** @private {string} */
    this.name_ = 'TaskQueue::' + getUid(this);

    /** @private {!ControlFlow} */
    this.flow_ = flow;

    /** @private {!Array<!Task>} */
    this.tasks_ = [];

    /** @private {Array<!Task>} */
    this.interrupts_ = null;

    /** @private {({task: !Task, q: !TaskQueue}|null)} */
    this.pending_ = null;

    /** @private {TaskQueue} */
    this.subQ_ = null;

    /** @private {TaskQueueState} */
    this.state_ = TaskQueueState.NEW;

    /** @private {!Set<!ManagedPromise>} */
    this.unhandledRejections_ = new Set();
  }

  /** @override */
  toString() {
    return 'TaskQueue::' + getUid(this);
  }

  /**
   * @param {!ManagedPromise} promise .
   */
  addUnhandledRejection(promise) {
    // TODO: node 4.0.0+
    vlog(2, () => this + ' registering unhandled rejection: ' + promise, this);
    this.unhandledRejections_.add(promise);
  }

  /**
   * @param {!ManagedPromise} promise .
   */
  clearUnhandledRejection(promise) {
    var deleted = this.unhandledRejections_.delete(promise);
    if (deleted) {
      // TODO: node 4.0.0+
      vlog(2, () => this + ' clearing unhandled rejection: ' + promise, this);
    }
  }

  /**
   * Enqueues a new task for execution.
   * @param {!Task} task The task to enqueue.
   * @throws {Error} If this instance has already started execution.
   */
  enqueue(task) {
    if (this.state_ !== TaskQueueState.NEW) {
      throw Error('TaskQueue has started: ' + this);
    }

    if (task.queue) {
      throw Error('Task is already scheduled in another queue');
    }

    this.tasks_.push(task);
    task.queue = this;
    ON_CANCEL_HANDLER.set(
        task.promise,
        (e) => this.onTaskCancelled_(task, e));

    vlog(1, () => this + '.enqueue(' + task + ')', this);
    vlog(2, () => this.flow_.toString(), this);
  }

  /**
   * Schedules the callbacks registered on the given promise in this queue.
   *
   * @param {!ManagedPromise} promise the promise whose callbacks should be
   *     registered as interrupts in this task queue.
   * @throws {Error} if this queue has already finished.
   */
  scheduleCallbacks(promise) {
    if (this.state_ === TaskQueueState.FINISHED) {
      throw new Error('cannot interrupt a finished q(' + this + ')');
    }

    if (this.pending_ && this.pending_.task.promise === promise) {
      this.pending_.task.promise.queue_ = null;
      this.pending_ = null;
      asyncRun(() => this.executeNext_());
    }

    if (!promise.callbacks_) {
      return;
    }
    promise.callbacks_.forEach(function(cb) {
      cb.blocked = false;
      if (cb.queue) {
        return;
      }

      ON_CANCEL_HANDLER.set(
          cb.promise,
          (e) => this.onTaskCancelled_(cb, e));

      if (cb.queue === this && this.tasks_.indexOf(cb) !== -1) {
        return;
      }

      if (cb.queue) {
        cb.queue.dropTask_(cb);
      }

      cb.queue = this;
      if (!this.interrupts_) {
        this.interrupts_ = [];
      }
      this.interrupts_.push(cb);
    }, this);
    promise.callbacks_ = null;
    vlog(2, () => this + ' interrupted\n' + this.flow_, this);
  }

  /**
   * Starts executing tasks in this queue. Once called, no further tasks may
   * be {@linkplain #enqueue() enqueued} with this instance.
   *
   * @throws {Error} if this queue has already been started.
   */
  start() {
    if (this.state_ !== TaskQueueState.NEW) {
      throw new Error('TaskQueue has already started');
    }
    // Always asynchronously execute next, even if there doesn't look like
    // there is anything in the queue. This will catch pending unhandled
    // rejections that were registered before start was called.
    asyncRun(() => this.executeNext_());
  }

  /**
   * Aborts this task queue. If there are any scheduled tasks, they are silently
   * cancelled and discarded (their callbacks will never fire). If this queue
   * has a _pending_ task, the abortion error is used to cancel that task.
   * Otherwise, this queue will emit an error event.
   *
   * @param {*} error The abortion reason.
   * @private
   */
  abort_(error) {
    var cancellation;

    if (error instanceof FlowResetError) {
      cancellation = error;
    } else {
      cancellation = new DiscardedTaskError(error);
    }

    if (this.interrupts_ && this.interrupts_.length) {
      this.interrupts_.forEach((t) => t.reject(cancellation));
      this.interrupts_ = [];
    }

    if (this.tasks_ && this.tasks_.length) {
      this.tasks_.forEach((t) => t.reject(cancellation));
      this.tasks_ = [];
    }

    // Now that all of the remaining tasks have been silently cancelled (e.g. no
    // existing callbacks on those tasks will fire), clear the silence bit on
    // the cancellation error. This ensures additional callbacks registered in
    // the future will actually execute.
    cancellation.silent_ = false;

    if (this.pending_) {
      vlog(2, () => this + '.abort(); cancelling pending task', this);
      this.pending_.task.promise.cancel(
          /** @type {!CancellationError} */(error));

    } else {
      vlog(2, () => this + '.abort(); emitting error event', this);
      this.emit('error', error, this);
    }
  }

  /** @private */
  executeNext_() {
    if (this.state_ === TaskQueueState.FINISHED) {
      return;
    }
    this.state_ = TaskQueueState.STARTED;

    if (this.pending_ !== null || this.processUnhandledRejections_()) {
      return;
    }

    var task;
    do {
      task = this.getNextTask_();
    } while (task && !isPending(task.promise));

    if (!task) {
      this.state_ = TaskQueueState.FINISHED;
      this.tasks_ = [];
      this.interrupts_ = null;
      vlog(2, () => this + '.emit(end)', this);
      this.emit('end', this);
      return;
    }

    let result = undefined;
    this.subQ_ = new TaskQueue(this.flow_);

    this.subQ_.once('end', () => {  // On task completion.
      this.subQ_ = null;
      this.pending_ && this.pending_.task.resolve(result);
    });

    this.subQ_.once('error', e => {  // On task failure.
      this.subQ_ = null;
      if (Thenable.isImplementation(result)) {
        result.cancel(CancellationError.wrap(e));
      }
      this.pending_ && this.pending_.task.reject(e);
    });
    vlog(2, () => `${this} created ${this.subQ_} for ${task}`);

    try {
      this.pending_ = {task: task, q: this.subQ_};
      task.promise.queue_ = this;
      result = this.subQ_.execute_(task.execute);
      this.subQ_.start();
    } catch (ex) {
      this.subQ_.abort_(ex);
    }
  }

  /**
   * @param {!Function} fn .
   * @return {T} .
   * @template T
   * @private
   */
  execute_(fn) {
    try {
      activeFlows.push(this.flow_);
      this.flow_.activeQueue_ = this;
      return fn();
    } finally {
      this.flow_.activeQueue_ = null;
      activeFlows.pop();
    }
  }

  /**
   * Process any unhandled rejections registered with this task queue. If there
   * is a rejection, this queue will be aborted with the rejection error. If
   * there are multiple rejections registered, this queue will be aborted with
   * a {@link MultipleUnhandledRejectionError}.
   * @return {boolean} whether there was an unhandled rejection.
   * @private
   */
  processUnhandledRejections_() {
    if (!this.unhandledRejections_.size) {
      return false;
    }

    var errors = new Set();
    for (var rejection of this.unhandledRejections_) {
      errors.add(rejection.value_);
    }
    this.unhandledRejections_.clear();

    var errorToReport = errors.size === 1
        ? errors.values().next().value
        : new MultipleUnhandledRejectionError(errors);

    vlog(1, () => this + ' aborting due to unhandled rejections', this);
    if (this.flow_.propagateUnhandledRejections_) {
      this.abort_(errorToReport);
      return true;
    } else {
      vlog(1, 'error propagation disabled; reporting to control flow');
      this.flow_.reportUncaughtException_(errorToReport);
      return false;
    }
  }

  /**
   * @param {!Task} task The task to drop.
   * @private
   */
  dropTask_(task) {
    var index;
    if (this.interrupts_) {
      index = this.interrupts_.indexOf(task);
      if (index != -1) {
        task.queue = null;
        this.interrupts_.splice(index, 1);
        return;
      }
    }

    index = this.tasks_.indexOf(task);
    if (index != -1) {
      task.queue = null;
      this.tasks_.splice(index, 1);
    }
  }

  /**
   * @param {!Task} task The task that was cancelled.
   * @param {!CancellationError} reason The cancellation reason.
   * @private
   */
  onTaskCancelled_(task, reason) {
    if (this.pending_ && this.pending_.task === task) {
      this.pending_.q.abort_(reason);
    } else {
      this.dropTask_(task);
    }
  }

  /**
   * @return {(Task|undefined)} the next task scheduled within this queue,
   *     if any.
   * @private
   */
  getNextTask_() {
    var task = undefined;
    while (true) {
      if (this.interrupts_) {
        task = this.interrupts_.shift();
      }
      if (!task && this.tasks_) {
        task = this.tasks_.shift();
      }
      if (task && task.blocked) {
        vlog(2, () => this + ' skipping blocked task ' + task, this);
        task.queue = null;
        task = null;
        // TODO: recurse when tail-call optimization is available in node.
      } else {
        break;
      }
    }
    return task;
  }
}



/**
 * The default flow to use if no others are active.
 * @type {ControlFlow}
 */
var defaultFlow;


/**
 * A stack of active control flows, with the top of the stack used to schedule
 * commands. When there are multiple flows on the stack, the flow at index N
 * represents a callback triggered within a task owned by the flow at index
 * N-1.
 * @type {!Array<!ControlFlow>}
 */
var activeFlows = [];


/**
 * Changes the default flow to use when no others are active.
 * @param {!ControlFlow} flow The new default flow.
 * @throws {Error} If the default flow is not currently active.
 */
function setDefaultFlow(flow) {
  if (!usePromiseManager()) {
    throw Error(
        'You  may not change set the control flow when the promise'
            +' manager is disabled');
  }
  if (activeFlows.length) {
    throw Error('You may only change the default flow while it is active');
  }
  defaultFlow = flow;
}


/**
 * @return {!ControlFlow} The currently active control flow.
 * @suppress {checkTypes}
 */
function controlFlow() {
  if (!usePromiseManager()) {
    return SIMPLE_SCHEDULER;
  }

  if (activeFlows.length) {
    return activeFlows[activeFlows.length - 1];
  }

  if (!defaultFlow) {
    defaultFlow = new ControlFlow;
  }
  return defaultFlow;
}


/**
 * Creates a new control flow. The provided callback will be invoked as the
 * first task within the new flow, with the flow as its sole argument. Returns
 * a promise that resolves to the callback result.
 * @param {function(!ControlFlow)} callback The entry point
 *     to the newly created flow.
 * @return {!Thenable} A promise that resolves to the callback result.
 */
function createFlow(callback) {
  var flow = new ControlFlow;
  return flow.execute(function() {
    return callback(flow);
  });
}


/**
 * Tests is a function is a generator.
 * @param {!Function} fn The function to test.
 * @return {boolean} Whether the function is a generator.
 */
function isGenerator(fn) {
  return fn.constructor.name === 'GeneratorFunction';
}


/**
 * Consumes a {@code GeneratorFunction}. Each time the generator yields a
 * promise, this function will wait for it to be fulfilled before feeding the
 * fulfilled value back into {@code next}. Likewise, if a yielded promise is
 * rejected, the rejection error will be passed to {@code throw}.
 *
 * __Example 1:__ the Fibonacci Sequence.
 *
 *     promise.consume(function* fibonacci() {
 *       var n1 = 1, n2 = 1;
 *       for (var i = 0; i < 4; ++i) {
 *         var tmp = yield n1 + n2;
 *         n1 = n2;
 *         n2 = tmp;
 *       }
 *       return n1 + n2;
 *     }).then(function(result) {
 *       console.log(result);  // 13
 *     });
 *
 * __Example 2:__ a generator that throws.
 *
 *     promise.consume(function* () {
 *       yield promise.delayed(250).then(function() {
 *         throw Error('boom');
 *       });
 *     }).catch(function(e) {
 *       console.log(e.toString());  // Error: boom
 *     });
 *
 * @param {!Function} generatorFn The generator function to execute.
 * @param {Object=} opt_self The object to use as "this" when invoking the
 *     initial generator.
 * @param {...*} var_args Any arguments to pass to the initial generator.
 * @return {!Thenable<?>} A promise that will resolve to the
 *     generator's final result.
 * @throws {TypeError} If the given function is not a generator.
 */
function consume(generatorFn, opt_self, ...var_args) {
  if (!isGenerator(generatorFn)) {
    throw new TypeError('Input is not a GeneratorFunction: ' +
        generatorFn.constructor.name);
  }

  let ret;
  return ret = createPromise((resolve, reject) => {
    let generator = generatorFn.apply(opt_self, var_args);
    callNext();

    /** @param {*=} opt_value . */
    function callNext(opt_value) {
      pump(generator.next, opt_value);
    }

    /** @param {*=} opt_error . */
    function callThrow(opt_error) {
      pump(generator.throw, opt_error);
    }

    function pump(fn, opt_arg) {
      if (ret instanceof ManagedPromise && !isPending(ret)) {
        return;  // Deferred was cancelled; silently abort.
      }

      try {
        var result = fn.call(generator, opt_arg);
      } catch (ex) {
        reject(ex);
        return;
      }

      if (result.done) {
        resolve(result.value);
        return;
      }

      asap(result.value, callNext, callThrow);
    }
  });
}


// PUBLIC API


module.exports = {
  CancellableThenable: CancellableThenable,
  CancellationError: CancellationError,
  ControlFlow: ControlFlow,
  Deferred: Deferred,
  MultipleUnhandledRejectionError: MultipleUnhandledRejectionError,
  Thenable: Thenable,
  Promise: ManagedPromise,
  Resolver: Resolver,
  Scheduler: Scheduler,
  all: all,
  asap: asap,
  captureStackTrace: captureStackTrace,
  checkedNodeCall: checkedNodeCall,
  consume: consume,
  controlFlow: controlFlow,
  createFlow: createFlow,
  createPromise: createPromise,
  defer: defer,
  delayed: delayed,
  filter: filter,
  finally: thenFinally,
  fulfilled: fulfilled,
  fullyResolved: fullyResolved,
  isGenerator: isGenerator,
  isPromise: isPromise,
  map: map,
  rejected: rejected,
  setDefaultFlow: setDefaultFlow,
  when: when,

  /**
   * Indicates whether the promise manager is currently enabled. When disabled,
   * attempting to use the {@link ControlFlow} or {@link ManagedPromise Promise}
   * classes will generate an error.
   *
   * The promise manager is currently enabled by default, but may be disabled
   * by setting the environment variable `SELENIUM_PROMISE_MANAGER=0` or by
   * setting this property to false. Setting this property will always take
   * precedence over the use of the environment variable.
   *
   * @return {boolean} Whether the promise manager is enabled.
   * @see <https://github.com/SeleniumHQ/selenium/issues/2969>
   */
  get USE_PROMISE_MANAGER() { return usePromiseManager(); },
  set USE_PROMISE_MANAGER(/** boolean */value) { USE_PROMISE_MANAGER = value; },

  get LONG_STACK_TRACES() { return LONG_STACK_TRACES; },
  set LONG_STACK_TRACES(v) { LONG_STACK_TRACES = v; },
};


/***/ }),
/* 57 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Licensed to the Software Freedom Conservancy (SFC) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The SFC licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.



const {Capabilities} = __webpack_require__(58);


/**
 * Contains information about a single WebDriver session.
 */
class Session {

  /**
   * @param {string} id The session ID.
   * @param {!(Object|Capabilities)} capabilities The session
   *     capabilities.
   */
  constructor(id, capabilities) {
    /** @private {string} */
    this.id_ = id;

    /** @private {!Capabilities} */
    this.caps_ = capabilities instanceof Capabilities
        ? /** @type {!Capabilities} */(capabilities)
        : new Capabilities(capabilities);
  }

  /**
   * @return {string} This session's ID.
   */
  getId() {
    return this.id_;
  }

  /**
   * @return {!Capabilities} This session's capabilities.
   */
  getCapabilities() {
    return this.caps_;
  }

  /**
   * Retrieves the value of a specific capability.
   * @param {string} key The capability to retrieve.
   * @return {*} The capability value.
   */
  getCapability(key) {
    return this.caps_.get(key);
  }

  /**
   * Returns the JSON representation of this object, which is just the string
   * session ID.
   * @return {string} The JSON representation of this Session.
   */
  toJSON() {
    return this.getId();
  }
}


// PUBLIC API


module.exports = {Session: Session};


/***/ }),
/* 58 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Licensed to the Software Freedom Conservancy (SFC) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The SFC licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.



/**
 * @fileoverview Defines types related to describing the capabilities of a
 * WebDriver session.
 */

const Symbols = __webpack_require__(59);


/**
 * Recognized browser names.
 * @enum {string}
 */
const Browser = {
  ANDROID: 'android',
  CHROME: 'chrome',
  EDGE: 'MicrosoftEdge',
  FIREFOX: 'firefox',
  IE: 'internet explorer',
  INTERNET_EXPLORER: 'internet explorer',
  IPAD: 'iPad',
  IPHONE: 'iPhone',
  OPERA: 'opera',
  PHANTOM_JS: 'phantomjs',
  SAFARI: 'safari',
  HTMLUNIT: 'htmlunit'
};


/**
 * Common Capability keys.
 * @enum {string}
 */
const Capability = {

  /**
   * Indicates whether a driver should accept all SSL certs by default. This
   * capability only applies when requesting a new session. To query whether
   * a driver can handle insecure SSL certs, see {@link #SECURE_SSL}.
   */
  ACCEPT_SSL_CERTS: 'acceptSslCerts',


  /**
   * The browser name. Common browser names are defined in the {@link Browser}
   * enum.
   */
  BROWSER_NAME: 'browserName',

  /**
   * Defines how elements should be scrolled into the viewport for interaction.
   * This capability will be set to zero (0) if elements are aligned with the
   * top of the viewport, or one (1) if aligned with the bottom. The default
   * behavior is to align with the top of the viewport.
   */
  ELEMENT_SCROLL_BEHAVIOR: 'elementScrollBehavior',

  /**
   * Whether the driver is capable of handling modal alerts (e.g. alert,
   * confirm, prompt). To define how a driver <i>should</i> handle alerts,
   * use {@link #UNEXPECTED_ALERT_BEHAVIOR}.
   */
  HANDLES_ALERTS: 'handlesAlerts',

  /**
   * Key for the logging driver logging preferences.
   */
  LOGGING_PREFS: 'loggingPrefs',

  /**
   * Whether this session generates native events when simulating user input.
   */
  NATIVE_EVENTS: 'nativeEvents',

  /**
   * Describes the platform the browser is running on. Will be one of
   * ANDROID, IOS, LINUX, MAC, UNIX, or WINDOWS. When <i>requesting</i> a
   * session, ANY may be used to indicate no platform preference (this is
   * semantically equivalent to omitting the platform capability).
   */
  PLATFORM: 'platform',

  /**
   * Describes the proxy configuration to use for a new WebDriver session.
   */
  PROXY: 'proxy',

  /** Whether the driver supports changing the browser's orientation. */
  ROTATABLE: 'rotatable',

  /**
   * Whether a driver is only capable of handling secure SSL certs. To request
   * that a driver accept insecure SSL certs by default, use
   * {@link #ACCEPT_SSL_CERTS}.
   */
  SECURE_SSL: 'secureSsl',

  /** Whether the driver supports manipulating the app cache. */
  SUPPORTS_APPLICATION_CACHE: 'applicationCacheEnabled',

  /** Whether the driver supports locating elements with CSS selectors. */
  SUPPORTS_CSS_SELECTORS: 'cssSelectorsEnabled',

  /** Whether the browser supports JavaScript. */
  SUPPORTS_JAVASCRIPT: 'javascriptEnabled',

  /** Whether the driver supports controlling the browser's location info. */
  SUPPORTS_LOCATION_CONTEXT: 'locationContextEnabled',

  /** Whether the driver supports taking screenshots. */
  TAKES_SCREENSHOT: 'takesScreenshot',

  /**
   * Defines how the driver should handle unexpected alerts. The value should
   * be one of "accept", "dismiss", or "ignore".
   */
  UNEXPECTED_ALERT_BEHAVIOR: 'unexpectedAlertBehaviour',

  /** Defines the browser version. */
  VERSION: 'version'
};


/**
 * Describes how a proxy should be configured for a WebDriver session.
 * @record
 */
function ProxyConfig() {}

/**
 * The proxy type. Must be one of {"manual", "pac", "system"}.
 * @type {string}
 */
ProxyConfig.prototype.proxyType;

/**
 * URL for the PAC file to use. Only used if {@link #proxyType} is "pac".
 * @type {(string|undefined)}
 */
ProxyConfig.prototype.proxyAutoconfigUrl;

/**
 * The proxy host for FTP requests. Only used if {@link #proxyType} is "manual".
 * @type {(string|undefined)}
 */
ProxyConfig.prototype.ftpProxy;

/**
 * The proxy host for HTTP requests. Only used if {@link #proxyType} is
 * "manual".
 * @type {(string|undefined)}
 */
ProxyConfig.prototype.httpProxy;

/**
 * The proxy host for HTTPS requests. Only used if {@link #proxyType} is
 * "manual".
 * @type {(string|undefined)}
 */
ProxyConfig.prototype.sslProxy;

/**
 * A comma delimited list of hosts which should bypass all proxies. Only used if
 * {@link #proxyType} is "manual".
 * @type {(string|undefined)}
 */
ProxyConfig.prototype.noProxy;


/**
 * Converts a generic hash object to a map.
 * @param {!Object<string, ?>} hash The hash object.
 * @return {!Map<string, ?>} The converted map.
 */
function toMap(hash) {
  let m = new Map;
  for (let key in hash) {
    if (hash.hasOwnProperty(key)) {
      m.set(key, hash[key]);
    }
  }
  return m;
}


/**
 * Describes a set of capabilities for a WebDriver session.
 */
class Capabilities {
  /**
   * @param {(Capabilities|Map<string, ?>|Object)=} other Another set of
   *     capabilities to initialize this instance from.
   */
  constructor(other = undefined) {
    if (other instanceof Capabilities) {
      other = other.map_;
    } else if (other && !(other instanceof Map)) {
      other = toMap(other);
    }
    /** @private @const {!Map<string, ?>} */
    this.map_ = new Map(other);
  }

  /**
   * @return {!Capabilities} A basic set of capabilities for Android.
   */
  static android() {
    return new Capabilities()
        .set(Capability.BROWSER_NAME, Browser.ANDROID);
  }

  /**
   * @return {!Capabilities} A basic set of capabilities for Chrome.
   */
  static chrome() {
    return new Capabilities().set(Capability.BROWSER_NAME, Browser.CHROME);
  }

  /**
   * @return {!Capabilities} A basic set of capabilities for Microsoft Edge.
   */
  static edge() {
    return new Capabilities()
        .set(Capability.BROWSER_NAME, Browser.EDGE);
  }

  /**
   * @return {!Capabilities} A basic set of capabilities for Firefox.
   */
  static firefox() {
    return new Capabilities().set(Capability.BROWSER_NAME, Browser.FIREFOX);
  }

  /**
   * @return {!Capabilities} A basic set of capabilities for Internet Explorer.
   */
  static ie() {
    return new Capabilities().
        set(Capability.BROWSER_NAME, Browser.INTERNET_EXPLORER);
  }

  /**
   * @return {!Capabilities} A basic set of capabilities for iPad.
   */
  static ipad() {
    return new Capabilities().
        set(Capability.BROWSER_NAME, Browser.IPAD);
  }

  /**
   * @return {!Capabilities} A basic set of capabilities for iPhone.
   */
  static iphone() {
    return new Capabilities().
        set(Capability.BROWSER_NAME, Browser.IPHONE);
  }

  /**
   * @return {!Capabilities} A basic set of capabilities for Opera.
   */
  static opera() {
    return new Capabilities().
        set(Capability.BROWSER_NAME, Browser.OPERA);
  }

  /**
   * @return {!Capabilities} A basic set of capabilities for PhantomJS.
   */
  static phantomjs() {
    return new Capabilities().
        set(Capability.BROWSER_NAME, Browser.PHANTOM_JS);
  }

  /**
   * @return {!Capabilities} A basic set of capabilities for Safari.
   */
  static safari() {
    return new Capabilities().
        set(Capability.BROWSER_NAME, Browser.SAFARI);
  }

  /**
   * @return {!Capabilities} A basic set of capabilities for HTMLUnit.
   */
  static htmlunit() {
    return new Capabilities().
        set(Capability.BROWSER_NAME, Browser.HTMLUNIT);
  }

  /**
   * @return {!Capabilities} A basic set of capabilities for HTMLUnit
   *     with enabled Javascript.
   */
  static htmlunitwithjs() {
    return new Capabilities().
        set(Capability.BROWSER_NAME, Browser.HTMLUNIT).
        set(Capability.SUPPORTS_JAVASCRIPT, true);
  }

  /**
   * @return {!Object<string, ?>} The JSON representation of this instance.
   *     Note, the returned object may contain nested promised values.
   * @suppress {checkTypes} Suppress [] access on a struct (state inherited from
   *     Map).
   */
  [Symbols.serialize]() {
    return serialize(this);
  }

  /**
   * @param {string} key the parameter key to get.
   * @return {T} the stored parameter value.
   * @template T
   */
  get(key) {
    return this.map_.get(key);
  }

  /**
   * @param {string} key the key to test.
   * @return {boolean} whether this capability set has the specified key.
   */
  has(key) {
    return this.map_.has(key);
  }

  /**
   * @return {!Iterator<string>} an iterator of the keys set.
   */
  keys() {
    return this.map_.keys();
  }

  /** @return {number} The number of capabilities set. */
  get size() {
    return this.map_.size;
  }

  /**
   * Merges another set of capabilities into this instance.
   * @param {!(Capabilities|Map<String, ?>|Object<string, ?>)} other The other
   *     set of capabilities to merge.
   * @return {!Capabilities} A self reference.
   */
  merge(other) {
    if (!other) {
      throw new TypeError('no capabilities provided for merge');
    }

    let map;
    if (other instanceof Capabilities) {
      map = other.map_;
    } else if (other instanceof Map) {
      map = other;
    } else {
      other = toMap(other);
    }

    for (let key of other.keys()) {
      this.set(key, other.get(key));
    }

    return this;
  }

  /**
   * Deletes an entry from this set of capabilities.
   *
   * @param {string} key the capability key to delete.
   */
  delete(key) {
    this.map_.delete(key);
  }

  /**
   * @param {string} key The capability key.
   * @param {*} value The capability value.
   * @return {!Capabilities} A self reference.
   * @throws {TypeError} If the `key` is not a string.
   */
  set(key, value) {
    if (typeof key !== 'string') {
      throw new TypeError('Capability keys must be strings: ' + typeof key);
    }
    this.map_.set(key, value);
    return this;
  }

  /**
   * Sets the logging preferences. Preferences may be specified as a
   * {@link ./logging.Preferences} instance, or as a map of log-type to
   * log-level.
   * @param {!(./logging.Preferences|Object<string>)} prefs The logging
   *     preferences.
   * @return {!Capabilities} A self reference.
   */
  setLoggingPrefs(prefs) {
    return this.set(Capability.LOGGING_PREFS, prefs);
  }

  /**
   * Sets the proxy configuration for this instance.
   * @param {ProxyConfig} proxy The desired proxy configuration.
   * @return {!Capabilities} A self reference.
   */
  setProxy(proxy) {
    return this.set(Capability.PROXY, proxy);
  }

  /**
   * Sets whether native events should be used.
   * @param {boolean} enabled Whether to enable native events.
   * @return {!Capabilities} A self reference.
   */
  setEnableNativeEvents(enabled) {
    return this.set(Capability.NATIVE_EVENTS, enabled);
  }

  /**
   * Sets how elements should be scrolled into view for interaction.
   * @param {number} behavior The desired scroll behavior: either 0 to align
   *     with the top of the viewport or 1 to align with the bottom.
   * @return {!Capabilities} A self reference.
   */
  setScrollBehavior(behavior) {
    return this.set(Capability.ELEMENT_SCROLL_BEHAVIOR, behavior);
  }

  /**
   * Sets the default action to take with an unexpected alert before returning
   * an error.
   * @param {string} behavior The desired behavior should be "accept",
   *     "dismiss", or "ignore". Defaults to "dismiss".
   * @return {!Capabilities} A self reference.
   */
  setAlertBehavior(behavior) {
    return this.set(Capability.UNEXPECTED_ALERT_BEHAVIOR, behavior);
  }
}


/**
 * Serializes a capabilities object. This is defined as a standalone function
 * so it may be type checked (where Capabilities[Symbols.serialize] has type
 * checking disabled since it is defined with [] access on a struct).
 *
 * @param {!Capabilities} caps The capabilities to serialize.
 * @return {!Object<string, ?>} The JSON representation of this instance.
 *     Note, the returned object may contain nested promised values.
 */
function serialize(caps) {
  let ret = {};
  for (let key of caps.keys()) {
    let cap = caps.get(key);
    if (cap !== undefined && cap !== null) {
      ret[key] = cap;
    }
  }
  return ret;
}


// PUBLIC API


module.exports = {
  Browser: Browser,
  Capabilities: Capabilities,
  Capability: Capability,
  ProxyConfig: ProxyConfig
};


/***/ }),
/* 59 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Licensed to the Software Freedom Conservancy (SFC) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The SFC licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.



/**
 * @fileoverview Defines well-known symbols used within the selenium-webdriver
 * library.
 */


module.exports = {
  /**
   * The serialize symbol specifies a method that returns an object's serialized
   * representation. If an object's serialized form is not immediately
   * available, the serialize method will return a promise that will be resolved
   * with the serialized form.
   *
   * Note that the described method is analogous to objects that define a
   * `toJSON()` method, except the serialized result may be a promise, or
   * another object with a promised property.
   */
  serialize: Symbol('serialize')
};


/***/ }),
/* 60 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Licensed to the Software Freedom Conservancy (SFC) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The SFC licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.



/**
 * @fileoverview Defines types related to user input with the WebDriver API.
 */


/**
 * Enumeration of the buttons used in the advanced interactions API.
 * @enum {number}
 */
const Button = {
  LEFT: 0,
  MIDDLE: 1,
  RIGHT: 2
};



/**
 * Representations of pressable keys that aren't text.  These are stored in
 * the Unicode PUA (Private Use Area) code points, 0xE000-0xF8FF.  Refer to
 * http://www.google.com.au/search?&q=unicode+pua&btnG=Search
 *
 * @enum {string}
 */
const Key = {
  NULL:         '\uE000',
  CANCEL:       '\uE001',  // ^break
  HELP:         '\uE002',
  BACK_SPACE:   '\uE003',
  TAB:          '\uE004',
  CLEAR:        '\uE005',
  RETURN:       '\uE006',
  ENTER:        '\uE007',
  SHIFT:        '\uE008',
  CONTROL:      '\uE009',
  ALT:          '\uE00A',
  PAUSE:        '\uE00B',
  ESCAPE:       '\uE00C',
  SPACE:        '\uE00D',
  PAGE_UP:      '\uE00E',
  PAGE_DOWN:    '\uE00F',
  END:          '\uE010',
  HOME:         '\uE011',
  ARROW_LEFT:   '\uE012',
  LEFT:         '\uE012',
  ARROW_UP:     '\uE013',
  UP:           '\uE013',
  ARROW_RIGHT:  '\uE014',
  RIGHT:        '\uE014',
  ARROW_DOWN:   '\uE015',
  DOWN:         '\uE015',
  INSERT:       '\uE016',
  DELETE:       '\uE017',
  SEMICOLON:    '\uE018',
  EQUALS:       '\uE019',

  NUMPAD0:      '\uE01A',  // number pad keys
  NUMPAD1:      '\uE01B',
  NUMPAD2:      '\uE01C',
  NUMPAD3:      '\uE01D',
  NUMPAD4:      '\uE01E',
  NUMPAD5:      '\uE01F',
  NUMPAD6:      '\uE020',
  NUMPAD7:      '\uE021',
  NUMPAD8:      '\uE022',
  NUMPAD9:      '\uE023',
  MULTIPLY:     '\uE024',
  ADD:          '\uE025',
  SEPARATOR:    '\uE026',
  SUBTRACT:     '\uE027',
  DECIMAL:      '\uE028',
  DIVIDE:       '\uE029',

  F1:           '\uE031',  // function keys
  F2:           '\uE032',
  F3:           '\uE033',
  F4:           '\uE034',
  F5:           '\uE035',
  F6:           '\uE036',
  F7:           '\uE037',
  F8:           '\uE038',
  F9:           '\uE039',
  F10:          '\uE03A',
  F11:          '\uE03B',
  F12:          '\uE03C',

  COMMAND:      '\uE03D',  // Apple command key
  META:         '\uE03D'   // alias for Windows key
};


/**
 * Simulate pressing many keys at once in a "chord". Takes a sequence of
 * {@linkplain Key keys} or strings, appends each of the values to a string,
 * adds the chord termination key ({@link Key.NULL}) and returns the resulting
 * string.
 *
 * Note: when the low-level webdriver key handlers see Keys.NULL, active
 * modifier keys (CTRL/ALT/SHIFT/etc) release via a keyup event.
 *
 * @param {...string} var_args The key sequence to concatenate.
 * @return {string} The null-terminated key sequence.
 */
Key.chord = function(var_args) {
  return Array.prototype.slice.call(arguments, 0).join('') + Key.NULL;
};


/**
 * Used with {@link ./webelement.WebElement#sendKeys WebElement#sendKeys} on
 * file input elements (`<input type="file">`) to detect when the entered key
 * sequence defines the path to a file.
 *
 * By default, {@linkplain ./webelement.WebElement WebElement's} will enter all
 * key sequences exactly as entered. You may set a
 * {@linkplain ./webdriver.WebDriver#setFileDetector file detector} on the
 * parent WebDriver instance to define custom behavior for handling file
 * elements. Of particular note is the
 * {@link selenium-webdriver/remote.FileDetector}, which should be used when
 * running against a remote
 * [Selenium Server](http://docs.seleniumhq.org/download/).
 */
class FileDetector {

  /**
   * Handles the file specified by the given path, preparing it for use with
   * the current browser. If the path does not refer to a valid file, it will
   * be returned unchanged, otherwise a path suitable for use with the current
   * browser will be returned.
   *
   * This default implementation is a no-op. Subtypes may override this function
   * for custom tailored file handling.
   *
   * @param {!./webdriver.WebDriver} driver The driver for the current browser.
   * @param {string} path The path to process.
   * @return {!Promise<string>} A promise for the processed file path.
   * @package
   */
  handleFile(driver, path) {
    return Promise.resolve(path);
  }
}


// PUBLIC API


module.exports = {
  Button: Button,
  Key: Key,
  FileDetector: FileDetector
};


/***/ }),
/* 61 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_img_404_jpg__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_img_404_jpg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__assets_img_404_jpg__);
//
//
//
//
//


/* harmony default export */ __webpack_exports__["a"] = ({
    data: function data() {
        return {
            nothing: __WEBPACK_IMPORTED_MODULE_0__assets_img_404_jpg___default.a
        };
    }
});

/***/ }),
/* 62 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__assets_img_profile_1_jpg__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__assets_img_profile_1_jpg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__assets_img_profile_1_jpg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__assets_img_profile_2_jpg__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__assets_img_profile_2_jpg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__assets_img_profile_2_jpg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__assets_img_logo_coba_coba_png__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__assets_img_logo_coba_coba_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__assets_img_logo_coba_coba_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__mixin_auth_js__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["a"] = ({
    mixins: [__WEBPACK_IMPORTED_MODULE_5__mixin_auth_js__["a" /* auth */]],
    data: function data() {
        return {
            gamar1: __WEBPACK_IMPORTED_MODULE_1__assets_img_profile_1_jpg___default.a,
            gamar2: __WEBPACK_IMPORTED_MODULE_2__assets_img_profile_2_jpg___default.a,
            logocoba: __WEBPACK_IMPORTED_MODULE_3__assets_img_logo_coba_coba_png___default.a,
            posts: [],
            rank: [],
            rengking: [],
            hitung: []
        };
    },
    created: function created() {
        var _this = this;

        __WEBPACK_IMPORTED_MODULE_0_axios___default.a.get('http://103.49.223.93/advokasi/api/profile', {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        }).then(function (response) {
            _this.posts = response.data;
            _this.rankUser();
            _this.rengkingKelas();
            _this.loading = false;
        }).catch(function (e) {
            _this.error.push(e);
        });
    },

    methods: {
        back: function back() {
            window.history.back();
        },
        leaderBoard: function leaderBoard() {
            this.$router.push('/leaderboard');
        },
        Activity: function Activity() {
            this.$router.push('/activity');
        },
        rankUser: function rankUser() {
            var _this2 = this;

            __WEBPACK_IMPORTED_MODULE_0_axios___default.a.get('http://103.49.223.93/advokasi/api/userpoint', {
                headers: {
                    Authorization: 'Bearer ' + localStorage.getItem('token')
                }
            }).then(function (response) {
                _this2.rank = response.data[0];
                _this2.hitung = response.data;
            }).catch(function (e) {
                _this2.error.push(e);
            });
        },
        rengkingKelas: function rengkingKelas() {
            var _this3 = this;

            __WEBPACK_IMPORTED_MODULE_0_axios___default.a.get('http://103.49.223.93/advokasi/api/rankuser', {
                headers: {
                    Authorization: 'Bearer ' + localStorage.getItem('token')
                }
            }).then(function (response) {
                _this3.rengking = response.data;
            }).catch(function (e) {
                _this3.error.push(e);
            });
        },
        edit: function edit() {
            this.$router.push('/edit');
        },
        logout: function logout() {
            var _this4 = this;

            __WEBPACK_IMPORTED_MODULE_4_sweetalert___default()({
                text: "Apakah kamu mau keluar?",
                buttons: true,
                dangerMode: false
            }).then(function (willDelete) {
                if (willDelete) {
                    localStorage.clear();
                    __WEBPACK_IMPORTED_MODULE_4_sweetalert___default()("Kamu telah keluar");
                    // location.reload()
                    _this4.$router.push("/login");
                } else {
                    __WEBPACK_IMPORTED_MODULE_4_sweetalert___default()("Kamu batal keluar?");
                }
            });
        }
    }
});

/***/ }),
/* 63 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/img/profile-1.ae93a8f.jpg";

/***/ }),
/* 64 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__assets_img_profile_1_jpg__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__assets_img_profile_1_jpg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__assets_img_profile_1_jpg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__assets_img_profile_2_jpg__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__assets_img_profile_2_jpg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__assets_img_profile_2_jpg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__assets_img_logo_coba_coba_png__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__assets_img_logo_coba_coba_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__assets_img_logo_coba_coba_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__mixin_auth_js__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["a"] = ({
    mixins: [__WEBPACK_IMPORTED_MODULE_5__mixin_auth_js__["a" /* auth */]],
    data: function data() {
        return {
            edit: [],
            editing: [],
            preview: null,
            files: ''
        };
    },
    created: function created() {
        var _this = this;

        __WEBPACK_IMPORTED_MODULE_0_axios___default.a.get('http://103.49.223.93/advokasi/api/edit', {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        }).then(function (response) {
            _this.edit = response.data;
            console.log(_this.edit);
        }).catch(function (e) {
            _this.error.push(e);
        });
    },

    methods: {
        back: function back() {
            window.history.back();
        },
        onFileChange: function onFileChange(event) {
            var aku = event.target.files[0];
            // swal("Maaf, untuk sementara fitur ganti foto belum tersedia")

            var data = new FormData();

            data.append('action', 'ADD');
            data.append('param', 0);
            data.append('secondParam', 0);
            data.append('aku', new Blob(['test payload'], { type: 'image/jpeg' }));
            this.files = aku;
            console.log(this.files);
        },
        save: function save() {
            var _this2 = this;

            __WEBPACK_IMPORTED_MODULE_0_axios___default.a.post('http://103.49.223.93/advokasi/api/update_edit', {
                nama: this.edit.nama,
                email: this.edit.email,
                no_handphone: this.edit.no_handphone,
                no_rekening: this.edit.no_rekening,
                image: this.files
            }, {
                headers: {
                    Authorization: 'Bearer ' + localStorage.getItem('token')
                }
            }).then(function (response) {
                _this2.editing = response.data.data;
                console.log(_this2.editing);
                __WEBPACK_IMPORTED_MODULE_4_sweetalert___default()({
                    text: "Edit success!",
                    icon: "success",
                    button: "Ok"
                });
            }).catch(function (error) {
                console.log(error);
            });
        },
        gantipass: function gantipass() {
            this.$router.push('/password');
        }
    }
});

/***/ }),
/* 65 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_img_logo_coba_coba_png__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_img_logo_coba_coba_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__assets_img_logo_coba_coba_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_axios__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["a"] = ({
    data: function data() {
        return {
            logincoba: __WEBPACK_IMPORTED_MODULE_0__assets_img_logo_coba_coba_png___default.a,
            input: {
                nik: "",
                password: ""
            },
            message: "",
            respon: null,
            error: null
        };
    },

    methods: {
        login: function login() {
            var _this = this;

            __WEBPACK_IMPORTED_MODULE_1_axios___default.a.post('http://103.49.223.93/advokasi/api/login', {
                nik: this.input.nik,
                password: this.input.password
            }).then(function (response) {
                _this.respon = response.data.token;
                localStorage.setItem('token', _this.respon);
                var token = localStorage.getItem('token');
                _this.$emit("authenticated", true);
                _this.$router.push({ name: "PageFeedapi" });
            }).catch(function (error) {
                if (error.response) {
                    _this.message = "NIK dan Password tidak cocok/tidak ditemukan";
                }
            });
        }
    }
});

/***/ }),
/* 66 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__pages_PageAnaliticFacebook_vue__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__assets_img_logo_coba_coba_png__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__assets_img_logo_coba_coba_png___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__assets_img_logo_coba_coba_png__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_PageAnaliticTwitter_vue__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_PageAnaliticLikedin_vue__ = __webpack_require__(184);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__assets_img_facebook_svg__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__assets_img_facebook_svg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__assets_img_facebook_svg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__assets_img_twitter_svg__ = __webpack_require__(188);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__assets_img_twitter_svg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__assets_img_twitter_svg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_img_linkedin_svg__ = __webpack_require__(189);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__assets_img_linkedin_svg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6__assets_img_linkedin_svg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__assets_img_retweet_svg__ = __webpack_require__(190);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__assets_img_retweet_svg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__assets_img_retweet_svg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__assets_img_heart_svg__ = __webpack_require__(191);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__assets_img_heart_svg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8__assets_img_heart_svg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__assets_img_thumb_svg__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__assets_img_thumb_svg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9__assets_img_thumb_svg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__assets_img_chat_svg__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__assets_img_chat_svg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10__assets_img_chat_svg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_axios__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__mixin_auth_js__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//














/* harmony default export */ __webpack_exports__["a"] = ({
    mixins: [__WEBPACK_IMPORTED_MODULE_12__mixin_auth_js__["a" /* auth */]],
    data: function data() {
        return {
            active: 0,
            facebook: __WEBPACK_IMPORTED_MODULE_4__assets_img_facebook_svg___default.a,
            twitter: __WEBPACK_IMPORTED_MODULE_5__assets_img_twitter_svg___default.a,
            linkedin: __WEBPACK_IMPORTED_MODULE_6__assets_img_linkedin_svg___default.a,
            retweet: __WEBPACK_IMPORTED_MODULE_7__assets_img_retweet_svg___default.a,
            favorite: __WEBPACK_IMPORTED_MODULE_8__assets_img_heart_svg___default.a,
            thumb: __WEBPACK_IMPORTED_MODULE_9__assets_img_thumb_svg___default.a,
            chats: __WEBPACK_IMPORTED_MODULE_10__assets_img_chat_svg___default.a,
            loading_one: true,
            loading_two: false,
            loading_thre: true,
            size: '8px',
            color: '#6c757d',
            logocoba: __WEBPACK_IMPORTED_MODULE_1__assets_img_logo_coba_coba_png___default.a,
            count: []
        };
    },

    components: {
        PageAnaliticFacebook: __WEBPACK_IMPORTED_MODULE_0__pages_PageAnaliticFacebook_vue__["a" /* default */], PageAnaliticTwiter: __WEBPACK_IMPORTED_MODULE_2__pages_PageAnaliticTwitter_vue__["a" /* default */], PageAnaliticLinkedin: __WEBPACK_IMPORTED_MODULE_3__pages_PageAnaliticLikedin_vue__["a" /* default */]
    },
    created: function created() {
        var _this = this;

        __WEBPACK_IMPORTED_MODULE_11_axios___default.a.get('http://103.49.223.93/advokasi/api/countshare', {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        }).then(function (response) {
            _this.count = response.data;
        }).catch(function (e) {
            _this.error.push(e);
        });
    }
});

/***/ }),
/* 67 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_LoadingActivity_vue__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_link_prevue__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_link_prevue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_link_prevue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_NotFound__ = __webpack_require__(15);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["a"] = ({
    data: function data() {
        return {
            postfb: [],
            hitung: []
        };
    },

    components: {
        LinkPrevue: __WEBPACK_IMPORTED_MODULE_1_link_prevue___default.a, LoadingActivity: __WEBPACK_IMPORTED_MODULE_0__components_LoadingActivity_vue__["a" /* default */], NotFound: __WEBPACK_IMPORTED_MODULE_3__components_NotFound__["a" /* default */]
    },
    created: function created() {
        var _this = this;

        this.loading = true;
        __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get('http://103.49.223.93/advokasi/api/sharefb', {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        }).then(function (response) {
            _this.postfb = response.data.data;
            _this.hitung = response.data.data.length;
            _this.loading = false;
        }).catch(function (e) {
            _this.error.push(e);
        });
    }
});

/***/ }),
/* 68 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue_content_loader__ = __webpack_require__(12);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["a"] = ({
    components: {
        ContentLoader: __WEBPACK_IMPORTED_MODULE_0_vue_content_loader__["a" /* ContentLoader */]
    }
});

/***/ }),
/* 69 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_LoadingActivity_vue__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_link_prevue__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_link_prevue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_link_prevue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_NotFound__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__mixin_auth_js__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["a"] = ({
    mixins: [__WEBPACK_IMPORTED_MODULE_4__mixin_auth_js__["a" /* auth */]],
    data: function data() {
        return {
            posts: [],
            hitungtw: []
        };
    },

    components: {
        LinkPrevue: __WEBPACK_IMPORTED_MODULE_1_link_prevue___default.a, LoadingActivity: __WEBPACK_IMPORTED_MODULE_0__components_LoadingActivity_vue__["a" /* default */], NotFound: __WEBPACK_IMPORTED_MODULE_3__components_NotFound__["a" /* default */]
    },
    created: function created() {
        var _this = this;

        this.loading = true;
        __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get('http://103.49.223.93/advokasi/api/sharetw', {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        }).then(function (response) {
            _this.posts = response.data.data;
            _this.hitungtw = response.data.data.length;
            _this.loading = false;
        }).catch(function (e) {
            _this.error.push(e);
        });
    }
});

/***/ }),
/* 70 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_LoadingActivity_vue__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_link_prevue__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_link_prevue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_link_prevue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_NotFound__ = __webpack_require__(15);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["a"] = ({
    data: function data() {
        return {
            posts: [],
            hitungin: []
        };
    },

    components: {
        LinkPrevue: __WEBPACK_IMPORTED_MODULE_1_link_prevue___default.a, LoadingActivity: __WEBPACK_IMPORTED_MODULE_0__components_LoadingActivity_vue__["a" /* default */], NotFound: __WEBPACK_IMPORTED_MODULE_3__components_NotFound__["a" /* default */]
    },
    created: function created() {
        var _this = this;

        this.loading = true;
        __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get('http://103.49.223.93/advokasi/api/sharelinked', {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        }).then(function (response) {
            _this.posts = response.data.data;
            _this.hitungin = response.data.data.length;
            _this.loading = false;
        }).catch(function (e) {
            _this.error.push(e);
        });
    }
});

/***/ }),
/* 71 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_img_profile_2_jpg__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__assets_img_profile_2_jpg___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__assets_img_profile_2_jpg__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mixin_auth_js__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["a"] = ({
    mixins: [__WEBPACK_IMPORTED_MODULE_2__mixin_auth_js__["a" /* auth */]],
    data: function data() {
        return {
            gamar2: __WEBPACK_IMPORTED_MODULE_0__assets_img_profile_2_jpg___default.a,
            posts: []
        };
    },
    created: function created() {
        var _this = this;

        this.loading = true;
        __WEBPACK_IMPORTED_MODULE_1_axios___default.a.get('http://103.49.223.93/advokasi/api/leaderboard', {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        }).then(function (response) {
            _this.posts = response.data;
            _this.loading = false;
        }).catch(function (e) {
            _this.error.push(e);
        });
    },

    methods: {
        back: function back() {
            window.history.back();
        }
    }
});

/***/ }),
/* 72 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_LoadingActivity_vue__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_link_prevue__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_link_prevue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_link_prevue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_NotFound__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__mixin_auth_js__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["a"] = ({
    mixins: [__WEBPACK_IMPORTED_MODULE_4__mixin_auth_js__["a" /* auth */]],
    data: function data() {
        return {
            loading: false,
            id_user: 8,
            posts: [],
            hitung: []
        };
    },

    components: {
        LinkPrevue: __WEBPACK_IMPORTED_MODULE_1_link_prevue___default.a, LoadingActivity: __WEBPACK_IMPORTED_MODULE_0__components_LoadingActivity_vue__["a" /* default */], NotFound: __WEBPACK_IMPORTED_MODULE_3__components_NotFound__["a" /* default */]
    },
    methods: {
        back: function back() {
            window.history.back();
        }
    },
    created: function created() {
        var _this = this;

        this.loading = true;
        __WEBPACK_IMPORTED_MODULE_2_axios___default.a.get('http://103.49.223.93/advokasi/api/share_user', {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        }).then(function (response) {
            _this.posts = response.data.data;
            _this.hitung = response.data.data.length;
            _this.loading = false;
        }).catch(function (e) {
            _this.error.push(e);
        });
    }
});

/***/ }),
/* 73 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_sweetalert__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mixin_auth_js__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["a"] = ({
    mixins: [__WEBPACK_IMPORTED_MODULE_2__mixin_auth_js__["a" /* auth */]],
    data: function data() {
        return {
            active: 0,
            posts: [],
            voucher_reedem: null,
            point: []
        };
    },
    created: function created() {
        var _this = this;

        var id_voucher = this.$route.params.id_voucher;
        __WEBPACK_IMPORTED_MODULE_0_axios___default.a.get('http://103.49.223.93/advokasi/api/voucher/' + id_voucher, {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        }).then(function (response) {
            _this.posts = response.data;
            _this.maPoint();
            console.log(_this.posts);
            _this.loading = false;
        }).catch(function (e) {
            _this.error.push(e);
        });
    },

    methods: {
        reedemVoucher: function reedemVoucher() {
            var _this2 = this;

            var expired = new Date(this.posts.expired_date);
            var today = new Date();
            if (expired < today) {
                __WEBPACK_IMPORTED_MODULE_1_sweetalert___default()("Voucher telah kadaluarsa");
            } else if (this.posts.jml_voucher === 0) {
                __WEBPACK_IMPORTED_MODULE_1_sweetalert___default()("Voucher tidak tersedia");
            } else if (this.posts.point_voucher > this.point) {
                __WEBPACK_IMPORTED_MODULE_1_sweetalert___default()("Poin kamu tidak mencukupi");
            } else {
                var id_voucher = this.$route.params.id_voucher;
                __WEBPACK_IMPORTED_MODULE_0_axios___default.a.post('http://103.49.223.93/advokasi/api/reedem_this', {
                    id_voucher: id_voucher
                }, {
                    headers: {
                        Authorization: 'Bearer ' + localStorage.getItem('token')
                    }
                }).then(function (response) {
                    _this2.voucher_reedem = response;
                    _this2.$router.push("/rewards");
                    __WEBPACK_IMPORTED_MODULE_1_sweetalert___default()({
                        text: "Redeem berhasil!",
                        icon: "success",
                        button: "Ok"
                    });
                }).catch(function (e) {
                    _this2.error.push(e);
                });
            }
        },
        maPoint: function maPoint() {
            var _this3 = this;

            __WEBPACK_IMPORTED_MODULE_0_axios___default.a.get('http://103.49.223.93/advokasi/api/userpoint', {
                headers: {
                    Authorization: 'Bearer ' + localStorage.getItem('token')
                }
            }).then(function (response) {
                _this3.point = response.data[0].point;
                console.log(_this3.point);
            }).catch(function (e) {
                _this3.error.push(e);
            });
        },
        back: function back() {
            window.history.back();
        }
    }
});

/***/ }),
/* 74 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_sweetalert__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mixin_auth_js__ = __webpack_require__(3);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["a"] = ({
    mixins: [__WEBPACK_IMPORTED_MODULE_2__mixin_auth_js__["a" /* auth */]],
    data: function data() {
        return {
            edit: [],
            password: "",
            confirm: "",
            editpass: []
        };
    },
    created: function created() {
        var _this = this;

        __WEBPACK_IMPORTED_MODULE_0_axios___default.a.get('http://103.49.223.93/advokasi/api/edit', {
            headers: {
                Authorization: 'Bearer ' + localStorage.getItem('token')
            }
        }).then(function (response) {
            _this.edit = response.data;
        }).catch(function (e) {
            _this.error.push(e);
        });
    },

    methods: {
        back: function back() {
            window.history.back();
        },
        change: function change() {
            var _this2 = this;

            __WEBPACK_IMPORTED_MODULE_0_axios___default.a.post('http://103.49.223.93/advokasi/api/gantipass', {
                password: this.password,
                confirm: this.confirm
            }, {
                headers: {
                    Authorization: 'Bearer ' + localStorage.getItem('token')
                }
            }).then(function (response) {
                _this2.editpass = response.data;
                if (_this2.editpass.succes === false) {
                    __WEBPACK_IMPORTED_MODULE_1_sweetalert___default()("Password dan konfirmasi password tidak cocok");
                } else {
                    __WEBPACK_IMPORTED_MODULE_1_sweetalert___default()({
                        text: "Edit password sukses!",
                        icon: "success",
                        button: "Ok"
                    });
                    _this2.$router.push("/edit");
                }
            }).catch(function (e) {
                _this2.error.push(e);
            });
        }
    }
});

/***/ }),
/* 75 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_sweetalert__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mixin_auth_js__ = __webpack_require__(3);
//
//
//



/* harmony default export */ __webpack_exports__["a"] = ({
    mixins: [__WEBPACK_IMPORTED_MODULE_1__mixin_auth_js__["a" /* auth */]],
    data: function data() {
        return {
            id_account: null,
            message: null,
            screenName: null,
            status: null
        };
    },
    mounted: function mounted() {
        var succes = this.$route.query;
        localStorage.setItem('id_account', succes.id_account);
        localStorage.setItem('message', succes.message);
        localStorage.setItem('screenName', succes.screenName);
        localStorage.setItem('status', succes.status);
        if (succes.status === 'OK') {
            this.$router.push("/feed");
        }
    }
});

/***/ }),
/* 76 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_axios___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_axios__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_sweetalert__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_sweetalert___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_sweetalert__);
//
//
//



/* harmony default export */ __webpack_exports__["a"] = ({
    data: function data() {
        return {
            codes: [],
            profiles: [],
            token_linked: null
        };
    },
    mounted: function mounted() {
        var _this = this;

        var succes = this.$route.query.code;
        console.log(succes);
        __WEBPACK_IMPORTED_MODULE_0_axios___default.a.post('http://103.49.223.93/advokasi/api/linkedin-token', {
            code: succes
        }).then(function (response) {
            _this.codes = JSON.parse(response.data);
            localStorage.setItem('token_linked', _this.codes.access_token);
            _this.$router.push("/feed");
        }).catch(function (e) {
            _this.error.push(e);
        });
    },

    methods: {
        profile: function profile() {
            var _this2 = this;

            var token = localStorage.getItem('token_linked');
            console.log(token);
            __WEBPACK_IMPORTED_MODULE_0_axios___default.a.get('https://api.linkedin.com/v1/people', {
                headers: {
                    Authorization: 'Bearer ' + localStorage.getItem('token_linked')
                }
            }).then(function (response) {
                _this2.profiles = response.data;
                console.log(_this2.profiles);
            }).catch(function (e) {
                _this2.error.push(e);
            });
        }
    }

});

/***/ }),
/* 77 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";

/* harmony default export */ __webpack_exports__["a"] = ({
  name: 'vue-particles',
  props: {
    color: {
      type: String,
      default: '#dedede'
    },
    particleOpacity: {
      type: Number,
      default: 0.7
    },
    particlesNumber: {
      type: Number,
      default: 80
    },
    shapeType: {
      type: String,
      default: 'circle'
    },
    particleSize: {
      type: Number,
      default: 4
    },
    linesColor: {
      type: String,
      default: '#dedede'
    },
    linesWidth: {
      type: Number,
      default: 1
    },
    lineLinked: {
      type: Boolean,
      default: true
    },
    lineOpacity: {
      type: Number,
      default: 0.4
    },
    linesDistance: {
      type: Number,
      default: 150
    },
    moveSpeed: {
      type: Number,
      default: 3
    },
    hoverEffect: {
      type: Boolean,
      default: true
    },
    hoverMode: {
      type: String,
      default: 'grab'
    },
    clickEffect: {
      type: Boolean,
      default: true
    },
    clickMode: {
      type: String,
      default: 'push'
    }
  },
  mounted: function mounted() {
    var _this = this;

    __webpack_require__(220);
    this.$nextTick(function () {
      _this.initParticleJS(_this.color, _this.particleOpacity, _this.particlesNumber, _this.shapeType, _this.particleSize, _this.linesColor, _this.linesWidth, _this.lineLinked, _this.lineOpacity, _this.linesDistance, _this.moveSpeed, _this.hoverEffect, _this.hoverMode, _this.clickEffect, _this.clickMode);
    });
  },

  methods: {
    initParticleJS: function initParticleJS(color, particleOpacity, particlesNumber, shapeType, particleSize, linesColor, linesWidth, lineLinked, lineOpacity, linesDistance, moveSpeed, hoverEffect, hoverMode, clickEffect, clickMode) {
      particlesJS('particles-js', {
        "particles": {
          "number": {
            "value": particlesNumber,
            "density": {
              "enable": true,
              "value_area": 800
            }
          },
          "color": {
            "value": color
          },
          "shape": {
            "type": shapeType,
            "stroke": {
              "width": 0,
              "color": "#192231"
            },
            "polygon": {
              "nb_sides": 5
            }
          },
          "opacity": {
            "value": particleOpacity,
            "random": false,
            "anim": {
              "enable": false,
              "speed": 1,
              "opacity_min": 0.1,
              "sync": false
            }
          },
          "size": {
            "value": particleSize,
            "random": true,
            "anim": {
              "enable": false,
              "speed": 40,
              "size_min": 0.1,
              "sync": false
            }
          },
          "line_linked": {
            "enable": lineLinked,
            "distance": linesDistance,
            "color": linesColor,
            "opacity": lineOpacity,
            "width": linesWidth
          },
          "move": {
            "enable": true,
            "speed": moveSpeed,
            "direction": "none",
            "random": false,
            "straight": false,
            "out_mode": "out",
            "bounce": false,
            "attract": {
              "enable": false,
              "rotateX": 600,
              "rotateY": 1200
            }
          }
        },
        "interactivity": {
          "detect_on": "canvas",
          "events": {
            "onhover": {
              "enable": hoverEffect,
              "mode": hoverMode
            },
            "onclick": {
              "enable": clickEffect,
              "mode": clickMode
            },
            "onresize": {

              "enable": true,
              "density_auto": true,
              "density_area": 400

            }
          },
          "modes": {
            "grab": {
              "distance": 140,
              "line_linked": {
                "opacity": 1
              }
            },
            "bubble": {
              "distance": 400,
              "size": 40,
              "duration": 2,
              "opacity": 8,
              "speed": 3
            },
            "repulse": {
              "distance": 200,
              "duration": 0.4
            },
            "push": {
              "particles_nb": 4
            },
            "remove": {
              "particles_nb": 2
            }
          }
        },
        "retina_detect": true
      });
    }
  }
});

/***/ }),
/* 78 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__App__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__router__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_bootstrap_css_only_css_bootstrap_min_css__ = __webpack_require__(211);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_bootstrap_css_only_css_bootstrap_min_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_bootstrap_css_only_css_bootstrap_min_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_mdbvue_build_css_mdb_css__ = __webpack_require__(212);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_mdbvue_build_css_mdb_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_mdbvue_build_css_mdb_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_muse_ui__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_muse_ui_dist_muse_ui_css__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_muse_ui_dist_muse_ui_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_muse_ui_dist_muse_ui_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__kevindesousa_vue_image_loader__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__kevindesousa_vue_image_loader___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7__kevindesousa_vue_image_loader__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_vue_material__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_vue_material___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_vue_material__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_vue_material_dist_vue_material_min_css__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_vue_material_dist_vue_material_min_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_vue_material_dist_vue_material_min_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_vue_particles__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_vue_moment__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_vue_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_vue_moment__);
// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.











var SocialSharing = __webpack_require__(222);

var infiniteScroll = __webpack_require__(224);

__WEBPACK_IMPORTED_MODULE_0_vue__["default"].use(infiniteScroll);
__WEBPACK_IMPORTED_MODULE_0_vue__["default"].use(__WEBPACK_IMPORTED_MODULE_11_vue_moment___default.a);
__WEBPACK_IMPORTED_MODULE_0_vue__["default"].use(SocialSharing);
__WEBPACK_IMPORTED_MODULE_0_vue__["default"].use(__WEBPACK_IMPORTED_MODULE_10_vue_particles__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_0_vue__["default"].use(SocialSharing);
__WEBPACK_IMPORTED_MODULE_0_vue__["default"].config.productionTip = false;
__WEBPACK_IMPORTED_MODULE_0_vue__["default"].use(__WEBPACK_IMPORTED_MODULE_5_muse_ui__["a" /* default */]);
__WEBPACK_IMPORTED_MODULE_0_vue__["default"].use(__WEBPACK_IMPORTED_MODULE_7__kevindesousa_vue_image_loader___default.a);
__WEBPACK_IMPORTED_MODULE_0_vue__["default"].use(__WEBPACK_IMPORTED_MODULE_8_vue_material___default.a);

/* eslint-disable no-new */
new __WEBPACK_IMPORTED_MODULE_0_vue__["default"]({
  el: '#app',
  router: __WEBPACK_IMPORTED_MODULE_2__router__["a" /* default */],
  template: '<App/>',
  components: { App: __WEBPACK_IMPORTED_MODULE_1__App__["a" /* default */] }
});

/***/ }),
/* 79 */,
/* 80 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_App_vue__ = __webpack_require__(26);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_119c471d_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_App_vue__ = __webpack_require__(83);
function injectStyle (ssrContext) {
  __webpack_require__(81)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_App_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_119c471d_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_App_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 81 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 82 */,
/* 83 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"app"},[_c('router-view')],1)}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 84 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_vue__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_vue_router__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_layout_LayoutApp__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_PageFeed__ = __webpack_require__(91);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_PageFeedapi__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_PageRewards__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_PageProfile__ = __webpack_require__(165);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_PageProfiledit__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_PageLogin__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_PageAnalitic__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_PageLeaderboard__ = __webpack_require__(195);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_PageActivity__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_PageReedem__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_PagePassword__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_LoginTwitter__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_LoginLinkedin__ = __webpack_require__(209);

















__WEBPACK_IMPORTED_MODULE_0_vue__["default"].use(__WEBPACK_IMPORTED_MODULE_1_vue_router__["a" /* default */]);

/* harmony default export */ __webpack_exports__["a"] = (new __WEBPACK_IMPORTED_MODULE_1_vue_router__["a" /* default */]({
  mode: 'history',
  routes: [{
    path: '/login',
    name: 'PageLogin',
    component: __WEBPACK_IMPORTED_MODULE_8__components_PageLogin__["a" /* default */]
  }, {
    path: '/',
    redirect: {
      name: 'PageLogin'
    }
  }, {
    path: '*', redirect: '/login'
  }, {
    path: '/',
    name: 'LayoutApp',
    component: __WEBPACK_IMPORTED_MODULE_2__pages_layout_LayoutApp__["a" /* default */],
    redirect: '/feed',
    children: [{
      path: 'feed2',
      name: 'PageFeed',
      component: __WEBPACK_IMPORTED_MODULE_3__pages_PageFeed__["a" /* default */]
    }, {
      path: 'feed',
      name: 'PageFeedapi',
      component: __WEBPACK_IMPORTED_MODULE_4__pages_PageFeedapi__["a" /* default */]
    }, {
      path: 'rewards',
      name: 'PageRewards',
      component: __WEBPACK_IMPORTED_MODULE_5__pages_PageRewards__["a" /* default */]
    }, {
      path: 'profile',
      name: 'PageProfile',
      component: __WEBPACK_IMPORTED_MODULE_6__pages_PageProfile__["a" /* default */]
    }, {
      path: 'analitic',
      name: 'PageAnalitic',
      component: __WEBPACK_IMPORTED_MODULE_9__pages_PageAnalitic__["a" /* default */]
    }, {
      path: 'leaderboard',
      name: 'PageLeaderboard',
      component: __WEBPACK_IMPORTED_MODULE_10__pages_PageLeaderboard__["a" /* default */]
    }, {
      path: 'activity',
      name: 'PageActivity',
      component: __WEBPACK_IMPORTED_MODULE_11__pages_PageActivity__["a" /* default */]
    }, {
      path: 'reedem/:id_voucher',
      name: 'PageReedem',
      component: __WEBPACK_IMPORTED_MODULE_12__pages_PageReedem__["a" /* default */]
    }, {
      path: 'edit',
      name: 'PageEdit',
      component: __WEBPACK_IMPORTED_MODULE_7__pages_PageProfiledit__["a" /* default */]
    }, {
      path: 'password',
      name: 'PagePassword',
      component: __WEBPACK_IMPORTED_MODULE_13__pages_PagePassword__["a" /* default */]
    }, {
      path: 'login-twitter',
      name: 'LoginTwitter',
      component: __WEBPACK_IMPORTED_MODULE_14__pages_LoginTwitter__["a" /* default */]
    }, {
      path: 'login-linkedin',
      name: 'LoginLinkedin',
      component: __WEBPACK_IMPORTED_MODULE_15__pages_LoginLinkedin__["a" /* default */]
    }]
  }]
}));

/***/ }),
/* 85 */,
/* 86 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_LayoutApp_vue__ = __webpack_require__(27);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_12a98a66_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_LayoutApp_vue__ = __webpack_require__(90);
function injectStyle (ssrContext) {
  __webpack_require__(87)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_LayoutApp_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_12a98a66_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_LayoutApp_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 87 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 88 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_template_compiler_index_id_data_v_20f42f98_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_ContentAplikasi_vue__ = __webpack_require__(89);
var normalizeComponent = __webpack_require__(0)
/* script */
var __vue_script__ = null
/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __WEBPACK_IMPORTED_MODULE_0__node_modules_vue_loader_lib_template_compiler_index_id_data_v_20f42f98_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_ContentAplikasi_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 89 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('router-view')}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 90 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"layout"},[_c('content-aplikasi',{on:{"authenticated":_vm.setAuthenticated}}),_vm._v(" "),_c('mu-bottom-nav',{staticClass:"nav-bottom index"},[_c('mu-bottom-nav-item',{attrs:{"icon":"format_list_bulleted","to":"/feed","exact":""}}),_vm._v(" "),_c('mu-bottom-nav-item',{attrs:{"icon":"star_border","to":"/rewards"}}),_vm._v(" "),_c('mu-bottom-nav-item',{attrs:{"icon":"insert_chart","to":"/analitic"}}),_vm._v(" "),_c('mu-bottom-nav-item',{attrs:{"icon":"person_outline","to":"/profile"}})],1)],1)}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 91 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageFeed_vue__ = __webpack_require__(28);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_6c9ecfc8_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageFeed_vue__ = __webpack_require__(114);
function injectStyle (ssrContext) {
  __webpack_require__(92)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageFeed_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_6c9ecfc8_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageFeed_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 92 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 93 */,
/* 94 */,
/* 95 */,
/* 96 */,
/* 97 */,
/* 98 */,
/* 99 */,
/* 100 */,
/* 101 */,
/* 102 */,
/* 103 */,
/* 104 */,
/* 105 */,
/* 106 */,
/* 107 */,
/* 108 */,
/* 109 */,
/* 110 */,
/* 111 */,
/* 112 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('mu-card',{staticClass:"card-feed",staticStyle:{"margin":"0 auto"}},[_c('content-loader',{attrs:{"height":470,"width":411,"speed":3,"primaryColor":"#f3f3f3","secondaryColor":"#d9d9d9"}},[_c('rect',{attrs:{"x":"70","y":"15","rx":"4","ry":"4","width":"117","height":"11.46"}}),_vm._v(" "),_c('rect',{attrs:{"x":"70","y":"35","rx":"3","ry":"3","width":"85","height":"6.4"}}),_vm._v(" "),_c('circle',{attrs:{"cx":"41.5","cy":"29.5","r":"19.5"}}),_vm._v(" "),_c('rect',{attrs:{"x":"7.67","y":"68.27","rx":"0","ry":"0","width":"397.98","height":"202.65"}}),_vm._v(" "),_c('rect',{attrs:{"x":"270","y":"117.27","rx":"0","ry":"0","width":"0","height":"0"}}),_vm._v(" "),_c('rect',{attrs:{"x":"30","y":"288.27","rx":"0","ry":"0","width":"133.98","height":"16"}}),_vm._v(" "),_c('rect',{attrs:{"x":"174","y":"289.27","rx":"0","ry":"0","width":"214.4","height":"16"}}),_vm._v(" "),_c('rect',{attrs:{"x":"29","y":"314.27","rx":"0","ry":"0","width":"198","height":"16"}}),_vm._v(" "),_c('rect',{attrs:{"x":"29","y":"338.27","rx":"0","ry":"0","width":"94","height":"7"}}),_vm._v(" "),_c('rect',{attrs:{"x":"28.5","y":"360.77","rx":"0","ry":"0","width":"110.24","height":"11"}}),_vm._v(" "),_c('rect',{attrs:{"x":"152","y":"360.27","rx":"0","ry":"0","width":"153","height":"11"}}),_vm._v(" "),_c('rect',{attrs:{"x":"318","y":"360.27","rx":"0","ry":"0","width":"69","height":"11"}}),_vm._v(" "),_c('rect',{attrs:{"x":"30","y":"380.27","rx":"0","ry":"0","width":"141","height":"11"}}),_vm._v(" "),_c('rect',{attrs:{"x":"183","y":"379.27","rx":"0","ry":"0","width":"195","height":"12"}}),_vm._v(" "),_c('rect',{attrs:{"x":"29","y":"399.27","rx":"0","ry":"0","width":"213","height":"11"}}),_vm._v(" "),_c('rect',{attrs:{"x":"254","y":"398.27","rx":"0","ry":"0","width":"137","height":"12"}}),_vm._v(" "),_c('rect',{attrs:{"x":"158","y":"429.27","rx":"0","ry":"0","width":"111","height":"32"}}),_vm._v(" "),_c('rect',{attrs:{"x":"284","y":"429.27","rx":"0","ry":"0","width":"113","height":"32"}}),_vm._v(" "),_c('rect',{attrs:{"x":"31","y":"429.27","rx":"0","ry":"0","width":"110.88000000000001","height":"33"}})])],1),_vm._v(" "),_c('mu-card',{staticClass:"card-feed",staticStyle:{"margin":"0 auto"}},[_c('content-loader',{attrs:{"height":470,"width":411,"speed":3,"primaryColor":"#f3f3f3","secondaryColor":"#d9d9d9"}},[_c('rect',{attrs:{"x":"70","y":"15","rx":"4","ry":"4","width":"117","height":"11.46"}}),_vm._v(" "),_c('rect',{attrs:{"x":"70","y":"35","rx":"3","ry":"3","width":"85","height":"6.4"}}),_vm._v(" "),_c('circle',{attrs:{"cx":"41.5","cy":"29.5","r":"19.5"}}),_vm._v(" "),_c('rect',{attrs:{"x":"7.67","y":"68.27","rx":"0","ry":"0","width":"397.98","height":"202.65"}}),_vm._v(" "),_c('rect',{attrs:{"x":"270","y":"117.27","rx":"0","ry":"0","width":"0","height":"0"}}),_vm._v(" "),_c('rect',{attrs:{"x":"30","y":"288.27","rx":"0","ry":"0","width":"133.98","height":"16"}}),_vm._v(" "),_c('rect',{attrs:{"x":"174","y":"289.27","rx":"0","ry":"0","width":"214.4","height":"16"}}),_vm._v(" "),_c('rect',{attrs:{"x":"29","y":"314.27","rx":"0","ry":"0","width":"198","height":"16"}}),_vm._v(" "),_c('rect',{attrs:{"x":"29","y":"338.27","rx":"0","ry":"0","width":"94","height":"7"}}),_vm._v(" "),_c('rect',{attrs:{"x":"28.5","y":"360.77","rx":"0","ry":"0","width":"110.24","height":"11"}}),_vm._v(" "),_c('rect',{attrs:{"x":"152","y":"360.27","rx":"0","ry":"0","width":"153","height":"11"}}),_vm._v(" "),_c('rect',{attrs:{"x":"318","y":"360.27","rx":"0","ry":"0","width":"69","height":"11"}}),_vm._v(" "),_c('rect',{attrs:{"x":"30","y":"380.27","rx":"0","ry":"0","width":"141","height":"11"}}),_vm._v(" "),_c('rect',{attrs:{"x":"183","y":"379.27","rx":"0","ry":"0","width":"195","height":"12"}}),_vm._v(" "),_c('rect',{attrs:{"x":"29","y":"399.27","rx":"0","ry":"0","width":"213","height":"11"}}),_vm._v(" "),_c('rect',{attrs:{"x":"254","y":"398.27","rx":"0","ry":"0","width":"137","height":"12"}}),_vm._v(" "),_c('rect',{attrs:{"x":"158","y":"429.27","rx":"0","ry":"0","width":"111","height":"32"}}),_vm._v(" "),_c('rect',{attrs:{"x":"284","y":"429.27","rx":"0","ry":"0","width":"113","height":"32"}}),_vm._v(" "),_c('rect',{attrs:{"x":"31","y":"429.27","rx":"0","ry":"0","width":"110.88000000000001","height":"33"}})])],1),_vm._v(" "),_c('mu-card',{staticClass:"card-feed",staticStyle:{"margin":"0 auto"}},[_c('content-loader',{attrs:{"height":470,"width":411,"speed":3,"primaryColor":"#f3f3f3","secondaryColor":"#d9d9d9"}},[_c('rect',{attrs:{"x":"70","y":"15","rx":"4","ry":"4","width":"117","height":"11.46"}}),_vm._v(" "),_c('rect',{attrs:{"x":"70","y":"35","rx":"3","ry":"3","width":"85","height":"6.4"}}),_vm._v(" "),_c('circle',{attrs:{"cx":"41.5","cy":"29.5","r":"19.5"}}),_vm._v(" "),_c('rect',{attrs:{"x":"7.67","y":"68.27","rx":"0","ry":"0","width":"397.98","height":"202.65"}}),_vm._v(" "),_c('rect',{attrs:{"x":"270","y":"117.27","rx":"0","ry":"0","width":"0","height":"0"}}),_vm._v(" "),_c('rect',{attrs:{"x":"30","y":"288.27","rx":"0","ry":"0","width":"133.98","height":"16"}}),_vm._v(" "),_c('rect',{attrs:{"x":"174","y":"289.27","rx":"0","ry":"0","width":"214.4","height":"16"}}),_vm._v(" "),_c('rect',{attrs:{"x":"29","y":"314.27","rx":"0","ry":"0","width":"198","height":"16"}}),_vm._v(" "),_c('rect',{attrs:{"x":"29","y":"338.27","rx":"0","ry":"0","width":"94","height":"7"}}),_vm._v(" "),_c('rect',{attrs:{"x":"28.5","y":"360.77","rx":"0","ry":"0","width":"110.24","height":"11"}}),_vm._v(" "),_c('rect',{attrs:{"x":"152","y":"360.27","rx":"0","ry":"0","width":"153","height":"11"}}),_vm._v(" "),_c('rect',{attrs:{"x":"318","y":"360.27","rx":"0","ry":"0","width":"69","height":"11"}}),_vm._v(" "),_c('rect',{attrs:{"x":"30","y":"380.27","rx":"0","ry":"0","width":"141","height":"11"}}),_vm._v(" "),_c('rect',{attrs:{"x":"183","y":"379.27","rx":"0","ry":"0","width":"195","height":"12"}}),_vm._v(" "),_c('rect',{attrs:{"x":"29","y":"399.27","rx":"0","ry":"0","width":"213","height":"11"}}),_vm._v(" "),_c('rect',{attrs:{"x":"254","y":"398.27","rx":"0","ry":"0","width":"137","height":"12"}}),_vm._v(" "),_c('rect',{attrs:{"x":"158","y":"429.27","rx":"0","ry":"0","width":"111","height":"32"}}),_vm._v(" "),_c('rect',{attrs:{"x":"284","y":"429.27","rx":"0","ry":"0","width":"113","height":"32"}}),_vm._v(" "),_c('rect',{attrs:{"x":"31","y":"429.27","rx":"0","ry":"0","width":"110.88000000000001","height":"33"}})])],1)],1)}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 113 */,
/* 114 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_vm._m(0),_vm._v(" "),_c('div',{staticClass:"container-fluid feed-page"},[(_vm.loading)?_c('div',[_c('loading-feed')],1):_c('div',_vm._l((_vm.posts),function(post){return _c('div',{key:post.id},[_c('mu-card',{staticClass:"card-feed",staticStyle:{"margin":"0 auto"}},[_c('mu-card-header',{attrs:{"title":post.source.name,"sub-title":"2 Okt"}},[_c('mu-avatar',{attrs:{"slot":"avatar"},slot:"avatar"},[_c('img',{attrs:{"src":_vm.avatar}})])],1),_vm._v(" "),_c('mu-card-media',[(post.urlToImage == null)?_c('div',[_c('img',{staticClass:"img-kosong",attrs:{"src":_vm.imagekosong}})]):_c('div',[_c('img',{attrs:{"src":post.urlToImage}})])]),_vm._v(" "),_c('mu-card-title',{attrs:{"title":post.title,"sub-title":"Kontributor"}}),_vm._v(" "),_c('mu-card-text',[_vm._v("\n            "+_vm._s(post.content)+" \n          ")]),_vm._v(" "),_c('mu-card-actions',{staticClass:"card-action"},[_c('mu-button',{attrs:{"flat":""}},[_c('i',{staticClass:"fa fa-facebook"}),_vm._v(" Facebook\n              ")]),_vm._v(" "),_c('mu-button',{attrs:{"flat":""}},[_c('i',{staticClass:"fa fa-twitter"}),_vm._v(" Twitter\n              ")]),_vm._v(" "),_c('mu-button',{staticClass:"last-but",attrs:{"flat":""}},[_c('i',{staticClass:"fa fa-linkedin"}),_vm._v(" Linkedin\n              ")])],1)],1)],1)}),0)])])}
var staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('nav',{staticClass:"navbar navbar-dark fixed-top bg-dark"},[_c('a',{staticClass:"navbar-brand",attrs:{"href":"#"}},[_vm._v("apa")])])}]
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 115 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageFeedapi_vue__ = __webpack_require__(38);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_46ab1eb6_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageFeedapi_vue__ = __webpack_require__(119);
function injectStyle (ssrContext) {
  __webpack_require__(116)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-46ab1eb6"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageFeedapi_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_46ab1eb6_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageFeedapi_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 116 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 117 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_OneLoading_vue__ = __webpack_require__(39);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_3da2a0a2_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_OneLoading_vue__ = __webpack_require__(118);
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_OneLoading_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_3da2a0a2_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_OneLoading_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 118 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('mu-card',{staticClass:"card-feed",staticStyle:{"margin":"0 auto"}},[_c('content-loader',{attrs:{"height":300,"width":411,"speed":3,"primaryColor":"#f3f3f3","secondaryColor":"#d9d9d9"}},[_c('rect',{attrs:{"x":"6","y":"4.23","rx":"0","ry":"0","width":"403.38","height":"150"}}),_vm._v(" "),_c('rect',{attrs:{"x":"71","y":"219.27","rx":"0","ry":"0","width":"0","height":"0"}}),_vm._v(" "),_c('rect',{attrs:{"x":"9.89","y":"163.67","rx":"0","ry":"0","width":"338.52","height":"19"}}),_vm._v(" "),_c('rect',{attrs:{"x":"10.89","y":"230.67","rx":"0","ry":"0","width":"211","height":"14"}}),_vm._v(" "),_c('rect',{attrs:{"x":"9.89","y":"189.67","rx":"0","ry":"0","width":"257.28","height":"20"}}),_vm._v(" "),_c('rect',{attrs:{"x":"231.89","y":"230.67","rx":"0","ry":"0","width":"159","height":"14"}}),_vm._v(" "),_c('rect',{attrs:{"x":"11.89","y":"254.67","rx":"0","ry":"0","width":"144.57","height":"14"}}),_vm._v(" "),_c('rect',{attrs:{"x":"168.89","y":"254.67","rx":"0","ry":"0","width":"159","height":"13"}}),_vm._v(" "),_c('rect',{attrs:{"x":"11.89","y":"279.67","rx":"0","ry":"0","width":"226","height":"14"}}),_vm._v(" "),_c('rect',{attrs:{"x":"134.89","y":"325.67","rx":"0","ry":"0","width":"0","height":"0"}})])],1)],1)}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 119 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('nav',{staticClass:"navbar navbar-dark fixed-top bg-atas"},[_c('a',{staticClass:"navbar-brand",attrs:{"href":"#"}},[_c('img',{attrs:{"src":_vm.logocoba}})])]),_vm._v(" "),_c('div',{staticClass:"container-fluid feed-page"},[_c('div',{directives:[{name:"infinite-scroll",rawName:"v-infinite-scroll",value:(_vm.loadMore),expression:"loadMore"}],attrs:{"infinite-scroll-disabled":"busy"}},_vm._l((_vm.datanya),function(post){return _c('div',{key:post.id},[_c('mu-card',{staticClass:"card-feed",staticStyle:{"margin":"0 auto"}},[_c('link-prevue',{attrs:{"url":post.url},scopedSlots:_vm._u([{key:"default",fn:function(props){return [_c('mu-card-media',[_c('img',{staticClass:"card-img-top img-show",attrs:{"src":props.img,"alt":props.title}})]),_vm._v(" "),_c('mu-card-title',{attrs:{"title":props.title,"sub-title":post.author}}),_vm._v(" "),_c('mu-card-text',[_vm._v("\n                            "+_vm._s(props.description)+" "),_c('a',{staticClass:"more",attrs:{"href":props.url,"target":"_blank"}},[_vm._v("More")])]),_vm._v(" "),_c('mu-card-actions',{staticClass:"card-action"},[_c('div',[_c('social-sharing',{attrs:{"url":post.url,"networks":_vm.overriddenNetworks},on:{"close":function($event){return _vm.shareFeed(post.id_feed, post.point)}},inlineTemplate:{render:function(){var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('network',{attrs:{"network":"fb"}},[_c('mu-button',{attrs:{"flat":""}},[_c('i',{staticClass:"fa fa-facebook"}),_vm._v(" Facebook"+_vm._s(_vm.picked)+"\n                                  ")])],1)},staticRenderFns:[]}}),_vm._v(" "),_c('div',{staticClass:"share-fb"},[_c('mu-button',{attrs:{"flat":""},on:{"click":function($event){return _vm.sharetw(post.id_feed, post.point, post.url)}}},[_c('i',{staticClass:"fa fa-twitter"}),_vm._v(" TWITTER\n                                ")])],1),_vm._v(" "),_c('div',{staticClass:"share-fb"},[_c('mu-button',{attrs:{"flat":""},on:{"click":function($event){return _vm.sharein(post.id_feed, post.point, post.url)}}},[_c('i',{staticClass:"fa fa-linkedin"}),_vm._v(" LINKEDIN\n                                ")])],1)],1)])]}}],null,true)},[_vm._v(" "),_c('template',{slot:"loading"},[_c('one-loading')],1)],2)],1)],1)}),0)])])}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 120 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageRewards_vue__ = __webpack_require__(40);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_df42481c_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageRewards_vue__ = __webpack_require__(164);
function injectStyle (ssrContext) {
  __webpack_require__(121)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-df42481c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageRewards_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_df42481c_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageRewards_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 121 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 122 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_LoadingRewards_vue__ = __webpack_require__(41);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_4d9e0daa_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_LoadingRewards_vue__ = __webpack_require__(124);
function injectStyle (ssrContext) {
  __webpack_require__(123)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-4d9e0daa"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_LoadingRewards_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_4d9e0daa_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_LoadingRewards_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 123 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 124 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"reward-list"},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-6"},[_c('div',{staticClass:"card card-redem"},[_c('content-loader',{attrs:{"height":270,"width":200,"speed":3,"primaryColor":"#f3f3f3","secondaryColor":"#d9d9d9"}},[_c('rect',{attrs:{"x":"4","y":"4.23","rx":"0","ry":"0","width":"192.91","height":"126"}}),_vm._v(" "),_c('rect',{attrs:{"x":"4","y":"132.27","rx":"0","ry":"0","width":"192.91","height":"28"}}),_vm._v(" "),_c('rect',{attrs:{"x":"4","y":"162.27","rx":"0","ry":"0","width":"192.1","height":"28"}}),_vm._v(" "),_c('rect',{attrs:{"x":"71","y":"219.27","rx":"0","ry":"0","width":"0","height":"0"}}),_vm._v(" "),_c('rect',{attrs:{"x":"5","y":"193.27","rx":"0","ry":"0","width":"192","height":"28"}}),_vm._v(" "),_c('rect',{attrs:{"x":"5","y":"223.27","rx":"0","ry":"0","width":"191.58","height":"40"}})])],1)]),_vm._v(" "),_c('div',{staticClass:"col-6"},[_c('div',{staticClass:"card card-redem"},[_c('content-loader',{attrs:{"height":270,"width":200,"speed":3,"primaryColor":"#f3f3f3","secondaryColor":"#d9d9d9"}},[_c('rect',{attrs:{"x":"4","y":"4.23","rx":"0","ry":"0","width":"192.91","height":"126"}}),_vm._v(" "),_c('rect',{attrs:{"x":"4","y":"132.27","rx":"0","ry":"0","width":"192.91","height":"28"}}),_vm._v(" "),_c('rect',{attrs:{"x":"4","y":"162.27","rx":"0","ry":"0","width":"192.1","height":"28"}}),_vm._v(" "),_c('rect',{attrs:{"x":"71","y":"219.27","rx":"0","ry":"0","width":"0","height":"0"}}),_vm._v(" "),_c('rect',{attrs:{"x":"5","y":"193.27","rx":"0","ry":"0","width":"192","height":"28"}}),_vm._v(" "),_c('rect',{attrs:{"x":"5","y":"223.27","rx":"0","ry":"0","width":"191.58","height":"40"}})])],1)]),_vm._v(" "),_c('div',{staticClass:"col-6"},[_c('div',{staticClass:"card card-redem"},[_c('content-loader',{attrs:{"height":270,"width":200,"speed":3,"primaryColor":"#f3f3f3","secondaryColor":"#d9d9d9"}},[_c('rect',{attrs:{"x":"4","y":"4.23","rx":"0","ry":"0","width":"192.91","height":"126"}}),_vm._v(" "),_c('rect',{attrs:{"x":"4","y":"132.27","rx":"0","ry":"0","width":"192.91","height":"28"}}),_vm._v(" "),_c('rect',{attrs:{"x":"4","y":"162.27","rx":"0","ry":"0","width":"192.1","height":"28"}}),_vm._v(" "),_c('rect',{attrs:{"x":"71","y":"219.27","rx":"0","ry":"0","width":"0","height":"0"}}),_vm._v(" "),_c('rect',{attrs:{"x":"5","y":"193.27","rx":"0","ry":"0","width":"192","height":"28"}}),_vm._v(" "),_c('rect',{attrs:{"x":"5","y":"223.27","rx":"0","ry":"0","width":"191.58","height":"40"}})])],1)]),_vm._v(" "),_c('div',{staticClass:"col-6"},[_c('div',{staticClass:"card card-redem"},[_c('content-loader',{attrs:{"height":270,"width":200,"speed":3,"primaryColor":"#f3f3f3","secondaryColor":"#d9d9d9"}},[_c('rect',{attrs:{"x":"4","y":"4.23","rx":"0","ry":"0","width":"192.91","height":"126"}}),_vm._v(" "),_c('rect',{attrs:{"x":"4","y":"132.27","rx":"0","ry":"0","width":"192.91","height":"28"}}),_vm._v(" "),_c('rect',{attrs:{"x":"4","y":"162.27","rx":"0","ry":"0","width":"192.1","height":"28"}}),_vm._v(" "),_c('rect',{attrs:{"x":"71","y":"219.27","rx":"0","ry":"0","width":"0","height":"0"}}),_vm._v(" "),_c('rect',{attrs:{"x":"5","y":"193.27","rx":"0","ry":"0","width":"192","height":"28"}}),_vm._v(" "),_c('rect',{attrs:{"x":"5","y":"223.27","rx":"0","ry":"0","width":"191.58","height":"40"}})])],1)]),_vm._v(" "),_c('div',{staticClass:"col-6"},[_c('div',{staticClass:"card card-redem"},[_c('content-loader',{attrs:{"height":270,"width":200,"speed":3,"primaryColor":"#f3f3f3","secondaryColor":"#d9d9d9"}},[_c('rect',{attrs:{"x":"4","y":"4.23","rx":"0","ry":"0","width":"192.91","height":"126"}}),_vm._v(" "),_c('rect',{attrs:{"x":"4","y":"132.27","rx":"0","ry":"0","width":"192.91","height":"28"}}),_vm._v(" "),_c('rect',{attrs:{"x":"4","y":"162.27","rx":"0","ry":"0","width":"192.1","height":"28"}}),_vm._v(" "),_c('rect',{attrs:{"x":"71","y":"219.27","rx":"0","ry":"0","width":"0","height":"0"}}),_vm._v(" "),_c('rect',{attrs:{"x":"5","y":"193.27","rx":"0","ry":"0","width":"192","height":"28"}}),_vm._v(" "),_c('rect',{attrs:{"x":"5","y":"223.27","rx":"0","ry":"0","width":"191.58","height":"40"}})])],1)]),_vm._v(" "),_c('div',{staticClass:"col-6"},[_c('div',{staticClass:"card card-redem"},[_c('content-loader',{attrs:{"height":270,"width":200,"speed":3,"primaryColor":"#f3f3f3","secondaryColor":"#d9d9d9"}},[_c('rect',{attrs:{"x":"4","y":"4.23","rx":"0","ry":"0","width":"192.91","height":"126"}}),_vm._v(" "),_c('rect',{attrs:{"x":"4","y":"132.27","rx":"0","ry":"0","width":"192.91","height":"28"}}),_vm._v(" "),_c('rect',{attrs:{"x":"4","y":"162.27","rx":"0","ry":"0","width":"192.1","height":"28"}}),_vm._v(" "),_c('rect',{attrs:{"x":"71","y":"219.27","rx":"0","ry":"0","width":"0","height":"0"}}),_vm._v(" "),_c('rect',{attrs:{"x":"5","y":"193.27","rx":"0","ry":"0","width":"192","height":"28"}}),_vm._v(" "),_c('rect',{attrs:{"x":"5","y":"223.27","rx":"0","ry":"0","width":"191.58","height":"40"}})])],1)])])])}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 125 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_LoadingReedem_vue__ = __webpack_require__(42);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_f90a406e_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_LoadingReedem_vue__ = __webpack_require__(127);
function injectStyle (ssrContext) {
  __webpack_require__(126)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-f90a406e"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_LoadingReedem_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_f90a406e_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_LoadingReedem_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 126 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 127 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"redemtions"},[_c('mu-card',{staticClass:"card-redemtios",staticStyle:{"margin":"0 auto"}},[_c('content-loader',{attrs:{"height":225,"width":405,"speed":3,"primaryColor":"#f3f3f3","secondaryColor":"#d9d9d9"}},[_c('rect',{attrs:{"x":"6","y":"4.23","rx":"0","ry":"0","width":"395.47","height":"150"}}),_vm._v(" "),_c('rect',{attrs:{"x":"71","y":"219.27","rx":"0","ry":"0","width":"0","height":"0"}}),_vm._v(" "),_c('rect',{attrs:{"x":"29.89","y":"167.67","rx":"0","ry":"0","width":"156","height":"19"}}),_vm._v(" "),_c('rect',{attrs:{"x":"30.89","y":"195.67","rx":"0","ry":"0","width":"211","height":"14"}})])],1),_vm._v(" "),_c('mu-card',{staticClass:"card-redemtios",staticStyle:{"margin":"0 auto"}},[_c('content-loader',{attrs:{"height":225,"width":405,"speed":3,"primaryColor":"#f3f3f3","secondaryColor":"#d9d9d9"}},[_c('rect',{attrs:{"x":"6","y":"4.23","rx":"0","ry":"0","width":"395.47","height":"150"}}),_vm._v(" "),_c('rect',{attrs:{"x":"71","y":"219.27","rx":"0","ry":"0","width":"0","height":"0"}}),_vm._v(" "),_c('rect',{attrs:{"x":"29.89","y":"167.67","rx":"0","ry":"0","width":"156","height":"19"}}),_vm._v(" "),_c('rect',{attrs:{"x":"30.89","y":"195.67","rx":"0","ry":"0","width":"211","height":"14"}})])],1),_vm._v(" "),_c('mu-card',{staticClass:"card-redemtios",staticStyle:{"margin":"0 auto"}},[_c('content-loader',{attrs:{"height":225,"width":405,"speed":3,"primaryColor":"#f3f3f3","secondaryColor":"#d9d9d9"}},[_c('rect',{attrs:{"x":"6","y":"4.23","rx":"0","ry":"0","width":"395.47","height":"150"}}),_vm._v(" "),_c('rect',{attrs:{"x":"71","y":"219.27","rx":"0","ry":"0","width":"0","height":"0"}}),_vm._v(" "),_c('rect',{attrs:{"x":"29.89","y":"167.67","rx":"0","ry":"0","width":"156","height":"19"}}),_vm._v(" "),_c('rect',{attrs:{"x":"30.89","y":"195.67","rx":"0","ry":"0","width":"211","height":"14"}})])],1),_vm._v(" "),_c('mu-card',{staticClass:"card-redemtios",staticStyle:{"margin":"0 auto"}},[_c('content-loader',{attrs:{"height":225,"width":405,"speed":3,"primaryColor":"#f3f3f3","secondaryColor":"#d9d9d9"}},[_c('rect',{attrs:{"x":"6","y":"4.23","rx":"0","ry":"0","width":"395.47","height":"150"}}),_vm._v(" "),_c('rect',{attrs:{"x":"71","y":"219.27","rx":"0","ry":"0","width":"0","height":"0"}}),_vm._v(" "),_c('rect',{attrs:{"x":"29.89","y":"167.67","rx":"0","ry":"0","width":"156","height":"19"}}),_vm._v(" "),_c('rect',{attrs:{"x":"30.89","y":"195.67","rx":"0","ry":"0","width":"211","height":"14"}})])],1)],1)}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 128 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_BeatLoader_vue__ = __webpack_require__(43);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_7b457085_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_vue_loader_lib_selector_type_template_index_0_BeatLoader_vue__ = __webpack_require__(130);
function injectStyle (ssrContext) {
  __webpack_require__(129)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_BeatLoader_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_7b457085_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_vue_loader_lib_selector_type_template_index_0_BeatLoader_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 129 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 130 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{directives:[{name:"show",rawName:"v-show",value:(_vm.loading),expression:"loading"}],staticClass:"v-spinner"},[_c('div',{staticClass:"v-beat v-beat-odd",style:(_vm.spinnerStyle)}),_c('div',{staticClass:"v-beat v-beat-even",style:(_vm.spinnerStyle)}),_c('div',{staticClass:"v-beat v-beat-odd",style:(_vm.spinnerStyle)})])}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 131 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIj8+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA1MTIgNTEyOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4IiBjbGFzcz0iIj48Zz48Zz4KCTxnPgoJCTxwYXRoIGQ9Ik00NTQuOTc2LDQzLjA1NmMtMy41OTEtMy41ODgtOS40MTEtMy41OTEtMTMuMDAzLDBsLTk1LjMyOSw5NS4zMjZjLTMuNTkyLDMuNTkxLTMuNTkyLDkuNDExLTAuMDAxLDEzLjAwMyAgICBjMS43OTYsMS43OTUsNC4xNDgsMi42OTIsNi41MDIsMi42OTJjMi4zNTIsMCw0LjcwNi0wLjg5Nyw2LjUwMi0yLjY5Mmw5NS4zMjktOTUuMzI2ICAgIEM0NTguNTY2LDUyLjQ2Nyw0NTguNTY2LDQ2LjY0Niw0NTQuOTc2LDQzLjA1NnoiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiM2Yzc1N2QiIGZpbGw9IiM2Yzc1N2QiLz4KCTwvZz4KPC9nPjxnPgoJPGc+CgkJPHBhdGggZD0iTTQ4MC45LDY4LjU3MWMtMy41OTItMy41ODktOS40MTItMy41ODktMTMuMDAzLDBsLTk1LjMyOCw5NS4zMjhjLTMuNTksMy41OTItMy41OSw5LjQxMi0wLjAwMSwxMy4wMDMgICAgYzEuNzk2LDEuNzk1LDQuMTUsMi42OTIsNi41MDIsMi42OTJjMi4zNTIsMCw0LjcwNi0wLjg5Nyw2LjUwMi0yLjY5Mkw0ODAuOSw4MS41NzNDNDg0LjQ5MSw3Ny45ODIsNDg0LjQ5MSw3Mi4xNjEsNDgwLjksNjguNTcxeiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgY2xhc3M9ImFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iIzZjNzU3ZCIgZmlsbD0iIzZjNzU3ZCIvPgoJPC9nPgo8L2c+PGc+Cgk8Zz4KCQk8cGF0aCBkPSJNMzkwLjk2OSwyMjkuNTQ4TDUwOS4zMDcsMTExLjIxYzMuNTkxLTMuNTkyLDMuNTkxLTkuNDEyLDAtMTMuMDAzYy0zLjU5Mi0zLjU4OC05LjQxMi0zLjU5MS0xMy4wMDMsMEwzNzcuOTY4LDIxNi41NDUgICAgYy0xNC41NzksMTQuNTc4LTM2Ljg3MSwxOC40MDMtNTUuNDcsOS41MjFjLTMuNy0xLjc2Ni04LjExOC0wLjg3My0xMC44MzksMi4xOTNsLTQwLjIwOSw0NS4zMDMgICAgYy0wLjAxNiwwLjAxNy0wLjAzNCwwLjAzMi0wLjA1LDAuMDVjLTAuMDE2LDAuMDE4LTAuMDI4LDAuMDM3LTAuMDQ0LDAuMDU1TDk3LjU2Nyw0NjkuNDY0Yy0wLjA0NCwwLjA1LTAuMDg4LDAuMTAxLTAuMTMxLDAuMTUyICAgIGMtMC41NzMsMC42NzMtMS4xMTQsMS4yNjQtMS42NjIsMS44MTNjLTExLjQ3NSwxMS40NzQtMzAuMTQ1LDExLjQ3My00MS42MTUsMGMtNS41NTgtNS41NTctOC42MTktMTIuOTQ4LTguNjE5LTIwLjgwOCAgICBzMy4wNjEtMTUuMjUsOC42MjktMjAuODE4YzAuNTM0LTAuNTM3LDEuMTI0LTEuMDc2LDEuODAzLTEuNjU0YzAuMDUyLTAuMDQzLDAuMTAyLTAuMDg3LDAuMTUyLTAuMTMxbDEwOC42NDgtOTYuNDM0ICAgIGMzLjc5OC0zLjM3MSw0LjE0My05LjE4MiwwLjc3My0xMi45NzljLTMuMzcxLTMuNzk5LTkuMTgzLTQuMTQ2LTEyLjk3OS0wLjc3M0w0My45OSw0MTQuMjAyICAgIGMtMS4wMzgsMC44ODUtMS45NjcsMS43NDItMi44MzMsMi42MDljLTkuMDMxLDkuMDMxLTE0LjAwNSwyMS4wMzgtMTQuMDA1LDMzLjgxYzAsMTIuNzcxLDQuOTczLDI0Ljc3OSwxNC4wMDQsMzMuODEgICAgYzkuMzIzLDkuMzIxLDIxLjU2NiwxMy45ODIsMzMuODEyLDEzLjk4MWMxMi4yNDEsMCwyNC40ODctNC42NTksMzMuODAzLTEzLjk3NmMwLjg4My0wLjg4MSwxLjczOC0xLjgxMSwyLjYxNi0yLjgzOSAgICBsMTMyLjQzMi0xNDkuMjA2bDEzMi40MzQsMTQ5LjIwOGMwLjg3NywxLjAyOSwxLjczMiwxLjk1OSwyLjYwOSwyLjgzNGM5LjMyMSw5LjMyMSwyMS41NjUsMTMuOTgxLDMzLjgwOSwxMy45ODEgICAgYzEyLjI0NSwwLDI0LjQ5LTQuNjYxLDMzLjgxMi0xMy45ODJjMTguNjQtMTguNjQzLDE4LjY0LTQ4Ljk3NSwwLjAwNS02Ny42MTNjLTAuODgtMC44ODMtMS44MTEtMS43MzgtMi44MzgtMi42MTUgICAgbC0xNTIuMzkxLTEzNS4yNmwyOS44MTUtMzMuNTkyQzM0NS4yMzEsMjUzLjkxMSwzNzIuNTY1LDI0Ny45NDksMzkwLjk2OSwyMjkuNTQ4eiBNNDMxLjUxNCw0MjguMDE5ICAgIGMwLjA1LDAuMDQ0LDAuMTAxLDAuMDg4LDAuMTUyLDAuMTMxYzAuNjczLDAuNTczLDEuMjY0LDEuMTE0LDEuODEzLDEuNjYyYzExLjQ3MywxMS40NzQsMTEuNDczLDMwLjE0MywwLjAwMSw0MS42MTUgICAgYy0xMS40NzUsMTEuNDczLTMwLjE0NCwxMS40NzMtNDEuNjIyLTAuMDA1Yy0wLjU0My0wLjU0My0xLjA4Ni0xLjEzNS0xLjY1OS0xLjgwOGMtMC4wNDMtMC4wNTEtMC4wODctMC4xMDItMC4xMzEtMC4xNTIgICAgTDI1Ni4xMTEsMzE4LjU0bDIyLjkzOS0yNS44NDVMNDMxLjUxNCw0MjguMDE5eiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgY2xhc3M9ImFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iIzZjNzU3ZCIgZmlsbD0iIzZjNzU3ZCIvPgoJPC9nPgo8L2c+PGc+Cgk8Zz4KCQk8cGF0aCBkPSJNNDI3LjM3OSwxNi4yNzhjLTMuNTkyLTMuNTg5LTkuNDEyLTMuNTg5LTEzLjAwMywwTDI5Ni4wNCwxMzQuNjE3Yy0xOC40MDYsMTguNDA0LTI0LjM2Niw0NS43MzUtMTUuODA3LDY5Ljg5NyAgICBsLTM2LjQxNSwzMi4zMjJsLTM0Ljk5OC0zMS4wNjRjMTguMDk5LTM1LjYxMiwxMS40OTItNzguODUxLTE3LjIyMi0xMDcuNTY2Yy0yOC41MTgtMjguNTE4LTYxLjU1LTQ4LjU4Ny05My4wMTEtNTYuNTA4ICAgIGMtMzIuODQzLTguMjY3LTYxLjAwMS0yLjc0OC03OS4yOTUsMTUuNTQ1cy0yMy44MTQsNDYuNDU1LTE1LjU0NSw3OS4yOTZjNy45MjIsMzEuNDU5LDI3Ljk5LDY0LjQ5MSw1Ni41MSw5My4wMDggICAgYzI4LjcxNCwyOC43MTYsNzEuOTUyLDM1LjMyMywxMDcuNTY2LDE3LjIyM2MwLDAsMzQuMjI1LDM4LjU1OCwzNC42NTksMzkuMDQ4YzEuODE3LDIuMDQ3LDQuMzQyLDMuMDksNi44OCwzLjA5ICAgIGMyLjE3MSwwLDQuMzQ5LTAuNzY0LDYuMS0yLjMxOGw4MS44NjYtNzIuNjY0YzMuMDY1LTIuNzIxLDMuOTU4LTcuMTQxLDIuMTkzLTEwLjgzOWMtOC44ODQtMTguNjAxLTUuMDU4LTQwLjg5MSw5LjUyMS01NS40NjggICAgTDQyNy4zNzksMjkuMjgxQzQzMC45NywyNS42ODksNDMwLjk3LDE5Ljg2OSw0MjcuMzc5LDE2LjI3OHogTTIxMC4xMzEsMjY2LjczN2wtMzMuNDY4LTM3LjcwNiAgICBjLTIuOTY0LTMuMzM5LTcuOTAyLTQuMDY5LTExLjcwMi0xLjcyMmMtMjkuNDc4LDE4LjE3Ny02Ny4xODgsMTMuNzU0LTkxLjcwNC0xMC43NjNjLTI2LjIwNC0yNi4yMDMtNDQuNTU3LTU2LjIxLTUxLjY4LTg0LjQ5NyAgICBjLTYuNjI1LTI2LjMxNy0yLjgxOS00OC4yNjcsMTAuNzE4LTYxLjgwM0M0NS44Myw1Ni43MDgsNjcuNzgsNTIuOTA0LDk0LjA5Niw1OS41M2MyOC4yODcsNy4xMjIsNTguMjk2LDI1LjQ3NCw4NC40OTgsNTEuNjc4ICAgIGMyNC41MTYsMjQuNTE1LDI4Ljk0LDYyLjIyNSwxMC43NjIsOTEuNzAzYy0yLjM0NCwzLjgwMS0xLjYxNyw4LjczNywxLjcyMiwxMS43MDJsMzguODg4LDM0LjUxNkwyMTAuMTMxLDI2Ni43Mzd6IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iYWN0aXZlLXBhdGgiIGRhdGEtb2xkX2NvbG9yPSIjNmM3NTdkIiBmaWxsPSIjNmM3NTdkIi8+Cgk8L2c+CjwvZz48L2c+IDwvc3ZnPgo="

/***/ }),
/* 132 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIj8+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDU5Ljk5OSA1OS45OTkiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDU5Ljk5OSA1OS45OTk7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiIGNsYXNzPSIiPjxnPjxnPgoJPHBhdGggZD0iTTM2LjE3Niw0OS45OTljLTEuOTMsMC0zLjUsMS41Ny0zLjUsMy41czEuNTcsMy41LDMuNSwzLjVzMy41LTEuNTcsMy41LTMuNVMzOC4xMDUsNDkuOTk5LDM2LjE3Niw0OS45OTl6IE0zNi4xNzYsNTQuOTk5ICAgYy0wLjgyNywwLTEuNS0wLjY3My0xLjUtMS41czAuNjczLTEuNSwxLjUtMS41czEuNSwwLjY3MywxLjUsMS41UzM3LjAwMyw1NC45OTksMzYuMTc2LDU0Ljk5OXoiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiM2Yzc1N2QiIGZpbGw9IiM2Yzc1N2QiLz4KCTxwYXRoIGQ9Ik00MC42NzYsMzYuOTk5YzIuMjA2LDAsNC0xLjc5NCw0LTRzLTEuNzk0LTQtNC00cy00LDEuNzk0LTQsNFMzOC40NjksMzYuOTk5LDQwLjY3NiwzNi45OTl6IE00MC42NzYsMzAuOTk5ICAgYzEuMTAzLDAsMiwwLjg5NywyLDJzLTAuODk3LDItMiwycy0yLTAuODk3LTItMlMzOS41NzMsMzAuOTk5LDQwLjY3NiwzMC45OTl6IiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iYWN0aXZlLXBhdGgiIGRhdGEtb2xkX2NvbG9yPSIjNmM3NTdkIiBmaWxsPSIjNmM3NTdkIi8+Cgk8cGF0aCBkPSJNMTYuNjc2LDExLjk5OWMwLjI1NiwwLDAuNTEyLTAuMDk4LDAuNzA3LTAuMjkzbDItMmMwLjM5MS0wLjM5MSwwLjM5MS0xLjAyMywwLTEuNDE0cy0xLjAyMy0wLjM5MS0xLjQxNCwwbC0yLDIgICBjLTAuMzkxLDAuMzkxLTAuMzkxLDEuMDIzLDAsMS40MTRDMTYuMTY0LDExLjkwMSwxNi40MiwxMS45OTksMTYuNjc2LDExLjk5OXoiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiM2Yzc1N2QiIGZpbGw9IiM2Yzc1N2QiLz4KCTxwYXRoIGQ9Ik01Mi42NzYsMjkuNzQ3YzAtMC44OC0wLjM0My0xLjcwNy0wLjk2NS0yLjMyOWwtMC41NTctMC41NTdjMC45NDktMC44NCwxLjUyMS0yLjA1NSwxLjUyMS0zLjM2MiAgIGMwLTIuNDgxLTIuMDE5LTQuNS00LjUtNC41Yy0wLjE4MiwwLTAuMzYyLDAuMDE4LTAuNTQsMC4wNGMwLjAyMi0wLjE3OSwwLjA0LTAuMzU3LDAuMDQtMC41NGMwLTIuNDgxLTIuMDE5LTQuNS00LjUtNC41ICAgYy0wLjE4MiwwLTAuMzYyLDAuMDE4LTAuNTQsMC4wNGMwLjAyMi0wLjE3OSwwLjA0LTAuMzU3LDAuMDQtMC41NGMwLTIuNDgxLTIuMDE5LTQuNS00LjUtNC41Yy0wLjE4MiwwLTAuMzYyLDAuMDE4LTAuNTQsMC4wNCAgIGMwLjAyMi0wLjE3OSwwLjA0LTAuMzU3LDAuMDQtMC41NGMwLTIuNDgxLTIuMDE5LTQuNS00LjUtNC41Yy0xLjMwOCwwLTIuNTIyLDAuNTczLTMuMzYyLDEuNTIxbC0wLjc5NC0wLjc5NGwtMy43NjMtMy43NjMgICBjLTEuMjg1LTEuMjg1LTMuMzc1LTEuMjg1LTQuNjU4LDBMOC4yODYsMTMuMjc1Yy0xLjI4MywxLjI4NS0xLjI4MywzLjM3NCwwLDQuNjU5bDMuNzYzLDMuNzYzbDQuOTEsNC45MSAgIGMtMS4zNTYsMC43NzYtMi4yODMsMi4yMjEtMi4yODMsMy44OTJjMCwxLjU2MywwLjgwMywyLjk0MSwyLjAxNywzLjc0OGMwLjA2MSwwLjA4MSwwLjEzMiwwLjE2MSwwLjIyNywwLjI0MmwxMC43NTYsMTAuNzE1djEuNzk2ICAgaC0ydjEzaDE4aDh2LTEzaC0yVjQzLjk3YzEuOTEzLTEuNjIxLDMtMy45NTIsMy02LjQ3MWMwLTEuNzQtMC41NDMtMy40My0xLjUzNi00Ljg1MmwwLjU3MS0wLjU3MSAgIEM1Mi4zMzMsMzEuNDU0LDUyLjY3NiwzMC42MjcsNTIuNjc2LDI5Ljc0N3ogTTUwLjY3NiwyMy40OTljMCwwLjc2OC0wLjM1NCwxLjQ3OS0wLjkzNywxLjk0N2wtMy41MTEtMy41MTEgICBjMC40NjgtMC41ODMsMS4xOC0wLjkzNywxLjk0Ny0wLjkzN0M0OS41NTQsMjAuOTk5LDUwLjY3NiwyMi4xMiw1MC42NzYsMjMuNDk5eiBNNDUuNjc2LDE4LjQ5OWMwLDAuNzY4LTAuMzU0LDEuNDc5LTAuOTM3LDEuOTQ3ICAgbC0zLjUxMS0zLjUxMWMwLjQ2OC0wLjU4MywxLjE4LTAuOTM3LDEuOTQ3LTAuOTM3QzQ0LjU1NCwxNS45OTksNDUuNjc2LDE3LjEyLDQ1LjY3NiwxOC40OTl6IE00MC42NzYsMTMuNDk5ICAgYzAsMC43NjgtMC4zNTQsMS40NzktMC45MzcsMS45NDdsLTMuNTExLTMuNTExYzAuNDY4LTAuNTgzLDEuMTgtMC45MzcsMS45NDctMC45MzdDMzkuNTU0LDEwLjk5OSw0MC42NzYsMTIuMTIsNDAuNjc2LDEzLjQ5OXogICAgTTMzLjE3Niw1Ljk5OWMxLjM3OSwwLDIuNSwxLjEyMSwyLjUsMi41YzAsMC43NjgtMC4zNTQsMS40NzktMC45MzcsMS45NDdsLTMuNTExLTMuNTExQzMxLjY5Niw2LjM1MywzMi40MDgsNS45OTksMzMuMTc2LDUuOTk5eiAgICBNOS43LDE2LjUyMWMtMC41MDQtMC41MDUtMC41MDQtMS4zMjYsMC0xLjgzMUwyMi4wMTIsMi4zNzhjMC4xMjUtMC4xMjUsMC4yNzEtMC4yMTksMC40MjYtMC4yODIgICBjMC4wNy0wLjAyOSwwLjE0OS0wLjAyOCwwLjIyMy0wLjA0NGMwLjE3OC0wLjAzOCwwLjM1NC0wLjAzOCwwLjUzMiwwYzAuMDc0LDAuMDE2LDAuMTUzLDAuMDE1LDAuMjIzLDAuMDQ0ICAgYzAuMTU1LDAuMDYzLDAuMywwLjE1NywwLjQyNiwwLjI4MmwyLjM0OSwyLjM0OUwxMi4wNDksMTguODY5TDkuNywxNi41MjF6IE0yNy42MDUsNi4xNDFsMS4xNzgsMS4xNzhsNSw1bDAuNTczLDAuNTczbDQuNDI3LDQuNDI3ICAgbDAuNTczLDAuNTczbDQuNDI3LDQuNDI3bDAuNTczLDAuNTczbDUsNWwwLjk0LDAuOTRjMC41MDUsMC41MDUsMC41MDUsMS4zMjUsMCwxLjgzTDQ5Ljg1OSwzMS4xbDAsMEwzNy45ODUsNDIuOTc1ICAgYy0wLjUwMywwLjUwNC0xLjMyNiwwLjUwNi0xLjgzMSwwbC03Ljk3NS03Ljk3NmgwLjk5NmMyLjQ4MSwwLDQuNS0yLjAxOSw0LjUtNC41cy0yLjAxOS00LjUtNC41LTQuNWgtOS45OTdsLTUuNzE2LTUuNzE2ICAgTDI3LjYwNSw2LjE0MXogTTE4LjYzNiwyOC4wNjFsMC40MTUtMC4wNDdjMC4wNDEtMC4wMDYsMC4wODItMC4wMTUsMC4xMjQtMC4wMTVoMTBjMS4zNzksMCwyLjUsMS4xMjEsMi41LDIuNXMtMS4xMjEsMi41LTIuNSwyLjUgICBoLTIuOTk2aC0yLjgyOGgtNC4xNzZjLTEuMzc5LDAtMi41LTEuMTIxLTIuNS0yLjVDMTYuNjc2LDI5LjMwNiwxNy41MTcsMjguMzA5LDE4LjYzNiwyOC4wNjF6IE0yNy42NzYsNTcuOTk5di05aDguNDI2ICAgYzMuMDczLDAsNS41NzQsMi41MDEsNS41NzQsNS41NzR2My40MjZIMjcuNjc2eiBNNDkuNjc2LDU3Ljk5OWgtNnYtMy40MjZjMC0yLjIwNi0wLjk1NC00LjE4OC0yLjQ2NC01LjU3NGg4LjQ2NFY1Ny45OTl6ICAgIE00OS43MDEsMzQuMDg3YzAuNjM0LDEuMDIxLDAuOTc1LDIuMjAyLDAuOTc1LDMuNDEyYzAsMi4wNTUtMC45NDgsMy45NDYtMi42MDIsNS4xOTFsLTAuMzk4LDAuM3Y0LjAwOUgzNi4xMDFoLTYuNDI2di0yLjcxOCAgIGwtMC4zNTMtMC4yOTljLTAuMDQ1LTAuMDM5LTAuMDkzLTAuMDc1LTAuMTgtMC4xNGwtOC44NzctOC44NDRoNS4wODVsOS4zODksOS4zOWMwLjY0MywwLjY0MiwxLjQ4NiwwLjk2MywyLjMyOSwwLjk2MyAgIGMwLjg0NCwwLDEuNjg4LTAuMzIxLDIuMzMtMC45NjNMNDkuNzAxLDM0LjA4N3oiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiM2Yzc1N2QiIGZpbGw9IiM2Yzc1N2QiLz4KPC9nPjwvZz4gPC9zdmc+Cg=="

/***/ }),
/* 133 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIj8+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA1MTEuOTk5IDUxMS45OTkiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDUxMS45OTkgNTExLjk5OTsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSI1MTJweCIgaGVpZ2h0PSI1MTJweCIgY2xhc3M9IiI+PGc+PGc+Cgk8Zz4KCQk8cGF0aCBkPSJNMjE0LjY4NSw0MDIuODI4Yy0yNC44MjksMC00NS4wMjksMjAuMi00NS4wMjksNDUuMDI5YzAsMjQuODI5LDIwLjIsNDUuMDI5LDQ1LjAyOSw0NS4wMjlzNDUuMDI5LTIwLjIsNDUuMDI5LTQ1LjAyOSAgICBDMjU5LjcxMyw0MjMuMDI4LDIzOS41MTMsNDAyLjgyOCwyMTQuNjg1LDQwMi44Mjh6IE0yMTQuNjg1LDQ2Ny43NDJjLTEwLjk2NiwwLTE5Ljg4Ny04LjkyMi0xOS44ODctMTkuODg3ICAgIGMwLTEwLjk2Niw4LjkyMi0xOS44ODcsMTkuODg3LTE5Ljg4N3MxOS44ODcsOC45MjIsMTkuODg3LDE5Ljg4N0MyMzQuNTcyLDQ1OC44MjIsMjI1LjY1LDQ2Ny43NDIsMjE0LjY4NSw0NjcuNzQyeiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgY2xhc3M9ImFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iIzZjNzU3ZCIgZmlsbD0iIzZjNzU3ZCIvPgoJPC9nPgo8L2c+PGc+Cgk8Zz4KCQk8cGF0aCBkPSJNMzcyLjYzLDQwMi44MjhjLTI0LjgyOSwwLTQ1LjAyOSwyMC4yLTQ1LjAyOSw0NS4wMjljMCwyNC44MjksMjAuMiw0NS4wMjksNDUuMDI5LDQ1LjAyOXM0NS4wMjktMjAuMiw0NS4wMjktNDUuMDI5ICAgIEM0MTcuNjU4LDQyMy4wMjgsMzk3LjQ1OCw0MDIuODI4LDM3Mi42Myw0MDIuODI4eiBNMzcyLjYzLDQ2Ny43NDJjLTEwLjk2NiwwLTE5Ljg4Ny04LjkyMi0xOS44ODctMTkuODg3ICAgIGMwLTEwLjk2Niw4LjkyMi0xOS44ODcsMTkuODg3LTE5Ljg4N2MxMC45NjYsMCwxOS44ODcsOC45MjIsMTkuODg3LDE5Ljg4N0MzOTIuNTE3LDQ1OC44MjIsMzgzLjU5NSw0NjcuNzQyLDM3Mi42Myw0NjcuNzQyeiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgY2xhc3M9ImFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iIzZjNzU3ZCIgZmlsbD0iIzZjNzU3ZCIvPgoJPC9nPgo8L2c+PGc+Cgk8Zz4KCQk8cGF0aCBkPSJNMzgzLjcxNiwxNjUuNzU1SDIwMy41NjdjLTYuOTQzLDAtMTIuNTcxLDUuNjI4LTEyLjU3MSwxMi41NzFjMCw2Ljk0Myw1LjYyOSwxMi41NzEsMTIuNTcxLDEyLjU3MWgxODAuMTQ5ICAgIGM2Ljk0MywwLDEyLjU3MS01LjYyOCwxMi41NzEtMTIuNTcxQzM5Ni4yODcsMTcxLjM4MiwzOTAuNjU5LDE2NS43NTUsMzgzLjcxNiwxNjUuNzU1eiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgY2xhc3M9ImFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iIzZjNzU3ZCIgZmlsbD0iIzZjNzU3ZCIvPgoJPC9nPgo8L2c+PGc+Cgk8Zz4KCQk8cGF0aCBkPSJNMzczLjkxMSwyMzEuMDM1SDIxMy4zNzNjLTYuOTQzLDAtMTIuNTcxLDUuNjI4LTEyLjU3MSwxMi41NzFzNS42MjgsMTIuNTcxLDEyLjU3MSwxMi41NzFoMTYwLjUzNyAgICBjNi45NDMsMCwxMi41NzEtNS42MjgsMTIuNTcxLTEyLjU3MUMzODYuNDgxLDIzNi42NjQsMzgwLjg1MywyMzEuMDM1LDM3My45MTEsMjMxLjAzNXoiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiM2Yzc1N2QiIGZpbGw9IiM2Yzc1N2QiLz4KCTwvZz4KPC9nPjxnPgoJPGc+CgkJPHBhdGggZD0iTTUwNi4zNDEsMTA5Ljc0NGMtNC43OTQtNS44ODQtMTEuODk4LTkuMjU4LTE5LjQ4OS05LjI1OEg5NS4yNzhMODcuMzcsNjIuMDk3Yy0xLjY1MS04LjAwOC03LjExMy0xNC43MzItMTQuNjE0LTE3Ljk4OSAgICBsLTU1LjE3Ny0yMy45NWMtNi4zNy0yLjc2Ny0xMy43NzMsMC4xNTYtMTYuNTM2LDYuNTI0Yy0yLjc2Niw2LjM3LDAuMTU3LDEzLjc3NCw2LjUyNCwxNi41MzdMNjIuNzQ1LDY3LjE3bDYwLjgyNiwyOTUuMjYxICAgIGMyLjM5NiwxMS42MjgsMTIuNzUyLDIwLjA2OCwyNC42MjUsMjAuMDY4aDMwMS4xNjZjNi45NDMsMCwxMi41NzEtNS42MjgsMTIuNTcxLTEyLjU3MWMwLTYuOTQzLTUuNjI4LTEyLjU3MS0xMi41NzEtMTIuNTcxICAgIEgxNDguMTk3bC03LjM5OS0zNS45MTZINDUxLjY5YzExLjg3MiwwLDIyLjIyOS04LjQ0LDI0LjYyNC0yMC4wNjhsMzUuMTYzLTE3MC42NzUgICAgQzUxMy4wMDgsMTIzLjI2Niw1MTEuMTM2LDExNS42MjcsNTA2LjM0MSwxMDkuNzQ0eiBNNDUxLjY5LDI5Ni4zMDFIMTM1LjYxOWwtMzUuMTYxLTE3MC42NzRsMzg2LjM5MywwLjAwMUw0NTEuNjksMjk2LjMwMXoiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiM2Yzc1N2QiIGZpbGw9IiM2Yzc1N2QiLz4KCTwvZz4KPC9nPjwvZz4gPC9zdmc+Cg=="

/***/ }),
/* 134 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIj8+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB2aWV3Qm94PSIwIDAgNTEyLjAwMSA1MTIiIHdpZHRoPSI1MTJweCIgaGVpZ2h0PSI1MTJweCIgY2xhc3M9IiI+PGc+PHBhdGggZD0ibTEzMS4yMTg3NSAyMTYuODM1OTM4YzIuNjMyODEyIDAgNS4yMTA5MzgtMS4wNzAzMTMgNy4wNzAzMTItMi45Mjk2ODggMS44NTkzNzYtMS44NjMyODEgMi45Mjk2ODgtNC40NDE0MDYgMi45Mjk2ODgtNy4wNzAzMTIgMC0yLjYzMjgxMy0xLjA3MDMxMi01LjIxMDkzOC0yLjkyOTY4OC03LjA3MDMxMy0xLjg1OTM3NC0xLjg1OTM3NS00LjQzNzUtMi45Mjk2ODctNy4wNzAzMTItMi45Mjk2ODctMi42NDA2MjUgMC01LjIwNzAzMSAxLjA3MDMxMi03LjA3MDMxMiAyLjkyOTY4Ny0xLjg1OTM3NiAxLjg1OTM3NS0yLjkyOTY4OCA0LjQzNzUtMi45Mjk2ODggNy4wNzAzMTMgMCAyLjYyODkwNiAxLjA3MDMxMiA1LjIwNzAzMSAyLjkyOTY4OCA3LjA3MDMxMiAxLjg2MzI4MSAxLjg1OTM3NSA0LjQ0MTQwNiAyLjkyOTY4OCA3LjA3MDMxMiAyLjkyOTY4OHptMCAwIiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iYWN0aXZlLXBhdGgiIGRhdGEtb2xkX2NvbG9yPSIjNmM3NTdkIiBmaWxsPSIjNmM3NTdkIi8+PHBhdGggZD0ibTI3NC40Njg3NSAyMDMuNTkzNzVjLTEuODY3MTg4LTEuODU5Mzc1LTQuNDM3NS0yLjkyOTY4OC03LjA3ODEyNS0yLjkyOTY4OC0yLjYyODkwNiAwLTUuMjEwOTM3IDEuMDcwMzEzLTcuMDcwMzEzIDIuOTI5Njg4LTEuODU5Mzc0IDEuODU5Mzc1LTIuOTI5Njg3IDQuNDQxNDA2LTIuOTI5Njg3IDcuMDcwMzEyIDAgMi42Mjg5MDcgMS4wNzAzMTMgNS4yMTA5MzggMi45Mjk2ODcgNy4wNzAzMTMgMS44NTkzNzYgMS44NTkzNzUgNC40NDE0MDcgMi45Mjk2ODcgNy4wNzAzMTMgMi45Mjk2ODcgMi42NDA2MjUgMCA1LjIxMDkzNy0xLjA3MDMxMiA3LjA3ODEyNS0yLjkyOTY4NyAxLjg1OTM3NS0xLjg1OTM3NSAyLjkyMTg3NS00LjQyOTY4NyAyLjkyMTg3NS03LjA3MDMxMyAwLTIuNjI4OTA2LTEuMDYyNS01LjIxMDkzNy0yLjkyMTg3NS03LjA3MDMxMnptMCAwIiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iYWN0aXZlLXBhdGgiIGRhdGEtb2xkX2NvbG9yPSIjNmM3NTdkIiBmaWxsPSIjNmM3NTdkIi8+PHBhdGggZD0ibTMwMi4zOTg0MzggMTg1LjY2NDA2MmMyLjYzMjgxMiAwIDUuMjEwOTM3LTEuMDcwMzEyIDcuMDcwMzEyLTIuOTI5Njg3czIuOTIxODc1LTQuNDQxNDA2IDIuOTIxODc1LTcuMDcwMzEzYzAtMi42Mjg5MDYtMS4wNjI1LTUuMjEwOTM3LTIuOTIxODc1LTcuMDcwMzEycy00LjQzNzUtMi45Mjk2ODgtNy4wNzAzMTItMi45Mjk2ODhjLTIuNjM2NzE5IDAtNS4yMTg3NSAxLjA3MDMxMy03LjA3ODEyNiAyLjkyOTY4OC0xLjg1OTM3NCAxLjg1OTM3NS0yLjkyOTY4NyA0LjQ0MTQwNi0yLjkyOTY4NyA3LjA3MDMxMiAwIDIuNjI4OTA3IDEuMDcwMzEzIDUuMjEwOTM4IDIuOTI5Njg3IDcuMDcwMzEzIDEuODU5Mzc2IDEuODU5Mzc1IDQuNDQxNDA3IDIuOTI5Njg3IDcuMDc4MTI2IDIuOTI5Njg3em0wIDAiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiM2Yzc1N2QiIGZpbGw9IiM2Yzc1N2QiLz48cGF0aCBkPSJtNDgyIDI0NS45OTYwOTRoLTI3LjkwMjM0NGw0OS4xMTcxODgtNDkuMTE3MTg4YzUuNjY0MDYyLTUuNjY3OTY4IDguNzg1MTU2LTEzLjIwMzEyNSA4Ljc4NTE1Ni0yMS4yMTQ4NDQgMC04LjAxMTcxOC0zLjEyMTA5NC0xNS41NDY4NzQtOC43ODUxNTYtMjEuMjEwOTM3bC00NC41NTA3ODItNDQuNTUwNzgxYy0zLjkwMjM0My0zLjkwMjM0NC0xMC4yMzQzNzQtMy45MDIzNDQtMTQuMTQwNjI0IDAtNS42NjQwNjMgNS42Njc5NjgtMTMuMTk5MjE5IDguNzg5MDYyLTIxLjIxMDkzOCA4Ljc4OTA2Mi04LjAxNTYyNSAwLTE1LjU0Njg3NS0zLjEyMTA5NC0yMS4yMTQ4NDQtOC43ODkwNjItNS42Njc5NjgtNS42NjQwNjMtOC43ODUxNTYtMTMuMTk5MjE5LTguNzg1MTU2LTIxLjIxMDkzOCAwLTguMDE1NjI1IDMuMTIxMDk0LTE1LjU0Njg3NSA4Ljc4NTE1Ni0yMS4yMTQ4NDQgMS44NzUtMS44NzUgMi45Mjk2ODgtNC40MTc5NjggMi45Mjk2ODgtNy4wNzAzMTJzLTEuMDU0Njg4LTUuMTk1MzEyLTIuOTI5Njg4LTcuMDcwMzEybC00NC41NDY4NzUtNDQuNTQ2ODc2Yy01LjY2Nzk2OS01LjY2Nzk2OC0xMy4xOTkyMTktOC43ODkwNjItMjEuMjE0ODQzLTguNzg5MDYyLTguMDExNzE5IDAtMTUuNTQ2ODc2IDMuMTIxMDk0LTIxLjIxMDkzOCA4Ljc4OTA2MmwtMTY1LjE2NDA2MiAxNjUuMTYwMTU3Yy0zLjkwNjI1IDMuOTAyMzQzLTMuOTA2MjUgMTAuMjM0Mzc1IDAgMTQuMTQwNjI1czEwLjIzODI4MSAzLjkwNjI1IDE0LjE0NDUzMSAwbDE2NS4xNjAxNTYtMTY1LjE2MDE1NmMxLjg5MDYyNS0xLjg5MDYyNiA0LjM5ODQzNy0yLjkyOTY4OCA3LjA3MDMxMy0yLjkyOTY4OCAyLjY3MTg3NCAwIDUuMTgzNTkzIDEuMDM5MDYyIDcuMDcwMzEyIDIuOTI5Njg4bDM4LjE4MzU5NCAzOC4xNzk2ODdjLTUuMzg2NzE5IDguMTA5Mzc1LTguMjc3MzQ0IDE3LjYyNS04LjI3NzM0NCAyNy41ODIwMzEgMCAxMy4zNTU0NjkgNS4xOTkyMTkgMjUuOTEwMTU2IDE0LjY0NDUzMSAzNS4zNTU0NjkgOS40NDUzMTMgOS40NDE0MDYgMjIgMTQuNjQ0NTMxIDM1LjM1NTQ2OSAxNC42NDQ1MzEgOS45NTcwMzEgMCAxOS40Njg3NS0yLjg5MDYyNSAyNy41ODIwMzEtOC4yNzczNDRsMzguMTc5Njg4IDM4LjE3OTY4OGMzLjg5ODQzNyAzLjg5ODQzOCAzLjg5ODQzNyAxMC4yNDIxODggMCAxNC4xNDA2MjVsLTYzLjI2MTcxOSA2My4yNjE3MTloLTQzLjgzOTg0NGwzMi44NTU0NjktMzIuODU1NDY5YzMuOTAyMzQ0LTMuOTA2MjUgMy45MDIzNDQtMTAuMjM4MjgxIDAtMTQuMTQwNjI1bC0xMDEuODI0MjE5LTEwMS44MjQyMTljLTMuOTA2MjUtMy45MDYyNS0xMC4yMzgyODEtMy45MDYyNS0xNC4xNDQ1MzEgMGwtMTQ4LjgyMDMxMyAxNDguODIwMzEzaC0xMjAuMDM5MDYyYy0xNi41NDI5NjkgMC0zMCAxMy40NTcwMzEtMzAgMzB2NjNjMCA1LjUxOTUzMSA0LjQ3NjU2MiAxMCAxMCAxMCAxNi41NDI5NjkgMCAzMCAxMy40NTcwMzEgMzAgMzAgMCAxNi41MzkwNjItMTMuNDU3MDMxIDMwLTMwIDMwLTUuNTIzNDM4IDAtMTAgNC40NzY1NjItMTAgMTB2NjNjMCAxNi41MzkwNjIgMTMuNDU3MDMxIDMwIDMwIDMwaDQ1MmMxNi41NDI5NjkgMCAzMC0xMy40NjA5MzggMzAtMzB2LTYzYzAtNS41MjM0MzgtNC40NzY1NjItMTAtMTAtMTAtMTYuNTQyOTY5IDAtMzAtMTMuNDYwOTM4LTMwLTMwIDAtMTYuNTQyOTY5IDEzLjQ1NzAzMS0zMCAzMC0zMCA1LjUyMzQzOCAwIDEwLTQuNDgwNDY5IDEwLTEwdi02M2MwLTE2LjU0Mjk2OS0xMy40NTcwMzEtMzAtMzAtMzB6bS0xNzYuMDcwMzEyLTEyNy42MDkzNzUgODcuNjgzNTkzIDg3LjY4MzU5My0zOS45MjU3ODEgMzkuOTI1NzgyaC0zOS41OTc2NTZsMjkuMzE2NDA2LTI5LjMyMDMxM2MzLjkwNjI1LTMuOTAyMzQzIDMuOTA2MjUtMTAuMjM0Mzc1IDAtMTQuMTQwNjI1LTMuOTAyMzQ0LTMuOTA2MjUtMTAuMjM0Mzc1LTMuOTA2MjUtMTQuMTQwNjI1IDBsLTQzLjQ2MDkzNyA0My40NTcwMzJoLTEwNy40ODA0Njl6bTE4Ni4wNzAzMTIgMjExLjYxMzI4MWMtMjIuNzkyOTY5IDQuNjQ0NTMxLTQwIDI0Ljg0NzY1Ni00MCA0OC45OTYwOTQgMCAyNC4xNDQ1MzEgMTcuMjA3MDMxIDQ0LjM0NzY1NiA0MCA0OC45OTYwOTR2NTQuMDAzOTA2YzAgNS41MTE3MTgtNC40ODQzNzUgMTAtMTAgMTBoLTQ1MmMtNS41MTU2MjUgMC0xMC00LjQ4ODI4Mi0xMC0xMHYtNTQuMDAzOTA2YzIyLjc5Mjk2OS00LjY0ODQzOCA0MC0yNC44NTE1NjMgNDAtNDguOTk2MDk0IDAtMjQuMTQ4NDM4LTE3LjIwNzAzMS00NC4zNDc2NTYtNDAtNDguOTk2MDk0di01NC4wMDM5MDZjMC01LjUxNTYyNSA0LjQ4NDM3NS0xMCAxMC0xMGg0NTJjNS41MTU2MjUgMCAxMCA0LjQ4NDM3NSAxMCAxMHptMCAwIiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iYWN0aXZlLXBhdGgiIGRhdGEtb2xkX2NvbG9yPSIjNmM3NTdkIiBmaWxsPSIjNmM3NTdkIi8+PHBhdGggZD0ibTIxMy41NzAzMTIgMzYyLjA2NjQwNmMxLjg1OTM3Ni0xLjg2MzI4MSAyLjkyOTY4OC00LjQ0MTQwNiAyLjkyOTY4OC03LjA3MDMxMiAwLTIuNjMyODEzLTEuMDcwMzEyLTUuMjEwOTM4LTIuOTI5Njg4LTcuMDcwMzEzLTEuODU5Mzc0LTEuODU5Mzc1LTQuNDQxNDA2LTIuOTI5Njg3LTcuMDcwMzEyLTIuOTI5Njg3cy01LjIxMDkzOCAxLjA3MDMxMi03LjA3MDMxMiAyLjkyOTY4N2MtMS44NTkzNzYgMS44NTkzNzUtMi45Mjk2ODggNC40Mzc1LTIuOTI5Njg4IDcuMDcwMzEzIDAgMi42Mjg5MDYgMS4wNzAzMTIgNS4yMDcwMzEgMi45Mjk2ODggNy4wNzAzMTIgMS44NTkzNzQgMS44NTkzNzUgNC40NDE0MDYgMi45Mjk2ODggNy4wNzAzMTIgMi45Mjk2ODhzNS4yMTA5MzgtMS4wNzAzMTMgNy4wNzAzMTItMi45Mjk2ODh6bTAgMCIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgY2xhc3M9ImFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iIzZjNzU3ZCIgZmlsbD0iIzZjNzU3ZCIvPjxwYXRoIGQ9Im0zMTIuNTcwMzEyIDM2Mi4wNjY0MDZjMS44NTkzNzYtMS44NjMyODEgMi45Mjk2ODgtNC40NDE0MDYgMi45Mjk2ODgtNy4wNzAzMTIgMC0yLjYzMjgxMy0xLjA3MDMxMi01LjIxMDkzOC0yLjkyOTY4OC03LjA3MDMxMy0xLjg1OTM3NC0xLjg1OTM3NS00LjQ0MTQwNi0yLjkyOTY4Ny03LjA3MDMxMi0yLjkyOTY4N3MtNS4yMTA5MzggMS4wNzAzMTItNy4wNzAzMTIgMi45Mjk2ODdjLTEuODU5Mzc2IDEuODU5Mzc1LTIuOTI5Njg4IDQuNDM3NS0yLjkyOTY4OCA3LjA3MDMxMyAwIDIuNjI4OTA2IDEuMDcwMzEyIDUuMjA3MDMxIDIuOTI5Njg4IDcuMDcwMzEyIDEuODU5Mzc0IDEuODU5Mzc1IDQuNDQxNDA2IDIuOTI5Njg4IDcuMDcwMzEyIDIuOTI5Njg4czUuMjEwOTM4LTEuMDcwMzEzIDcuMDcwMzEyLTIuOTI5Njg4em0wIDAiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiM2Yzc1N2QiIGZpbGw9IiM2Yzc1N2QiLz48cGF0aCBkPSJtMjYzLjA3MDMxMiAzNjIuMDY2NDA2YzEuODU5Mzc2LTEuODYzMjgxIDIuOTI5Njg4LTQuNDQxNDA2IDIuOTI5Njg4LTcuMDcwMzEyIDAtMi42MzI4MTMtMS4wNzAzMTItNS4yMTA5MzgtMi45Mjk2ODgtNy4wNzAzMTMtMS44NTkzNzQtMS44NTkzNzUtNC40NDE0MDYtMi45Mjk2ODctNy4wNzAzMTItMi45Mjk2ODdzLTUuMjEwOTM4IDEuMDcwMzEyLTcuMDcwMzEyIDIuOTI5Njg3Yy0xLjg1OTM3NiAxLjg1OTM3NS0yLjkyOTY4OCA0LjQzNzUtMi45Mjk2ODggNy4wNzAzMTMgMCAyLjYyODkwNiAxLjA3MDMxMiA1LjIwNzAzMSAyLjkyOTY4OCA3LjA3MDMxMiAxLjg1OTM3NCAxLjg1OTM3NSA0LjQ0MTQwNiAyLjkyOTY4OCA3LjA3MDMxMiAyLjkyOTY4OHM1LjIxMDkzOC0xLjA3MDMxMyA3LjA3MDMxMi0yLjkyOTY4OHptMCAwIiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iYWN0aXZlLXBhdGgiIGRhdGEtb2xkX2NvbG9yPSIjNmM3NTdkIiBmaWxsPSIjNmM3NTdkIi8+PHBhdGggZD0ibTE1NyAzNjQuOTk2MDk0YzIuNjI4OTA2IDAgNS4yMTA5MzgtMS4wNzAzMTMgNy4wNzAzMTItMi45Mjk2ODggMS44NTkzNzYtMS44NjMyODEgMi45Mjk2ODgtNC40NDE0MDYgMi45Mjk2ODgtNy4wNzAzMTIgMC0yLjYzMjgxMy0xLjA3MDMxMi01LjIxMDkzOC0yLjkyOTY4OC03LjA3MDMxMy0xLjg1OTM3NC0xLjg1OTM3NS00LjQ0MTQwNi0yLjkyOTY4Ny03LjA3MDMxMi0yLjkyOTY4N3MtNS4yMTA5MzggMS4wNzAzMTItNy4wNzAzMTIgMi45Mjk2ODdjLTEuODU5Mzc2IDEuODU5Mzc1LTIuOTI5Njg4IDQuNDM3NS0yLjkyOTY4OCA3LjA3MDMxMyAwIDIuNjI4OTA2IDEuMDcwMzEyIDUuMjA3MDMxIDIuOTI5Njg4IDcuMDcwMzEyIDEuODU5Mzc0IDEuODU5Mzc1IDQuNDQxNDA2IDIuOTI5Njg4IDcuMDcwMzEyIDIuOTI5Njg4em0wIDAiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiM2Yzc1N2QiIGZpbGw9IiM2Yzc1N2QiLz48cGF0aCBkPSJtMzYyLjA3MDMxMiAzNjIuMDY2NDA2YzEuODU5Mzc2LTEuODYzMjgxIDIuOTI5Njg4LTQuNDQxNDA2IDIuOTI5Njg4LTcuMDcwMzEyIDAtMi42MzI4MTMtMS4wNzAzMTItNS4yMTA5MzgtMi45Mjk2ODgtNy4wNzAzMTMtMS44NTkzNzQtMS44NTkzNzUtNC40NDE0MDYtMi45Mjk2ODctNy4wNzAzMTItMi45Mjk2ODdzLTUuMjEwOTM4IDEuMDcwMzEyLTcuMDcwMzEyIDIuOTI5Njg3Yy0xLjg1OTM3NiAxLjg1OTM3NS0yLjkyOTY4OCA0LjQzNzUtMi45Mjk2ODggNy4wNzAzMTMgMCAyLjYyODkwNiAxLjA3MDMxMiA1LjIwNzAzMSAyLjkyOTY4OCA3LjA3MDMxMiAxLjg1OTM3NCAxLjg1OTM3NSA0LjQ0MTQwNiAyLjkyOTY4OCA3LjA3MDMxMiAyLjkyOTY4OHM1LjIxMDkzOC0xLjA3MDMxMyA3LjA3MDMxMi0yLjkyOTY4OHptMCAwIiBkYXRhLW9yaWdpbmFsPSIjMDAwMDAwIiBjbGFzcz0iYWN0aXZlLXBhdGgiIGRhdGEtb2xkX2NvbG9yPSIjNmM3NTdkIiBmaWxsPSIjNmM3NTdkIi8+PHBhdGggZD0ibTE1NyA0MTIuOTk2MDk0aDE5OGM1LjUyMzQzOCAwIDEwLTQuNDgwNDY5IDEwLTEwIDAtNS41MjM0MzgtNC40NzY1NjItMTAtMTAtMTBoLTE5OGMtNS41MjM0MzggMC0xMCA0LjQ3NjU2Mi0xMCAxMCAwIDUuNTE5NTMxIDQuNDc2NTYyIDEwIDEwIDEwem0wIDAiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiM2Yzc1N2QiIGZpbGw9IiM2Yzc1N2QiLz48cGF0aCBkPSJtMTkzLjMyMDMxMiA0NDAuOTk2MDk0Yy0yLjYyODkwNiAwLTUuMjEwOTM3IDEuMDcwMzEyLTcuMDcwMzEyIDIuOTI5Njg3cy0yLjkyOTY4OCA0LjQzNzUtMi45Mjk2ODggNy4wNzAzMTNjMCAyLjYyODkwNiAxLjA3MDMxMyA1LjIwNzAzMSAyLjkyOTY4OCA3LjA3MDMxMiAxLjg1OTM3NSAxLjg1OTM3NSA0LjQ0MTQwNiAyLjkyOTY4OCA3LjA3MDMxMiAyLjkyOTY4OCAyLjY0MDYyNiAwIDUuMjEwOTM4LTEuMDcwMzEzIDcuMDcwMzEzLTIuOTI5Njg4IDEuODcxMDk0LTEuODYzMjgxIDIuOTI5Njg3LTQuNDQxNDA2IDIuOTI5Njg3LTcuMDcwMzEyIDAtMi42MzI4MTMtMS4wNTg1OTMtNS4yMTA5MzgtMi45Mjk2ODctNy4wNzAzMTMtMS44NTkzNzUtMS44NTkzNzUtNC40Mjk2ODctMi45Mjk2ODctNy4wNzAzMTMtMi45Mjk2ODd6bTAgMCIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgY2xhc3M9ImFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iIzZjNzU3ZCIgZmlsbD0iIzZjNzU3ZCIvPjxwYXRoIGQ9Im00MDggMjk2Ljk5NjA5NGgtMzA0Yy01LjUyMzQzOCAwLTEwIDQuNDc2NTYyLTEwIDEwdjE0NGMwIDUuNTE5NTMxIDQuNDc2NTYyIDEwIDEwIDEwaDQxLjY1NjI1YzUuNTIzNDM4IDAgMTAtNC40ODA0NjkgMTAtMTAgMC01LjUyMzQzOC00LjQ3NjU2Mi0xMC0xMC0xMGgtMzEuNjU2MjV2LTEyNGgyODR2MTI0aC0xNjIuMzQzNzVjLTUuNTIzNDM4IDAtMTAgNC40NzY1NjItMTAgMTAgMCA1LjUxOTUzMSA0LjQ3NjU2MiAxMCAxMCAxMGgxNzIuMzQzNzVjNS41MjM0MzggMCAxMC00LjQ4MDQ2OSAxMC0xMHYtMTQ0YzAtNS41MjM0MzgtNC40NzY1NjItMTAtMTAtMTB6bTAgMCIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgY2xhc3M9ImFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iIzZjNzU3ZCIgZmlsbD0iIzZjNzU3ZCIvPjwvZz4gPC9zdmc+Cg=="

/***/ }),
/* 135 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(Buffer) {// Licensed to the Software Freedom Conservancy (SFC) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The SFC licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

/**
 * @fileoverview Defines an {@linkplain cmd.Executor command executor} that
 * communicates with a remote end using HTTP + JSON.
 */



const http = __webpack_require__(45);
const https = __webpack_require__(153);
const url = __webpack_require__(22);

const httpLib = __webpack_require__(154);


/**
 * @typedef {{protocol: (?string|undefined),
 *            auth: (?string|undefined),
 *            hostname: (?string|undefined),
 *            host: (?string|undefined),
 *            port: (?string|undefined),
 *            path: (?string|undefined),
 *            pathname: (?string|undefined)}}
 */
var RequestOptions;


/**
 * @param {string} aUrl The request URL to parse.
 * @return {RequestOptions} The request options.
 * @throws {Error} if the URL does not include a hostname.
 */
function getRequestOptions(aUrl) {
  let options = url.parse(aUrl);
  if (!options.hostname) {
    throw new Error('Invalid URL: ' + aUrl);
  }
  // Delete the search and has portions as they are not used.
  options.search = null;
  options.hash = null;
  options.path = options.pathname;
  return options;
}


/**
 * A basic HTTP client used to send messages to a remote end.
 *
 * @implements {httpLib.Client}
 */
class HttpClient {
  /**
   * @param {string} serverUrl URL for the WebDriver server to send commands to.
   * @param {http.Agent=} opt_agent The agent to use for each request.
   *     Defaults to `http.globalAgent`.
   * @param {?string=} opt_proxy The proxy to use for the connection to the
   *     server. Default is to use no proxy.
   */
  constructor(serverUrl, opt_agent, opt_proxy) {
    /** @private {http.Agent} */
    this.agent_ = opt_agent || null;

    /**
     * Base options for each request.
     * @private {RequestOptions}
     */
    this.options_ = getRequestOptions(serverUrl);

    /**
     * @private {?RequestOptions}
     */
    this.proxyOptions_ = opt_proxy ? getRequestOptions(opt_proxy) : null;
  }

  /** @override */
  send(httpRequest) {
    let data;

    let headers = {};
    httpRequest.headers.forEach(function(value, name) {
      headers[name] = value;
    });

    headers['Content-Length'] = 0;
    if (httpRequest.method == 'POST' || httpRequest.method == 'PUT') {
      data = JSON.stringify(httpRequest.data);
      headers['Content-Length'] = Buffer.byteLength(data, 'utf8');
      headers['Content-Type'] = 'application/json;charset=UTF-8';
    }

    let path = this.options_.path;
    if (path.endsWith('/') && httpRequest.path.startsWith('/')) {
      path += httpRequest.path.substring(1);
    } else {
      path += httpRequest.path;
    }
    let parsedPath = url.parse(path);

    let options = {
      agent: this.agent_ || null,
      method: httpRequest.method,

      auth: this.options_.auth,
      hostname: this.options_.hostname,
      port: this.options_.port,
      protocol: this.options_.protocol,

      path: parsedPath.path,
      pathname: parsedPath.pathname,
      search: parsedPath.search,
      hash: parsedPath.hash,

      headers,
    };

    return new Promise((fulfill, reject) => {
      sendRequest(options, fulfill, reject, data, this.proxyOptions_);
    });
  }
}


/**
 * Sends a single HTTP request.
 * @param {!Object} options The request options.
 * @param {function(!httpLib.Response)} onOk The function to call if the
 *     request succeeds.
 * @param {function(!Error)} onError The function to call if the request fails.
 * @param {?string=} opt_data The data to send with the request.
 * @param {?RequestOptions=} opt_proxy The proxy server to use for the request.
 */
function sendRequest(options, onOk, onError, opt_data, opt_proxy) {
  var hostname = options.hostname;
  var port = options.port;

  if (opt_proxy) {
    let proxy = /** @type {RequestOptions} */(opt_proxy);

    // RFC 2616, section 5.1.2:
    // The absoluteURI form is REQUIRED when the request is being made to a
    // proxy.
    let absoluteUri = url.format(options);

    // RFC 2616, section 14.23:
    // An HTTP/1.1 proxy MUST ensure that any request message it forwards does
    // contain an appropriate Host header field that identifies the service
    // being requested by the proxy.
    let targetHost = options.hostname
    if (options.port) {
      targetHost += ':' + options.port;
    }

    // Update the request options with our proxy info.
    options.headers['Host'] = targetHost;
    options.path = absoluteUri;
    options.host = proxy.host;
    options.hostname = proxy.hostname;
    options.port = proxy.port;

    if (proxy.auth) {
      options.headers['Proxy-Authorization'] =
          'Basic ' + new Buffer(proxy.auth).toString('base64');
    }
  }

  let requestFn = options.protocol === 'https:' ? https.request : http.request;
  var request = requestFn(options, function onResponse(response) {
    if (response.statusCode == 302 || response.statusCode == 303) {
      try {
        var location = url.parse(response.headers['location']);
      } catch (ex) {
        onError(Error(
            'Failed to parse "Location" header for server redirect: ' +
            ex.message + '\nResponse was: \n' +
            new httpLib.Response(response.statusCode, response.headers, '')));
        return;
      }

      if (!location.hostname) {
        location.hostname = hostname;
        location.port = port;
      }

      request.abort();
      sendRequest({
        method: 'GET',
        protocol: location.protocol || options.protocol,
        hostname: location.hostname,
        port: location.port,
        path: location.path,
        pathname: location.pathname,
        search: location.search,
        hash: location.hash,
        headers: {
          'Accept': 'application/json; charset=utf-8'
        }
      }, onOk, onError, undefined, opt_proxy);
      return;
    }

    var body = [];
    response.on('data', body.push.bind(body));
    response.on('end', function() {
      var resp = new httpLib.Response(
          /** @type {number} */(response.statusCode),
          /** @type {!Object<string>} */(response.headers),
          body.join('').replace(/\0/g, ''));
      onOk(resp);
    });
  });

  request.on('error', function(e) {
    if (e.code === 'ECONNRESET') {
      setTimeout(function() {
        sendRequest(options, onOk, onError, opt_data, opt_proxy);
      }, 15);
    } else {
      var message = e.message;
      if (e.code) {
        message = e.code + ' ' + message;
      }
      onError(new Error(message));
    }
  });

  if (opt_data) {
    request.write(opt_data);
  }

  request.end();
}


// PUBLIC API

exports.Executor = httpLib.Executor;
exports.HttpClient = HttpClient;
exports.Request = httpLib.Request;
exports.Response = httpLib.Response;

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(9).Buffer))

/***/ }),
/* 136 */,
/* 137 */,
/* 138 */,
/* 139 */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),
/* 140 */,
/* 141 */
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),
/* 142 */,
/* 143 */,
/* 144 */,
/* 145 */,
/* 146 */,
/* 147 */,
/* 148 */,
/* 149 */,
/* 150 */,
/* 151 */,
/* 152 */,
/* 153 */,
/* 154 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Licensed to the Software Freedom Conservancy (SFC) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The SFC licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

/**
 * @fileoverview Defines an environment agnostic {@linkplain cmd.Executor
 * command executor} that communicates with a remote end using JSON over HTTP.
 *
 * Clients should implement the {@link Client} interface, which is used by
 * the {@link Executor} to send commands to the remote end.
 */



const cmd = __webpack_require__(23);
const error = __webpack_require__(19);
const logging = __webpack_require__(24);
const promise = __webpack_require__(56);
const Session = __webpack_require__(57).Session;
const WebElement = __webpack_require__(156).WebElement;

const {getAttribute, isDisplayed} = /** @suppress {undefinedVars|uselessCode} */(function() {
  try {
    return {
      getAttribute: __webpack_require__(159),
      isDisplayed: __webpack_require__(160)
    };
  } catch (ex) {
    throw Error(
        'Failed to import atoms modules. If running in devmode, you need to run'
            + ' `./go node:atoms` from the project root: ' + ex);
  }
})();


/**
 * Converts a headers map to a HTTP header block string.
 * @param {!Map<string, string>} headers The map to convert.
 * @return {string} The headers as a string.
 */
function headersToString(headers) {
  let ret = [];
  headers.forEach(function(value, name) {
    ret.push(`${name.toLowerCase()}: ${value}`);
  });
  return ret.join('\n');
}


/**
 * Represents a HTTP request message. This class is a "partial" request and only
 * defines the path on the server to send a request to. It is each client's
 * responsibility to build the full URL for the final request.
 * @final
 */
class Request {
  /**
   * @param {string} method The HTTP method to use for the request.
   * @param {string} path The path on the server to send the request to.
   * @param {Object=} opt_data This request's non-serialized JSON payload data.
   */
  constructor(method, path, opt_data) {
    this.method = /** string */method;
    this.path = /** string */path;
    this.data = /** Object */opt_data;
    this.headers = /** !Map<string, string> */new Map(
        [['Accept', 'application/json; charset=utf-8']]);
  }

  /** @override */
  toString() {
    let ret = `${this.method} ${this.path} HTTP/1.1\n`;
    ret += headersToString(this.headers) + '\n\n';
    if (this.data) {
      ret += JSON.stringify(this.data);
    }
    return ret;
  }
}


/**
 * Represents a HTTP response message.
 * @final
 */
class Response {
  /**
   * @param {number} status The response code.
   * @param {!Object<string>} headers The response headers. All header names
   *     will be converted to lowercase strings for consistent lookups.
   * @param {string} body The response body.
   */
  constructor(status, headers, body) {
    this.status = /** number */status;
    this.body = /** string */body;
    this.headers = /** !Map<string, string>*/new Map;
    for (let header in headers) {
      this.headers.set(header.toLowerCase(), headers[header]);
    }
  }

  /** @override */
  toString() {
    let ret = `HTTP/1.1 ${this.status}\n${headersToString(this.headers)}\n\n`;
    if (this.body) {
      ret += this.body;
    }
    return ret;
  }
}


const DEV_ROOT = '../../../../buck-out/gen/javascript/';

/** @enum {!Function} */
const Atom = {
  GET_ATTRIBUTE: getAttribute,
  IS_DISPLAYED: isDisplayed
};


const LOG = logging.getLogger('webdriver.http');


function post(path) { return resource('POST', path); }
function del(path)  { return resource('DELETE', path); }
function get(path)  { return resource('GET', path); }
function resource(method, path) { return {method: method, path: path}; }


/** @typedef {{method: string, path: string}} */
var CommandSpec;


/** @typedef {function(!cmd.Command): !Promise<!cmd.Command>} */
var CommandTransformer;


class InternalTypeError extends TypeError {}


/**
 * @param {!cmd.Command} command The initial command.
 * @param {Atom} atom The name of the atom to execute.
 * @return {!Promise<!cmd.Command>} The transformed command to execute.
 */
function toExecuteAtomCommand(command, atom, ...params) {
  return new Promise((resolve, reject) => {
    if (typeof atom !== 'function') {
      reject(new InternalTypeError('atom is not a function: ' + typeof atom));
      return;
    }

    let newCmd = new cmd.Command(cmd.Name.EXECUTE_SCRIPT)
        .setParameter('sessionId', command.getParameter('sessionId'))
        .setParameter('script', `return (${atom}).apply(null, arguments)`)
        .setParameter('args', params.map(param => command.getParameter(param)));
    resolve(newCmd);
  });
}



/** @const {!Map<string, CommandSpec>} */
const COMMAND_MAP = new Map([
    [cmd.Name.GET_SERVER_STATUS, get('/status')],
    [cmd.Name.NEW_SESSION, post('/session')],
    [cmd.Name.GET_SESSIONS, get('/sessions')],
    [cmd.Name.DESCRIBE_SESSION, get('/session/:sessionId')],
    [cmd.Name.QUIT, del('/session/:sessionId')],
    [cmd.Name.CLOSE, del('/session/:sessionId/window')],
    [cmd.Name.GET_CURRENT_WINDOW_HANDLE, get('/session/:sessionId/window_handle')],
    [cmd.Name.GET_WINDOW_HANDLES, get('/session/:sessionId/window_handles')],
    [cmd.Name.GET_CURRENT_URL, get('/session/:sessionId/url')],
    [cmd.Name.GET, post('/session/:sessionId/url')],
    [cmd.Name.GO_BACK, post('/session/:sessionId/back')],
    [cmd.Name.GO_FORWARD, post('/session/:sessionId/forward')],
    [cmd.Name.REFRESH, post('/session/:sessionId/refresh')],
    [cmd.Name.ADD_COOKIE, post('/session/:sessionId/cookie')],
    [cmd.Name.GET_ALL_COOKIES, get('/session/:sessionId/cookie')],
    [cmd.Name.DELETE_ALL_COOKIES, del('/session/:sessionId/cookie')],
    [cmd.Name.DELETE_COOKIE, del('/session/:sessionId/cookie/:name')],
    [cmd.Name.FIND_ELEMENT, post('/session/:sessionId/element')],
    [cmd.Name.FIND_ELEMENTS, post('/session/:sessionId/elements')],
    [cmd.Name.GET_ACTIVE_ELEMENT, post('/session/:sessionId/element/active')],
    [cmd.Name.FIND_CHILD_ELEMENT, post('/session/:sessionId/element/:id/element')],
    [cmd.Name.FIND_CHILD_ELEMENTS, post('/session/:sessionId/element/:id/elements')],
    [cmd.Name.CLEAR_ELEMENT, post('/session/:sessionId/element/:id/clear')],
    [cmd.Name.CLICK_ELEMENT, post('/session/:sessionId/element/:id/click')],
    [cmd.Name.SEND_KEYS_TO_ELEMENT, post('/session/:sessionId/element/:id/value')],
    [cmd.Name.SUBMIT_ELEMENT, post('/session/:sessionId/element/:id/submit')],
    [cmd.Name.GET_ELEMENT_TEXT, get('/session/:sessionId/element/:id/text')],
    [cmd.Name.GET_ELEMENT_TAG_NAME, get('/session/:sessionId/element/:id/name')],
    [cmd.Name.IS_ELEMENT_SELECTED, get('/session/:sessionId/element/:id/selected')],
    [cmd.Name.IS_ELEMENT_ENABLED, get('/session/:sessionId/element/:id/enabled')],
    [cmd.Name.IS_ELEMENT_DISPLAYED, get('/session/:sessionId/element/:id/displayed')],
    [cmd.Name.GET_ELEMENT_LOCATION, get('/session/:sessionId/element/:id/location')],
    [cmd.Name.GET_ELEMENT_SIZE, get('/session/:sessionId/element/:id/size')],
    [cmd.Name.GET_ELEMENT_ATTRIBUTE, get('/session/:sessionId/element/:id/attribute/:name')],
    [cmd.Name.GET_ELEMENT_VALUE_OF_CSS_PROPERTY, get('/session/:sessionId/element/:id/css/:propertyName')],
    [cmd.Name.ELEMENT_EQUALS, get('/session/:sessionId/element/:id/equals/:other')],
    [cmd.Name.TAKE_ELEMENT_SCREENSHOT, get('/session/:sessionId/element/:id/screenshot')],
    [cmd.Name.SWITCH_TO_WINDOW, post('/session/:sessionId/window')],
    [cmd.Name.MAXIMIZE_WINDOW, post('/session/:sessionId/window/current/maximize')],
    [cmd.Name.GET_WINDOW_POSITION, get('/session/:sessionId/window/current/position')],
    [cmd.Name.SET_WINDOW_POSITION, post('/session/:sessionId/window/current/position')],
    [cmd.Name.GET_WINDOW_SIZE, get('/session/:sessionId/window/current/size')],
    [cmd.Name.SET_WINDOW_SIZE, post('/session/:sessionId/window/current/size')],
    [cmd.Name.SWITCH_TO_FRAME, post('/session/:sessionId/frame')],
    [cmd.Name.GET_PAGE_SOURCE, get('/session/:sessionId/source')],
    [cmd.Name.GET_TITLE, get('/session/:sessionId/title')],
    [cmd.Name.EXECUTE_SCRIPT, post('/session/:sessionId/execute')],
    [cmd.Name.EXECUTE_ASYNC_SCRIPT, post('/session/:sessionId/execute_async')],
    [cmd.Name.SCREENSHOT, get('/session/:sessionId/screenshot')],
    [cmd.Name.GET_TIMEOUT, get('/session/:sessionId/timeouts')],
    [cmd.Name.SET_TIMEOUT, post('/session/:sessionId/timeouts')],
    [cmd.Name.MOVE_TO, post('/session/:sessionId/moveto')],
    [cmd.Name.CLICK, post('/session/:sessionId/click')],
    [cmd.Name.DOUBLE_CLICK, post('/session/:sessionId/doubleclick')],
    [cmd.Name.MOUSE_DOWN, post('/session/:sessionId/buttondown')],
    [cmd.Name.MOUSE_UP, post('/session/:sessionId/buttonup')],
    [cmd.Name.MOVE_TO, post('/session/:sessionId/moveto')],
    [cmd.Name.SEND_KEYS_TO_ACTIVE_ELEMENT, post('/session/:sessionId/keys')],
    [cmd.Name.TOUCH_SINGLE_TAP, post('/session/:sessionId/touch/click')],
    [cmd.Name.TOUCH_DOUBLE_TAP, post('/session/:sessionId/touch/doubleclick')],
    [cmd.Name.TOUCH_DOWN, post('/session/:sessionId/touch/down')],
    [cmd.Name.TOUCH_UP, post('/session/:sessionId/touch/up')],
    [cmd.Name.TOUCH_MOVE, post('/session/:sessionId/touch/move')],
    [cmd.Name.TOUCH_SCROLL, post('/session/:sessionId/touch/scroll')],
    [cmd.Name.TOUCH_LONG_PRESS, post('/session/:sessionId/touch/longclick')],
    [cmd.Name.TOUCH_FLICK, post('/session/:sessionId/touch/flick')],
    [cmd.Name.ACCEPT_ALERT, post('/session/:sessionId/accept_alert')],
    [cmd.Name.DISMISS_ALERT, post('/session/:sessionId/dismiss_alert')],
    [cmd.Name.GET_ALERT_TEXT, get('/session/:sessionId/alert_text')],
    [cmd.Name.SET_ALERT_TEXT, post('/session/:sessionId/alert_text')],
    [cmd.Name.SET_ALERT_CREDENTIALS, post('/session/:sessionId/alert/credentials')],
    [cmd.Name.GET_LOG, post('/session/:sessionId/log')],
    [cmd.Name.GET_AVAILABLE_LOG_TYPES, get('/session/:sessionId/log/types')],
    [cmd.Name.GET_SESSION_LOGS, post('/logs')],
    [cmd.Name.UPLOAD_FILE, post('/session/:sessionId/file')],
]);


/** @const {!Map<string, (CommandSpec|CommandTransformer)>} */
const W3C_COMMAND_MAP = new Map([
  [cmd.Name.GET_ACTIVE_ELEMENT, get('/session/:sessionId/element/active')],
  [cmd.Name.GET_ALERT_TEXT, get('/session/:sessionId/alert/text')],
  [cmd.Name.SET_ALERT_TEXT, post('/session/:sessionId/alert/text')],
  [cmd.Name.ACCEPT_ALERT, post('/session/:sessionId/alert/accept')],
  [cmd.Name.DISMISS_ALERT, post('/session/:sessionId/alert/dismiss')],
  [cmd.Name.GET_ELEMENT_ATTRIBUTE, (cmd) => {
    return toExecuteAtomCommand(cmd, Atom.GET_ATTRIBUTE, 'id', 'name');
  }],
  [cmd.Name.GET_ELEMENT_LOCATION, get('/session/:sessionId/element/:id/rect')],
  [cmd.Name.GET_ELEMENT_SIZE, get('/session/:sessionId/element/:id/rect')],
  [cmd.Name.IS_ELEMENT_DISPLAYED, (cmd) => {
    return toExecuteAtomCommand(cmd, Atom.IS_DISPLAYED, 'id');
  }],
  [cmd.Name.EXECUTE_SCRIPT, post('/session/:sessionId/execute/sync')],
  [cmd.Name.EXECUTE_ASYNC_SCRIPT, post('/session/:sessionId/execute/async')],
  [cmd.Name.MAXIMIZE_WINDOW, post('/session/:sessionId/window/maximize')],
  [cmd.Name.GET_WINDOW_POSITION, get('/session/:sessionId/window/position')],
  [cmd.Name.SET_WINDOW_POSITION, post('/session/:sessionId/window/position')],
  [cmd.Name.GET_WINDOW_SIZE, get('/session/:sessionId/window/size')],
  [cmd.Name.SET_WINDOW_SIZE, post('/session/:sessionId/window/size')],
  [cmd.Name.GET_CURRENT_WINDOW_HANDLE, get('/session/:sessionId/window')],
  [cmd.Name.GET_WINDOW_HANDLES, get('/session/:sessionId/window/handles')],
]);


/**
 * Handles sending HTTP messages to a remote end.
 *
 * @interface
 */
class Client {

  /**
   * Sends a request to the server. The client will automatically follow any
   * redirects returned by the server, fulfilling the returned promise with the
   * final response.
   *
   * @param {!Request} httpRequest The request to send.
   * @return {!Promise<Response>} A promise that will be fulfilled with the
   *     server's response.
   */
  send(httpRequest) {}
}


const CLIENTS =
    /** !WeakMap<!Executor, !(Client|IThenable<!Client>)> */new WeakMap;


/**
 * Sends a request using the given executor.
 * @param {!Executor} executor
 * @param {!Request} request
 * @return {!Promise<Response>}
 */
function doSend(executor, request) {
  const client = CLIENTS.get(executor);
  if (promise.isPromise(client)) {
    return client.then(client => {
      CLIENTS.set(executor, client);
      return client.send(request);
    });
  } else {
    return client.send(request);
  }
}


/**
 * @param {Map<string, CommandSpec>} customCommands
 *     A map of custom command definitions.
 * @param {boolean} w3c Whether to use W3C command mappings.
 * @param {!cmd.Command} command The command to resolve.
 * @return {!Promise<!Request>} A promise that will resolve with the
 *     command to execute.
 */
function buildRequest(customCommands, w3c, command) {
  LOG.finest(() => `Translating command: ${command.getName()}`);
  let spec = customCommands && customCommands.get(command.getName());
  if (spec) {
    return toHttpRequest(spec);
  }

  if (w3c) {
    spec = W3C_COMMAND_MAP.get(command.getName());
    if (typeof spec === 'function') {
      LOG.finest(() => `Transforming command for W3C: ${command.getName()}`);
      return spec(command)
          .then(newCommand => buildRequest(customCommands, w3c, newCommand));
    } else if (spec) {
      return toHttpRequest(spec);
    }
  }

  spec = COMMAND_MAP.get(command.getName());
  if (spec) {
    return toHttpRequest(spec);
  }
  return Promise.reject(
      new error.UnknownCommandError(
          'Unrecognized command: ' + command.getName()));

  /**
   * @param {CommandSpec} resource
   * @return {!Promise<!Request>}
   */
  function toHttpRequest(resource) {
    LOG.finest(() => `Building HTTP request: ${JSON.stringify(resource)}`);
    let parameters = command.getParameters();
    let path = buildPath(resource.path, parameters);
    return Promise.resolve(new Request(resource.method, path, parameters));
  }
}


/**
 * A command executor that communicates with the server using JSON over HTTP.
 *
 * By default, each instance of this class will use the legacy wire protocol
 * from [Selenium project][json]. The executor will automatically switch to the
 * [W3C wire protocol][w3c] if the remote end returns a compliant response to
 * a new session command.
 *
 * [json]: https://github.com/SeleniumHQ/selenium/wiki/JsonWireProtocol
 * [w3c]: https://w3c.github.io/webdriver/webdriver-spec.html
 *
 * @implements {cmd.Executor}
 */
class Executor {
  /**
   * @param {!(Client|IThenable<!Client>)} client The client to use for sending
   *     requests to the server, or a promise-like object that will resolve to
   *     to the client.
   */
  constructor(client) {
    CLIENTS.set(this, client);

    /**
     * Whether this executor should use the W3C wire protocol. The executor
     * will automatically switch if the remote end sends a compliant response
     * to a new session command, however, this property may be directly set to
     * `true` to force the executor into W3C mode.
     * @type {boolean}
     */
    this.w3c = false;

    /** @private {Map<string, CommandSpec>} */
    this.customCommands_ = null;

    /** @private {!logging.Logger} */
    this.log_ = logging.getLogger('webdriver.http.Executor');
  }

  /**
   * Defines a new command for use with this executor. When a command is sent,
   * the {@code path} will be preprocessed using the command's parameters; any
   * path segments prefixed with ":" will be replaced by the parameter of the
   * same name. For example, given "/person/:name" and the parameters
   * "{name: 'Bob'}", the final command path will be "/person/Bob".
   *
   * @param {string} name The command name.
   * @param {string} method The HTTP method to use when sending this command.
   * @param {string} path The path to send the command to, relative to
   *     the WebDriver server's command root and of the form
   *     "/path/:variable/segment".
   */
  defineCommand(name, method, path) {
    if (!this.customCommands_) {
      this.customCommands_ = new Map;
    }
    this.customCommands_.set(name, {method, path});
  }

  /** @override */
  execute(command) {
    let request = buildRequest(this.customCommands_, this.w3c, command);
    return request.then(request => {
      this.log_.finer(() => `>>> ${request.method} ${request.path}`);
      return doSend(this, request).then(response => {
        this.log_.finer(() => `>>>\n${request}\n<<<\n${response}`);

        let httpResponse = /** @type {!Response} */(response);
        let {isW3C, value} = parseHttpResponse(command, httpResponse);

        if (command.getName() === cmd.Name.NEW_SESSION
            || command.getName() === cmd.Name.DESCRIBE_SESSION) {
          if (!value || !value.sessionId) {
            throw new error.WebDriverError(
                `Unable to parse new session response: ${response.body}`);
          }

          // The remote end is a W3C compliant server if there is no `status`
          // field in the response. This is not applicable for the DESCRIBE_SESSION
          // command, which is not defined in the W3C spec.
          if (command.getName() === cmd.Name.NEW_SESSION) {
            this.w3c = this.w3c || isW3C;
          }

          // No implementations use the `capabilities` key yet...
          let capabilities = value.capabilities || value.value;
          return new Session(value.sessionId, capabilities);
        }

        return typeof value === 'undefined' ? null : value;
      });
    });
  }
}


/**
 * @param {string} str .
 * @return {?} .
 */
function tryParse(str) {
  try {
    return JSON.parse(str);
  } catch (ignored) {
    // Do nothing.
  }
}


/**
 * Callback used to parse {@link Response} objects from a
 * {@link HttpClient}.
 *
 * @param {!cmd.Command} command The command the response is for.
 * @param {!Response} httpResponse The HTTP response to parse.
 * @return {{isW3C: boolean, value: ?}} An object describing the parsed
 *     response. This object will have two fields: `isW3C` indicates whether
 *     the response looks like it came from a remote end that conforms with the
 *     W3C WebDriver spec, and `value`, the actual response value.
 * @throws {WebDriverError} If the HTTP response is an error.
 */
function parseHttpResponse(command, httpResponse) {
  if (httpResponse.status < 200) {
    // This should never happen, but throw the raw response so users report it.
    throw new error.WebDriverError(
        `Unexpected HTTP response:\n${httpResponse}`);
  }

  let parsed = tryParse(httpResponse.body);
  if (parsed && typeof parsed === 'object') {
    let value = parsed.value;
    let isW3C =
        value !== null && typeof value === 'object'
            && typeof parsed.status === 'undefined';

    if (!isW3C) {
      error.checkLegacyResponse(parsed);

      // Adjust legacy new session responses to look like W3C to simplify
      // later processing.
      if (command.getName() === cmd.Name.NEW_SESSION
          || command.getName() == cmd.Name.DESCRIBE_SESSION) {
        value = parsed;
      }

    } else if (httpResponse.status > 399) {
      error.throwDecodedError(value);
    }

    return {isW3C, value};
  }

  if (parsed !== undefined) {
    return {isW3C: false, value: parsed};
  }

  let value = httpResponse.body.replace(/\r\n/g, '\n');

  // 404 represents an unknown command; anything else > 399 is a generic unknown
  // error.
  if (httpResponse.status == 404) {
    throw new error.UnsupportedOperationError(value);
  } else if (httpResponse.status >= 400) {
    throw new error.WebDriverError(value);
  }

  return {isW3C: false, value: value || null};
}


/**
 * Builds a fully qualified path using the given set of command parameters. Each
 * path segment prefixed with ':' will be replaced by the value of the
 * corresponding parameter. All parameters spliced into the path will be
 * removed from the parameter map.
 * @param {string} path The original resource path.
 * @param {!Object<*>} parameters The parameters object to splice into the path.
 * @return {string} The modified path.
 */
function buildPath(path, parameters) {
  let pathParameters = path.match(/\/:(\w+)\b/g);
  if (pathParameters) {
    for (let i = 0; i < pathParameters.length; ++i) {
      let key = pathParameters[i].substring(2);  // Trim the /:
      if (key in parameters) {
        let value = parameters[key];
        if (WebElement.isId(value)) {
          // When inserting a WebElement into the URL, only use its ID value,
          // not the full JSON.
          value = WebElement.extractId(value);
        }
        path = path.replace(pathParameters[i], '/' + value);
        delete parameters[key];
      } else {
        throw new error.InvalidArgumentError(
            'Missing required parameter: ' + key);
      }
    }
  }
  return path;
}


// PUBLIC API

exports.Executor = Executor;
exports.Client = Client;
exports.Request = Request;
exports.Response = Response;
exports.buildPath = buildPath;  // Exported for testing.


/***/ }),
/* 155 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Licensed to the Software Freedom Conservancy (SFC) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The SFC licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.



/**
 * Describes an event listener registered on an {@linkplain EventEmitter}.
 */
class Listener {
  /**
   * @param {!Function} fn The actual listener function.
   * @param {(Object|undefined)} scope The object in whose scope to invoke the
   *     listener.
   * @param {boolean} oneshot Whether this listener should only be used once.
   */
  constructor(fn, scope, oneshot) {
    this.fn = fn;
    this.scope = scope;
    this.oneshot = oneshot;
  }
}


/** @type {!WeakMap<!EventEmitter, !Map<string, !Set<!Listener>>>} */
const EVENTS = new WeakMap;


/**
 * Object that can emit events for others to listen for.
 */
class EventEmitter {
  /**
   * Fires an event and calls all listeners.
   * @param {string} type The type of event to emit.
   * @param {...*} var_args Any arguments to pass to each listener.
   */
  emit(type, var_args) {
    let events = EVENTS.get(this);
    if (!events) {
      return;
    }

    let args = Array.prototype.slice.call(arguments, 1);

    let listeners = events.get(type);
    if (listeners) {
      for (let listener of listeners) {
        listener.fn.apply(listener.scope, args);
        if (listener.oneshot) {
          listeners.delete(listener);
        }
      }
    }
  }

  /**
   * Returns a mutable list of listeners for a specific type of event.
   * @param {string} type The type of event to retrieve the listeners for.
   * @return {!Set<!Listener>} The registered listeners for the given event
   *     type.
   */
  listeners(type) {
    let events = EVENTS.get(this);
    if (!events) {
      events = new Map;
      EVENTS.set(this, events);
    }

    let listeners = events.get(type);
    if (!listeners) {
      listeners = new Set;
      events.set(type, listeners);
    }
    return listeners;
  }

  /**
   * Registers a listener.
   * @param {string} type The type of event to listen for.
   * @param {!Function} fn The function to invoke when the event is fired.
   * @param {Object=} opt_self The object in whose scope to invoke the listener.
   * @param {boolean=} opt_oneshot Whether the listener should b (e removed after
   *    the first event is fired.
   * @return {!EventEmitter} A self reference.
   * @private
   */
  addListener_(type, fn, opt_self, opt_oneshot) {
    let listeners = this.listeners(type);
    for (let listener of listeners) {
      if (listener.fn === fn) {
        return this;
      }
    }
    listeners.add(new Listener(fn, opt_self || undefined, !!opt_oneshot));
    return this;
  }

  /**
   * Registers a listener.
   * @param {string} type The type of event to listen for.
   * @param {!Function} fn The function to invoke when the event is fired.
   * @param {Object=} opt_self The object in whose scope to invoke the listener.
   * @return {!EventEmitter} A self reference.
   */
  addListener(type, fn, opt_self) {
    return this.addListener_(type, fn, opt_self, false);
  }

  /**
   * Registers a one-time listener which will be called only the first time an
   * event is emitted, after which it will be removed.
   * @param {string} type The type of event to listen for.
   * @param {!Function} fn The function to invoke when the event is fired.
   * @param {Object=} opt_self The object in whose scope to invoke the listener.
   * @return {!EventEmitter} A self reference.
   */
  once(type, fn, opt_self) {
    return this.addListener_(type, fn, opt_self, true);
  }

  /**
   * An alias for {@link #addListener() addListener()}.
   * @param {string} type The type of event to listen for.
   * @param {!Function} fn The function to invoke when the event is fired.
   * @param {Object=} opt_self The object in whose scope to invoke the listener.
   * @return {!EventEmitter} A self reference.
   */
  on(type, fn, opt_self) {
    return this.addListener(type, fn, opt_self);
  }

  /**
   * Removes a previously registered event listener.
   * @param {string} type The type of event to unregister.
   * @param {!Function} listenerFn The handler function to remove.
   * @return {!EventEmitter} A self reference.
   */
  removeListener(type, listenerFn) {
    if (typeof type !== 'string' || typeof listenerFn !== 'function') {
      throw TypeError('invalid args: expected (string, function), got ('
          + (typeof type) + ', ' + (typeof listenerFn) + ')');
    }

    let events = EVENTS.get(this);
    if (!events) {
      return this;
    }

    let listeners = events.get(type);
    if (!listeners) {
      return this;
    }

    let match;
    for (let listener of listeners) {
      if (listener.fn === listenerFn) {
        match = listener;
        break;
      }
    }
    if (match) {
      listeners.delete(match);
      if (!listeners.size) {
        events.delete(type);
      }
    }
    return this;
  }

  /**
   * Removes all listeners for a specific type of event. If no event is
   * specified, all listeners across all types will be removed.
   * @param {string=} opt_type The type of event to remove listeners from.
   * @return {!EventEmitter} A self reference.
   */
  removeAllListeners(opt_type) {
    let events = EVENTS.get(this);
    if (events) {
      if (typeof opt_type === 'string') {
        events.delete(opt_type);
      } else {
        EVENTS.delete(this);
      }
    }
    return this;
  }
}


// PUBLIC API


module.exports = {
  EventEmitter: EventEmitter,
  Listener: Listener
};


/***/ }),
/* 156 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Licensed to the Software Freedom Conservancy (SFC) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The SFC licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

/**
 * @fileoverview The heart of the WebDriver JavaScript API.
 */



const actions = __webpack_require__(157);
const by = __webpack_require__(158);
const Capabilities = __webpack_require__(58).Capabilities;
const command = __webpack_require__(23);
const error = __webpack_require__(19);
const input = __webpack_require__(60);
const logging = __webpack_require__(24);
const {Session} = __webpack_require__(57);
const Symbols = __webpack_require__(59);
const promise = __webpack_require__(56);


/**
 * Defines a condition for use with WebDriver's {@linkplain WebDriver#wait wait
 * command}.
 *
 * @template OUT
 */
class Condition {
  /**
   * @param {string} message A descriptive error message. Should complete the
   *     sentence "Waiting [...]"
   * @param {function(!WebDriver): OUT} fn The condition function to
   *     evaluate on each iteration of the wait loop.
   */
  constructor(message, fn) {
    /** @private {string} */
    this.description_ = 'Waiting ' + message;

    /** @type {function(!WebDriver): OUT} */
    this.fn = fn;
  }

  /** @return {string} A description of this condition. */
  description() {
    return this.description_;
  }
}


/**
 * Defines a condition that will result in a {@link WebElement}.
 *
 * @extends {Condition<!(WebElement|IThenable<!WebElement>)>}
 */
class WebElementCondition extends Condition {
  /**
   * @param {string} message A descriptive error message. Should complete the
   *     sentence "Waiting [...]"
   * @param {function(!WebDriver): !(WebElement|IThenable<!WebElement>)}
   *     fn The condition function to evaluate on each iteration of the wait
   *     loop.
   */
  constructor(message, fn) {
    super(message, fn);
  }
}


//////////////////////////////////////////////////////////////////////////////
//
//  WebDriver
//
//////////////////////////////////////////////////////////////////////////////


/**
 * Translates a command to its wire-protocol representation before passing it
 * to the given `executor` for execution.
 * @param {!command.Executor} executor The executor to use.
 * @param {!command.Command} command The command to execute.
 * @return {!Promise} A promise that will resolve with the command response.
 */
function executeCommand(executor, command) {
  return toWireValue(command.getParameters()).
      then(function(parameters) {
        command.setParameters(parameters);
        return executor.execute(command);
      });
}


/**
 * Converts an object to its JSON representation in the WebDriver wire protocol.
 * When converting values of type object, the following steps will be taken:
 * <ol>
 * <li>if the object is a WebElement, the return value will be the element's
 *     server ID
 * <li>if the object defines a {@link Symbols.serialize} method, this algorithm
 *     will be recursively applied to the object's serialized representation
 * <li>if the object provides a "toJSON" function, this algorithm will
 *     recursively be applied to the result of that function
 * <li>otherwise, the value of each key will be recursively converted according
 *     to the rules above.
 * </ol>
 *
 * @param {*} obj The object to convert.
 * @return {!Promise<?>} A promise that will resolve to the input value's JSON
 *     representation.
 */
function toWireValue(obj) {
  if (promise.isPromise(obj)) {
    return Promise.resolve(obj).then(toWireValue);
  }
  return Promise.resolve(convertValue(obj));
}


function convertValue(value) {
  if (value === void 0 || value === null) {
    return value;
  }

  if (typeof value === 'boolean'
      || typeof value === 'number'
      || typeof value === 'string') {
    return value;
  }

  if (Array.isArray(value)) {
    return convertKeys(value);
  }

  if (typeof value === 'function') {
    return '' + value;
  }

  if (typeof value[Symbols.serialize] === 'function') {
    return toWireValue(value[Symbols.serialize]());
  } else if (typeof value.toJSON === 'function') {
    return toWireValue(value.toJSON());
  }
  return convertKeys(value);
}


function convertKeys(obj) {
  const isArray = Array.isArray(obj);
  const numKeys = isArray ? obj.length : Object.keys(obj).length;
  const ret = isArray ? new Array(numKeys) : {};
  if (!numKeys) {
    return Promise.resolve(ret);
  }

  let numResolved = 0;

  function forEachKey(obj, fn) {
    if (Array.isArray(obj)) {
      for (let i = 0, n = obj.length; i < n; i++) {
        fn(obj[i], i);
      }
    } else {
      for (let key in obj) {
        fn(obj[key], key);
      }
    }
  }

  return new Promise(function(done, reject) {
    forEachKey(obj, function(value, key) {
      if (promise.isPromise(value)) {
        value.then(toWireValue).then(setValue, reject);
      } else {
        value = convertValue(value);
        if (promise.isPromise(value)) {
          value.then(toWireValue).then(setValue, reject);
        } else {
          setValue(value);
        }
      }

      function setValue(value) {
        ret[key] = value;
        maybeFulfill();
      }
    });

    function maybeFulfill() {
      if (++numResolved === numKeys) {
        done(ret);
      }
    }
  });
}


/**
 * Converts a value from its JSON representation according to the WebDriver wire
 * protocol. Any JSON object that defines a WebElement ID will be decoded to a
 * {@link WebElement} object. All other values will be passed through as is.
 *
 * @param {!WebDriver} driver The driver to use as the parent of any unwrapped
 *     {@link WebElement} values.
 * @param {*} value The value to convert.
 * @return {*} The converted value.
 */
function fromWireValue(driver, value) {
  if (Array.isArray(value)) {
    value = value.map(v => fromWireValue(driver, v));
  } else if (WebElement.isId(value)) {
    let id = WebElement.extractId(value);
    value = new WebElement(driver, id);
  } else if (value && typeof value === 'object') {
    let result = {};
    for (let key in value) {
      if (value.hasOwnProperty(key)) {
        result[key] = fromWireValue(driver, value[key]);
      }
    }
    value = result;
  }
  return value;
}


/**
 * Structural interface for a WebDriver client.
 *
 * @record
 */
class IWebDriver {

  /** @return {!promise.ControlFlow} The control flow used by this instance. */
  controlFlow() {}

  /**
   * Schedules a {@link command.Command} to be executed by this driver's
   * {@link command.Executor}.
   *
   * @param {!command.Command} command The command to schedule.
   * @param {string} description A description of the command for debugging.
   * @return {!promise.Thenable<T>} A promise that will be resolved
   *     with the command result.
   * @template T
   */
  schedule(command, description) {}

  /**
   * Sets the {@linkplain input.FileDetector file detector} that should be
   * used with this instance.
   * @param {input.FileDetector} detector The detector to use or {@code null}.
   */
  setFileDetector(detector) {}

  /**
   * @return {!command.Executor} The command executor used by this instance.
   */
  getExecutor() {}

  /**
   * @return {!promise.Thenable<!Session>} A promise for this client's session.
   */
  getSession() {}

  /**
   * @return {!promise.Thenable<!Capabilities>} A promise that will resolve with
   *     the this instance's capabilities.
   */
  getCapabilities() {}

  /**
   * Terminates the browser session. After calling quit, this instance will be
   * invalidated and may no longer be used to issue commands against the
   * browser.
   *
   * @return {!promise.Thenable<void>} A promise that will be resolved when the
   *     command has completed.
   */
  quit() {}

  /**
   * Creates a new action sequence using this driver. The sequence will not be
   * scheduled for execution until {@link actions.ActionSequence#perform} is
   * called. Example:
   *
   *     driver.actions().
   *         mouseDown(element1).
   *         mouseMove(element2).
   *         mouseUp().
   *         perform();
   *
   * @return {!actions.ActionSequence} A new action sequence for this instance.
   */
  actions() {}

  /**
   * Creates a new touch sequence using this driver. The sequence will not be
   * scheduled for execution until {@link actions.TouchSequence#perform} is
   * called. Example:
   *
   *     driver.touchActions().
   *         tap(element1).
   *         doubleTap(element2).
   *         perform();
   *
   * @return {!actions.TouchSequence} A new touch sequence for this instance.
   */
  touchActions() {}

  /**
   * Schedules a command to execute JavaScript in the context of the currently
   * selected frame or window. The script fragment will be executed as the body
   * of an anonymous function. If the script is provided as a function object,
   * that function will be converted to a string for injection into the target
   * window.
   *
   * Any arguments provided in addition to the script will be included as script
   * arguments and may be referenced using the {@code arguments} object.
   * Arguments may be a boolean, number, string, or {@linkplain WebElement}.
   * Arrays and objects may also be used as script arguments as long as each item
   * adheres to the types previously mentioned.
   *
   * The script may refer to any variables accessible from the current window.
   * Furthermore, the script will execute in the window's context, thus
   * {@code document} may be used to refer to the current document. Any local
   * variables will not be available once the script has finished executing,
   * though global variables will persist.
   *
   * If the script has a return value (i.e. if the script contains a return
   * statement), then the following steps will be taken for resolving this
   * functions return value:
   *
   * - For a HTML element, the value will resolve to a {@linkplain WebElement}
   * - Null and undefined return values will resolve to null</li>
   * - Booleans, numbers, and strings will resolve as is</li>
   * - Functions will resolve to their string representation</li>
   * - For arrays and objects, each member item will be converted according to
   *     the rules above
   *
   * @param {!(string|Function)} script The script to execute.
   * @param {...*} var_args The arguments to pass to the script.
   * @return {!promise.Thenable<T>} A promise that will resolve to the
   *    scripts return value.
   * @template T
   */
  executeScript(script, var_args) {}

  /**
   * Schedules a command to execute asynchronous JavaScript in the context of the
   * currently selected frame or window. The script fragment will be executed as
   * the body of an anonymous function. If the script is provided as a function
   * object, that function will be converted to a string for injection into the
   * target window.
   *
   * Any arguments provided in addition to the script will be included as script
   * arguments and may be referenced using the {@code arguments} object.
   * Arguments may be a boolean, number, string, or {@code WebElement}.
   * Arrays and objects may also be used as script arguments as long as each item
   * adheres to the types previously mentioned.
   *
   * Unlike executing synchronous JavaScript with {@link #executeScript},
   * scripts executed with this function must explicitly signal they are finished
   * by invoking the provided callback. This callback will always be injected
   * into the executed function as the last argument, and thus may be referenced
   * with {@code arguments[arguments.length - 1]}. The following steps will be
   * taken for resolving this functions return value against the first argument
   * to the script's callback function:
   *
   * - For a HTML element, the value will resolve to a
   *     {@link WebElement}
   * - Null and undefined return values will resolve to null
   * - Booleans, numbers, and strings will resolve as is
   * - Functions will resolve to their string representation
   * - For arrays and objects, each member item will be converted according to
   *     the rules above
   *
   * __Example #1:__ Performing a sleep that is synchronized with the currently
   * selected window:
   *
   *     var start = new Date().getTime();
   *     driver.executeAsyncScript(
   *         'window.setTimeout(arguments[arguments.length - 1], 500);').
   *         then(function() {
   *           console.log(
   *               'Elapsed time: ' + (new Date().getTime() - start) + ' ms');
   *         });
   *
   * __Example #2:__ Synchronizing a test with an AJAX application:
   *
   *     var button = driver.findElement(By.id('compose-button'));
   *     button.click();
   *     driver.executeAsyncScript(
   *         'var callback = arguments[arguments.length - 1];' +
   *         'mailClient.getComposeWindowWidget().onload(callback);');
   *     driver.switchTo().frame('composeWidget');
   *     driver.findElement(By.id('to')).sendKeys('dog@example.com');
   *
   * __Example #3:__ Injecting a XMLHttpRequest and waiting for the result. In
   * this example, the inject script is specified with a function literal. When
   * using this format, the function is converted to a string for injection, so it
   * should not reference any symbols not defined in the scope of the page under
   * test.
   *
   *     driver.executeAsyncScript(function() {
   *       var callback = arguments[arguments.length - 1];
   *       var xhr = new XMLHttpRequest();
   *       xhr.open("GET", "/resource/data.json", true);
   *       xhr.onreadystatechange = function() {
   *         if (xhr.readyState == 4) {
   *           callback(xhr.responseText);
   *         }
   *       };
   *       xhr.send('');
   *     }).then(function(str) {
   *       console.log(JSON.parse(str)['food']);
   *     });
   *
   * @param {!(string|Function)} script The script to execute.
   * @param {...*} var_args The arguments to pass to the script.
   * @return {!promise.Thenable<T>} A promise that will resolve to the
   *    scripts return value.
   * @template T
   */
  executeAsyncScript(script, var_args) {}

  /**
   * Schedules a command to execute a custom function.
   * @param {function(...): (T|IThenable<T>)} fn The function to execute.
   * @param {Object=} opt_scope The object in whose scope to execute the function.
   * @param {...*} var_args Any arguments to pass to the function.
   * @return {!promise.Thenable<T>} A promise that will be resolved'
   *     with the function's result.
   * @template T
   */
  call(fn, opt_scope, var_args) {}

  /**
   * Schedules a command to wait for a condition to hold. The condition may be
   * specified by a {@link Condition}, as a custom function, or as any
   * promise-like thenable.
   *
   * For a {@link Condition} or function, the wait will repeatedly
   * evaluate the condition until it returns a truthy value. If any errors occur
   * while evaluating the condition, they will be allowed to propagate. In the
   * event a condition returns a {@link promise.Promise promise}, the polling
   * loop will wait for it to be resolved and use the resolved value for whether
   * the condition has been satisfied. Note the resolution time for a promise
   * is factored into whether a wait has timed out.
   *
   * Note, if the provided condition is a {@link WebElementCondition}, then
   * the wait will return a {@link WebElementPromise} that will resolve to the
   * element that satisfied the condition.
   *
   * _Example:_ waiting up to 10 seconds for an element to be present on the
   * page.
   *
   *     var button = driver.wait(until.elementLocated(By.id('foo')), 10000);
   *     button.click();
   *
   * This function may also be used to block the command flow on the resolution
   * of any thenable promise object. When given a promise, the command will
   * simply wait for its resolution before completing. A timeout may be provided
   * to fail the command if the promise does not resolve before the timeout
   * expires.
   *
   * _Example:_ Suppose you have a function, `startTestServer`, that returns a
   * promise for when a server is ready for requests. You can block a WebDriver
   * client on this promise with:
   *
   *     var started = startTestServer();
   *     driver.wait(started, 5 * 1000, 'Server should start within 5 seconds');
   *     driver.get(getServerUrl());
   *
   * @param {!(IThenable<T>|
   *           Condition<T>|
   *           function(!WebDriver): T)} condition The condition to
   *     wait on, defined as a promise, condition object, or  a function to
   *     evaluate as a condition.
   * @param {number=} opt_timeout How long to wait for the condition to be true.
   * @param {string=} opt_message An optional message to use if the wait times
   *     out.
   * @return {!(promise.Thenable<T>|WebElementPromise)} A promise that will be
   *     resolved with the first truthy value returned by the condition
   *     function, or rejected if the condition times out. If the input
   *     input condition is an instance of a {@link WebElementCondition},
   *     the returned value will be a {@link WebElementPromise}.
   * @throws {TypeError} if the provided `condition` is not a valid type.
   * @template T
   */
  wait(condition, opt_timeout, opt_message) {}

  /**
   * Schedules a command to make the driver sleep for the given amount of time.
   * @param {number} ms The amount of time, in milliseconds, to sleep.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the sleep has finished.
   */
  sleep(ms) {}

  /**
   * Schedules a command to retrieve the current window handle.
   * @return {!promise.Thenable<string>} A promise that will be
   *     resolved with the current window handle.
   */
  getWindowHandle() {}

  /**
   * Schedules a command to retrieve the current list of available window handles.
   * @return {!promise.Thenable<!Array<string>>} A promise that will
   *     be resolved with an array of window handles.
   */
  getAllWindowHandles() {}

  /**
   * Schedules a command to retrieve the current page's source. The page source
   * returned is a representation of the underlying DOM: do not expect it to be
   * formatted or escaped in the same way as the response sent from the web
   * server.
   * @return {!promise.Thenable<string>} A promise that will be
   *     resolved with the current page source.
   */
  getPageSource() {}

  /**
   * Schedules a command to close the current window.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when this command has completed.
   */
  close() {}

  /**
   * Schedules a command to navigate to the given URL.
   * @param {string} url The fully qualified URL to open.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the document has finished loading.
   */
  get(url) {}

  /**
   * Schedules a command to retrieve the URL of the current page.
   * @return {!promise.Thenable<string>} A promise that will be
   *     resolved with the current URL.
   */
  getCurrentUrl() {}

  /**
   * Schedules a command to retrieve the current page's title.
   * @return {!promise.Thenable<string>} A promise that will be
   *     resolved with the current page's title.
   */
  getTitle() {}

  /**
   * Schedule a command to find an element on the page. If the element cannot be
   * found, a {@link bot.ErrorCode.NO_SUCH_ELEMENT} result will be returned
   * by the driver. Unlike other commands, this error cannot be suppressed. In
   * other words, scheduling a command to find an element doubles as an assert
   * that the element is present on the page. To test whether an element is
   * present on the page, use {@link #findElements}:
   *
   *     driver.findElements(By.id('foo'))
   *         .then(found => console.log('Element found? %s', !!found.length));
   *
   * The search criteria for an element may be defined using one of the
   * factories in the {@link webdriver.By} namespace, or as a short-hand
   * {@link webdriver.By.Hash} object. For example, the following two statements
   * are equivalent:
   *
   *     var e1 = driver.findElement(By.id('foo'));
   *     var e2 = driver.findElement({id:'foo'});
   *
   * You may also provide a custom locator function, which takes as input this
   * instance and returns a {@link WebElement}, or a promise that will resolve
   * to a WebElement. If the returned promise resolves to an array of
   * WebElements, WebDriver will use the first element. For example, to find the
   * first visible link on a page, you could write:
   *
   *     var link = driver.findElement(firstVisibleLink);
   *
   *     function firstVisibleLink(driver) {
   *       var links = driver.findElements(By.tagName('a'));
   *       return promise.filter(links, function(link) {
   *         return link.isDisplayed();
   *       });
   *     }
   *
   * @param {!(by.By|Function)} locator The locator to use.
   * @return {!WebElementPromise} A WebElement that can be used to issue
   *     commands against the located element. If the element is not found, the
   *     element will be invalidated and all scheduled commands aborted.
   */
  findElement(locator) {}

  /**
   * Schedule a command to search for multiple elements on the page.
   *
   * @param {!(by.By|Function)} locator The locator to use.
   * @return {!promise.Thenable<!Array<!WebElement>>} A
   *     promise that will resolve to an array of WebElements.
   */
  findElements(locator) {}

  /**
   * Schedule a command to take a screenshot. The driver makes a best effort to
   * return a screenshot of the following, in order of preference:
   *
   * 1. Entire page
   * 2. Current window
   * 3. Visible portion of the current frame
   * 4. The entire display containing the browser
   *
   * @return {!promise.Thenable<string>} A promise that will be
   *     resolved to the screenshot as a base-64 encoded PNG.
   */
  takeScreenshot() {}

  /**
   * @return {!Options} The options interface for this instance.
   */
  manage() {}

  /**
   * @return {!Navigation} The navigation interface for this instance.
   */
  navigate() {}

  /**
   * @return {!TargetLocator} The target locator interface for this
   *     instance.
   */
  switchTo() {}
}


/**
 * Each WebDriver instance provides automated control over a browser session.
 *
 * @implements {IWebDriver}
 */
class WebDriver {
  /**
   * @param {!(Session|IThenable<!Session>)} session Either a known session or a
   *     promise that will be resolved to a session.
   * @param {!command.Executor} executor The executor to use when sending
   *     commands to the browser.
   * @param {promise.ControlFlow=} opt_flow The flow to
   *     schedule commands through. Defaults to the active flow object.
   * @param {(function(this: void): ?)=} opt_onQuit A function to call, if any,
   *     when the session is terminated.
   */
  constructor(session, executor, opt_flow, opt_onQuit) {
    /** @private {!promise.ControlFlow} */
    this.flow_ = opt_flow || promise.controlFlow();

    /** @private {!promise.Thenable<!Session>} */
    this.session_ = this.flow_.promise(resolve => resolve(session));

    /** @private {!command.Executor} */
    this.executor_ = executor;

    /** @private {input.FileDetector} */
    this.fileDetector_ = null;

    /** @private @const {(function(this: void): ?|undefined)} */
    this.onQuit_ = opt_onQuit;
  }

  /**
   * Creates a new WebDriver client for an existing session.
   * @param {!command.Executor} executor Command executor to use when querying
   *     for session details.
   * @param {string} sessionId ID of the session to attach to.
   * @param {promise.ControlFlow=} opt_flow The control flow all
   *     driver commands should execute under. Defaults to the
   *     {@link promise.controlFlow() currently active}  control flow.
   * @return {!WebDriver} A new client for the specified session.
   */
  static attachToSession(executor, sessionId, opt_flow) {
    let flow = opt_flow || promise.controlFlow();
    let cmd = new command.Command(command.Name.DESCRIBE_SESSION)
        .setParameter('sessionId', sessionId);
    let session = flow.execute(
        () => executeCommand(executor, cmd).catch(err => {
          // The DESCRIBE_SESSION command is not supported by the W3C spec, so
          // if we get back an unknown command, just return a session with
          // unknown capabilities.
          if (err instanceof error.UnknownCommandError) {
            return new Session(sessionId, new Capabilities);
          }
          throw err;
        }),
        'WebDriver.attachToSession()');
    return new WebDriver(session, executor, flow);
  }

  /**
   * Creates a new WebDriver session.
   *
   * By default, the requested session `capabilities` are merely "desired" and
   * the remote end will still create a new session even if it cannot satisfy
   * all of the requested capabilities. You can query which capabilities a
   * session actually has using the
   * {@linkplain #getCapabilities() getCapabilities()} method on the returned
   * WebDriver instance.
   *
   * To define _required capabilities_, provide the `capabilities` as an object
   * literal with `required` and `desired` keys. The `desired` key may be
   * omitted if all capabilities are required, and vice versa. If the server
   * cannot create a session with all of the required capabilities, it will
   * return an {@linkplain error.SessionNotCreatedError}.
   *
   *     let required = new Capabilities().set('browserName', 'firefox');
   *     let desired = new Capabilities().set('version', '45');
   *     let driver = WebDriver.createSession(executor, {required, desired});
   *
   * This function will always return a WebDriver instance. If there is an error
   * creating the session, such as the aforementioned SessionNotCreatedError,
   * the driver will have a rejected {@linkplain #getSession session} promise.
   * It is recommended that this promise is left _unhandled_ so it will
   * propagate through the {@linkplain promise.ControlFlow control flow} and
   * cause subsequent commands to fail.
   *
   *     let required = Capabilities.firefox();
   *     let driver = WebDriver.createSession(executor, {required});
   *
   *     // If the createSession operation failed, then this command will also
   *     // also fail, propagating the creation failure.
   *     driver.get('http://www.google.com').catch(e => console.log(e));
   *
   * @param {!command.Executor} executor The executor to create the new session
   *     with.
   * @param {(!Capabilities|
   *          {desired: (Capabilities|undefined),
   *           required: (Capabilities|undefined)})} capabilities The desired
   *     capabilities for the new session.
   * @param {promise.ControlFlow=} opt_flow The control flow all driver
   *     commands should execute under, including the initial session creation.
   *     Defaults to the {@link promise.controlFlow() currently active}
   *     control flow.
   * @param {(function(this: void): ?)=} opt_onQuit A callback to invoke when
   *    the newly created session is terminated. This should be used to clean
   *    up any resources associated with the session.
   * @return {!WebDriver} The driver for the newly created session.
   */
  static createSession(executor, capabilities, opt_flow, opt_onQuit) {
    let flow = opt_flow || promise.controlFlow();
    let cmd = new command.Command(command.Name.NEW_SESSION);

    if (capabilities && (capabilities.desired || capabilities.required)) {
      cmd.setParameter('desiredCapabilities', capabilities.desired);
      cmd.setParameter('requiredCapabilities', capabilities.required);
    } else {
      cmd.setParameter('desiredCapabilities', capabilities);
    }

    let session = flow.execute(
        () => executeCommand(executor, cmd),
        'WebDriver.createSession()');
    if (typeof opt_onQuit === 'function') {
      session = session.catch(err => {
        return Promise.resolve(opt_onQuit.call(void 0)).then(_ => {throw err;});
      });
    }
    return new this(session, executor, flow, opt_onQuit);
  }

  /** @override */
  controlFlow() {
    return this.flow_;
  }

  /** @override */
  schedule(command, description) {
    command.setParameter('sessionId', this.session_);

    // If any of the command parameters are rejected promises, those
    // rejections may be reported as unhandled before the control flow
    // attempts to execute the command. To ensure parameters errors
    // propagate through the command itself, we resolve all of the
    // command parameters now, but suppress any errors until the ControlFlow
    // actually executes the command. This addresses scenarios like catching
    // an element not found error in:
    //
    //     driver.findElement(By.id('foo')).click().catch(function(e) {
    //       if (e instanceof NoSuchElementError) {
    //         // Do something.
    //       }
    //     });
    var prepCommand = toWireValue(command.getParameters());
    prepCommand.catch(function() {});

    var flow = this.flow_;
    var executor = this.executor_;
    return flow.execute(() => {
      // Retrieve resolved command parameters; any previously suppressed errors
      // will now propagate up through the control flow as part of the command
      // execution.
      return prepCommand.then(function(parameters) {
        command.setParameters(parameters);
        return executor.execute(command);
      }).then(value => fromWireValue(this, value));
    }, description);
  }

  /** @override */
  setFileDetector(detector) {
    this.fileDetector_ = detector;
  }

  /** @override */
  getExecutor() {
    return this.executor_;
  }

  /** @override */
  getSession() {
    return this.session_;
  }

  /** @override */
  getCapabilities() {
    return this.session_.then(s => s.getCapabilities());
  }

  /** @override */
  quit() {
    var result = this.schedule(
        new command.Command(command.Name.QUIT),
        'WebDriver.quit()');
    // Delete our session ID when the quit command finishes; this will allow us
    // to throw an error when attempting to use a driver post-quit.
    return /** @type {!promise.Thenable} */(promise.finally(result, () => {
      this.session_ = this.flow_.promise((_, reject) => {
        reject(new error.NoSuchSessionError(
            'This driver instance does not have a valid session ID ' +
            '(did you call WebDriver.quit()?) and may no longer be used.'));
      });

      // Only want the session rejection to bubble if accessed.
      this.session_.catch(function() {});

      if (this.onQuit_) {
        return this.onQuit_.call(void 0);
      }
    }));
  }

  /** @override */
  actions() {
    return new actions.ActionSequence(this);
  }

  /** @override */
  touchActions() {
    return new actions.TouchSequence(this);
  }

  /** @override */
  executeScript(script, var_args) {
    if (typeof script === 'function') {
      script = 'return (' + script + ').apply(null, arguments);';
    }
    let args =
        arguments.length > 1 ? Array.prototype.slice.call(arguments, 1) : [];
   return this.schedule(
        new command.Command(command.Name.EXECUTE_SCRIPT).
            setParameter('script', script).
            setParameter('args', args),
        'WebDriver.executeScript()');
  }

  /** @override */
  executeAsyncScript(script, var_args) {
    if (typeof script === 'function') {
      script = 'return (' + script + ').apply(null, arguments);';
    }
    let args = Array.prototype.slice.call(arguments, 1);
    return this.schedule(
        new command.Command(command.Name.EXECUTE_ASYNC_SCRIPT).
            setParameter('script', script).
            setParameter('args', args),
        'WebDriver.executeScript()');
  }

  /** @override */
  call(fn, opt_scope, var_args) {
    let args = Array.prototype.slice.call(arguments, 2);
    return this.flow_.execute(function() {
      return promise.fullyResolved(args).then(function(args) {
        if (promise.isGenerator(fn)) {
          args.unshift(fn, opt_scope);
          return promise.consume.apply(null, args);
        }
        return fn.apply(opt_scope, args);
      });
    }, 'WebDriver.call(' + (fn.name || 'function') + ')');
  }

  /** @override */
  wait(condition, opt_timeout, opt_message) {
    if (promise.isPromise(condition)) {
      return this.flow_.wait(
          /** @type {!IThenable} */(condition),
          opt_timeout, opt_message);
    }

    var message = opt_message;
    var fn = /** @type {!Function} */(condition);
    if (condition instanceof Condition) {
      message = message || condition.description();
      fn = condition.fn;
    }

    if (typeof fn !== 'function') {
      throw TypeError(
          'Wait condition must be a promise-like object, function, or a '
              + 'Condition object');
    }

    var driver = this;
    var result = this.flow_.wait(function() {
      if (promise.isGenerator(fn)) {
        return promise.consume(fn, null, [driver]);
      }
      return fn(driver);
    }, opt_timeout, message);

    if (condition instanceof WebElementCondition) {
      result = new WebElementPromise(this, result.then(function(value) {
        if (!(value instanceof WebElement)) {
          throw TypeError(
              'WebElementCondition did not resolve to a WebElement: '
                  + Object.prototype.toString.call(value));
        }
        return value;
      }));
    }
    return result;
  }

  /** @override */
  sleep(ms) {
    return this.flow_.timeout(ms, 'WebDriver.sleep(' + ms + ')');
  }

  /** @override */
  getWindowHandle() {
    return this.schedule(
        new command.Command(command.Name.GET_CURRENT_WINDOW_HANDLE),
        'WebDriver.getWindowHandle()');
  }

  /** @override */
  getAllWindowHandles() {
    return this.schedule(
        new command.Command(command.Name.GET_WINDOW_HANDLES),
        'WebDriver.getAllWindowHandles()');
  }

  /** @override */
  getPageSource() {
    return this.schedule(
        new command.Command(command.Name.GET_PAGE_SOURCE),
        'WebDriver.getPageSource()');
  }

  /** @override */
  close() {
    return this.schedule(new command.Command(command.Name.CLOSE),
                         'WebDriver.close()');
  }

  /** @override */
  get(url) {
    return this.navigate().to(url);
  }

  /** @override */
  getCurrentUrl() {
    return this.schedule(
        new command.Command(command.Name.GET_CURRENT_URL),
        'WebDriver.getCurrentUrl()');
  }

  /** @override */
  getTitle() {
    return this.schedule(new command.Command(command.Name.GET_TITLE),
                         'WebDriver.getTitle()');
  }

  /** @override */
  findElement(locator) {
    let id;
    locator = by.checkedLocator(locator);
    if (typeof locator === 'function') {
      id = this.findElementInternal_(locator, this);
    } else {
      let cmd = new command.Command(command.Name.FIND_ELEMENT).
          setParameter('using', locator.using).
          setParameter('value', locator.value);
      id = this.schedule(cmd, 'WebDriver.findElement(' + locator + ')');
    }
    return new WebElementPromise(this, id);
  }

  /**
   * @param {!Function} locatorFn The locator function to use.
   * @param {!(WebDriver|WebElement)} context The search
   *     context.
   * @return {!promise.Thenable<!WebElement>} A
   *     promise that will resolve to a list of WebElements.
   * @private
   */
  findElementInternal_(locatorFn, context) {
    return this.call(() => locatorFn(context)).then(function(result) {
      if (Array.isArray(result)) {
        result = result[0];
      }
      if (!(result instanceof WebElement)) {
        throw new TypeError('Custom locator did not return a WebElement');
      }
      return result;
    });
  }

  /** @override */
  findElements(locator) {
    locator = by.checkedLocator(locator);
    if (typeof locator === 'function') {
      return this.findElementsInternal_(locator, this);
    } else {
      let cmd = new command.Command(command.Name.FIND_ELEMENTS).
          setParameter('using', locator.using).
          setParameter('value', locator.value);
      return this.schedule(cmd, 'WebDriver.findElements(' + locator + ')')
          .then(
              (res) => Array.isArray(res) ? res : [],
              (e) =>  {
                if (e instanceof error.NoSuchElementError) {
                  return [];
                }
                throw e;
              });
    }
  }

  /**
   * @param {!Function} locatorFn The locator function to use.
   * @param {!(WebDriver|WebElement)} context The search context.
   * @return {!promise.Thenable<!Array<!WebElement>>} A promise that
   *     will resolve to an array of WebElements.
   * @private
   */
  findElementsInternal_(locatorFn, context) {
    return this.call(() => locatorFn(context)).then(function(result) {
      if (result instanceof WebElement) {
        return [result];
      }

      if (!Array.isArray(result)) {
        return [];
      }

      return result.filter(function(item) {
        return item instanceof WebElement;
      });
    });
  }

  /** @override */
  takeScreenshot() {
    return this.schedule(new command.Command(command.Name.SCREENSHOT),
        'WebDriver.takeScreenshot()');
  }

  /** @override */
  manage() {
    return new Options(this);
  }

  /** @override */
  navigate() {
    return new Navigation(this);
  }

  /** @override */
  switchTo() {
    return new TargetLocator(this);
  }
}


/**
 * Interface for navigating back and forth in the browser history.
 *
 * This class should never be instantiated directly. Instead, obtain an instance
 * with
 *
 *    webdriver.navigate()
 *
 * @see WebDriver#navigate()
 */
class Navigation {
  /**
   * @param {!WebDriver} driver The parent driver.
   * @private
   */
  constructor(driver) {
    /** @private {!WebDriver} */
    this.driver_ = driver;
  }

  /**
   * Schedules a command to navigate to a new URL.
   * @param {string} url The URL to navigate to.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the URL has been loaded.
   */
  to(url) {
    return this.driver_.schedule(
        new command.Command(command.Name.GET).
            setParameter('url', url),
        'WebDriver.navigate().to(' + url + ')');
  }

  /**
   * Schedules a command to move backwards in the browser history.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the navigation event has completed.
   */
  back() {
    return this.driver_.schedule(
        new command.Command(command.Name.GO_BACK),
        'WebDriver.navigate().back()');
  }

  /**
   * Schedules a command to move forwards in the browser history.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the navigation event has completed.
   */
  forward() {
    return this.driver_.schedule(
        new command.Command(command.Name.GO_FORWARD),
        'WebDriver.navigate().forward()');
  }

  /**
   * Schedules a command to refresh the current page.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the navigation event has completed.
   */
  refresh() {
    return this.driver_.schedule(
        new command.Command(command.Name.REFRESH),
        'WebDriver.navigate().refresh()');
  }
}


/**
 * Provides methods for managing browser and driver state.
 *
 * This class should never be instantiated directly. Instead, obtain an instance
 * with {@linkplain WebDriver#manage() webdriver.manage()}.
 */
class Options {
  /**
   * @param {!WebDriver} driver The parent driver.
   * @private
   */
  constructor(driver) {
    /** @private {!WebDriver} */
    this.driver_ = driver;
  }

  /**
   * Schedules a command to add a cookie.
   *
   * __Sample Usage:__
   *
   *     // Set a basic cookie.
   *     driver.manage().addCookie({name: 'foo', value: 'bar'});
   *
   *     // Set a cookie that expires in 10 minutes.
   *     let expiry = new Date(Date.now() + (10 * 60 * 1000));
   *     driver.manage().addCookie({name: 'foo', value: 'bar', expiry});
   *
   *     // The cookie expiration may also be specified in seconds since epoch.
   *     driver.manage().addCookie({
   *       name: 'foo',
   *       value: 'bar',
   *       expiry: Math.floor(Date.now() / 1000)
   *     });
   *
   * @param {!Options.Cookie} spec Defines the cookie to add.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the cookie has been added to the page.
   * @throws {error.InvalidArgumentError} if any of the cookie parameters are
   *     invalid.
   * @throws {TypeError} if `spec` is not a cookie object.
   */
  addCookie({name, value, path, domain, secure, httpOnly, expiry}) {
    // We do not allow '=' or ';' in the name.
    if (/[;=]/.test(name)) {
      throw new error.InvalidArgumentError(
          'Invalid cookie name "' + name + '"');
    }

    // We do not allow ';' in value.
    if (/;/.test(value)) {
      throw new error.InvalidArgumentError(
          'Invalid cookie value "' + value + '"');
    }

    let cookieString = name + '=' + value +
        (domain ? ';domain=' + domain : '') +
        (path ? ';path=' + path : '') +
        (secure ? ';secure' : '');

    if (typeof expiry === 'number') {
      expiry = Math.floor(expiry);
      cookieString += ';expires=' + new Date(expiry * 1000).toUTCString();
    } else if (expiry instanceof Date) {
      let date = /** @type {!Date} */(expiry);
      expiry = Math.floor(date.getTime() / 1000);
      cookieString += ';expires=' + date.toUTCString();
    }

    return this.driver_.schedule(
        new command.Command(command.Name.ADD_COOKIE).
            setParameter('cookie', {
              'name': name,
              'value': value,
              'path': path,
              'domain': domain,
              'secure': !!secure,
              'httpOnly': !!httpOnly,
              'expiry': expiry
            }),
        'WebDriver.manage().addCookie(' + cookieString + ')');
  }

  /**
   * Schedules a command to delete all cookies visible to the current page.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when all cookies have been deleted.
   */
  deleteAllCookies() {
    return this.driver_.schedule(
        new command.Command(command.Name.DELETE_ALL_COOKIES),
        'WebDriver.manage().deleteAllCookies()');
  }

  /**
   * Schedules a command to delete the cookie with the given name. This command
   * is a no-op if there is no cookie with the given name visible to the current
   * page.
   * @param {string} name The name of the cookie to delete.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the cookie has been deleted.
   */
  deleteCookie(name) {
    return this.driver_.schedule(
        new command.Command(command.Name.DELETE_COOKIE).
            setParameter('name', name),
        'WebDriver.manage().deleteCookie(' + name + ')');
  }

  /**
   * Schedules a command to retrieve all cookies visible to the current page.
   * Each cookie will be returned as a JSON object as described by the WebDriver
   * wire protocol.
   * @return {!promise.Thenable<!Array<!Options.Cookie>>} A promise that will be
   *     resolved with the cookies visible to the current browsing context.
   */
  getCookies() {
    return this.driver_.schedule(
        new command.Command(command.Name.GET_ALL_COOKIES),
        'WebDriver.manage().getCookies()');
  }

  /**
   * Schedules a command to retrieve the cookie with the given name. Returns null
   * if there is no such cookie. The cookie will be returned as a JSON object as
   * described by the WebDriver wire protocol.
   *
   * @param {string} name The name of the cookie to retrieve.
   * @return {!promise.Thenable<?Options.Cookie>} A promise that will be resolved
   *     with the named cookie, or `null` if there is no such cookie.
   */
  getCookie(name) {
    return this.getCookies().then(function(cookies) {
      for (let cookie of cookies) {
        if (cookie && cookie['name'] === name) {
          return cookie;
        }
      }
      return null;
    });
  }

  /**
   * Schedules a command to fetch the timeouts currently configured for the
   * current session.
   *
   * @return {!promise.Thenable<{script: number,
   *                             pageLoad: number,
   *                             implicit: number}>} A promise that will be
   *     resolved with the timeouts currently configured for the current
   *     session.
   * @see #setTimeouts()
   */
  getTimeouts() {
    return this.driver_.schedule(
        new command.Command(command.Name.GET_TIMEOUT),
        `WebDriver.manage().getTimeouts()`)
  }

  /**
   * Schedules a command to set timeout durations associated with the current
   * session.
   *
   * The following timeouts are supported (all timeouts are specified in
   * milliseconds):
   *
   * -  `implicit` specifies the maximum amount of time to wait for an element
   *    locator to succeed when {@linkplain WebDriver#findElement locating}
   *    {@linkplain WebDriver#findElements elements} on the page.
   *    Defaults to 0 milliseconds.
   *
   * -  `pageLoad` specifies the maximum amount of time to wait for a page to
   *    finishing loading. Defaults to 300000 milliseconds.
   *
   * -  `script` specifies the maximum amount of time to wait for an
   *    {@linkplain WebDriver#executeScript evaluated script} to run. If set to
   *    `null`, the script timeout will be indefinite.
   *    Defaults to 30000 milliseconds.
   *
   * @param {{script: (number|null|undefined),
   *          pageLoad: (number|null|undefined),
   *          implicit: (number|null|undefined)}} conf
   *     The desired timeout configuration.
   * @return {!promise.Thenable<void>} A promise that will be resolved when the
   *     timeouts have been set.
   * @throws {!TypeError} if an invalid options object is provided.
   * @see #getTimeouts()
   * @see <https://w3c.github.io/webdriver/webdriver-spec.html#dfn-set-timeouts>
   */
  setTimeouts({script, pageLoad, implicit} = {}) {
    let cmd = new command.Command(command.Name.SET_TIMEOUT);

    let valid = false;
    function setParam(key, value) {
      if (value === null || typeof value === 'number') {
        valid = true;
        cmd.setParameter(key, value);
      } else if (typeof value !== 'undefined') {
        throw TypeError(
            'invalid timeouts configuration:'
                + ` expected "${key}" to be a number, got ${typeof value}`);
      }
    }
    setParam('implicit', implicit);
    setParam('pageLoad', pageLoad);
    setParam('script', script);

    if (valid) {
      return this.driver_.schedule(cmd, `WebDriver.manage().setTimeouts()`)
          .catch(() => {
            // Fallback to the legacy method.
            let cmds = [];
            if (typeof script === 'number') {
              cmds.push(legacyTimeout(this.driver_, 'script', script));
            }
            if (typeof implicit === 'number') {
              cmds.push(legacyTimeout(this.driver_, 'implicit', implicit));
            }
            if (typeof pageLoad === 'number') {
              cmds.push(legacyTimeout(this.driver_, 'page load', pageLoad));
            }
            return Promise.all(cmds);
          });
    }
    throw TypeError('no timeouts specified');
  }

  /**
   * @return {!Logs} The interface for managing driver
   *     logs.
   */
  logs() {
    return new Logs(this.driver_);
  }

  /**
   * @return {!Timeouts} The interface for managing driver timeouts.
   * @deprecated Use {@link #setTimeouts()} instead.
   */
  timeouts() {
    return new Timeouts(this.driver_);
  }

  /**
   * @return {!Window} The interface for managing the current window.
   */
  window() {
    return new Window(this.driver_);
  }
}


/**
 * @param {!WebDriver} driver
 * @param {string} type
 * @param {number} ms
 * @return {!promise.Thenable<void>}
 */
function legacyTimeout(driver, type, ms) {
  return driver.schedule(
      new command.Command(command.Name.SET_TIMEOUT)
          .setParameter('type', type)
          .setParameter('ms', ms),
      `WebDriver.manage().setTimeouts({${type}: ${ms}})`);
}



/**
 * A record object describing a browser cookie.
 *
 * @record
 */
Options.Cookie = function() {};


/**
 * The name of the cookie.
 *
 * @type {string}
 */
Options.Cookie.prototype.name;


/**
 * The cookie value.
 *
 * @type {string}
 */
Options.Cookie.prototype.value;


/**
 * The cookie path. Defaults to "/" when adding a cookie.
 *
 * @type {(string|undefined)}
 */
Options.Cookie.prototype.path;


/**
 * The domain the cookie is visible to. Defaults to the current browsing
 * context's document's URL when adding a cookie.
 *
 * @type {(string|undefined)}
 */
Options.Cookie.prototype.domain;


/**
 * Whether the cookie is a secure cookie. Defaults to false when adding a new
 * cookie.
 *
 * @type {(boolean|undefined)}
 */
Options.Cookie.prototype.secure;


/**
 * Whether the cookie is an HTTP only cookie. Defaults to false when adding a
 * new cookie.
 *
 * @type {(boolean|undefined)}
 */
Options.Cookie.prototype.httpOnly;


/**
 * When the cookie expires.
 *
 * When {@linkplain Options#addCookie() adding a cookie}, this may be specified
 * as a {@link Date} object, or in _seconds_ since Unix epoch (January 1, 1970).
 *
 * The expiry is always returned in seconds since epoch when
 * {@linkplain Options#getCookies() retrieving cookies} from the browser.
 *
 * @type {(!Date|number|undefined)}
 */
Options.Cookie.prototype.expiry;


/**
 * An interface for managing timeout behavior for WebDriver instances.
 *
 * This class should never be instantiated directly. Instead, obtain an instance
 * with
 *
 *    webdriver.manage().timeouts()
 *
 * @deprecated This has been deprecated in favor of
 *     {@link Options#setTimeouts()}, which supports setting multiple timeouts
 *     at once.
 * @see WebDriver#manage()
 * @see Options#timeouts()
 */
class Timeouts {
  /**
   * @param {!WebDriver} driver The parent driver.
   * @private
   */
  constructor(driver) {
    /** @private {!WebDriver} */
    this.driver_ = driver;
  }

  /**
   * Specifies the amount of time the driver should wait when searching for an
   * element if it is not immediately present.
   *
   * When searching for a single element, the driver should poll the page
   * until the element has been found, or this timeout expires before failing
   * with a {@link bot.ErrorCode.NO_SUCH_ELEMENT} error. When searching
   * for multiple elements, the driver should poll the page until at least one
   * element has been found or this timeout has expired.
   *
   * Setting the wait timeout to 0 (its default value), disables implicit
   * waiting.
   *
   * Increasing the implicit wait timeout should be used judiciously as it
   * will have an adverse effect on test run time, especially when used with
   * slower location strategies like XPath.
   *
   * @param {number} ms The amount of time to wait, in milliseconds.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the implicit wait timeout has been set.
   * @deprecated Use {@link Options#setTimeouts()
   *     driver.manage().setTimeouts({implicit: ms})}.
   */
  implicitlyWait(ms) {
    return this.driver_.manage().setTimeouts({implicit: ms});
  }

  /**
   * Sets the amount of time to wait, in milliseconds, for an asynchronous
   * script to finish execution before returning an error. If the timeout is
   * less than or equal to 0, the script will be allowed to run indefinitely.
   *
   * @param {number} ms The amount of time to wait, in milliseconds.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the script timeout has been set.
   * @deprecated Use {@link Options#setTimeouts()
   *     driver.manage().setTimeouts({script: ms})}.
   */
  setScriptTimeout(ms) {
    return this.driver_.manage().setTimeouts({script: ms});
  }

  /**
   * Sets the amount of time to wait for a page load to complete before
   * returning an error.  If the timeout is negative, page loads may be
   * indefinite.
   *
   * @param {number} ms The amount of time to wait, in milliseconds.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the timeout has been set.
   * @deprecated Use {@link Options#setTimeouts()
   *     driver.manage().setTimeouts({pageLoad: ms})}.
   */
  pageLoadTimeout(ms) {
    return this.driver_.manage().setTimeouts({pageLoad: ms});
  }
}


/**
 * An interface for managing the current window.
 *
 * This class should never be instantiated directly. Instead, obtain an instance
 * with
 *
 *    webdriver.manage().window()
 *
 * @see WebDriver#manage()
 * @see Options#window()
 */
class Window {
  /**
   * @param {!WebDriver} driver The parent driver.
   * @private
   */
  constructor(driver) {
    /** @private {!WebDriver} */
    this.driver_ = driver;
  }

  /**
   * Retrieves the window's current position, relative to the top left corner of
   * the screen.
   * @return {!promise.Thenable<{x: number, y: number}>} A promise
   *     that will be resolved with the window's position in the form of a
   *     {x:number, y:number} object literal.
   */
  getPosition() {
    return this.driver_.schedule(
        new command.Command(command.Name.GET_WINDOW_POSITION).
            setParameter('windowHandle', 'current'),
        'WebDriver.manage().window().getPosition()');
  }

  /**
   * Repositions the current window.
   * @param {number} x The desired horizontal position, relative to the left
   *     side of the screen.
   * @param {number} y The desired vertical position, relative to the top of the
   *     of the screen.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the command has completed.
   */
  setPosition(x, y) {
    return this.driver_.schedule(
        new command.Command(command.Name.SET_WINDOW_POSITION).
            setParameter('windowHandle', 'current').
            setParameter('x', x).
            setParameter('y', y),
        'WebDriver.manage().window().setPosition(' + x + ', ' + y + ')');
  }

  /**
   * Retrieves the window's current size.
   * @return {!promise.Thenable<{width: number, height: number}>} A
   *     promise that will be resolved with the window's size in the form of a
   *     {width:number, height:number} object literal.
   */
  getSize() {
    return this.driver_.schedule(
        new command.Command(command.Name.GET_WINDOW_SIZE).
            setParameter('windowHandle', 'current'),
        'WebDriver.manage().window().getSize()');
  }

  /**
   * Resizes the current window.
   * @param {number} width The desired window width.
   * @param {number} height The desired window height.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the command has completed.
   */
  setSize(width, height) {
    return this.driver_.schedule(
        new command.Command(command.Name.SET_WINDOW_SIZE).
            setParameter('windowHandle', 'current').
            setParameter('width', width).
            setParameter('height', height),
        'WebDriver.manage().window().setSize(' + width + ', ' + height + ')');
  }

  /**
   * Maximizes the current window.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the command has completed.
   */
  maximize() {
    return this.driver_.schedule(
        new command.Command(command.Name.MAXIMIZE_WINDOW).
            setParameter('windowHandle', 'current'),
        'WebDriver.manage().window().maximize()');
  }
}


/**
 * Interface for managing WebDriver log records.
 *
 * This class should never be instantiated directly. Instead, obtain an
 * instance with
 *
 *     webdriver.manage().logs()
 *
 * @see WebDriver#manage()
 * @see Options#logs()
 */
class Logs {
  /**
   * @param {!WebDriver} driver The parent driver.
   * @private
   */
  constructor(driver) {
    /** @private {!WebDriver} */
    this.driver_ = driver;
  }

  /**
   * Fetches available log entries for the given type.
   *
   * Note that log buffers are reset after each call, meaning that available
   * log entries correspond to those entries not yet returned for a given log
   * type. In practice, this means that this call will return the available log
   * entries since the last call, or from the start of the session.
   *
   * @param {!logging.Type} type The desired log type.
   * @return {!promise.Thenable<!Array.<!logging.Entry>>} A
   *   promise that will resolve to a list of log entries for the specified
   *   type.
   */
  get(type) {
    let cmd = new command.Command(command.Name.GET_LOG).
        setParameter('type', type);
    return this.driver_.schedule(
        cmd, 'WebDriver.manage().logs().get(' + type + ')').
        then(function(entries) {
          return entries.map(function(entry) {
            if (!(entry instanceof logging.Entry)) {
              return new logging.Entry(
                  entry['level'], entry['message'], entry['timestamp'],
                  entry['type']);
            }
            return entry;
          });
        });
  }

  /**
   * Retrieves the log types available to this driver.
   * @return {!promise.Thenable<!Array<!logging.Type>>} A
   *     promise that will resolve to a list of available log types.
   */
  getAvailableLogTypes() {
    return this.driver_.schedule(
        new command.Command(command.Name.GET_AVAILABLE_LOG_TYPES),
        'WebDriver.manage().logs().getAvailableLogTypes()');
  }
}


/**
 * An interface for changing the focus of the driver to another frame or window.
 *
 * This class should never be instantiated directly. Instead, obtain an
 * instance with
 *
 *     webdriver.switchTo()
 *
 * @see WebDriver#switchTo()
 */
class TargetLocator {
  /**
   * @param {!WebDriver} driver The parent driver.
   * @private
   */
  constructor(driver) {
    /** @private {!WebDriver} */
    this.driver_ = driver;
  }

  /**
   * Schedules a command retrieve the {@code document.activeElement} element on
   * the current document, or {@code document.body} if activeElement is not
   * available.
   * @return {!WebElementPromise} The active element.
   */
  activeElement() {
    var id = this.driver_.schedule(
        new command.Command(command.Name.GET_ACTIVE_ELEMENT),
        'WebDriver.switchTo().activeElement()');
    return new WebElementPromise(this.driver_, id);
  }

  /**
   * Schedules a command to switch focus of all future commands to the topmost
   * frame on the page.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the driver has changed focus to the default content.
   */
  defaultContent() {
    return this.driver_.schedule(
        new command.Command(command.Name.SWITCH_TO_FRAME).
            setParameter('id', null),
        'WebDriver.switchTo().defaultContent()');
  }

  /**
   * Schedules a command to switch the focus of all future commands to another
   * frame on the page. The target frame may be specified as one of the
   * following:
   *
   * - A number that specifies a (zero-based) index into [window.frames](
   *   https://developer.mozilla.org/en-US/docs/Web/API/Window.frames).
   * - A {@link WebElement} reference, which correspond to a `frame` or `iframe`
   *   DOM element.
   * - The `null` value, to select the topmost frame on the page. Passing `null`
   *   is the same as calling {@link #defaultContent defaultContent()}.
   *
   * If the specified frame can not be found, the returned promise will be
   * rejected with a {@linkplain error.NoSuchFrameError}.
   *
   * @param {(number|WebElement|null)} id The frame locator.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the driver has changed focus to the specified frame.
   */
  frame(id) {
    return this.driver_.schedule(
        new command.Command(command.Name.SWITCH_TO_FRAME).
            setParameter('id', id),
        'WebDriver.switchTo().frame(' + id + ')');
  }

  /**
   * Schedules a command to switch the focus of all future commands to another
   * window. Windows may be specified by their {@code window.name} attribute or
   * by its handle (as returned by {@link WebDriver#getWindowHandles}).
   *
   * If the specified window cannot be found, the returned promise will be
   * rejected with a {@linkplain error.NoSuchWindowError}.
   *
   * @param {string} nameOrHandle The name or window handle of the window to
   *     switch focus to.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the driver has changed focus to the specified window.
   */
  window(nameOrHandle) {
    return this.driver_.schedule(
        new command.Command(command.Name.SWITCH_TO_WINDOW).
            // "name" supports the legacy drivers. "handle" is the W3C
            // compliant parameter.
            setParameter('name', nameOrHandle).
            setParameter('handle', nameOrHandle),
        'WebDriver.switchTo().window(' + nameOrHandle + ')');
  }

  /**
   * Schedules a command to change focus to the active modal dialog, such as
   * those opened by `window.alert()`, `window.confirm()`, and
   * `window.prompt()`. The returned promise will be rejected with a
   * {@linkplain error.NoSuchAlertError} if there are no open alerts.
   *
   * @return {!AlertPromise} The open alert.
   */
  alert() {
    var text = this.driver_.schedule(
        new command.Command(command.Name.GET_ALERT_TEXT),
        'WebDriver.switchTo().alert()');
    var driver = this.driver_;
    return new AlertPromise(driver, text.then(function(text) {
      return new Alert(driver, text);
    }));
  }
}


//////////////////////////////////////////////////////////////////////////////
//
//  WebElement
//
//////////////////////////////////////////////////////////////////////////////


const LEGACY_ELEMENT_ID_KEY = 'ELEMENT';
const ELEMENT_ID_KEY = 'element-6066-11e4-a52e-4f735466cecf';


/**
 * Represents a DOM element. WebElements can be found by searching from the
 * document root using a {@link WebDriver} instance, or by searching
 * under another WebElement:
 *
 *     driver.get('http://www.google.com');
 *     var searchForm = driver.findElement(By.tagName('form'));
 *     var searchBox = searchForm.findElement(By.name('q'));
 *     searchBox.sendKeys('webdriver');
 */
class WebElement {
  /**
   * @param {!WebDriver} driver the parent WebDriver instance for this element.
   * @param {(!IThenable<string>|string)} id The server-assigned opaque ID for
   *     the underlying DOM element.
   */
  constructor(driver, id) {
    /** @private {!WebDriver} */
    this.driver_ = driver;

    /** @private {!promise.Thenable<string>} */
    this.id_ = driver.controlFlow().promise(resolve => resolve(id));
  }

  /**
   * @param {string} id The raw ID.
   * @param {boolean=} opt_noLegacy Whether to exclude the legacy element key.
   * @return {!Object} The element ID for use with WebDriver's wire protocol.
   */
  static buildId(id, opt_noLegacy) {
    return opt_noLegacy
        ? {[ELEMENT_ID_KEY]: id}
        : {[ELEMENT_ID_KEY]: id, [LEGACY_ELEMENT_ID_KEY]: id};
  }

  /**
   * Extracts the encoded WebElement ID from the object.
   *
   * @param {?} obj The object to extract the ID from.
   * @return {string} the extracted ID.
   * @throws {TypeError} if the object is not a valid encoded ID.
   */
  static extractId(obj) {
    if (obj && typeof obj === 'object') {
      if (typeof obj[ELEMENT_ID_KEY] === 'string') {
        return obj[ELEMENT_ID_KEY];
      } else if (typeof obj[LEGACY_ELEMENT_ID_KEY] === 'string') {
        return obj[LEGACY_ELEMENT_ID_KEY];
      }
    }
    throw new TypeError('object is not a WebElement ID');
  }

  /**
   * @param {?} obj the object to test.
   * @return {boolean} whether the object is a valid encoded WebElement ID.
   */
  static isId(obj) {
    return obj && typeof obj === 'object'
        && (typeof obj[ELEMENT_ID_KEY] === 'string'
            || typeof obj[LEGACY_ELEMENT_ID_KEY] === 'string');
  }

  /**
   * Compares two WebElements for equality.
   *
   * @param {!WebElement} a A WebElement.
   * @param {!WebElement} b A WebElement.
   * @return {!promise.Thenable<boolean>} A promise that will be
   *     resolved to whether the two WebElements are equal.
   */
  static equals(a, b) {
    if (a === b) {
      return a.driver_.controlFlow().promise(resolve => resolve(true));
    }
    let ids = [a.getId(), b.getId()];
    return promise.all(ids).then(function(ids) {
      // If the two element's have the same ID, they should be considered
      // equal. Otherwise, they may still be equivalent, but we'll need to
      // ask the server to check for us.
      if (ids[0] === ids[1]) {
        return true;
      }

      let cmd = new command.Command(command.Name.ELEMENT_EQUALS);
      cmd.setParameter('id', ids[0]);
      cmd.setParameter('other', ids[1]);
      return a.driver_.schedule(cmd, 'WebElement.equals()');
    });
  }

  /** @return {!WebDriver} The parent driver for this instance. */
  getDriver() {
    return this.driver_;
  }

  /**
   * @return {!promise.Thenable<string>} A promise that resolves to
   *     the server-assigned opaque ID assigned to this element.
   */
  getId() {
    return this.id_;
  }

  /**
   * @return {!Object} Returns the serialized representation of this WebElement.
   */
  [Symbols.serialize]() {
    return this.getId().then(WebElement.buildId);
  }

  /**
   * Schedules a command that targets this element with the parent WebDriver
   * instance. Will ensure this element's ID is included in the command
   * parameters under the "id" key.
   *
   * @param {!command.Command} command The command to schedule.
   * @param {string} description A description of the command for debugging.
   * @return {!promise.Thenable<T>} A promise that will be resolved
   *     with the command result.
   * @template T
   * @see WebDriver#schedule
   * @private
   */
  schedule_(command, description) {
    command.setParameter('id', this);
    return this.driver_.schedule(command, description);
  }

  /**
   * Schedule a command to find a descendant of this element. If the element
   * cannot be found, the returned promise will be rejected with a
   * {@linkplain error.NoSuchElementError NoSuchElementError}.
   *
   * The search criteria for an element may be defined using one of the static
   * factories on the {@link by.By} class, or as a short-hand
   * {@link ./by.ByHash} object. For example, the following two statements
   * are equivalent:
   *
   *     var e1 = element.findElement(By.id('foo'));
   *     var e2 = element.findElement({id:'foo'});
   *
   * You may also provide a custom locator function, which takes as input this
   * instance and returns a {@link WebElement}, or a promise that will resolve
   * to a WebElement. If the returned promise resolves to an array of
   * WebElements, WebDriver will use the first element. For example, to find the
   * first visible link on a page, you could write:
   *
   *     var link = element.findElement(firstVisibleLink);
   *
   *     function firstVisibleLink(element) {
   *       var links = element.findElements(By.tagName('a'));
   *       return promise.filter(links, function(link) {
   *         return link.isDisplayed();
   *       });
   *     }
   *
   * @param {!(by.By|Function)} locator The locator strategy to use when
   *     searching for the element.
   * @return {!WebElementPromise} A WebElement that can be used to issue
   *     commands against the located element. If the element is not found, the
   *     element will be invalidated and all scheduled commands aborted.
   */
  findElement(locator) {
    locator = by.checkedLocator(locator);
    let id;
    if (typeof locator === 'function') {
      id = this.driver_.findElementInternal_(locator, this);
    } else {
      let cmd = new command.Command(
          command.Name.FIND_CHILD_ELEMENT).
          setParameter('using', locator.using).
          setParameter('value', locator.value);
      id = this.schedule_(cmd, 'WebElement.findElement(' + locator + ')');
    }
    return new WebElementPromise(this.driver_, id);
  }

  /**
   * Schedules a command to find all of the descendants of this element that
   * match the given search criteria.
   *
   * @param {!(by.By|Function)} locator The locator strategy to use when
   *     searching for the element.
   * @return {!promise.Thenable<!Array<!WebElement>>} A
   *     promise that will resolve to an array of WebElements.
   */
  findElements(locator) {
    locator = by.checkedLocator(locator);
    let id;
    if (typeof locator === 'function') {
      return this.driver_.findElementsInternal_(locator, this);
    } else {
      var cmd = new command.Command(
          command.Name.FIND_CHILD_ELEMENTS).
          setParameter('using', locator.using).
          setParameter('value', locator.value);
      return this.schedule_(cmd, 'WebElement.findElements(' + locator + ')')
          .then(result => Array.isArray(result) ? result : []);
    }
  }

  /**
   * Schedules a command to click on this element.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the click command has completed.
   */
  click() {
    return this.schedule_(
        new command.Command(command.Name.CLICK_ELEMENT),
        'WebElement.click()');
  }

  /**
   * Schedules a command to type a sequence on the DOM element represented by
   * this instance.
   *
   * Modifier keys (SHIFT, CONTROL, ALT, META) are stateful; once a modifier is
   * processed in the key sequence, that key state is toggled until one of the
   * following occurs:
   *
   * - The modifier key is encountered again in the sequence. At this point the
   *   state of the key is toggled (along with the appropriate keyup/down
   *   events).
   * - The {@link input.Key.NULL} key is encountered in the sequence. When
   *   this key is encountered, all modifier keys current in the down state are
   *   released (with accompanying keyup events). The NULL key can be used to
   *   simulate common keyboard shortcuts:
   *
   *         element.sendKeys("text was",
   *                          Key.CONTROL, "a", Key.NULL,
   *                          "now text is");
   *         // Alternatively:
   *         element.sendKeys("text was",
   *                          Key.chord(Key.CONTROL, "a"),
   *                          "now text is");
   *
   * - The end of the key sequence is encountered. When there are no more keys
   *   to type, all depressed modifier keys are released (with accompanying
   *   keyup events).
   *
   * If this element is a file input ({@code <input type="file">}), the
   * specified key sequence should specify the path to the file to attach to
   * the element. This is analogous to the user clicking "Browse..." and entering
   * the path into the file select dialog.
   *
   *     var form = driver.findElement(By.css('form'));
   *     var element = form.findElement(By.css('input[type=file]'));
   *     element.sendKeys('/path/to/file.txt');
   *     form.submit();
   *
   * For uploads to function correctly, the entered path must reference a file
   * on the _browser's_ machine, not the local machine running this script. When
   * running against a remote Selenium server, a {@link input.FileDetector}
   * may be used to transparently copy files to the remote machine before
   * attempting to upload them in the browser.
   *
   * __Note:__ On browsers where native keyboard events are not supported
   * (e.g. Firefox on OS X), key events will be synthesized. Special
   * punctuation keys will be synthesized according to a standard QWERTY en-us
   * keyboard layout.
   *
   * @param {...(number|string|!IThenable<(number|string)>)} var_args The
   *     sequence of keys to type. Number keys may be referenced numerically or
   *     by string (1 or '1'). All arguments will be joined into a single
   *     sequence.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when all keys have been typed.
   */
  sendKeys(var_args) {
    let keys = Promise.all(Array.prototype.slice.call(arguments, 0)).
        then(keys => {
          let ret = [];
          keys.forEach(key => {
            let type = typeof key;
            if (type === 'number') {
              key = String(key);
            } else if (type !== 'string') {
              throw TypeError(
                  'each key must be a number of string; got ' + type);
            }

            // The W3C protocol requires keys to be specified as an array where
            // each element is a single key.
            ret.push.apply(ret, key.split(''));
          });
          return ret;
        });

    if (!this.driver_.fileDetector_) {
      return this.schedule_(
          new command.Command(command.Name.SEND_KEYS_TO_ELEMENT).
              setParameter('text', keys.then(keys => keys.join(''))).
              setParameter('value', keys),
          'WebElement.sendKeys()');
    }

    // Suppress unhandled rejection errors until the flow executes the command.
    keys.catch(function() {});

    var element = this;
    return this.getDriver().controlFlow().execute(function() {
      return keys.then(function(keys) {
        return element.driver_.fileDetector_
            .handleFile(element.driver_, keys.join(''));
      }).then(function(keys) {
        return element.schedule_(
            new command.Command(command.Name.SEND_KEYS_TO_ELEMENT).
                setParameter('text', keys).
                setParameter('value', keys.split('')),
            'WebElement.sendKeys()');
      });
    }, 'WebElement.sendKeys()');
  }

  /**
   * Schedules a command to query for the tag/node name of this element.
   * @return {!promise.Thenable<string>} A promise that will be
   *     resolved with the element's tag name.
   */
  getTagName() {
    return this.schedule_(
        new command.Command(command.Name.GET_ELEMENT_TAG_NAME),
        'WebElement.getTagName()');
  }

  /**
   * Schedules a command to query for the computed style of the element
   * represented by this instance. If the element inherits the named style from
   * its parent, the parent will be queried for its value.  Where possible, color
   * values will be converted to their hex representation (e.g. #00ff00 instead
   * of rgb(0, 255, 0)).
   *
   * _Warning:_ the value returned will be as the browser interprets it, so
   * it may be tricky to form a proper assertion.
   *
   * @param {string} cssStyleProperty The name of the CSS style property to look
   *     up.
   * @return {!promise.Thenable<string>} A promise that will be
   *     resolved with the requested CSS value.
   */
  getCssValue(cssStyleProperty) {
    var name = command.Name.GET_ELEMENT_VALUE_OF_CSS_PROPERTY;
    return this.schedule_(
        new command.Command(name).
            setParameter('propertyName', cssStyleProperty),
        'WebElement.getCssValue(' + cssStyleProperty + ')');
  }

  /**
   * Schedules a command to query for the value of the given attribute of the
   * element. Will return the current value, even if it has been modified after
   * the page has been loaded. More exactly, this method will return the value
   * of the given attribute, unless that attribute is not present, in which case
   * the value of the property with the same name is returned. If neither value
   * is set, null is returned (for example, the "value" property of a textarea
   * element). The "style" attribute is converted as best can be to a
   * text representation with a trailing semi-colon. The following are deemed to
   * be "boolean" attributes and will return either "true" or null:
   *
   * async, autofocus, autoplay, checked, compact, complete, controls, declare,
   * defaultchecked, defaultselected, defer, disabled, draggable, ended,
   * formnovalidate, hidden, indeterminate, iscontenteditable, ismap, itemscope,
   * loop, multiple, muted, nohref, noresize, noshade, novalidate, nowrap, open,
   * paused, pubdate, readonly, required, reversed, scoped, seamless, seeking,
   * selected, spellcheck, truespeed, willvalidate
   *
   * Finally, the following commonly mis-capitalized attribute/property names
   * are evaluated as expected:
   *
   * - "class"
   * - "readonly"
   *
   * @param {string} attributeName The name of the attribute to query.
   * @return {!promise.Thenable<?string>} A promise that will be
   *     resolved with the attribute's value. The returned value will always be
   *     either a string or null.
   */
  getAttribute(attributeName) {
    return this.schedule_(
        new command.Command(command.Name.GET_ELEMENT_ATTRIBUTE).
            setParameter('name', attributeName),
        'WebElement.getAttribute(' + attributeName + ')');
  }

  /**
   * Get the visible (i.e. not hidden by CSS) innerText of this element,
   * including sub-elements, without any leading or trailing whitespace.
   *
   * @return {!promise.Thenable<string>} A promise that will be
   *     resolved with the element's visible text.
   */
  getText() {
    return this.schedule_(
        new command.Command(command.Name.GET_ELEMENT_TEXT),
        'WebElement.getText()');
  }

  /**
   * Schedules a command to compute the size of this element's bounding box, in
   * pixels.
   * @return {!promise.Thenable<{width: number, height: number}>} A
   *     promise that will be resolved with the element's size as a
   *     {@code {width:number, height:number}} object.
   */
  getSize() {
    return this.schedule_(
        new command.Command(command.Name.GET_ELEMENT_SIZE),
        'WebElement.getSize()');
  }

  /**
   * Schedules a command to compute the location of this element in page space.
   * @return {!promise.Thenable<{x: number, y: number}>} A promise that
   *     will be resolved to the element's location as a
   *     {@code {x:number, y:number}} object.
   */
  getLocation() {
    return this.schedule_(
        new command.Command(command.Name.GET_ELEMENT_LOCATION),
        'WebElement.getLocation()');
  }

  /**
   * Schedules a command to query whether the DOM element represented by this
   * instance is enabled, as dictated by the {@code disabled} attribute.
   * @return {!promise.Thenable<boolean>} A promise that will be
   *     resolved with whether this element is currently enabled.
   */
  isEnabled() {
    return this.schedule_(
        new command.Command(command.Name.IS_ELEMENT_ENABLED),
        'WebElement.isEnabled()');
  }

  /**
   * Schedules a command to query whether this element is selected.
   * @return {!promise.Thenable<boolean>} A promise that will be
   *     resolved with whether this element is currently selected.
   */
  isSelected() {
    return this.schedule_(
        new command.Command(command.Name.IS_ELEMENT_SELECTED),
        'WebElement.isSelected()');
  }

  /**
   * Schedules a command to submit the form containing this element (or this
   * element if it is a FORM element). This command is a no-op if the element is
   * not contained in a form.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the form has been submitted.
   */
  submit() {
    return this.schedule_(
        new command.Command(command.Name.SUBMIT_ELEMENT),
        'WebElement.submit()');
  }

  /**
   * Schedules a command to clear the `value` of this element. This command has
   * no effect if the underlying DOM element is neither a text INPUT element
   * nor a TEXTAREA element.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when the element has been cleared.
   */
  clear() {
    return this.schedule_(
        new command.Command(command.Name.CLEAR_ELEMENT),
        'WebElement.clear()');
  }

  /**
   * Schedules a command to test whether this element is currently displayed.
   * @return {!promise.Thenable<boolean>} A promise that will be
   *     resolved with whether this element is currently visible on the page.
   */
  isDisplayed() {
    return this.schedule_(
        new command.Command(command.Name.IS_ELEMENT_DISPLAYED),
        'WebElement.isDisplayed()');
  }

  /**
   * Take a screenshot of the visible region encompassed by this element's
   * bounding rectangle.
   *
   * @param {boolean=} opt_scroll Optional argument that indicates whether the
   *     element should be scrolled into view before taking a screenshot.
   *     Defaults to false.
   * @return {!promise.Thenable<string>} A promise that will be
   *     resolved to the screenshot as a base-64 encoded PNG.
   */
  takeScreenshot(opt_scroll) {
    var scroll = !!opt_scroll;
    return this.schedule_(
        new command.Command(command.Name.TAKE_ELEMENT_SCREENSHOT)
            .setParameter('scroll', scroll),
        'WebElement.takeScreenshot(' + scroll + ')');
  }
}


/**
 * WebElementPromise is a promise that will be fulfilled with a WebElement.
 * This serves as a forward proxy on WebElement, allowing calls to be
 * scheduled without directly on this instance before the underlying
 * WebElement has been fulfilled. In other words, the following two statements
 * are equivalent:
 *
 *     driver.findElement({id: 'my-button'}).click();
 *     driver.findElement({id: 'my-button'}).then(function(el) {
 *       return el.click();
 *     });
 *
 * @implements {promise.CancellableThenable<!WebElement>}
 * @final
 */
class WebElementPromise extends WebElement {
  /**
   * @param {!WebDriver} driver The parent WebDriver instance for this
   *     element.
   * @param {!promise.Thenable<!WebElement>} el A promise
   *     that will resolve to the promised element.
   */
  constructor(driver, el) {
    super(driver, 'unused');

    /**
     * Cancel operation is only supported if the wrapped thenable is also
     * cancellable.
     * @param {(string|Error)=} opt_reason
     * @override
     */
    this.cancel = function(opt_reason) {
      if (promise.CancellableThenable.isImplementation(el)) {
        /** @type {!promise.CancellableThenable} */(el).cancel(opt_reason);
      }
    };

    /** @override */
    this.then = el.then.bind(el);

    /** @override */
    this.catch = el.catch.bind(el);

    /**
     * Defers returning the element ID until the wrapped WebElement has been
     * resolved.
     * @override
     */
    this.getId = function() {
      return el.then(function(el) {
        return el.getId();
      });
    };
  }
}
promise.CancellableThenable.addImplementation(WebElementPromise);


//////////////////////////////////////////////////////////////////////////////
//
//  Alert
//
//////////////////////////////////////////////////////////////////////////////


/**
 * Represents a modal dialog such as {@code alert}, {@code confirm}, or
 * {@code prompt}. Provides functions to retrieve the message displayed with
 * the alert, accept or dismiss the alert, and set the response text (in the
 * case of {@code prompt}).
 */
class Alert {
  /**
   * @param {!WebDriver} driver The driver controlling the browser this alert
   *     is attached to.
   * @param {string} text The message text displayed with this alert.
   */
  constructor(driver, text) {
    /** @private {!WebDriver} */
    this.driver_ = driver;

    /** @private {!promise.Thenable<string>} */
    this.text_ = driver.controlFlow().promise(resolve => resolve(text));
  }

  /**
   * Retrieves the message text displayed with this alert. For instance, if the
   * alert were opened with alert("hello"), then this would return "hello".
   *
   * @return {!promise.Thenable<string>} A promise that will be
   *     resolved to the text displayed with this alert.
   */
  getText() {
    return this.text_;
  }

  /**
   * Sets the username and password in an alert prompting for credentials (such
   * as a Basic HTTP Auth prompt). This method will implicitly
   * {@linkplain #accept() submit} the dialog.
   *
   * @param {string} username The username to send.
   * @param {string} password The password to send.
   * @return {!promise.Thenable<void>} A promise that will be resolved when this
   *     command has completed.
   */
  authenticateAs(username, password) {
    return this.driver_.schedule(
        new command.Command(command.Name.SET_ALERT_CREDENTIALS),
        'WebDriver.switchTo().alert()'
            + `.authenticateAs("${username}", "${password}")`);
  }

  /**
   * Accepts this alert.
   *
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when this command has completed.
   */
  accept() {
    return this.driver_.schedule(
        new command.Command(command.Name.ACCEPT_ALERT),
        'WebDriver.switchTo().alert().accept()');
  }

  /**
   * Dismisses this alert.
   *
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when this command has completed.
   */
  dismiss() {
    return this.driver_.schedule(
        new command.Command(command.Name.DISMISS_ALERT),
        'WebDriver.switchTo().alert().dismiss()');
  }

  /**
   * Sets the response text on this alert. This command will return an error if
   * the underlying alert does not support response text (e.g. window.alert and
   * window.confirm).
   *
   * @param {string} text The text to set.
   * @return {!promise.Thenable<void>} A promise that will be resolved
   *     when this command has completed.
   */
  sendKeys(text) {
    return this.driver_.schedule(
        new command.Command(command.Name.SET_ALERT_TEXT).
            setParameter('text', text),
        'WebDriver.switchTo().alert().sendKeys(' + text + ')');
  }
}


/**
 * AlertPromise is a promise that will be fulfilled with an Alert. This promise
 * serves as a forward proxy on an Alert, allowing calls to be scheduled
 * directly on this instance before the underlying Alert has been fulfilled. In
 * other words, the following two statements are equivalent:
 *
 *     driver.switchTo().alert().dismiss();
 *     driver.switchTo().alert().then(function(alert) {
 *       return alert.dismiss();
 *     });
 *
 * @implements {promise.CancellableThenable<!webdriver.Alert>}
 * @final
 */
class AlertPromise extends Alert {
  /**
   * @param {!WebDriver} driver The driver controlling the browser this
   *     alert is attached to.
   * @param {!promise.Thenable<!Alert>} alert A thenable
   *     that will be fulfilled with the promised alert.
   */
  constructor(driver, alert) {
    super(driver, 'unused');

    /**
     * Cancel operation is only supported if the wrapped thenable is also
     * cancellable.
     * @param {(string|Error)=} opt_reason
     * @override
     */
    this.cancel = function(opt_reason) {
      if (promise.CancellableThenable.isImplementation(alert)) {
        /** @type {!promise.CancellableThenable} */(alert).cancel(opt_reason);
      }
    };

    /** @override */
    this.then = alert.then.bind(alert);

    /** @override */
    this.catch = alert.catch.bind(alert);

    /**
     * Defer returning text until the promised alert has been resolved.
     * @override
     */
    this.getText = function() {
      return alert.then(function(alert) {
        return alert.getText();
      });
    };

    /**
     * Defers action until the alert has been located.
     * @override
     */
    this.authenticateAs = function(username, password) {
      return alert.then(function(alert) {
        return alert.authenticateAs(username, password);
      });
    };

    /**
     * Defers action until the alert has been located.
     * @override
     */
    this.accept = function() {
      return alert.then(function(alert) {
        return alert.accept();
      });
    };

    /**
     * Defers action until the alert has been located.
     * @override
     */
    this.dismiss = function() {
      return alert.then(function(alert) {
        return alert.dismiss();
      });
    };

    /**
     * Defers action until the alert has been located.
     * @override
     */
    this.sendKeys = function(text) {
      return alert.then(function(alert) {
        return alert.sendKeys(text);
      });
    };
  }
}
promise.CancellableThenable.addImplementation(AlertPromise);


// PUBLIC API


module.exports = {
  Alert: Alert,
  AlertPromise: AlertPromise,
  Condition: Condition,
  Logs: Logs,
  Navigation: Navigation,
  Options: Options,
  TargetLocator: TargetLocator,
  Timeouts: Timeouts,
  IWebDriver: IWebDriver,
  WebDriver: WebDriver,
  WebElement: WebElement,
  WebElementCondition: WebElementCondition,
  WebElementPromise: WebElementPromise,
  Window: Window
};


/***/ }),
/* 157 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Licensed to the Software Freedom Conservancy (SFC) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The SFC licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.



const command = __webpack_require__(23);
const error = __webpack_require__(19);
const input = __webpack_require__(60);


/**
 * @param {!IArrayLike} args .
 * @return {!Array} .
 */
function flatten(args) {
  let result = [];
  for (let i = 0; i < args.length; i++) {
    let element = args[i];
    if (Array.isArray(element)) {
      result.push.apply(result, flatten(element));
    } else {
      result.push(element);
    }
  }
  return result;
}


const MODIFIER_KEYS = new Set([
  input.Key.ALT,
  input.Key.CONTROL,
  input.Key.SHIFT,
  input.Key.COMMAND
]);


/**
 * Checks that a key is a modifier key.
 * @param {!input.Key} key The key to check.
 * @throws {error.InvalidArgumentError} If the key is not a modifier key.
 * @private
 */
function checkModifierKey(key) {
  if (!MODIFIER_KEYS.has(key)) {
    throw new error.InvalidArgumentError('Not a modifier key');
  }
}


/**
 * Class for defining sequences of complex user interactions. Each sequence
 * will not be executed until {@link #perform} is called.
 *
 * This class should not be instantiated directly. Instead, obtain an instance
 * using {@link ./webdriver.WebDriver#actions() WebDriver.actions()}.
 *
 * Sample usage:
 *
 *     driver.actions().
 *         keyDown(Key.SHIFT).
 *         click(element1).
 *         click(element2).
 *         dragAndDrop(element3, element4).
 *         keyUp(Key.SHIFT).
 *         perform();
 *
 */
class ActionSequence {
  /**
   * @param {!./webdriver.WebDriver} driver The driver that should be used to
   *     perform this action sequence.
   */
  constructor(driver) {
    /** @private {!./webdriver.WebDriver} */
    this.driver_ = driver;

    /** @private {!Array<{description: string, command: !command.Command}>} */
    this.actions_ = [];
  }

  /**
   * Schedules an action to be executed each time {@link #perform} is called on
   * this instance.
   *
   * @param {string} description A description of the command.
   * @param {!command.Command} command The command.
   * @private
   */
  schedule_(description, command) {
    this.actions_.push({
      description: description,
      command: command
    });
  }

  /**
   * Executes this action sequence.
   *
   * @return {!./promise.Thenable} A promise that will be resolved once
   *     this sequence has completed.
   */
  perform() {
    // Make a protected copy of the scheduled actions. This will protect against
    // users defining additional commands before this sequence is actually
    // executed.
    let actions = this.actions_.concat();
    let driver = this.driver_;
    return driver.controlFlow().execute(function() {
      let results = actions.map(action => {
        return driver.schedule(action.command, action.description);
      });
      return Promise.all(results);
    }, 'ActionSequence.perform');
  }

  /**
   * Moves the mouse. The location to move to may be specified in terms of the
   * mouse's current location, an offset relative to the top-left corner of an
   * element, or an element (in which case the middle of the element is used).
   *
   * @param {(!./webdriver.WebElement|{x: number, y: number})} location The
   *     location to drag to, as either another WebElement or an offset in
   *     pixels.
   * @param {{x: number, y: number}=} opt_offset If the target {@code location}
   *     is defined as a {@link ./webdriver.WebElement}, this parameter defines
   *     an offset within that element. The offset should be specified in pixels
   *     relative to the top-left corner of the element's bounding box. If
   *     omitted, the element's center will be used as the target offset.
   * @return {!ActionSequence} A self reference.
   */
  mouseMove(location, opt_offset) {
    let cmd = new command.Command(command.Name.MOVE_TO);

    if (typeof location.x === 'number') {
      setOffset(/** @type {{x: number, y: number}} */(location));
    } else {
      cmd.setParameter('element', location.getId());
      if (opt_offset) {
        setOffset(opt_offset);
      }
    }

    this.schedule_('mouseMove', cmd);
    return this;

    /** @param {{x: number, y: number}} offset The offset to use. */
    function setOffset(offset) {
      cmd.setParameter('xoffset', offset.x || 0);
      cmd.setParameter('yoffset', offset.y || 0);
    }
  }

  /**
   * Schedules a mouse action.
   * @param {string} description A simple descriptive label for the scheduled
   *     action.
   * @param {!command.Name} commandName The name of the command.
   * @param {(./webdriver.WebElement|input.Button)=} opt_elementOrButton Either
   *     the element to interact with or the button to click with.
   *     Defaults to {@link input.Button.LEFT} if neither an element nor
   *     button is specified.
   * @param {input.Button=} opt_button The button to use. Defaults to
   *     {@link input.Button.LEFT}. Ignored if the previous argument is
   *     provided as a button.
   * @return {!ActionSequence} A self reference.
   * @private
   */
  scheduleMouseAction_(
      description, commandName, opt_elementOrButton, opt_button) {
    let button;
    if (typeof opt_elementOrButton === 'number') {
      button = opt_elementOrButton;
    } else {
      if (opt_elementOrButton) {
        this.mouseMove(
            /** @type {!./webdriver.WebElement} */ (opt_elementOrButton));
      }
      button = opt_button !== void(0) ? opt_button : input.Button.LEFT;
    }

    let cmd = new command.Command(commandName).
        setParameter('button', button);
    this.schedule_(description, cmd);
    return this;
  }

  /**
   * Presses a mouse button. The mouse button will not be released until
   * {@link #mouseUp} is called, regardless of whether that call is made in this
   * sequence or another. The behavior for out-of-order events (e.g. mouseDown,
   * click) is undefined.
   *
   * If an element is provided, the mouse will first be moved to the center
   * of that element. This is equivalent to:
   *
   *     sequence.mouseMove(element).mouseDown()
   *
   * Warning: this method currently only supports the left mouse button. See
   * [issue 4047](http://code.google.com/p/selenium/issues/detail?id=4047).
   *
   * @param {(./webdriver.WebElement|input.Button)=} opt_elementOrButton Either
   *     the element to interact with or the button to click with.
   *     Defaults to {@link input.Button.LEFT} if neither an element nor
   *     button is specified.
   * @param {input.Button=} opt_button The button to use. Defaults to
   *     {@link input.Button.LEFT}. Ignored if a button is provided as the
   *     first argument.
   * @return {!ActionSequence} A self reference.
   */
  mouseDown(opt_elementOrButton, opt_button) {
    return this.scheduleMouseAction_('mouseDown',
        command.Name.MOUSE_DOWN, opt_elementOrButton, opt_button);
  }

  /**
   * Releases a mouse button. Behavior is undefined for calling this function
   * without a previous call to {@link #mouseDown}.
   *
   * If an element is provided, the mouse will first be moved to the center
   * of that element. This is equivalent to:
   *
   *     sequence.mouseMove(element).mouseUp()
   *
   * Warning: this method currently only supports the left mouse button. See
   * [issue 4047](http://code.google.com/p/selenium/issues/detail?id=4047).
   *
   * @param {(./webdriver.WebElement|input.Button)=} opt_elementOrButton Either
   *     the element to interact with or the button to click with.
   *     Defaults to {@link input.Button.LEFT} if neither an element nor
   *     button is specified.
   * @param {input.Button=} opt_button The button to use. Defaults to
   *     {@link input.Button.LEFT}. Ignored if a button is provided as the
   *     first argument.
   * @return {!ActionSequence} A self reference.
   */
  mouseUp(opt_elementOrButton, opt_button) {
    return this.scheduleMouseAction_('mouseUp',
        command.Name.MOUSE_UP, opt_elementOrButton, opt_button);
  }

  /**
   * Convenience function for performing a "drag and drop" manuever. The target
   * element may be moved to the location of another element, or by an offset (in
   * pixels).
   *
   * @param {!./webdriver.WebElement} element The element to drag.
   * @param {(!./webdriver.WebElement|{x: number, y: number})} location The
   *     location to drag to, either as another WebElement or an offset in
   *     pixels.
   * @return {!ActionSequence} A self reference.
   */
  dragAndDrop(element, location) {
    return this.mouseDown(element).mouseMove(location).mouseUp();
  }

  /**
   * Clicks a mouse button.
   *
   * If an element is provided, the mouse will first be moved to the center
   * of that element. This is equivalent to:
   *
   *     sequence.mouseMove(element).click()
   *
   * @param {(./webdriver.WebElement|input.Button)=} opt_elementOrButton Either
   *     the element to interact with or the button to click with.
   *     Defaults to {@link input.Button.LEFT} if neither an element nor
   *     button is specified.
   * @param {input.Button=} opt_button The button to use. Defaults to
   *     {@link input.Button.LEFT}. Ignored if a button is provided as the
   *     first argument.
   * @return {!ActionSequence} A self reference.
   */
  click(opt_elementOrButton, opt_button) {
    return this.scheduleMouseAction_('click',
        command.Name.CLICK, opt_elementOrButton, opt_button);
  }

  /**
   * Double-clicks a mouse button.
   *
   * If an element is provided, the mouse will first be moved to the center of
   * that element. This is equivalent to:
   *
   *     sequence.mouseMove(element).doubleClick()
   *
   * Warning: this method currently only supports the left mouse button. See
   * [issue 4047](http://code.google.com/p/selenium/issues/detail?id=4047).
   *
   * @param {(./webdriver.WebElement|input.Button)=} opt_elementOrButton Either
   *     the element to interact with or the button to click with.
   *     Defaults to {@link input.Button.LEFT} if neither an element nor
   *     button is specified.
   * @param {input.Button=} opt_button The button to use. Defaults to
   *     {@link input.Button.LEFT}. Ignored if a button is provided as the
   *     first argument.
   * @return {!ActionSequence} A self reference.
   */
  doubleClick(opt_elementOrButton, opt_button) {
    return this.scheduleMouseAction_('doubleClick',
        command.Name.DOUBLE_CLICK, opt_elementOrButton, opt_button);
  }

  /**
   * Schedules a keyboard action.
   *
   * @param {string} description A simple descriptive label for the scheduled
   *     action.
   * @param {!Array<(string|!input.Key)>} keys The keys to send.
   * @return {!ActionSequence} A self reference.
   * @private
   */
  scheduleKeyboardAction_(description, keys) {
    let cmd = new command.Command(command.Name.SEND_KEYS_TO_ACTIVE_ELEMENT)
        .setParameter('value', keys);
    this.schedule_(description, cmd);
    return this;
  }

  /**
   * Performs a modifier key press. The modifier key is <em>not released</em>
   * until {@link #keyUp} or {@link #sendKeys} is called. The key press will be
   * targeted at the currently focused element.
   *
   * @param {!input.Key} key The modifier key to push. Must be one of
   *     {ALT, CONTROL, SHIFT, COMMAND, META}.
   * @return {!ActionSequence} A self reference.
   * @throws {error.InvalidArgumentError} If the key is not a valid modifier
   *     key.
   */
  keyDown(key) {
    checkModifierKey(key);
    return this.scheduleKeyboardAction_('keyDown', [key]);
  }

  /**
   * Performs a modifier key release. The release is targeted at the currently
   * focused element.
   * @param {!input.Key} key The modifier key to release. Must be one of
   *     {ALT, CONTROL, SHIFT, COMMAND, META}.
   * @return {!ActionSequence} A self reference.
   * @throws {error.InvalidArgumentError} If the key is not a valid modifier
   *     key.
   */
  keyUp(key) {
    checkModifierKey(key);
    return this.scheduleKeyboardAction_('keyUp', [key]);
  }

  /**
   * Simulates typing multiple keys. Each modifier key encountered in the
   * sequence will not be released until it is encountered again. All key events
   * will be targeted at the currently focused element.
   *
   * @param {...(string|!input.Key|!Array<(string|!input.Key)>)} var_args
   *     The keys to type.
   * @return {!ActionSequence} A self reference.
   * @throws {Error} If the key is not a valid modifier key.
   */
  sendKeys(var_args) {
    let keys = flatten(arguments);
    return this.scheduleKeyboardAction_('sendKeys', keys);
  }
}


/**
 * Class for defining sequences of user touch interactions. Each sequence
 * will not be executed until {@link #perform} is called.
 *
 * This class should not be instantiated directly. Instead, obtain an instance
 * using {@link ./webdriver.WebDriver#touchActions() WebDriver.touchActions()}.
 *
 * Sample usage:
 *
 *     driver.touchActions().
 *         tapAndHold({x: 0, y: 0}).
 *         move({x: 3, y: 4}).
 *         release({x: 10, y: 10}).
 *         perform();
 *
 */
class TouchSequence {
  /**
   * @param {!./webdriver.WebDriver} driver The driver that should be used to
   *     perform this action sequence.
   */
  constructor(driver) {
    /** @private {!./webdriver.WebDriver} */
    this.driver_ = driver;

    /** @private {!Array<{description: string, command: !command.Command}>} */
    this.actions_ = [];
  }

  /**
   * Schedules an action to be executed each time {@link #perform} is called on
   * this instance.
   * @param {string} description A description of the command.
   * @param {!command.Command} command The command.
   * @private
   */
  schedule_(description, command) {
    this.actions_.push({
      description: description,
      command: command
    });
  }

  /**
   * Executes this action sequence.
   * @return {!./promise.Thenable} A promise that will be resolved once
   *     this sequence has completed.
   */
  perform() {
    // Make a protected copy of the scheduled actions. This will protect against
    // users defining additional commands before this sequence is actually
    // executed.
    let actions = this.actions_.concat();
    let driver = this.driver_;
    return driver.controlFlow().execute(function() {
      let results = actions.map(action => {
        return driver.schedule(action.command, action.description);
      });
      return Promise.all(results);
    }, 'TouchSequence.perform');
  }

  /**
   * Taps an element.
   *
   * @param {!./webdriver.WebElement} elem The element to tap.
   * @return {!TouchSequence} A self reference.
   */
  tap(elem) {
    let cmd = new command.Command(command.Name.TOUCH_SINGLE_TAP).
        setParameter('element', elem.getId());

    this.schedule_('tap', cmd);
    return this;
  }

  /**
   * Double taps an element.
   *
   * @param {!./webdriver.WebElement} elem The element to double tap.
   * @return {!TouchSequence} A self reference.
   */
  doubleTap(elem) {
    let cmd = new command.Command(command.Name.TOUCH_DOUBLE_TAP).
        setParameter('element', elem.getId());

    this.schedule_('doubleTap', cmd);
    return this;
  }

  /**
   * Long press on an element.
   *
   * @param {!./webdriver.WebElement} elem The element to long press.
   * @return {!TouchSequence} A self reference.
   */
  longPress(elem) {
    let cmd = new command.Command(command.Name.TOUCH_LONG_PRESS).
        setParameter('element', elem.getId());

    this.schedule_('longPress', cmd);
    return this;
  }

  /**
   * Touch down at the given location.
   *
   * @param {{x: number, y: number}} location The location to touch down at.
   * @return {!TouchSequence} A self reference.
   */
  tapAndHold(location) {
    let cmd = new command.Command(command.Name.TOUCH_DOWN).
        setParameter('x', location.x).
        setParameter('y', location.y);

    this.schedule_('tapAndHold', cmd);
    return this;
  }

  /**
   * Move a held {@linkplain #tapAndHold touch} to the specified location.
   *
   * @param {{x: number, y: number}} location The location to move to.
   * @return {!TouchSequence} A self reference.
   */
  move(location) {
    let cmd = new command.Command(command.Name.TOUCH_MOVE).
        setParameter('x', location.x).
        setParameter('y', location.y);

    this.schedule_('move', cmd);
    return this;
  }

  /**
   * Release a held {@linkplain #tapAndHold touch} at the specified location.
   *
   * @param {{x: number, y: number}} location The location to release at.
   * @return {!TouchSequence} A self reference.
   */
  release(location) {
    let cmd = new command.Command(command.Name.TOUCH_UP).
        setParameter('x', location.x).
        setParameter('y', location.y);

    this.schedule_('release', cmd);
    return this;
  }

  /**
   * Scrolls the touch screen by the given offset.
   *
   * @param {{x: number, y: number}} offset The offset to scroll to.
   * @return {!TouchSequence} A self reference.
   */
  scroll(offset) {
    let cmd = new command.Command(command.Name.TOUCH_SCROLL).
        setParameter('xoffset', offset.x).
        setParameter('yoffset', offset.y);

    this.schedule_('scroll', cmd);
    return this;
  }

  /**
   * Scrolls the touch screen, starting on `elem` and moving by the specified
   * offset.
   *
   * @param {!./webdriver.WebElement} elem The element where scroll starts.
   * @param {{x: number, y: number}} offset The offset to scroll to.
   * @return {!TouchSequence} A self reference.
   */
  scrollFromElement(elem, offset) {
    let cmd = new command.Command(command.Name.TOUCH_SCROLL).
        setParameter('element', elem.getId()).
        setParameter('xoffset', offset.x).
        setParameter('yoffset', offset.y);

    this.schedule_('scrollFromElement', cmd);
    return this;
  }

  /**
   * Flick, starting anywhere on the screen, at speed xspeed and yspeed.
   *
   * @param {{xspeed: number, yspeed: number}} speed The speed to flick in each
         direction, in pixels per second.
   * @return {!TouchSequence} A self reference.
   */
  flick(speed) {
    let cmd = new command.Command(command.Name.TOUCH_FLICK).
        setParameter('xspeed', speed.xspeed).
        setParameter('yspeed', speed.yspeed);

    this.schedule_('flick', cmd);
    return this;
  }

  /**
   * Flick starting at elem and moving by x and y at specified speed.
   *
   * @param {!./webdriver.WebElement} elem The element where flick starts.
   * @param {{x: number, y: number}} offset The offset to flick to.
   * @param {number} speed The speed to flick at in pixels per second.
   * @return {!TouchSequence} A self reference.
   */
  flickElement(elem, offset, speed) {
    let cmd = new command.Command(command.Name.TOUCH_FLICK).
        setParameter('element', elem.getId()).
        setParameter('xoffset', offset.x).
        setParameter('yoffset', offset.y).
        setParameter('speed', speed);

    this.schedule_('flickElement', cmd);
    return this;
  }
}


// PUBLIC API

module.exports = {
  ActionSequence: ActionSequence,
  TouchSequence: TouchSequence,
};


/***/ }),
/* 158 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
// Licensed to the Software Freedom Conservancy (SFC) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The SFC licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.



/**
 * @fileoverview Factory methods for the supported locator strategies.
 */

/**
 * Short-hand expressions for the primary element locator strategies.
 * For example the following two statements are equivalent:
 *
 *     var e1 = driver.findElement(By.id('foo'));
 *     var e2 = driver.findElement({id: 'foo'});
 *
 * Care should be taken when using JavaScript minifiers (such as the
 * Closure compiler), as locator hashes will always be parsed using
 * the un-obfuscated properties listed.
 *
 * @typedef {(
 *     {className: string}|
 *     {css: string}|
 *     {id: string}|
 *     {js: string}|
 *     {linkText: string}|
 *     {name: string}|
 *     {partialLinkText: string}|
 *     {tagName: string}|
 *     {xpath: string})}
 */
var ByHash;


/**
 * Error thrown if an invalid character is encountered while escaping a CSS
 * identifier.
 * @see https://drafts.csswg.org/cssom/#serialize-an-identifier
 */
class InvalidCharacterError extends Error {
  constructor() {
    super();
    this.name = this.constructor.name;
  }
}


/**
 * Escapes a CSS string.
 * @param {string} css the string to escape.
 * @return {string} the escaped string.
 * @throws {TypeError} if the input value is not a string.
 * @throws {InvalidCharacterError} if the string contains an invalid character.
 * @see https://drafts.csswg.org/cssom/#serialize-an-identifier
 */
function escapeCss(css) {
  if (typeof css !== 'string') {
    throw new TypeError('input must be a string');
  }
  let ret = '';
  const n = css.length;
  for (let i = 0; i  < n; i++) {
    const c = css.charCodeAt(i);
    if (c == 0x0) {
      throw new InvalidCharacterError();
    }

    if ((c >= 0x0001 && c <= 0x001F)
        || c == 0x007F
        || (i == 0 && c >= 0x0030 && c <= 0x0039)
        || (i == 1 && c >= 0x0030 && c <= 0x0039
            && css.charCodeAt(0) == 0x002D)) {
      ret += '\\' + c.toString(16) + ' ';
      continue;
    }

    if (i == 0 && c == 0x002D && n == 1) {
      ret += '\\' + css.charAt(i);
      continue;
    }

    if (c >= 0x0080
        || c == 0x002D                      // -
        || c == 0x005F                      // _
        || (c >= 0x0030 && c <= 0x0039)     // [0-9]
        || (c >= 0x0041 && c <= 0x005A)     // [A-Z]
        || (c >= 0x0061 && c <= 0x007A)) {  // [a-z]
      ret += css.charAt(i);
      continue;
    }

    ret += '\\' + css.charAt(i);
  }
  return ret;
}


/**
 * Describes a mechanism for locating an element on the page.
 * @final
 */
class By {
  /**
   * @param {string} using the name of the location strategy to use.
   * @param {string} value the value to search for.
   */
  constructor(using, value) {
    /** @type {string} */
    this.using = using;

    /** @type {string} */
    this.value = value;
  }

  /**
   * Locates elements that have a specific class name.
   *
   * @param {string} name The class name to search for.
   * @return {!By} The new locator.
   * @see http://www.w3.org/TR/2011/WD-html5-20110525/elements.html#classes
   * @see http://www.w3.org/TR/CSS2/selector.html#class-html
   */
  static className(name) {
    let names = name.split(/\s+/g)
        .filter(s => s.length > 0)
        .map(s => escapeCss(s));
    return By.css('.' + names.join('.'));
  }

  /**
   * Locates elements using a CSS selector.
   *
   * @param {string} selector The CSS selector to use.
   * @return {!By} The new locator.
   * @see http://www.w3.org/TR/CSS2/selector.html
   */
  static css(selector) {
    return new By('css selector', selector);
  }

  /**
   * Locates elements by the ID attribute. This locator uses the CSS selector
   * `*[id="$ID"]`, _not_ `document.getElementById`.
   *
   * @param {string} id The ID to search for.
   * @return {!By} The new locator.
   */
  static id(id) {
    return By.css('*[id="' + escapeCss(id) + '"]');
  }

  /**
   * Locates link elements whose
   * {@linkplain webdriver.WebElement#getText visible text} matches the given
   * string.
   *
   * @param {string} text The link text to search for.
   * @return {!By} The new locator.
   */
  static linkText(text) {
    return new By('link text', text);
  }

  /**
   * Locates an elements by evaluating a
   * {@linkplain webdriver.WebDriver#executeScript JavaScript expression}.
   * The result of this expression must be an element or list of elements.
   *
   * @param {!(string|Function)} script The script to execute.
   * @param {...*} var_args The arguments to pass to the script.
   * @return {function(!./webdriver.WebDriver): !./promise.Promise}
   *     A new JavaScript-based locator function.
   */
  static js(script, var_args) {
    let args = Array.prototype.slice.call(arguments, 0);
    return function(driver) {
      return driver.executeScript.apply(driver, args);
    };
  }

  /**
   * Locates elements whose `name` attribute has the given value.
   *
   * @param {string} name The name attribute to search for.
   * @return {!By} The new locator.
   */
  static name(name) {
    return By.css('*[name="' + escapeCss(name) + '"]');
  }

  /**
   * Locates link elements whose
   * {@linkplain webdriver.WebElement#getText visible text} contains the given
   * substring.
   *
   * @param {string} text The substring to check for in a link's visible text.
   * @return {!By} The new locator.
   */
  static partialLinkText(text) {
    return new By('partial link text', text);
  }

  /**
   * Locates elements with a given tag name.
   *
   * @param {string} name The tag name to search for.
   * @return {!By} The new locator.
   * @deprecated Use {@link By.css() By.css(tagName)} instead.
   */
  static tagName(name) {
    return By.css(name);
  }

  /**
   * Locates elements matching a XPath selector. Care should be taken when
   * using an XPath selector with a {@link webdriver.WebElement} as WebDriver
   * will respect the context in the specified in the selector. For example,
   * given the selector `//div`, WebDriver will search from the document root
   * regardless of whether the locator was used with a WebElement.
   *
   * @param {string} xpath The XPath selector to use.
   * @return {!By} The new locator.
   * @see http://www.w3.org/TR/xpath/
   */
  static xpath(xpath) {
    return new By('xpath', xpath);
  }

  /** @override */
  toString() {
    // The static By.name() overrides this.constructor.name.  Shame...
    return `By(${this.using}, ${this.value})`;
  }
}


/**
 * Checks if a value is a valid locator.
 * @param {!(By|Function|ByHash)} locator The value to check.
 * @return {!(By|Function)} The valid locator.
 * @throws {TypeError} If the given value does not define a valid locator
 *     strategy.
 */
function check(locator) {
  if (locator instanceof By || typeof locator === 'function') {
    return locator;
  }

  if (locator
      && typeof locator === 'object'
      && typeof locator.using === 'string'
      && typeof locator.value === 'string') {
    return new By(locator.using, locator.value);
  }

  for (let key in locator) {
    if (locator.hasOwnProperty(key) && By.hasOwnProperty(key)) {
      return By[key](locator[key]);
    }
  }
  throw new TypeError('Invalid locator');
}



// PUBLIC API

module.exports = {
  By: By,
  checkedLocator: check,
};


/***/ }),
/* 159 */
/***/ (function(module, exports) {

// GENERATED CODE - DO NOT EDIT
module.exports = function(){return function(){var d=this;function f(a){return"string"==typeof a};function h(a,b){this.code=a;this.a=l[a]||m;this.message=b||"";a=this.a.replace(/((?:^|\s+)[a-z])/g,function(a){return a.toUpperCase().replace(/^[\s\xa0]+/g,"")});b=a.length-5;if(0>b||a.indexOf("Error",b)!=b)a+="Error";this.name=a;a=Error(this.message);a.name=this.name;this.stack=a.stack||""}
(function(){var a=Error;function b(){}b.prototype=a.prototype;h.b=a.prototype;h.prototype=new b;h.prototype.constructor=h;h.a=function(b,c,g){for(var e=Array(arguments.length-2),k=2;k<arguments.length;k++)e[k-2]=arguments[k];return a.prototype[c].apply(b,e)}})();var m="unknown error",l={15:"element not selectable",11:"element not visible"};l[31]=m;l[30]=m;l[24]="invalid cookie domain";l[29]="invalid element coordinates";l[12]="invalid element state";l[32]="invalid selector";l[51]="invalid selector";
l[52]="invalid selector";l[17]="javascript error";l[405]="unsupported operation";l[34]="move target out of bounds";l[27]="no such alert";l[7]="no such element";l[8]="no such frame";l[23]="no such window";l[28]="script timeout";l[33]="session not created";l[10]="stale element reference";l[21]="timeout";l[25]="unable to set cookie";l[26]="unexpected alert open";l[13]=m;l[9]="unknown command";h.prototype.toString=function(){return this.name+": "+this.message};var n;a:{var p=d.navigator;if(p){var q=p.userAgent;if(q){n=q;break a}}n=""}function r(a){return-1!=n.indexOf(a)};function t(a,b){for(var e=a.length,c=f(a)?a.split(""):a,g=0;g<e;g++)g in c&&b.call(void 0,c[g],g,a)};function v(){return r("iPhone")&&!r("iPod")&&!r("iPad")};function w(){return(r("Chrome")||r("CriOS"))&&!r("Edge")};var x=r("Opera"),y=r("Trident")||r("MSIE"),z=r("Edge"),A=r("Gecko")&&!(-1!=n.toLowerCase().indexOf("webkit")&&!r("Edge"))&&!(r("Trident")||r("MSIE"))&&!r("Edge"),aa=-1!=n.toLowerCase().indexOf("webkit")&&!r("Edge");function B(){var a=d.document;return a?a.documentMode:void 0}var C;
a:{var D="",E=function(){var a=n;if(A)return/rv\:([^\);]+)(\)|;)/.exec(a);if(z)return/Edge\/([\d\.]+)/.exec(a);if(y)return/\b(?:MSIE|rv)[: ]([^\);]+)(\)|;)/.exec(a);if(aa)return/WebKit\/(\S+)/.exec(a);if(x)return/(?:Version)[ \/]?(\S+)/.exec(a)}();E&&(D=E?E[1]:"");if(y){var F=B();if(null!=F&&F>parseFloat(D)){C=String(F);break a}}C=D}var G;var H=d.document;G=H&&y?B()||("CSS1Compat"==H.compatMode?parseInt(C,10):5):void 0;var ba=r("Firefox"),ca=v()||r("iPod"),da=r("iPad"),I=r("Android")&&!(w()||r("Firefox")||r("Opera")||r("Silk")),ea=w(),J=r("Safari")&&!(w()||r("Coast")||r("Opera")||r("Edge")||r("Silk")||r("Android"))&&!(v()||r("iPad")||r("iPod"));function K(a){return(a=a.exec(n))?a[1]:""}(function(){if(ba)return K(/Firefox\/([0-9.]+)/);if(y||z||x)return C;if(ea)return v()||r("iPad")||r("iPod")?K(/CriOS\/([0-9.]+)/):K(/Chrome\/([0-9.]+)/);if(J&&!(v()||r("iPad")||r("iPod")))return K(/Version\/([0-9.]+)/);if(ca||da){var a=/Version\/(\S+).*Mobile\/(\S+)/.exec(n);if(a)return a[1]+"."+a[2]}else if(I)return(a=K(/Android\s+([0-9.]+)/))?a:K(/Version\/([0-9.]+)/);return""})();var L,M=function(){if(!A)return!1;var a=d.Components;if(!a)return!1;try{if(!a.classes)return!1}catch(g){return!1}var b=a.classes,a=a.interfaces,e=b["@mozilla.org/xpcom/version-comparator;1"].getService(a.nsIVersionComparator),c=b["@mozilla.org/xre/app-info;1"].getService(a.nsIXULAppInfo).version;L=function(a){e.compare(c,""+a)};return!0}(),N=y&&!(8<=Number(G)),fa=y&&!(9<=Number(G));I&&M&&L(2.3);I&&M&&L(4);J&&M&&L(6);var ga={SCRIPT:1,STYLE:1,HEAD:1,IFRAME:1,OBJECT:1},O={IMG:" ",BR:"\n"};function P(a,b,e){if(!(a.nodeName in ga))if(3==a.nodeType)e?b.push(String(a.nodeValue).replace(/(\r\n|\r|\n)/g,"")):b.push(a.nodeValue);else if(a.nodeName in O)b.push(O[a.nodeName]);else for(a=a.firstChild;a;)P(a,b,e),a=a.nextSibling};function Q(a,b){b=b.toLowerCase();return"style"==b?ha(a.style.cssText):N&&"value"==b&&R(a,"INPUT")?a.value:fa&&!0===a[b]?String(a.getAttribute(b)):(a=a.getAttributeNode(b))&&a.specified?a.value:null}var ia=/[;]+(?=(?:(?:[^"]*"){2})*[^"]*$)(?=(?:(?:[^']*'){2})*[^']*$)(?=(?:[^()]*\([^()]*\))*[^()]*$)/;
function ha(a){var b=[];t(a.split(ia),function(a){var c=a.indexOf(":");0<c&&(a=[a.slice(0,c),a.slice(c+1)],2==a.length&&b.push(a[0].toLowerCase(),":",a[1],";"))});b=b.join("");return b=";"==b.charAt(b.length-1)?b:b+";"}function S(a,b){N&&"value"==b&&R(a,"OPTION")&&null===Q(a,"value")?(b=[],P(a,b,!1),a=b.join("")):a=a[b];return a}function R(a,b){b&&"string"!==typeof b&&(b=b.toString());return!!a&&1==a.nodeType&&(!b||a.tagName.toUpperCase()==b)}
function T(a){return R(a,"OPTION")?!0:R(a,"INPUT")?(a=a.type.toLowerCase(),"checkbox"==a||"radio"==a):!1};var ja={"class":"className",readonly:"readOnly"},U="async autofocus autoplay checked compact complete controls declare defaultchecked defaultselected defer disabled draggable ended formnovalidate hidden indeterminate iscontenteditable ismap itemscope loop multiple muted nohref noresize noshade novalidate nowrap open paused pubdate readonly required reversed scoped seamless seeking selected spellcheck truespeed willvalidate".split(" ");function V(a,b){var e=null,c=b.toLowerCase();if("style"==c)return(e=a.style)&&!f(e)&&(e=e.cssText),e;if(("selected"==c||"checked"==c)&&T(a)){if(!T(a))throw new h(15,"Element is not selectable");b="selected";e=a.type&&a.type.toLowerCase();if("checkbox"==e||"radio"==e)b="checked";return S(a,b)?"true":null}var g=R(a,"A");if(R(a,"IMG")&&"src"==c||g&&"href"==c)return(e=Q(a,c))&&(e=S(a,c)),e;if("spellcheck"==c){e=Q(a,c);if(null!==e){if("false"==e.toLowerCase())return"false";if("true"==e.toLowerCase())return"true"}return S(a,
c)+""}g=ja[b]||b;a:if(f(U))c=f(c)&&1==c.length?U.indexOf(c,0):-1;else{for(var u=0;u<U.length;u++)if(u in U&&U[u]===c){c=u;break a}c=-1}if(0<=c)return(e=null!==Q(a,b)||S(a,g))?"true":null;try{var k=S(a,g)}catch(ka){}(c=null==k)||(c=typeof k,c="object"==c&&null!=k||"function"==c);c?e=Q(a,b):e=k;return null!=e?e.toString():null}var W=["_"],X=d;W[0]in X||!X.execScript||X.execScript("var "+W[0]);
for(var Y;W.length&&(Y=W.shift());){var Z;if(Z=!W.length)Z=void 0!==V;Z?X[Y]=V:X[Y]&&X[Y]!==Object.prototype[Y]?X=X[Y]:X=X[Y]={}};; return this._.apply(null,arguments);}.apply({navigator:typeof window!='undefined'?window.navigator:null,document:typeof window!='undefined'?window.document:null}, arguments);};


/***/ }),
/* 160 */
/***/ (function(module, exports) {

// GENERATED CODE - DO NOT EDIT
module.exports = function(){return function(){var k=this;function l(a){return void 0!==a}function m(a){return"string"==typeof a}function aa(a,b){a=a.split(".");var c=k;a[0]in c||!c.execScript||c.execScript("var "+a[0]);for(var d;a.length&&(d=a.shift());)!a.length&&l(b)?c[d]=b:c[d]&&c[d]!==Object.prototype[d]?c=c[d]:c=c[d]={}}
function ba(a){var b=typeof a;if("object"==b)if(a){if(a instanceof Array)return"array";if(a instanceof Object)return b;var c=Object.prototype.toString.call(a);if("[object Window]"==c)return"object";if("[object Array]"==c||"number"==typeof a.length&&"undefined"!=typeof a.splice&&"undefined"!=typeof a.propertyIsEnumerable&&!a.propertyIsEnumerable("splice"))return"array";if("[object Function]"==c||"undefined"!=typeof a.call&&"undefined"!=typeof a.propertyIsEnumerable&&!a.propertyIsEnumerable("call"))return"function"}else return"null";
else if("function"==b&&"undefined"==typeof a.call)return"object";return b}function ca(a,b,c){return a.call.apply(a.bind,arguments)}function da(a,b,c){if(!a)throw Error();if(2<arguments.length){var d=Array.prototype.slice.call(arguments,2);return function(){var c=Array.prototype.slice.call(arguments);Array.prototype.unshift.apply(c,d);return a.apply(b,c)}}return function(){return a.apply(b,arguments)}}
function ea(a,b,c){Function.prototype.bind&&-1!=Function.prototype.bind.toString().indexOf("native code")?ea=ca:ea=da;return ea.apply(null,arguments)}function fa(a,b){var c=Array.prototype.slice.call(arguments,1);return function(){var b=c.slice();b.push.apply(b,arguments);return a.apply(this,b)}}
function p(a,b){function c(){}c.prototype=b.prototype;a.L=b.prototype;a.prototype=new c;a.prototype.constructor=a;a.K=function(a,c,f){for(var d=Array(arguments.length-2),e=2;e<arguments.length;e++)d[e-2]=arguments[e];return b.prototype[c].apply(a,d)}};function ga(a,b){this.code=a;this.a=q[a]||ha;this.message=b||"";a=this.a.replace(/((?:^|\s+)[a-z])/g,function(a){return a.toUpperCase().replace(/^[\s\xa0]+/g,"")});b=a.length-5;if(0>b||a.indexOf("Error",b)!=b)a+="Error";this.name=a;a=Error(this.message);a.name=this.name;this.stack=a.stack||""}p(ga,Error);var ha="unknown error",q={15:"element not selectable",11:"element not visible"};q[31]=ha;q[30]=ha;q[24]="invalid cookie domain";q[29]="invalid element coordinates";q[12]="invalid element state";
q[32]="invalid selector";q[51]="invalid selector";q[52]="invalid selector";q[17]="javascript error";q[405]="unsupported operation";q[34]="move target out of bounds";q[27]="no such alert";q[7]="no such element";q[8]="no such frame";q[23]="no such window";q[28]="script timeout";q[33]="session not created";q[10]="stale element reference";q[21]="timeout";q[25]="unable to set cookie";q[26]="unexpected alert open";q[13]=ha;q[9]="unknown command";ga.prototype.toString=function(){return this.name+": "+this.message};var ia={aliceblue:"#f0f8ff",antiquewhite:"#faebd7",aqua:"#00ffff",aquamarine:"#7fffd4",azure:"#f0ffff",beige:"#f5f5dc",bisque:"#ffe4c4",black:"#000000",blanchedalmond:"#ffebcd",blue:"#0000ff",blueviolet:"#8a2be2",brown:"#a52a2a",burlywood:"#deb887",cadetblue:"#5f9ea0",chartreuse:"#7fff00",chocolate:"#d2691e",coral:"#ff7f50",cornflowerblue:"#6495ed",cornsilk:"#fff8dc",crimson:"#dc143c",cyan:"#00ffff",darkblue:"#00008b",darkcyan:"#008b8b",darkgoldenrod:"#b8860b",darkgray:"#a9a9a9",darkgreen:"#006400",
darkgrey:"#a9a9a9",darkkhaki:"#bdb76b",darkmagenta:"#8b008b",darkolivegreen:"#556b2f",darkorange:"#ff8c00",darkorchid:"#9932cc",darkred:"#8b0000",darksalmon:"#e9967a",darkseagreen:"#8fbc8f",darkslateblue:"#483d8b",darkslategray:"#2f4f4f",darkslategrey:"#2f4f4f",darkturquoise:"#00ced1",darkviolet:"#9400d3",deeppink:"#ff1493",deepskyblue:"#00bfff",dimgray:"#696969",dimgrey:"#696969",dodgerblue:"#1e90ff",firebrick:"#b22222",floralwhite:"#fffaf0",forestgreen:"#228b22",fuchsia:"#ff00ff",gainsboro:"#dcdcdc",
ghostwhite:"#f8f8ff",gold:"#ffd700",goldenrod:"#daa520",gray:"#808080",green:"#008000",greenyellow:"#adff2f",grey:"#808080",honeydew:"#f0fff0",hotpink:"#ff69b4",indianred:"#cd5c5c",indigo:"#4b0082",ivory:"#fffff0",khaki:"#f0e68c",lavender:"#e6e6fa",lavenderblush:"#fff0f5",lawngreen:"#7cfc00",lemonchiffon:"#fffacd",lightblue:"#add8e6",lightcoral:"#f08080",lightcyan:"#e0ffff",lightgoldenrodyellow:"#fafad2",lightgray:"#d3d3d3",lightgreen:"#90ee90",lightgrey:"#d3d3d3",lightpink:"#ffb6c1",lightsalmon:"#ffa07a",
lightseagreen:"#20b2aa",lightskyblue:"#87cefa",lightslategray:"#778899",lightslategrey:"#778899",lightsteelblue:"#b0c4de",lightyellow:"#ffffe0",lime:"#00ff00",limegreen:"#32cd32",linen:"#faf0e6",magenta:"#ff00ff",maroon:"#800000",mediumaquamarine:"#66cdaa",mediumblue:"#0000cd",mediumorchid:"#ba55d3",mediumpurple:"#9370db",mediumseagreen:"#3cb371",mediumslateblue:"#7b68ee",mediumspringgreen:"#00fa9a",mediumturquoise:"#48d1cc",mediumvioletred:"#c71585",midnightblue:"#191970",mintcream:"#f5fffa",mistyrose:"#ffe4e1",
moccasin:"#ffe4b5",navajowhite:"#ffdead",navy:"#000080",oldlace:"#fdf5e6",olive:"#808000",olivedrab:"#6b8e23",orange:"#ffa500",orangered:"#ff4500",orchid:"#da70d6",palegoldenrod:"#eee8aa",palegreen:"#98fb98",paleturquoise:"#afeeee",palevioletred:"#db7093",papayawhip:"#ffefd5",peachpuff:"#ffdab9",peru:"#cd853f",pink:"#ffc0cb",plum:"#dda0dd",powderblue:"#b0e0e6",purple:"#800080",red:"#ff0000",rosybrown:"#bc8f8f",royalblue:"#4169e1",saddlebrown:"#8b4513",salmon:"#fa8072",sandybrown:"#f4a460",seagreen:"#2e8b57",
seashell:"#fff5ee",sienna:"#a0522d",silver:"#c0c0c0",skyblue:"#87ceeb",slateblue:"#6a5acd",slategray:"#708090",slategrey:"#708090",snow:"#fffafa",springgreen:"#00ff7f",steelblue:"#4682b4",tan:"#d2b48c",teal:"#008080",thistle:"#d8bfd8",tomato:"#ff6347",turquoise:"#40e0d0",violet:"#ee82ee",wheat:"#f5deb3",white:"#ffffff",whitesmoke:"#f5f5f5",yellow:"#ffff00",yellowgreen:"#9acd32"};function ja(a,b){this.width=a;this.height=b}ja.prototype.toString=function(){return"("+this.width+" x "+this.height+")"};ja.prototype.ceil=function(){this.width=Math.ceil(this.width);this.height=Math.ceil(this.height);return this};ja.prototype.floor=function(){this.width=Math.floor(this.width);this.height=Math.floor(this.height);return this};ja.prototype.round=function(){this.width=Math.round(this.width);this.height=Math.round(this.height);return this};function ka(a,b){var c=la;return Object.prototype.hasOwnProperty.call(c,a)?c[a]:c[a]=b(a)};var ma=String.prototype.trim?function(a){return a.trim()}:function(a){return a.replace(/^[\s\xa0]+|[\s\xa0]+$/g,"")};function na(a,b){return a<b?-1:a>b?1:0}function oa(a){return String(a).replace(/\-([a-z])/g,function(a,c){return c.toUpperCase()})};/*

 The MIT License

 Copyright (c) 2007 Cybozu Labs, Inc.
 Copyright (c) 2012 Google Inc.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 IN THE SOFTWARE.
*/
function pa(a,b,c){this.a=a;this.b=b||1;this.f=c||1};function qa(a){this.b=a;this.a=0}function ra(a){a=a.match(sa);for(var b=0;b<a.length;b++)ta.test(a[b])&&a.splice(b,1);return new qa(a)}var sa=/\$?(?:(?![0-9-\.])(?:\*|[\w-\.]+):)?(?![0-9-\.])(?:\*|[\w-\.]+)|\/\/|\.\.|::|\d+(?:\.\d*)?|\.\d+|"[^"]*"|'[^']*'|[!<>]=|\s+|./g,ta=/^\s/;function t(a,b){return a.b[a.a+(b||0)]}function u(a){return a.b[a.a++]}function ua(a){return a.b.length<=a.a};var v;a:{var va=k.navigator;if(va){var wa=va.userAgent;if(wa){v=wa;break a}}v=""}function x(a){return-1!=v.indexOf(a)};function y(a,b){this.h=a;this.c=l(b)?b:null;this.b=null;switch(a){case "comment":this.b=8;break;case "text":this.b=3;break;case "processing-instruction":this.b=7;break;case "node":break;default:throw Error("Unexpected argument");}}function xa(a){return"comment"==a||"text"==a||"processing-instruction"==a||"node"==a}y.prototype.a=function(a){return null===this.b||this.b==a.nodeType};y.prototype.f=function(){return this.h};
y.prototype.toString=function(){var a="Kind Test: "+this.h;null===this.c||(a+=z(this.c));return a};function ya(a,b){this.j=a.toLowerCase();a="*"==this.j?"*":"http://www.w3.org/1999/xhtml";this.c=b?b.toLowerCase():a}ya.prototype.a=function(a){var b=a.nodeType;if(1!=b&&2!=b)return!1;b=l(a.localName)?a.localName:a.nodeName;return"*"!=this.j&&this.j!=b.toLowerCase()?!1:"*"==this.c?!0:this.c==(a.namespaceURI?a.namespaceURI.toLowerCase():"http://www.w3.org/1999/xhtml")};ya.prototype.f=function(){return this.j};
ya.prototype.toString=function(){return"Name Test: "+("http://www.w3.org/1999/xhtml"==this.c?"":this.c+":")+this.j};function za(a){switch(a.nodeType){case 1:return fa(Aa,a);case 9:return za(a.documentElement);case 11:case 10:case 6:case 12:return Ba;default:return a.parentNode?za(a.parentNode):Ba}}function Ba(){return null}function Aa(a,b){if(a.prefix==b)return a.namespaceURI||"http://www.w3.org/1999/xhtml";var c=a.getAttributeNode("xmlns:"+b);return c&&c.specified?c.value||null:a.parentNode&&9!=a.parentNode.nodeType?Aa(a.parentNode,b):null};function Ca(a,b){if(m(a))return m(b)&&1==b.length?a.indexOf(b,0):-1;for(var c=0;c<a.length;c++)if(c in a&&a[c]===b)return c;return-1}function A(a,b){for(var c=a.length,d=m(a)?a.split(""):a,e=0;e<c;e++)e in d&&b.call(void 0,d[e],e,a)}function Da(a,b){for(var c=a.length,d=[],e=0,f=m(a)?a.split(""):a,g=0;g<c;g++)if(g in f){var h=f[g];b.call(void 0,h,g,a)&&(d[e++]=h)}return d}function Ea(a,b,c){var d=c;A(a,function(c,f){d=b.call(void 0,d,c,f,a)});return d}
function Fa(a,b){for(var c=a.length,d=m(a)?a.split(""):a,e=0;e<c;e++)if(e in d&&b.call(void 0,d[e],e,a))return!0;return!1}function Ga(a,b){for(var c=a.length,d=m(a)?a.split(""):a,e=0;e<c;e++)if(e in d&&!b.call(void 0,d[e],e,a))return!1;return!0}function Ha(a,b){a:{for(var c=a.length,d=m(a)?a.split(""):a,e=0;e<c;e++)if(e in d&&b.call(void 0,d[e],e,a)){b=e;break a}b=-1}return 0>b?null:m(a)?a.charAt(b):a[b]}function Ia(a){return Array.prototype.concat.apply([],arguments)}
function Ja(a,b,c){return 2>=arguments.length?Array.prototype.slice.call(a,b):Array.prototype.slice.call(a,b,c)};function Ka(){return x("iPhone")&&!x("iPod")&&!x("iPad")};var La="backgroundColor borderTopColor borderRightColor borderBottomColor borderLeftColor color outlineColor".split(" "),Ma=/#([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])/,Na=/^#(?:[0-9a-f]{3}){1,2}$/i,Oa=/^(?:rgba)?\((\d{1,3}),\s?(\d{1,3}),\s?(\d{1,3}),\s?(0|1|0\.\d*)\)$/i,Pa=/^(?:rgb)?\((0|[1-9]\d{0,2}),\s?(0|[1-9]\d{0,2}),\s?(0|[1-9]\d{0,2})\)$/i;function Qa(){return(x("Chrome")||x("CriOS"))&&!x("Edge")};function Ra(a,b){this.x=l(a)?a:0;this.y=l(b)?b:0}Ra.prototype.toString=function(){return"("+this.x+", "+this.y+")"};Ra.prototype.ceil=function(){this.x=Math.ceil(this.x);this.y=Math.ceil(this.y);return this};Ra.prototype.floor=function(){this.x=Math.floor(this.x);this.y=Math.floor(this.y);return this};Ra.prototype.round=function(){this.x=Math.round(this.x);this.y=Math.round(this.y);return this};var Sa=x("Opera"),C=x("Trident")||x("MSIE"),Ta=x("Edge"),Ua=x("Gecko")&&!(-1!=v.toLowerCase().indexOf("webkit")&&!x("Edge"))&&!(x("Trident")||x("MSIE"))&&!x("Edge"),Va=-1!=v.toLowerCase().indexOf("webkit")&&!x("Edge");function Wa(){var a=k.document;return a?a.documentMode:void 0}var Xa;
a:{var Ya="",Za=function(){var a=v;if(Ua)return/rv\:([^\);]+)(\)|;)/.exec(a);if(Ta)return/Edge\/([\d\.]+)/.exec(a);if(C)return/\b(?:MSIE|rv)[: ]([^\);]+)(\)|;)/.exec(a);if(Va)return/WebKit\/(\S+)/.exec(a);if(Sa)return/(?:Version)[ \/]?(\S+)/.exec(a)}();Za&&(Ya=Za?Za[1]:"");if(C){var $a=Wa();if(null!=$a&&$a>parseFloat(Ya)){Xa=String($a);break a}}Xa=Ya}var la={};
function ab(a){return ka(a,function(){for(var b=0,c=ma(String(Xa)).split("."),d=ma(String(a)).split("."),e=Math.max(c.length,d.length),f=0;!b&&f<e;f++){var g=c[f]||"",h=d[f]||"";do{g=/(\d*)(\D*)(.*)/.exec(g)||["","","",""];h=/(\d*)(\D*)(.*)/.exec(h)||["","","",""];if(0==g[0].length&&0==h[0].length)break;b=na(0==g[1].length?0:parseInt(g[1],10),0==h[1].length?0:parseInt(h[1],10))||na(0==g[2].length,0==h[2].length)||na(g[2],h[2]);g=g[3];h=h[3]}while(!b)}return 0<=b})}var bb;var cb=k.document;
bb=cb&&C?Wa()||("CSS1Compat"==cb.compatMode?parseInt(Xa,10):5):void 0;function db(a,b,c,d){this.c=a;this.a=b;this.b=c;this.f=d}db.prototype.toString=function(){return"("+this.c+"t, "+this.a+"r, "+this.b+"b, "+this.f+"l)"};db.prototype.ceil=function(){this.c=Math.ceil(this.c);this.a=Math.ceil(this.a);this.b=Math.ceil(this.b);this.f=Math.ceil(this.f);return this};db.prototype.floor=function(){this.c=Math.floor(this.c);this.a=Math.floor(this.a);this.b=Math.floor(this.b);this.f=Math.floor(this.f);return this};
db.prototype.round=function(){this.c=Math.round(this.c);this.a=Math.round(this.a);this.b=Math.round(this.b);this.f=Math.round(this.f);return this};var eb=x("Firefox"),fb=Ka()||x("iPod"),gb=x("iPad"),hb=x("Android")&&!(Qa()||x("Firefox")||x("Opera")||x("Silk")),ib=Qa(),jb=x("Safari")&&!(Qa()||x("Coast")||x("Opera")||x("Edge")||x("Silk")||x("Android"))&&!(Ka()||x("iPad")||x("iPod"));var D=C&&!(9<=Number(bb)),kb=C&&!(8<=Number(bb));function E(a,b,c,d){this.a=a;this.b=b;this.width=c;this.height=d}E.prototype.toString=function(){return"("+this.a+", "+this.b+" - "+this.width+"w x "+this.height+"h)"};E.prototype.ceil=function(){this.a=Math.ceil(this.a);this.b=Math.ceil(this.b);this.width=Math.ceil(this.width);this.height=Math.ceil(this.height);return this};E.prototype.floor=function(){this.a=Math.floor(this.a);this.b=Math.floor(this.b);this.width=Math.floor(this.width);this.height=Math.floor(this.height);return this};
E.prototype.round=function(){this.a=Math.round(this.a);this.b=Math.round(this.b);this.width=Math.round(this.width);this.height=Math.round(this.height);return this};function lb(a){return(a=a.exec(v))?a[1]:""}(function(){if(eb)return lb(/Firefox\/([0-9.]+)/);if(C||Ta||Sa)return Xa;if(ib)return Ka()||x("iPad")||x("iPod")?lb(/CriOS\/([0-9.]+)/):lb(/Chrome\/([0-9.]+)/);if(jb&&!(Ka()||x("iPad")||x("iPod")))return lb(/Version\/([0-9.]+)/);if(fb||gb){var a=/Version\/(\S+).*Mobile\/(\S+)/.exec(v);if(a)return a[1]+"."+a[2]}else if(hb)return(a=lb(/Android\s+([0-9.]+)/))?a:lb(/Version\/([0-9.]+)/);return""})();function mb(a,b,c,d){this.a=a;this.nodeName=c;this.nodeValue=d;this.nodeType=2;this.parentNode=this.ownerElement=b}function nb(a,b){var c=kb&&"href"==b.nodeName?a.getAttribute(b.nodeName,2):b.nodeValue;return new mb(b,a,b.nodeName,c)};var ob,pb=function(){if(!Ua)return!1;var a=k.Components;if(!a)return!1;try{if(!a.classes)return!1}catch(e){return!1}var b=a.classes,a=a.interfaces,c=b["@mozilla.org/xpcom/version-comparator;1"].getService(a.nsIVersionComparator),d=b["@mozilla.org/xre/app-info;1"].getService(a.nsIXULAppInfo).version;ob=function(a){c.compare(d,""+a)};return!0}(),qb=C&&!(9<=Number(bb));hb&&pb&&ob(2.3);hb&&pb&&ob(4);jb&&pb&&ob(6);function rb(a,b){if(!a||!b)return!1;if(a.contains&&1==b.nodeType)return a==b||a.contains(b);if("undefined"!=typeof a.compareDocumentPosition)return a==b||!!(a.compareDocumentPosition(b)&16);for(;b&&a!=b;)b=b.parentNode;return b==a}
function sb(a,b){if(a==b)return 0;if(a.compareDocumentPosition)return a.compareDocumentPosition(b)&2?1:-1;if(C&&!(9<=Number(bb))){if(9==a.nodeType)return-1;if(9==b.nodeType)return 1}if("sourceIndex"in a||a.parentNode&&"sourceIndex"in a.parentNode){var c=1==a.nodeType,d=1==b.nodeType;if(c&&d)return a.sourceIndex-b.sourceIndex;var e=a.parentNode,f=b.parentNode;return e==f?tb(a,b):!c&&rb(e,b)?-1*ub(a,b):!d&&rb(f,a)?ub(b,a):(c?a.sourceIndex:e.sourceIndex)-(d?b.sourceIndex:f.sourceIndex)}d=F(a);c=d.createRange();
c.selectNode(a);c.collapse(!0);a=d.createRange();a.selectNode(b);a.collapse(!0);return c.compareBoundaryPoints(k.Range.START_TO_END,a)}function ub(a,b){var c=a.parentNode;if(c==b)return-1;for(;b.parentNode!=c;)b=b.parentNode;return tb(b,a)}function tb(a,b){for(;b=b.previousSibling;)if(b==a)return-1;return 1}function F(a){return 9==a.nodeType?a:a.ownerDocument||a.document}function vb(a,b){a&&(a=a.parentNode);for(var c=0;a;){if(b(a))return a;a=a.parentNode;c++}return null}
function wb(a){this.a=a||k.document||document}wb.prototype.getElementsByTagName=function(a,b){return(b||this.a).getElementsByTagName(String(a))};function G(a){var b=null,c=a.nodeType;1==c&&(b=a.textContent,b=void 0==b||null==b?a.innerText:b,b=void 0==b||null==b?"":b);if("string"!=typeof b)if(D&&"title"==a.nodeName.toLowerCase()&&1==c)b=a.text;else if(9==c||1==c){a=9==c?a.documentElement:a.firstChild;for(var c=0,d=[],b="";a;){do 1!=a.nodeType&&(b+=a.nodeValue),D&&"title"==a.nodeName.toLowerCase()&&(b+=a.text),d[c++]=a;while(a=a.firstChild);for(;c&&!(a=d[--c].nextSibling););}}else b=a.nodeValue;return""+b}
function H(a,b,c){if(null===b)return!0;try{if(!a.getAttribute)return!1}catch(d){return!1}kb&&"class"==b&&(b="className");return null==c?!!a.getAttribute(b):a.getAttribute(b,2)==c}function xb(a,b,c,d,e){return(D?yb:zb).call(null,a,b,m(c)?c:null,m(d)?d:null,e||new I)}
function yb(a,b,c,d,e){if(a instanceof ya||8==a.b||c&&null===a.b){var f=b.all;if(!f)return e;var g=Ab(a);if("*"!=g&&(f=b.getElementsByTagName(g),!f))return e;if(c){var h=[];for(a=0;b=f[a++];)H(b,c,d)&&h.push(b);f=h}for(a=0;b=f[a++];)"*"==g&&"!"==b.tagName||J(e,b);return e}Bb(a,b,c,d,e);return e}
function zb(a,b,c,d,e){b.getElementsByName&&d&&"name"==c&&!C?(b=b.getElementsByName(d),A(b,function(b){a.a(b)&&J(e,b)})):b.getElementsByClassName&&d&&"class"==c?(b=b.getElementsByClassName(d),A(b,function(b){b.className==d&&a.a(b)&&J(e,b)})):a instanceof y?Bb(a,b,c,d,e):b.getElementsByTagName&&(b=b.getElementsByTagName(a.f()),A(b,function(a){H(a,c,d)&&J(e,a)}));return e}
function Cb(a,b,c,d,e){var f;if((a instanceof ya||8==a.b||c&&null===a.b)&&(f=b.childNodes)){var g=Ab(a);if("*"!=g&&(f=Da(f,function(a){return a.tagName&&a.tagName.toLowerCase()==g}),!f))return e;c&&(f=Da(f,function(a){return H(a,c,d)}));A(f,function(a){"*"==g&&("!"==a.tagName||"*"==g&&1!=a.nodeType)||J(e,a)});return e}return Db(a,b,c,d,e)}function Db(a,b,c,d,e){for(b=b.firstChild;b;b=b.nextSibling)H(b,c,d)&&a.a(b)&&J(e,b);return e}
function Bb(a,b,c,d,e){for(b=b.firstChild;b;b=b.nextSibling)H(b,c,d)&&a.a(b)&&J(e,b),Bb(a,b,c,d,e)}function Ab(a){if(a instanceof y){if(8==a.b)return"!";if(null===a.b)return"*"}return a.f()};function K(a,b){b&&"string"!==typeof b&&(b=b.toString());return!!a&&1==a.nodeType&&(!b||a.tagName.toUpperCase()==b)};function I(){this.b=this.a=null;this.l=0}function Eb(a){this.node=a;this.a=this.b=null}function Fb(a,b){if(!a.a)return b;if(!b.a)return a;var c=a.a;b=b.a;for(var d=null,e,f=0;c&&b;){e=c.node;var g=b.node;e==g||e instanceof mb&&g instanceof mb&&e.a==g.a?(e=c,c=c.a,b=b.a):0<sb(c.node,b.node)?(e=b,b=b.a):(e=c,c=c.a);(e.b=d)?d.a=e:a.a=e;d=e;f++}for(e=c||b;e;)e.b=d,d=d.a=e,f++,e=e.a;a.b=d;a.l=f;return a}function Gb(a,b){b=new Eb(b);b.a=a.a;a.b?a.a.b=b:a.a=a.b=b;a.a=b;a.l++}
function J(a,b){b=new Eb(b);b.b=a.b;a.a?a.b.a=b:a.a=a.b=b;a.b=b;a.l++}function Hb(a){return(a=a.a)?a.node:null}function Ib(a){return(a=Hb(a))?G(a):""}function L(a,b){return new Jb(a,!!b)}function Jb(a,b){this.f=a;this.b=(this.s=b)?a.b:a.a;this.a=null}function N(a){var b=a.b;if(b){var c=a.a=b;a.b=a.s?b.b:b.a;return c.node}return null};function O(a){this.i=a;this.b=this.g=!1;this.f=null}function z(a){return"\n  "+a.toString().split("\n").join("\n  ")}function Kb(a,b){a.g=b}function Lb(a,b){a.b=b}function Q(a,b){a=a.a(b);return a instanceof I?+Ib(a):+a}function R(a,b){a=a.a(b);return a instanceof I?Ib(a):""+a}function Mb(a,b){a=a.a(b);return a instanceof I?!!a.l:!!a};function Nb(a,b,c){O.call(this,a.i);this.c=a;this.h=b;this.o=c;this.g=b.g||c.g;this.b=b.b||c.b;this.c==Ob&&(c.b||c.g||4==c.i||0==c.i||!b.f?b.b||b.g||4==b.i||0==b.i||!c.f||(this.f={name:c.f.name,u:b}):this.f={name:b.f.name,u:c})}p(Nb,O);
function Pb(a,b,c,d,e){b=b.a(d);c=c.a(d);var f;if(b instanceof I&&c instanceof I){b=L(b);for(d=N(b);d;d=N(b))for(e=L(c),f=N(e);f;f=N(e))if(a(G(d),G(f)))return!0;return!1}if(b instanceof I||c instanceof I){b instanceof I?(e=b,d=c):(e=c,d=b);f=L(e);for(var g=typeof d,h=N(f);h;h=N(f)){switch(g){case "number":h=+G(h);break;case "boolean":h=!!G(h);break;case "string":h=G(h);break;default:throw Error("Illegal primitive type for comparison.");}if(e==b&&a(h,d)||e==c&&a(d,h))return!0}return!1}return e?"boolean"==
typeof b||"boolean"==typeof c?a(!!b,!!c):"number"==typeof b||"number"==typeof c?a(+b,+c):a(b,c):a(+b,+c)}Nb.prototype.a=function(a){return this.c.m(this.h,this.o,a)};Nb.prototype.toString=function(){var a="Binary Expression: "+this.c,a=a+z(this.h);return a+=z(this.o)};function Qb(a,b,c,d){this.I=a;this.D=b;this.i=c;this.m=d}Qb.prototype.toString=function(){return this.I};var Rb={};
function S(a,b,c,d){if(Rb.hasOwnProperty(a))throw Error("Binary operator already created: "+a);a=new Qb(a,b,c,d);return Rb[a.toString()]=a}S("div",6,1,function(a,b,c){return Q(a,c)/Q(b,c)});S("mod",6,1,function(a,b,c){return Q(a,c)%Q(b,c)});S("*",6,1,function(a,b,c){return Q(a,c)*Q(b,c)});S("+",5,1,function(a,b,c){return Q(a,c)+Q(b,c)});S("-",5,1,function(a,b,c){return Q(a,c)-Q(b,c)});S("<",4,2,function(a,b,c){return Pb(function(a,b){return a<b},a,b,c)});
S(">",4,2,function(a,b,c){return Pb(function(a,b){return a>b},a,b,c)});S("<=",4,2,function(a,b,c){return Pb(function(a,b){return a<=b},a,b,c)});S(">=",4,2,function(a,b,c){return Pb(function(a,b){return a>=b},a,b,c)});var Ob=S("=",3,2,function(a,b,c){return Pb(function(a,b){return a==b},a,b,c,!0)});S("!=",3,2,function(a,b,c){return Pb(function(a,b){return a!=b},a,b,c,!0)});S("and",2,2,function(a,b,c){return Mb(a,c)&&Mb(b,c)});S("or",1,2,function(a,b,c){return Mb(a,c)||Mb(b,c)});function Sb(a,b){if(b.a.length&&4!=a.i)throw Error("Primary expression must evaluate to nodeset if filter has predicate(s).");O.call(this,a.i);this.c=a;this.h=b;this.g=a.g;this.b=a.b}p(Sb,O);Sb.prototype.a=function(a){a=this.c.a(a);return Tb(this.h,a)};Sb.prototype.toString=function(){var a="Filter:"+z(this.c);return a+=z(this.h)};function Ub(a,b){if(b.length<a.C)throw Error("Function "+a.j+" expects at least"+a.C+" arguments, "+b.length+" given");if(null!==a.A&&b.length>a.A)throw Error("Function "+a.j+" expects at most "+a.A+" arguments, "+b.length+" given");a.H&&A(b,function(b,d){if(4!=b.i)throw Error("Argument "+d+" to function "+a.j+" is not of type Nodeset: "+b);});O.call(this,a.i);this.v=a;this.c=b;Kb(this,a.g||Fa(b,function(a){return a.g}));Lb(this,a.G&&!b.length||a.F&&!!b.length||Fa(b,function(a){return a.b}))}
p(Ub,O);Ub.prototype.a=function(a){return this.v.m.apply(null,Ia(a,this.c))};Ub.prototype.toString=function(){var a="Function: "+this.v;if(this.c.length)var b=Ea(this.c,function(a,b){return a+z(b)},"Arguments:"),a=a+z(b);return a};function Vb(a,b,c,d,e,f,g,h,r){this.j=a;this.i=b;this.g=c;this.G=d;this.F=e;this.m=f;this.C=g;this.A=l(h)?h:g;this.H=!!r}Vb.prototype.toString=function(){return this.j};var Wb={};
function T(a,b,c,d,e,f,g,h){if(Wb.hasOwnProperty(a))throw Error("Function already created: "+a+".");Wb[a]=new Vb(a,b,c,d,!1,e,f,g,h)}T("boolean",2,!1,!1,function(a,b){return Mb(b,a)},1);T("ceiling",1,!1,!1,function(a,b){return Math.ceil(Q(b,a))},1);T("concat",3,!1,!1,function(a,b){return Ea(Ja(arguments,1),function(b,d){return b+R(d,a)},"")},2,null);T("contains",2,!1,!1,function(a,b,c){b=R(b,a);a=R(c,a);return-1!=b.indexOf(a)},2);T("count",1,!1,!1,function(a,b){return b.a(a).l},1,1,!0);
T("false",2,!1,!1,function(){return!1},0);T("floor",1,!1,!1,function(a,b){return Math.floor(Q(b,a))},1);T("id",4,!1,!1,function(a,b){function c(a){if(D){var b=e.all[a];if(b){if(b.nodeType&&a==b.id)return b;if(b.length)return Ha(b,function(b){return a==b.id})}return null}return e.getElementById(a)}var d=a.a,e=9==d.nodeType?d:d.ownerDocument;a=R(b,a).split(/\s+/);var f=[];A(a,function(a){a=c(a);!a||0<=Ca(f,a)||f.push(a)});f.sort(sb);var g=new I;A(f,function(a){J(g,a)});return g},1);
T("lang",2,!1,!1,function(){return!1},1);T("last",1,!0,!1,function(a){if(1!=arguments.length)throw Error("Function last expects ()");return a.f},0);T("local-name",3,!1,!0,function(a,b){return(a=b?Hb(b.a(a)):a.a)?a.localName||a.nodeName.toLowerCase():""},0,1,!0);T("name",3,!1,!0,function(a,b){return(a=b?Hb(b.a(a)):a.a)?a.nodeName.toLowerCase():""},0,1,!0);T("namespace-uri",3,!0,!1,function(){return""},0,1,!0);
T("normalize-space",3,!1,!0,function(a,b){return(b?R(b,a):G(a.a)).replace(/[\s\xa0]+/g," ").replace(/^\s+|\s+$/g,"")},0,1);T("not",2,!1,!1,function(a,b){return!Mb(b,a)},1);T("number",1,!1,!0,function(a,b){return b?Q(b,a):+G(a.a)},0,1);T("position",1,!0,!1,function(a){return a.b},0);T("round",1,!1,!1,function(a,b){return Math.round(Q(b,a))},1);T("starts-with",2,!1,!1,function(a,b,c){b=R(b,a);a=R(c,a);return!b.lastIndexOf(a,0)},2);T("string",3,!1,!0,function(a,b){return b?R(b,a):G(a.a)},0,1);
T("string-length",1,!1,!0,function(a,b){return(b?R(b,a):G(a.a)).length},0,1);T("substring",3,!1,!1,function(a,b,c,d){c=Q(c,a);if(isNaN(c)||Infinity==c||-Infinity==c)return"";d=d?Q(d,a):Infinity;if(isNaN(d)||-Infinity===d)return"";c=Math.round(c)-1;var e=Math.max(c,0);a=R(b,a);return Infinity==d?a.substring(e):a.substring(e,c+Math.round(d))},2,3);T("substring-after",3,!1,!1,function(a,b,c){b=R(b,a);a=R(c,a);c=b.indexOf(a);return-1==c?"":b.substring(c+a.length)},2);
T("substring-before",3,!1,!1,function(a,b,c){b=R(b,a);a=R(c,a);a=b.indexOf(a);return-1==a?"":b.substring(0,a)},2);T("sum",1,!1,!1,function(a,b){a=L(b.a(a));b=0;for(var c=N(a);c;c=N(a))b+=+G(c);return b},1,1,!0);T("translate",3,!1,!1,function(a,b,c,d){b=R(b,a);c=R(c,a);var e=R(d,a);d={};for(var f=0;f<c.length;f++)a=c.charAt(f),a in d||(d[a]=e.charAt(f));c="";for(f=0;f<b.length;f++)a=b.charAt(f),c+=a in d?d[a]:a;return c},3);T("true",2,!1,!1,function(){return!0},0);function Xb(a){O.call(this,3);this.c=a.substring(1,a.length-1)}p(Xb,O);Xb.prototype.a=function(){return this.c};Xb.prototype.toString=function(){return"Literal: "+this.c};function Yb(a){O.call(this,1);this.c=a}p(Yb,O);Yb.prototype.a=function(){return this.c};Yb.prototype.toString=function(){return"Number: "+this.c};function Zb(a,b){O.call(this,a.i);this.h=a;this.c=b;this.g=a.g;this.b=a.b;1==this.c.length&&(a=this.c[0],a.w||a.c!=$b||(a=a.o,"*"!=a.f()&&(this.f={name:a.f(),u:null})))}p(Zb,O);function ac(){O.call(this,4)}p(ac,O);ac.prototype.a=function(a){var b=new I;a=a.a;9==a.nodeType?J(b,a):J(b,a.ownerDocument);return b};ac.prototype.toString=function(){return"Root Helper Expression"};function bc(){O.call(this,4)}p(bc,O);bc.prototype.a=function(a){var b=new I;J(b,a.a);return b};bc.prototype.toString=function(){return"Context Helper Expression"};
function cc(a){return"/"==a||"//"==a}Zb.prototype.a=function(a){var b=this.h.a(a);if(!(b instanceof I))throw Error("Filter expression must evaluate to nodeset.");a=this.c;for(var c=0,d=a.length;c<d&&b.l;c++){var e=a[c],f=L(b,e.c.s);if(e.g||e.c!=dc)if(e.g||e.c!=ec){var g=N(f);for(b=e.a(new pa(g));g=N(f);)g=e.a(new pa(g)),b=Fb(b,g)}else g=N(f),b=e.a(new pa(g));else{for(g=N(f);(b=N(f))&&(!g.contains||g.contains(b))&&b.compareDocumentPosition(g)&8;g=b);b=e.a(new pa(g))}}return b};
Zb.prototype.toString=function(){var a="Path Expression:"+z(this.h);if(this.c.length){var b=Ea(this.c,function(a,b){return a+z(b)},"Steps:");a+=z(b)}return a};function fc(a,b){this.a=a;this.s=!!b}
function Tb(a,b,c){for(c=c||0;c<a.a.length;c++)for(var d=a.a[c],e=L(b),f=b.l,g,h=0;g=N(e);h++){var r=a.s?f-h:h+1;g=d.a(new pa(g,r,f));if("number"==typeof g)r=r==g;else if("string"==typeof g||"boolean"==typeof g)r=!!g;else if(g instanceof I)r=0<g.l;else throw Error("Predicate.evaluate returned an unexpected type.");if(!r){r=e;g=r.f;var w=r.a;if(!w)throw Error("Next must be called at least once before remove.");var n=w.b,w=w.a;n?n.a=w:g.a=w;w?w.b=n:g.b=n;g.l--;r.a=null}}return b}
fc.prototype.toString=function(){return Ea(this.a,function(a,b){return a+z(b)},"Predicates:")};function gc(a){O.call(this,1);this.c=a;this.g=a.g;this.b=a.b}p(gc,O);gc.prototype.a=function(a){return-Q(this.c,a)};gc.prototype.toString=function(){return"Unary Expression: -"+z(this.c)};function hc(a){O.call(this,4);this.c=a;Kb(this,Fa(this.c,function(a){return a.g}));Lb(this,Fa(this.c,function(a){return a.b}))}p(hc,O);hc.prototype.a=function(a){var b=new I;A(this.c,function(c){c=c.a(a);if(!(c instanceof I))throw Error("Path expression must evaluate to NodeSet.");b=Fb(b,c)});return b};hc.prototype.toString=function(){return Ea(this.c,function(a,b){return a+z(b)},"Union Expression:")};function U(a,b,c,d){O.call(this,4);this.c=a;this.o=b;this.h=c||new fc([]);this.w=!!d;b=this.h;b=0<b.a.length?b.a[0].f:null;a.J&&b&&(a=b.name,a=D?a.toLowerCase():a,this.f={name:a,u:b.u});a:{a=this.h;for(b=0;b<a.a.length;b++)if(c=a.a[b],c.g||1==c.i||0==c.i){a=!0;break a}a=!1}this.g=a}p(U,O);
U.prototype.a=function(a){var b=a.a,c=this.f,d=null,e=null,f=0;c&&(d=c.name,e=c.u?R(c.u,a):null,f=1);if(this.w)if(this.g||this.c!=ic)if(b=L((new U(jc,new y("node"))).a(a)),c=N(b))for(a=this.m(c,d,e,f);c=N(b);)a=Fb(a,this.m(c,d,e,f));else a=new I;else a=xb(this.o,b,d,e),a=Tb(this.h,a,f);else a=this.m(a.a,d,e,f);return a};U.prototype.m=function(a,b,c,d){a=this.c.v(this.o,a,b,c);return a=Tb(this.h,a,d)};
U.prototype.toString=function(){var a="Step:"+z("Operator: "+(this.w?"//":"/"));this.c.j&&(a+=z("Axis: "+this.c));a+=z(this.o);if(this.h.a.length){var b=Ea(this.h.a,function(a,b){return a+z(b)},"Predicates:");a+=z(b)}return a};function kc(a,b,c,d){this.j=a;this.v=b;this.s=c;this.J=d}kc.prototype.toString=function(){return this.j};var lc={};function V(a,b,c,d){if(lc.hasOwnProperty(a))throw Error("Axis already created: "+a);b=new kc(a,b,c,!!d);return lc[a]=b}
V("ancestor",function(a,b){for(var c=new I;b=b.parentNode;)a.a(b)&&Gb(c,b);return c},!0);V("ancestor-or-self",function(a,b){var c=new I;do a.a(b)&&Gb(c,b);while(b=b.parentNode);return c},!0);
var $b=V("attribute",function(a,b){var c=new I,d=a.f();if("style"==d&&D&&b.style)return J(c,new mb(b.style,b,"style",b.style.cssText)),c;var e=b.attributes;if(e)if(a instanceof y&&null===a.b||"*"==d)for(d=0;a=e[d];d++)D?a.nodeValue&&J(c,nb(b,a)):J(c,a);else(a=e.getNamedItem(d))&&(D?a.nodeValue&&J(c,nb(b,a)):J(c,a));return c},!1),ic=V("child",function(a,b,c,d,e){return(D?Cb:Db).call(null,a,b,m(c)?c:null,m(d)?d:null,e||new I)},!1,!0);V("descendant",xb,!1,!0);
var jc=V("descendant-or-self",function(a,b,c,d){var e=new I;H(b,c,d)&&a.a(b)&&J(e,b);return xb(a,b,c,d,e)},!1,!0),dc=V("following",function(a,b,c,d){var e=new I;do for(var f=b;f=f.nextSibling;)H(f,c,d)&&a.a(f)&&J(e,f),e=xb(a,f,c,d,e);while(b=b.parentNode);return e},!1,!0);V("following-sibling",function(a,b){for(var c=new I;b=b.nextSibling;)a.a(b)&&J(c,b);return c},!1);V("namespace",function(){return new I},!1);
var mc=V("parent",function(a,b){var c=new I;if(9==b.nodeType)return c;if(2==b.nodeType)return J(c,b.ownerElement),c;b=b.parentNode;a.a(b)&&J(c,b);return c},!1),ec=V("preceding",function(a,b,c,d){var e=new I,f=[];do f.unshift(b);while(b=b.parentNode);for(var g=1,h=f.length;g<h;g++){var r=[];for(b=f[g];b=b.previousSibling;)r.unshift(b);for(var w=0,n=r.length;w<n;w++)b=r[w],H(b,c,d)&&a.a(b)&&J(e,b),e=xb(a,b,c,d,e)}return e},!0,!0);
V("preceding-sibling",function(a,b){for(var c=new I;b=b.previousSibling;)a.a(b)&&Gb(c,b);return c},!0);var nc=V("self",function(a,b){var c=new I;a.a(b)&&J(c,b);return c},!1);function oc(a,b){this.a=a;this.b=b}function pc(a){for(var b,c=[];;){W(a,"Missing right hand side of binary expression.");b=qc(a);var d=u(a.a);if(!d)break;var e=(d=Rb[d]||null)&&d.D;if(!e){a.a.a--;break}for(;c.length&&e<=c[c.length-1].D;)b=new Nb(c.pop(),c.pop(),b);c.push(b,d)}for(;c.length;)b=new Nb(c.pop(),c.pop(),b);return b}function W(a,b){if(ua(a.a))throw Error(b);}function rc(a,b){a=u(a.a);if(a!=b)throw Error("Bad token, expected: "+b+" got: "+a);}
function sc(a){a=u(a.a);if(")"!=a)throw Error("Bad token: "+a);}function tc(a){a=u(a.a);if(2>a.length)throw Error("Unclosed literal string");return new Xb(a)}
function uc(a){var b=[];if(cc(t(a.a))){var c=u(a.a);var d=t(a.a);if("/"==c&&(ua(a.a)||"."!=d&&".."!=d&&"@"!=d&&"*"!=d&&!/(?![0-9])[\w]/.test(d)))return new ac;d=new ac;W(a,"Missing next location step.");c=vc(a,c);b.push(c)}else{a:{c=t(a.a);d=c.charAt(0);switch(d){case "$":throw Error("Variable reference not allowed in HTML XPath");case "(":u(a.a);c=pc(a);W(a,'unclosed "("');rc(a,")");break;case '"':case "'":c=tc(a);break;default:if(isNaN(+c))if(!xa(c)&&/(?![0-9])[\w]/.test(d)&&"("==t(a.a,1)){c=u(a.a);
c=Wb[c]||null;u(a.a);for(d=[];")"!=t(a.a);){W(a,"Missing function argument list.");d.push(pc(a));if(","!=t(a.a))break;u(a.a)}W(a,"Unclosed function argument list.");sc(a);c=new Ub(c,d)}else{c=null;break a}else c=new Yb(+u(a.a))}"["==t(a.a)&&(d=new fc(wc(a)),c=new Sb(c,d))}if(c)if(cc(t(a.a)))d=c;else return c;else c=vc(a,"/"),d=new bc,b.push(c)}for(;cc(t(a.a));)c=u(a.a),W(a,"Missing next location step."),c=vc(a,c),b.push(c);return new Zb(d,b)}
function vc(a,b){if("/"!=b&&"//"!=b)throw Error('Step op should be "/" or "//"');if("."==t(a.a)){var c=new U(nc,new y("node"));u(a.a);return c}if(".."==t(a.a))return c=new U(mc,new y("node")),u(a.a),c;if("@"==t(a.a)){var d=$b;u(a.a);W(a,"Missing attribute name")}else if("::"==t(a.a,1)){if(!/(?![0-9])[\w]/.test(t(a.a).charAt(0)))throw Error("Bad token: "+u(a.a));var e=u(a.a);d=lc[e]||null;if(!d)throw Error("No axis with name: "+e);u(a.a);W(a,"Missing node name")}else d=ic;e=t(a.a);if(/(?![0-9])[\w\*]/.test(e.charAt(0)))if("("==
t(a.a,1)){if(!xa(e))throw Error("Invalid node type: "+e);e=u(a.a);if(!xa(e))throw Error("Invalid type name: "+e);rc(a,"(");W(a,"Bad nodetype");var f=t(a.a).charAt(0),g=null;if('"'==f||"'"==f)g=tc(a);W(a,"Bad nodetype");sc(a);e=new y(e,g)}else if(e=u(a.a),f=e.indexOf(":"),-1==f)e=new ya(e);else{var g=e.substring(0,f);if("*"==g)var h="*";else if(h=a.b(g),!h)throw Error("Namespace prefix not declared: "+g);e=e.substr(f+1);e=new ya(e,h)}else throw Error("Bad token: "+u(a.a));a=new fc(wc(a),d.s);return c||
new U(d,e,a,"//"==b)}function wc(a){for(var b=[];"["==t(a.a);){u(a.a);W(a,"Missing predicate expression.");var c=pc(a);b.push(c);W(a,"Unclosed predicate expression.");rc(a,"]")}return b}function qc(a){if("-"==t(a.a))return u(a.a),new gc(qc(a));var b=uc(a);if("|"!=t(a.a))a=b;else{for(b=[b];"|"==u(a.a);)W(a,"Missing next union location path."),b.push(uc(a));a.a.a--;a=new hc(b)}return a};function xc(a,b){if(!a.length)throw Error("Empty XPath expression.");a=ra(a);if(ua(a))throw Error("Invalid XPath expression.");b?"function"==ba(b)||(b=ea(b.lookupNamespaceURI,b)):b=function(){return null};var c=pc(new oc(a,b));if(!ua(a))throw Error("Bad token: "+u(a));this.evaluate=function(a,b){a=c.a(new pa(a));return new X(a,b)}}
function X(a,b){if(!b)if(a instanceof I)b=4;else if("string"==typeof a)b=2;else if("number"==typeof a)b=1;else if("boolean"==typeof a)b=3;else throw Error("Unexpected evaluation result.");if(2!=b&&1!=b&&3!=b&&!(a instanceof I))throw Error("value could not be converted to the specified type");this.resultType=b;switch(b){case 2:this.stringValue=a instanceof I?Ib(a):""+a;break;case 1:this.numberValue=a instanceof I?+Ib(a):+a;break;case 3:this.booleanValue=a instanceof I?0<a.l:!!a;break;case 4:case 5:case 6:case 7:var c=
L(a);var d=[];for(var e=N(c);e;e=N(c))d.push(e instanceof mb?e.a:e);this.snapshotLength=a.l;this.invalidIteratorState=!1;break;case 8:case 9:a=Hb(a);this.singleNodeValue=a instanceof mb?a.a:a;break;default:throw Error("Unknown XPathResult type.");}var f=0;this.iterateNext=function(){if(4!=b&&5!=b)throw Error("iterateNext called with wrong result type");return f>=d.length?null:d[f++]};this.snapshotItem=function(a){if(6!=b&&7!=b)throw Error("snapshotItem called with wrong result type");return a>=d.length||
0>a?null:d[a]}}X.ANY_TYPE=0;X.NUMBER_TYPE=1;X.STRING_TYPE=2;X.BOOLEAN_TYPE=3;X.UNORDERED_NODE_ITERATOR_TYPE=4;X.ORDERED_NODE_ITERATOR_TYPE=5;X.UNORDERED_NODE_SNAPSHOT_TYPE=6;X.ORDERED_NODE_SNAPSHOT_TYPE=7;X.ANY_UNORDERED_NODE_TYPE=8;X.FIRST_ORDERED_NODE_TYPE=9;function yc(a){this.lookupNamespaceURI=za(a)}
function zc(a,b){a=a||k;var c=a.Document&&a.Document.prototype||a.document;if(!c.evaluate||b)a.XPathResult=X,c.evaluate=function(a,b,c,g){return(new xc(a,c)).evaluate(b,g)},c.createExpression=function(a,b){return new xc(a,b)},c.createNSResolver=function(a){return new yc(a)}}aa("wgxpath.install",zc);var Ac=function(){var a={M:"http://www.w3.org/2000/svg"};return function(b){return a[b]||null}}();
function Bc(a,b){var c=F(a);if(!c.documentElement)return null;(C||hb)&&zc(c?c.parentWindow||c.defaultView:window);try{var d=c.createNSResolver?c.createNSResolver(c.documentElement):Ac;if(C&&!ab(7))return c.evaluate.call(c,b,a,d,9,null);if(!C||9<=Number(bb)){for(var e={},f=c.getElementsByTagName("*"),g=0;g<f.length;++g){var h=f[g],r=h.namespaceURI;if(r&&!e[r]){var w=h.lookupPrefix(r);if(!w)var n=r.match(".*/(\\w+)/?$"),w=n?n[1]:"xhtml";e[r]=w}}var B={},M;for(M in e)B[e[M]]=M;d=function(a){return B[a]||
null}}try{return c.evaluate(b,a,d,9,null)}catch(P){if("TypeError"===P.name)return d=c.createNSResolver?c.createNSResolver(c.documentElement):Ac,c.evaluate(b,a,d,9,null);throw P;}}catch(P){if(!Ua||"NS_ERROR_ILLEGAL_VALUE"!=P.name)throw new ga(32,"Unable to locate an element with the xpath expression "+b+" because of the following error:\n"+P);}}
function Cc(a,b){var c=function(){var c=Bc(b,a);return c?c.singleNodeValue||null:b.selectSingleNode?(c=F(b),c.setProperty&&c.setProperty("SelectionLanguage","XPath"),b.selectSingleNode(a)):null}();if(null!==c&&(!c||1!=c.nodeType))throw new ga(32,'The result of the xpath expression "'+a+'" is: '+c+". It should be an element.");return c};var Dc="function"===typeof ShadowRoot;function Ec(a){for(a=a.parentNode;a&&1!=a.nodeType&&9!=a.nodeType&&11!=a.nodeType;)a=a.parentNode;return K(a)?a:null}
function Y(a,b){b=oa(b);if("float"==b||"cssFloat"==b||"styleFloat"==b)b=qb?"styleFloat":"cssFloat";a:{var c=b;var d=F(a);if(d.defaultView&&d.defaultView.getComputedStyle&&(d=d.defaultView.getComputedStyle(a,null))){c=d[c]||d.getPropertyValue(c)||"";break a}c=""}a=c||Fc(a,b);if(null===a)a=null;else if(0<=Ca(La,b)){b:{var e=a.match(Oa);if(e&&(b=Number(e[1]),c=Number(e[2]),d=Number(e[3]),e=Number(e[4]),0<=b&&255>=b&&0<=c&&255>=c&&0<=d&&255>=d&&0<=e&&1>=e)){b=[b,c,d,e];break b}b=null}if(!b)b:{if(d=a.match(Pa))if(b=
Number(d[1]),c=Number(d[2]),d=Number(d[3]),0<=b&&255>=b&&0<=c&&255>=c&&0<=d&&255>=d){b=[b,c,d,1];break b}b=null}if(!b)b:{b=a.toLowerCase();c=ia[b.toLowerCase()];if(!c&&(c="#"==b.charAt(0)?b:"#"+b,4==c.length&&(c=c.replace(Ma,"#$1$1$2$2$3$3")),!Na.test(c))){b=null;break b}b=[parseInt(c.substr(1,2),16),parseInt(c.substr(3,2),16),parseInt(c.substr(5,2),16),1]}a=b?"rgba("+b.join(", ")+")":a}return a}
function Fc(a,b){var c=a.currentStyle||a.style,d=c[b];!l(d)&&"function"==ba(c.getPropertyValue)&&(d=c.getPropertyValue(b));return"inherit"!=d?l(d)?d:null:(a=Ec(a))?Fc(a,b):null}
function Gc(a,b,c){function d(a){var b=Hc(a);return 0<b.height&&0<b.width?!0:K(a,"PATH")&&(0<b.height||0<b.width)?(a=Y(a,"stroke-width"),!!a&&0<parseInt(a,10)):"hidden"!=Y(a,"overflow")&&Fa(a.childNodes,function(a){return 3==a.nodeType||K(a)&&d(a)})}function e(a){return Ic(a)==Z&&Ga(a.childNodes,function(a){return!K(a)||e(a)||!d(a)})}if(!K(a))throw Error("Argument to isShown must be of type Element");if(K(a,"BODY"))return!0;if(K(a,"OPTION")||K(a,"OPTGROUP"))return a=vb(a,function(a){return K(a,"SELECT")}),
!!a&&Gc(a,!0,c);var f=Jc(a);if(f)return!!f.B&&0<f.rect.width&&0<f.rect.height&&Gc(f.B,b,c);if(K(a,"INPUT")&&"hidden"==a.type.toLowerCase()||K(a,"NOSCRIPT"))return!1;f=Y(a,"visibility");return"collapse"!=f&&"hidden"!=f&&c(a)&&(b||Kc(a))&&d(a)?!e(a):!1}var Z="hidden";
function Ic(a){function b(a){function b(a){return a==g?!0:!Y(a,"display").lastIndexOf("inline",0)||"absolute"==c&&"static"==Y(a,"position")?!1:!0}var c=Y(a,"position");if("fixed"==c)return w=!0,a==g?null:g;for(a=Ec(a);a&&!b(a);)a=Ec(a);return a}function c(a){var b=a;if("visible"==r)if(a==g&&h)b=h;else if(a==h)return{x:"visible",y:"visible"};b={x:Y(b,"overflow-x"),y:Y(b,"overflow-y")};a==g&&(b.x="visible"==b.x?"auto":b.x,b.y="visible"==b.y?"auto":b.y);return b}function d(a){if(a==g){var b=(new wb(f)).a;
a=b.scrollingElement?b.scrollingElement:Va||"CSS1Compat"!=b.compatMode?b.body||b.documentElement:b.documentElement;b=b.parentWindow||b.defaultView;a=C&&ab("10")&&b.pageYOffset!=a.scrollTop?new Ra(a.scrollLeft,a.scrollTop):new Ra(b.pageXOffset||a.scrollLeft,b.pageYOffset||a.scrollTop)}else a=new Ra(a.scrollLeft,a.scrollTop);return a}var e=Lc(a);var f=F(a),g=f.documentElement,h=f.body,r=Y(g,"overflow"),w;for(a=b(a);a;a=b(a)){var n=c(a);if("visible"!=n.x||"visible"!=n.y){var B=Hc(a);if(!B.width||!B.height)return Z;
var M=e.a<B.a,P=e.b<B.b;if(M&&"hidden"==n.x||P&&"hidden"==n.y)return Z;if(M&&"visible"!=n.x||P&&"visible"!=n.y){M=d(a);P=e.b<B.b-M.y;if(e.a<B.a-M.x&&"visible"!=n.x||P&&"visible"!=n.x)return Z;e=Ic(a);return e==Z?Z:"scroll"}M=e.f>=B.a+B.width;B=e.c>=B.b+B.height;if(M&&"hidden"==n.x||B&&"hidden"==n.y)return Z;if(M&&"visible"!=n.x||B&&"visible"!=n.y){if(w&&(n=d(a),e.f>=g.scrollWidth-n.x||e.a>=g.scrollHeight-n.y))return Z;e=Ic(a);return e==Z?Z:"scroll"}}}return"none"}
function Hc(a){var b=Jc(a);if(b)return b.rect;if(K(a,"HTML"))return a=F(a),a=((a?a.parentWindow||a.defaultView:window)||window).document,a="CSS1Compat"==a.compatMode?a.documentElement:a.body,a=new ja(a.clientWidth,a.clientHeight),new E(0,0,a.width,a.height);try{var c=a.getBoundingClientRect()}catch(d){return new E(0,0,0,0)}b=new E(c.left,c.top,c.right-c.left,c.bottom-c.top);C&&a.ownerDocument.body&&(a=F(a),b.a-=a.documentElement.clientLeft+a.body.clientLeft,b.b-=a.documentElement.clientTop+a.body.clientTop);
return b}function Jc(a){var b=K(a,"MAP");if(!b&&!K(a,"AREA"))return null;var c=b?a:K(a.parentNode,"MAP")?a.parentNode:null,d=null,e=null;c&&c.name&&(d=Cc('/descendant::*[@usemap = "#'+c.name+'"]',F(c)))&&(e=Hc(d),b||"default"==a.shape.toLowerCase()||(a=Mc(a),b=Math.min(Math.max(a.a,0),e.width),c=Math.min(Math.max(a.b,0),e.height),e=new E(b+e.a,c+e.b,Math.min(a.width,e.width-b),Math.min(a.height,e.height-c))));return{B:d,rect:e||new E(0,0,0,0)}}
function Mc(a){var b=a.shape.toLowerCase();a=a.coords.split(",");if("rect"==b&&4==a.length){var b=a[0],c=a[1];return new E(b,c,a[2]-b,a[3]-c)}if("circle"==b&&3==a.length)return b=a[2],new E(a[0]-b,a[1]-b,2*b,2*b);if("poly"==b&&2<a.length){for(var b=a[0],c=a[1],d=b,e=c,f=2;f+1<a.length;f+=2)b=Math.min(b,a[f]),d=Math.max(d,a[f]),c=Math.min(c,a[f+1]),e=Math.max(e,a[f+1]);return new E(b,c,d-b,e-c)}return new E(0,0,0,0)}function Lc(a){a=Hc(a);return new db(a.b,a.a+a.width,a.b+a.height,a.a)}
function Kc(a){if(qb){if("relative"==Y(a,"position"))return 1;a=Y(a,"filter");return(a=a.match(/^alpha\(opacity=(\d*)\)/)||a.match(/^progid:DXImageTransform.Microsoft.Alpha\(Opacity=(\d*)\)/))?Number(a[1])/100:1}return Nc(a)}function Nc(a){var b=1,c=Y(a,"opacity");c&&(b=Number(c));(a=Ec(a))&&(b*=Nc(a));return b};aa("_",function(a,b){var c=Dc?function(b){if("none"==Y(b,"display"))return!1;do{var d=b.parentNode;if(b.getDestinationInsertionPoints){var f=b.getDestinationInsertionPoints();0<f.length&&(d=f[f.length-1])}if(d instanceof ShadowRoot){if(d.host.shadowRoot!=d)return!1;d=d.host}else!d||9!=d.nodeType&&11!=d.nodeType||(d=null)}while(a&&1!=a.nodeType);return!d||c(d)}:function(a){if("none"==Y(a,"display"))return!1;a=Ec(a);return!a||c(a)};return Gc(a,!!b,c)});; return this._.apply(null,arguments);}.apply({navigator:typeof window!='undefined'?window.navigator:null,document:typeof window!='undefined'?window.document:null}, arguments);};


/***/ }),
/* 161 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 162 */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "static/img/404.05e37d7.jpg";

/***/ }),
/* 163 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"gm-nothing"},[_c('img',{staticClass:"nengah",attrs:{"src":_vm.nothing}})])}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 164 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('nav',{staticClass:"navbar navbar-dark fixed-top bg-atas"},[_c('a',{staticClass:"navbar-brand",attrs:{"href":"#"}},[_c('img',{attrs:{"src":_vm.logocoba}})])]),_vm._v(" "),_c('div',{staticClass:"container-fluid rewards-page"},[_c('mu-card',{staticClass:"card-feed card-rewards",staticStyle:{"margin":"0 auto"}},[_c('div',{staticClass:"reward-me"},[(_vm.loading_thre)?_c('div',{staticClass:"load3"},[_c('beat-loader',{attrs:{"color":_vm.color,"size":_vm.size}})],1):_c('div',[_c('i',{staticClass:"fa fa-star"}),_vm._v(" "),(this.hitung === 0)?_c('span',[_vm._v("You have 0 points to redem")]):_vm._l((_vm.point),function(points){return _c('span',{key:points.id},[_vm._v("Kamu punya "+_vm._s(points.point)+" poin untuk ditukarkan")])})],2)])]),_vm._v(" "),_c('mu-card',{staticClass:"card-feed card-rewards",staticStyle:{"margin":"0 auto"}},[_c('div',{staticClass:"reward-tab"},[_c('mu-tabs',{attrs:{"value":_vm.active},on:{"update:value":function($event){_vm.active=$event}}},[_c('mu-tab',[_vm._v("Voucher")]),_vm._v(" "),_c('mu-tab',{on:{"click":_vm.redemtions}},[_vm._v("Sudah ditukar")])],1),_vm._v(" "),(_vm.active === 0)?_c('div',{staticClass:"demo-text konten-reward"},[(_vm.loading_one)?_c('div',[_c('loading-rewards')],1):_c('div',[_c('div',{staticClass:"reward-list"},[_c('div',{staticClass:"row"},_vm._l((_vm.posts),function(post){return _c('div',{key:post.id,staticClass:"col-6"},[_c('div',{staticClass:"card card-redem"},[_c('div',{staticClass:"img-redem"},[_c('img',{staticClass:"card-img-top",attrs:{"src":post.image_voucher,"alt":post.nama_voucher}})]),_vm._v(" "),_c('div',{staticClass:"card-body"},[_c('div',{staticClass:"card-un"},[_c('div',{staticClass:"val-this"},[_vm._v("\n                                                    Nilai Voucher "),_c('span',[_vm._v(_vm._s(post.point_voucher))])])]),_vm._v(" "),_c('div',{staticClass:"card-un"},[_c('div',{staticClass:"val-this"},[_vm._v("\n                                                    Voucher tersisa"),_c('span',[_vm._v(_vm._s(post.jml_voucher))])])]),_vm._v(" "),_c('div',{staticClass:"card-un un-last"},[_c('div',{staticClass:"val-this"},[_vm._v("\n                                                    Kadaluarsa"),_c('span',[_vm._v(_vm._s(post.expired_date))])])])]),_vm._v(" "),_c('div',{staticClass:"card-baw"},[_c('router-link',{attrs:{"to":{ name: 'PageReedem', params: { id_voucher: post.id_voucher }}}},[_vm._v("Tukar")])],1)])])}),0)])])]):_vm._e(),_vm._v(" "),(_vm.active === 1)?_c('div',{staticClass:"demo-text konten-reward"},[(this.redeemhitung === 0)?_c('div',[_c('not-found')],1):_c('div',[(_vm.loading_two)?_c('div',[_c('loading-reedem')],1):_c('div',[_c('div',{staticClass:"redemtions"},_vm._l((_vm.redeem),function(item){return _c('mu-card',{key:item.id,staticClass:"card-redemtios",staticStyle:{"margin":"0 auto"}},[_c('mu-card-media',{staticClass:"img-redemtions"},[_c('img',{attrs:{"src":item.voucher.image_voucher}})]),_vm._v(" "),_c('mu-card-text',{staticClass:"text-redemtions"},[_c('p',{staticClass:"title-redemtions"},[_vm._v(_vm._s(item.voucher.nama_voucher))]),_vm._v("\n                                    Ditukar pada "+_vm._s(_vm._f("moment")(item.date_reedem,"LL"))+"\n                                ")]),_vm._v(" "),_c('div',{staticClass:"kode-unik"},[_c('a',{staticClass:"btn btn-primary btn-block",on:{"click":function($event){return _vm.detail(item.id_reedem)}}},[_vm._v("Kode Unik")])])],1)}),1)])])]):_vm._e()],1)])],1)])}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 165 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageProfile_vue__ = __webpack_require__(62);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_773537b2_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageProfile_vue__ = __webpack_require__(167);
function injectStyle (ssrContext) {
  __webpack_require__(166)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-773537b2"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageProfile_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_773537b2_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageProfile_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 166 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 167 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('nav',{staticClass:"navbar navbar-dark fixed-top bg-atas"},[_c('a',{staticClass:"navbar-brand",attrs:{"href":"#"}},[_c('img',{attrs:{"src":_vm.logocoba}})])]),_vm._v(" "),_c('div',{staticClass:"profile"},[_c('mu-card',{staticClass:"card-feed card-profil",staticStyle:{"margin":"0 auto"}},[_c('div',[(this.posts.image === null)?_c('mu-card-media',{staticClass:"banner"},[_c('img',{attrs:{"src":"http://103.49.223.93/cms-advokasi/public/img_user/TG0E5B_default-profile.png"}})]):_c('mu-card-media',{staticClass:"banner"},[_c('img',{attrs:{"src":this.posts.image}})]),_vm._v(" "),(this.posts.image === null)?_c('div',{staticClass:"avatar"},[_c('img',{attrs:{"src":"http://103.49.223.93/cms-advokasi/public/img_user/TG0E5B_default-profile.png"}})]):_c('div',{staticClass:"avatar"},[_c('img',{attrs:{"src":this.posts.image}})]),_vm._v(" "),_c('div',{staticClass:"real-name"},[_vm._v("\n                "+_vm._s(this.posts.nama)),_c('span',[_c('a',{on:{"click":_vm.edit}},[_c('i',{staticClass:"fa fa-pencil"})])])]),_vm._v(" "),_c('div',{staticClass:"uresult"},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-6"},[_c('div',{staticClass:"rengking-madrasah"},[(this.hitung.length === 0 )?_c('div',[_c('h2',[_vm._v("0")])]):_c('div',[_c('h2',[_vm._v(_vm._s(this.rank.point))])]),_vm._v(" "),_c('p',[_vm._v("Poin")])])]),_vm._v(" "),_c('div',{staticClass:"col-6"},[_c('div',{staticClass:"rengking-madrasah"},[(this.hitung.length === 0 )?_c('div',[_c('h2',[_vm._v("0")])]):_c('div',[_c('h2',[_vm._v(_vm._s(this.rank.jml_share))])]),_vm._v(" "),_c('p',[_vm._v("Membagikan")])])])])])],1),_vm._v(" "),_c('div',{staticClass:"con-title"},[_vm._v("\n                TOP USERS\n            ")]),_vm._v(" "),_c('div',{staticClass:"top-leader"},[_c('mu-ripple',{staticClass:"mu-ripple-demo",attrs:{"color":"rgb(33, 150, 243, 0.3)","opacity":0.3},on:{"click":_vm.leaderBoard}},[_c('i',{staticClass:"fa fa-trophy"}),_c('span',[_vm._v(" Leaderboard")]),_vm._v(" "),_c('div',{staticClass:"arah"},[_c('i',{staticClass:"fa fa-angle-right"})])]),_vm._v(" "),_c('mu-ripple',{staticClass:"mu-ripple-demo riple2",attrs:{"color":"rgb(33, 150, 243, 0.3)","opacity":0.3},on:{"click":_vm.Activity}},[_c('i',{staticClass:"fa fa-align-justify"}),_c('span',[_vm._v(" Activity")]),_vm._v(" "),_c('div',{staticClass:"arah2"},[_c('i',{staticClass:"fa fa-angle-right"})])]),_vm._v(" "),_c('mu-ripple',{staticClass:"mu-ripple-demo riple2",attrs:{"color":"rgb(33, 150, 243, 0.3)","opacity":0.3},on:{"click":_vm.logout}},[_c('i',{staticClass:"fa fa-sign-out"}),_c('span',[_vm._v(" Logout")])])],1)])],1)])}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 168 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageProfiledit_vue__ = __webpack_require__(64);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_6e335e7d_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageProfiledit_vue__ = __webpack_require__(170);
function injectStyle (ssrContext) {
  __webpack_require__(169)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6e335e7d"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageProfiledit_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_6e335e7d_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageProfiledit_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 169 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 170 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('nav',{staticClass:"navbar navbar-dark fixed-top bg-atas"},[_c('div',{staticClass:"navinih"},[_c('div',{staticClass:"back"},[_c('mu-ripple',{staticClass:"icon-back",attrs:{"color":"rgb(33, 150, 243, 0.3)","opacity":0.3},on:{"click":_vm.back}},[_c('md-icon',[_vm._v("arrow_back")])],1)],1),_vm._v(" "),_vm._m(0),_vm._v(" "),_c('div',{staticClass:"savage"},[_c('button',{staticClass:"btn btn-light btn-sm",attrs:{"type":"button"},on:{"click":_vm.save}},[_vm._v("Save")])])])]),_vm._v(" "),_c('div',{staticClass:"edit-profile"},[_c('mu-card',{staticClass:"card-feed card-edit",staticStyle:{"margin":"0 auto"}},[_c('div',{staticClass:"gambar"},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-2"},[(_vm.edit.image === null)?_c('div',{staticClass:"edit-gambar"},[_c('img',{attrs:{"src":"http://103.49.223.93/cms-advokasi/public/img_user/TG0E5B_default-profile.png"}})]):_c('div',{staticClass:"edit-gambar"},[_c('img',{attrs:{"src":this.edit.image}})])]),_vm._v(" "),_c('div',{staticClass:"col-10"})])])]),_vm._v(" "),_c('mu-card',{staticClass:"card-feed card-edit",staticStyle:{"margin":"0 auto"}},[_c('div',{staticClass:"name-edit"},[_c('h2',[_vm._v("NIK")]),_vm._v(" "),_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.edit.nik),expression:"edit.nik"}],staticClass:"form-control frm-edit",attrs:{"type":"text","disabled":""},domProps:{"value":(_vm.edit.nik)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.edit, "nik", $event.target.value)}}}),_vm._v(" "),_c('h2',[_vm._v("Name")]),_vm._v(" "),_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.edit.nama),expression:"edit.nama"}],staticClass:"form-control frm-edit",attrs:{"type":"text"},domProps:{"value":(_vm.edit.nama)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.edit, "nama", $event.target.value)}}}),_vm._v(" "),_c('h2',[_vm._v("Email")]),_vm._v(" "),_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.edit.email),expression:"edit.email"}],staticClass:"form-control frm-edit",attrs:{"type":"email"},domProps:{"value":(_vm.edit.email)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.edit, "email", $event.target.value)}}}),_vm._v(" "),_c('h2',[_vm._v("Phone Number")]),_vm._v(" "),_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.edit.no_handphone),expression:"edit.no_handphone"}],staticClass:"form-control frm-edit",attrs:{"type":"text"},domProps:{"value":(_vm.edit.no_handphone)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.edit, "no_handphone", $event.target.value)}}}),_vm._v(" "),_c('h2',[_vm._v("Account Number")]),_vm._v(" "),_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.edit.no_rekening),expression:"edit.no_rekening"}],staticClass:"form-control frm-edit",attrs:{"type":"text"},domProps:{"value":(_vm.edit.no_rekening)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.edit, "no_rekening", $event.target.value)}}})])]),_vm._v(" "),_c('mu-card',{staticClass:"card-feed card-edit",staticStyle:{"margin":"0 auto"}},[_c('div',{staticClass:"top-leader"},[_c('mu-ripple',{staticClass:"mu-ripple-demo",attrs:{"color":"rgb(33, 150, 243, 0.3)","opacity":0.3},on:{"click":_vm.gantipass}},[_c('i',{staticClass:"fa fa-key"}),_c('span',[_vm._v(" Change Password")]),_vm._v(" "),_c('div',{staticClass:"arah"},[_c('i',{staticClass:"fa fa-angle-right"})])])],1)])],1)])}
var staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"headbang2"},[_c('h1',[_vm._v("Edit Profile")])])}]
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 171 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageLogin_vue__ = __webpack_require__(65);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_6f9ea016_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageLogin_vue__ = __webpack_require__(173);
function injectStyle (ssrContext) {
  __webpack_require__(172)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-6f9ea016"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageLogin_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_6f9ea016_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageLogin_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 172 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 173 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"koran"},[_c('div',{staticClass:"login-page"},[_c('mu-card',{staticClass:"card-login",staticStyle:{"margin":"0 auto"}},[_c('mu-container',[_c('div',{staticClass:"logo-main"},[_c('img',{attrs:{"src":_vm.logincoba}})]),_vm._v(" "),_c('form',{attrs:{"id":"Login"}},[_c('div',{staticClass:"form-group"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.input.nik),expression:"input.nik"}],staticClass:"form-control form-login",attrs:{"type":"text","id":"inputNik","placeholder":"NIK"},domProps:{"value":(_vm.input.nik)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.input, "nik", $event.target.value)}}})]),_vm._v(" "),_c('div',{staticClass:"form-group"},[_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.input.password),expression:"input.password"}],staticClass:"form-control form-login",attrs:{"type":"password","id":"inputPassword","placeholder":"Password"},domProps:{"value":(_vm.input.password)},on:{"input":function($event){if($event.target.composing){ return; }_vm.$set(_vm.input, "password", $event.target.value)}}})]),_vm._v(" "),_c('div',{staticClass:"login-btn"},[_c('mu-flex',{attrs:{"justify-content":"center","align-items":"center"}},[_c('mu-button',{attrs:{"full-width":"","color":"primary"},on:{"click":_vm.login}},[_vm._v("Login")])],1)],1),_vm._v(" "),_c('div',{staticClass:"reg"},[_c('p',{staticStyle:{"color":"#b71c1c"}},[_vm._v(_vm._s(_vm.message))])])])])],1)],1)])}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 174 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageAnalitic_vue__ = __webpack_require__(66);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_644d522c_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageAnalitic_vue__ = __webpack_require__(194);
function injectStyle (ssrContext) {
  __webpack_require__(175)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-644d522c"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageAnalitic_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_644d522c_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageAnalitic_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 175 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 176 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageAnaliticFacebook_vue__ = __webpack_require__(67);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_29821594_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageAnaliticFacebook_vue__ = __webpack_require__(180);
function injectStyle (ssrContext) {
  __webpack_require__(177)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageAnaliticFacebook_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_29821594_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageAnaliticFacebook_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 177 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 178 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 179 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('content-loader',{attrs:{"height":70,"width":411,"speed":3,"primaryColor":"#f3f3f3","secondaryColor":"#d9d9d9"}},[_c('rect',{attrs:{"x":"71","y":"219.27","rx":"0","ry":"0","width":"0","height":"0"}}),_vm._v(" "),_c('rect',{attrs:{"x":"65.89","y":"19.67","rx":"0","ry":"0","width":"211","height":"14"}}),_vm._v(" "),_c('rect',{attrs:{"x":"65.89","y":"39.67","rx":"0","ry":"0","width":"159","height":"9.23"}}),_vm._v(" "),_c('rect',{attrs:{"x":"134.89","y":"325.67","rx":"0","ry":"0","width":"0","height":"0"}}),_vm._v(" "),_c('rect',{attrs:{"x":"6.23","y":"16.67","rx":"0","ry":"0","width":"50","height":"50"}}),_vm._v(" "),_c('rect',{attrs:{"x":"5.23","y":"2.67","rx":"0","ry":"0","width":"70","height":"9"}})])}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 180 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"section-historical"},[_c('mu-card',{staticClass:"card-feed card-analitic",staticStyle:{"margin":"0 auto"}},[(this.hitung === 0)?_c('div',[_c('not-found')],1):_c('div',_vm._l((_vm.postfb),function(post){return _c('div',{key:post.id,staticClass:"one-historical"},[_c('link-prevue',{attrs:{"url":post.feed.url},scopedSlots:_vm._u([{key:"default",fn:function(props){return [_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-2"},[_c('div',{staticClass:"img-historical"},[_c('img',{attrs:{"src":props.img}})])]),_vm._v(" "),_c('div',{staticClass:"col-10"},[_c('div',{staticClass:"title-historical"},[_c('h1',[_vm._v(_vm._s(props.title))])]),_vm._v(" "),_c('div',{staticClass:"date-historical"},[_c('span',[_vm._v("Dibagikan "+_vm._s(_vm._f("moment")(post.created_at,"LL")))])])])])]}}],null,true)},[_vm._v(" "),_c('template',{slot:"loading"},[_c('loading-activity')],1)],2)],1)}),0)])],1)}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 181 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageAnaliticTwitter_vue__ = __webpack_require__(69);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_c6fa5f8e_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageAnaliticTwitter_vue__ = __webpack_require__(183);
function injectStyle (ssrContext) {
  __webpack_require__(182)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageAnaliticTwitter_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_c6fa5f8e_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageAnaliticTwitter_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 182 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 183 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"section-historical"},[_c('mu-card',{staticClass:"card-feed card-analitic",staticStyle:{"margin":"0 auto"}},[(this.hitungtw === 0)?_c('div',[_c('not-found')],1):_c('div',_vm._l((_vm.posts),function(post){return _c('div',{key:post.id,staticClass:"one-historical"},[_c('link-prevue',{attrs:{"url":post.feed.url},scopedSlots:_vm._u([{key:"default",fn:function(props){return [_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-2"},[_c('div',{staticClass:"img-historical"},[_c('img',{attrs:{"src":props.img}})])]),_vm._v(" "),_c('div',{staticClass:"col-10"},[_c('div',{staticClass:"title-historical"},[_c('h1',[_vm._v(_vm._s(props.title))])]),_vm._v(" "),_c('div',{staticClass:"date-historical"},[_c('span',[_vm._v("Dibagikan "+_vm._s(_vm._f("moment")(post.created_at,"LL")))])])])])]}}],null,true)},[_vm._v(" "),_c('template',{slot:"loading"},[_c('loading-activity')],1)],2)],1)}),0)])],1)}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 184 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageAnaliticLikedin_vue__ = __webpack_require__(70);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_9ca1c342_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageAnaliticLikedin_vue__ = __webpack_require__(186);
function injectStyle (ssrContext) {
  __webpack_require__(185)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageAnaliticLikedin_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_9ca1c342_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageAnaliticLikedin_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 185 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 186 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"section-historical"},[_c('mu-card',{staticClass:"card-feed card-analitic",staticStyle:{"margin":"0 auto"}},[(this.hitungin === 0)?_c('div',[_c('not-found')],1):_c('div',_vm._l((_vm.posts),function(post){return _c('div',{key:post.id,staticClass:"one-historical"},[_c('link-prevue',{attrs:{"url":post.feed.url},scopedSlots:_vm._u([{key:"default",fn:function(props){return [_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-2"},[_c('div',{staticClass:"img-historical"},[_c('img',{attrs:{"src":props.img}})])]),_vm._v(" "),_c('div',{staticClass:"col-10"},[_c('div',{staticClass:"title-historical"},[_c('h1',[_vm._v(_vm._s(props.title))])]),_vm._v(" "),_c('div',{staticClass:"date-historical"},[_c('span',[_vm._v("Dibagikan "+_vm._s(_vm._f("moment")(post.created_at,"LL")))])])])])]}}],null,true)},[_vm._v(" "),_c('template',{slot:"loading"},[_c('loading-activity')],1)],2)],1)}),0)])],1)}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 187 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE4LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDExMi4xOTYgMTEyLjE5NiIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMTEyLjE5NiAxMTIuMTk2OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8Zz4NCgk8Y2lyY2xlIHN0eWxlPSJmaWxsOiMzQjU5OTg7IiBjeD0iNTYuMDk4IiBjeT0iNTYuMDk4IiByPSI1Ni4wOTgiLz4NCgk8cGF0aCBzdHlsZT0iZmlsbDojRkZGRkZGOyIgZD0iTTcwLjIwMSw1OC4yOTRoLTEwLjAxdjM2LjY3Mkg0NS4wMjVWNTguMjk0aC03LjIxM1Y0NS40MDZoNy4yMTN2LTguMzQNCgkJYzAtNS45NjQsMi44MzMtMTUuMzAzLDE1LjMwMS0xNS4zMDNMNzEuNTYsMjEuODF2MTIuNTFoLTguMTUxYy0xLjMzNywwLTMuMjE3LDAuNjY4LTMuMjE3LDMuNTEzdjcuNTg1aDExLjMzNEw3MC4yMDEsNTguMjk0eiIvPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPC9zdmc+DQo="

/***/ }),
/* 188 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE4LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDExMi4xOTcgMTEyLjE5NyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMTEyLjE5NyAxMTIuMTk3OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8Zz4NCgk8Y2lyY2xlIHN0eWxlPSJmaWxsOiM1NUFDRUU7IiBjeD0iNTYuMDk5IiBjeT0iNTYuMDk4IiByPSI1Ni4wOTgiLz4NCgk8Zz4NCgkJPHBhdGggc3R5bGU9ImZpbGw6I0YxRjJGMjsiIGQ9Ik05MC40NjEsNDAuMzE2Yy0yLjQwNCwxLjA2Ni00Ljk5LDEuNzg3LTcuNzAyLDIuMTA5YzIuNzY5LTEuNjU5LDQuODk0LTQuMjg0LDUuODk3LTcuNDE3DQoJCQljLTIuNTkxLDEuNTM3LTUuNDYyLDIuNjUyLTguNTE1LDMuMjUzYy0yLjQ0Ni0yLjYwNS01LjkzMS00LjIzMy05Ljc5LTQuMjMzYy03LjQwNCwwLTEzLjQwOSw2LjAwNS0xMy40MDksMTMuNDA5DQoJCQljMCwxLjA1MSwwLjExOSwyLjA3NCwwLjM0OSwzLjA1NmMtMTEuMTQ0LTAuNTU5LTIxLjAyNS01Ljg5Ny0yNy42MzktMTQuMDEyYy0xLjE1NCwxLjk4LTEuODE2LDQuMjg1LTEuODE2LDYuNzQyDQoJCQljMCw0LjY1MSwyLjM2OSw4Ljc1Nyw1Ljk2NSwxMS4xNjFjLTIuMTk3LTAuMDY5LTQuMjY2LTAuNjcyLTYuMDczLTEuNjc5Yy0wLjAwMSwwLjA1Ny0wLjAwMSwwLjExNC0wLjAwMSwwLjE3DQoJCQljMCw2LjQ5Nyw0LjYyNCwxMS45MTYsMTAuNzU3LDEzLjE0N2MtMS4xMjQsMC4zMDgtMi4zMTEsMC40NzEtMy41MzIsMC40NzFjLTAuODY2LDAtMS43MDUtMC4wODMtMi41MjMtMC4yMzkNCgkJCWMxLjcwNiw1LjMyNiw2LjY1Nyw5LjIwMywxMi41MjYsOS4zMTJjLTQuNTksMy41OTctMTAuMzcxLDUuNzQtMTYuNjU1LDUuNzRjLTEuMDgsMC0yLjE1LTAuMDYzLTMuMTk3LTAuMTg4DQoJCQljNS45MzEsMy44MDYsMTIuOTgxLDYuMDI1LDIwLjU1Myw2LjAyNWMyNC42NjQsMCwzOC4xNTItMjAuNDMyLDM4LjE1Mi0zOC4xNTNjMC0wLjU4MS0wLjAxMy0xLjE2LTAuMDM5LTEuNzM0DQoJCQlDODYuMzkxLDQ1LjM2Niw4OC42NjQsNDMuMDA1LDkwLjQ2MSw0MC4zMTZMOTAuNDYxLDQwLjMxNnoiLz4NCgk8L2c+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8L3N2Zz4NCg=="

/***/ }),
/* 189 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pg0KPCEtLSBHZW5lcmF0b3I6IEFkb2JlIElsbHVzdHJhdG9yIDE5LjAuMCwgU1ZHIEV4cG9ydCBQbHVnLUluIC4gU1ZHIFZlcnNpb246IDYuMDAgQnVpbGQgMCkgIC0tPg0KPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJMYXllcl8xIiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB4PSIwcHgiIHk9IjBweCINCgkgdmlld0JveD0iMCAwIDI5MS4zMTkgMjkxLjMxOSIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMjkxLjMxOSAyOTEuMzE5OyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+DQo8Zz4NCgk8cGF0aCBzdHlsZT0iZmlsbDojMEU3NkE4OyIgZD0iTTE0NS42NTksMGM4MC40NSwwLDE0NS42Niw2NS4yMTksMTQ1LjY2LDE0NS42NnMtNjUuMjEsMTQ1LjY1OS0xNDUuNjYsMTQ1LjY1OVMwLDIyNi4xLDAsMTQ1LjY2DQoJCVM2NS4yMSwwLDE0NS42NTksMHoiLz4NCgk8cGF0aCBzdHlsZT0iZmlsbDojRkZGRkZGOyIgZD0iTTgyLjA3OSwyMDAuMTM2aDI3LjI3NXYtOTAuOTFIODIuMDc5VjIwMC4xMzZ6IE0xODguMzM4LDEwNi4wNzcNCgkJYy0xMy4yMzcsMC0yNS4wODEsNC44MzQtMzMuNDgzLDE1LjUwNHYtMTIuNjU0SDEyNy40OHY5MS4yMWgyNy4zNzV2LTQ5LjMyNGMwLTEwLjQyNCw5LjU1LTIwLjU5MywyMS41MTItMjAuNTkzDQoJCXMxNC45MTIsMTAuMTY5LDE0LjkxMiwyMC4zMzh2NDkuNTdoMjcuMjc1di01MS42QzIxOC41NTMsMTEyLjY4NiwyMDEuNTg0LDEwNi4wNzcsMTg4LjMzOCwxMDYuMDc3eiBNOTUuNTg5LDEwMC4xNDENCgkJYzcuNTM4LDAsMTMuNjU2LTYuMTE4LDEzLjY1Ni0xMy42NTZTMTAzLjEyNyw3Mi44Myw5NS41ODksNzIuODNzLTEzLjY1Niw2LjExOC0xMy42NTYsMTMuNjU2Uzg4LjA1MSwxMDAuMTQxLDk1LjU4OSwxMDAuMTQxeiIvPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPGc+DQo8L2c+DQo8Zz4NCjwvZz4NCjxnPg0KPC9nPg0KPC9zdmc+DQo="

/***/ }),
/* 190 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIj8+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgd2lkdGg9IjUxMnB4IiB2ZXJzaW9uPSIxLjEiIGhlaWdodD0iNTEycHgiIHZpZXdCb3g9IjAgMCA2NCA2NCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgNjQgNjQiIGNsYXNzPSIiPjxnPjxnPgogICAgPGcgZmlsbD0iIzFEMUQxQiI+CiAgICAgIDxwYXRoIGQ9Im0xNS40ODYsMjUuNTE1YzAuMzk4LDAuNDU0IDAuOTUyLDAuNjg3IDEuNTA3LDAuNjg3IDAuNDc4LDAgMC45NTgtMC4xNzIgMS4zNDUtMC41MTggMC44MzItMC43NSAwLjkwNi0yLjA0MyAwLjE2NS0yLjg4N2wtNy40ODgtOC41MjhjLTAuMDE0LTAuMDE1LTAuMDMyLTAuMDIxLTAuMDQ2LTAuMDM0LTAuMDI5LTAuMDMxLTAuMDU3LTAuMDYtMC4wODgtMC4wODgtMC4wMTYtMC4wMTUtMC4wMi0wLjAzMy0wLjAzNS0wLjA0Ny0wLjA3My0wLjA2Ni0wLjE2My0wLjA5LTAuMjQxLTAuMTQ0LTAuMDkzLTAuMDYyLTAuMTc3LTAuMTQyLTAuMjc1LTAuMTg3LTAuMDM3LTAuMDE4LTAuMDc1LTAuMDI3LTAuMTEyLTAuMDQxLTAuMTA4LTAuMDQxLTAuMjE5LTAuMDUyLTAuMzMxLTAuMDc0LTAuMTA4LTAuMDIxLTAuMjExLTAuMDU3LTAuMzIzLTAuMDYtMC4wMjEtMC4wMDEtMC4wMzgtMC4wMTItMC4wNTgtMC4wMTJzLTAuMDM3LDAuMDExLTAuMDU4LDAuMDEyYy0wLjExMiwwLjAwMy0wLjIxNywwLjAzOC0wLjMyNywwLjA2LTAuMTEyLDAuMDIyLTAuMjIxLDAuMDMzLTAuMzI3LDAuMDc0LTAuMDM3LDAuMDE0LTAuMDc0LDAuMDIzLTAuMTEsMC4wNDEtMC4xMDEsMC4wNDUtMC4xODQsMC4xMjQtMC4yNzgsMC4xODctMC4wOCwwLjA1NC0wLjE3MSwwLjA3OC0wLjI0NCwwLjE0NC0wLjAxNiwwLjAxNS0wLjAyLDAuMDM0LTAuMDM1LDAuMDQ5LTAuMDMsMC4wMjctMC4wNTgsMC4wNTYtMC4wODUsMC4wODYtMC4wMTQsMC4wMTQtMC4wMzEsMC4wMi0wLjA0NiwwLjAzNGwtNy40ODYsOC41MjhjLTAuNzQxLDAuODQ0LTAuNjY2LDIuMTM3IDAuMTY4LDIuODg3IDAuMzg1LDAuMzQ2IDAuODYzLDAuNTE4IDEuMzQsMC41MTggMC41NTcsMCAxLjExLTAuMjMyIDEuNTA5LTAuNjg3bDMuOTYtNC41MTF2MjMuNDQ1YzAsMy4zODMgMi43MTcsNi4xMzQgNi4wNTgsNi4xMzRoMjkuMTRjMS4xMTUsMCAyLjAxOS0wLjkxNSAyLjAxOS0yLjA0NCAwLTEuMTMtMC45MDMtMi4wNDUtMi4wMTktMi4wNDVoLTI5LjE0Yy0xLjExNSwwLTIuMDItMC45MTgtMi4wMi0yLjA0NXYtMjMuNDQ1bDMuOTYxLDQuNTExeiIgZGF0YS1vcmlnaW5hbD0iIzAwMDAwMCIgY2xhc3M9ImFjdGl2ZS1wYXRoIiBkYXRhLW9sZF9jb2xvcj0iIzZjNzU3ZCIgZmlsbD0iIzZjNzU3ZCIvPgogICAgICA8cGF0aCBkPSJtNjAuNDczLDM4LjY1MmwtMy45NTksNC41MXYtMjMuNDQ1YzAtMy4zODMtMi43MTgtNi4xMzQtNi4wNTgtNi4xMzRoLTI4LjQxNWMtMS4xMTcsMC0yLjAyLDAuOTE1LTIuMDIsMi4wNDQgMCwxLjEzIDAuOTAyLDIuMDQ1IDIuMDIsMi4wNDVoMjguNDE1YzEuMTE1LDAgMi4wMiwwLjkxOCAyLjAyLDIuMDQ1djIzLjQ0NWwtMy45NjItNC41MWMtMC43NDItMC44NDQtMi4wMTYtMC45Mi0yLjg1Mi0wLjE2OC0wLjgzMiwwLjc1LTAuOTA2LDIuMDQzLTAuMTY2LDIuODg2bDcuNDg5LDguNTI3YzAuMDEyLDAuMDE1IDAuMDMyLDAuMDE5IDAuMDQ0LDAuMDMyIDAuMDI5LDAuMDMyIDAuMDU5LDAuMDYyIDAuMDksMC4wOTIgMC4wMTQsMC4wMTMgMC4wMiwwLjAzMSAwLjAzNSwwLjA0NSAwLjA5NSwwLjA4NCAwLjIwNiwwLjEyNSAwLjMwOSwwLjE4OSAwLjAzMywwLjAyMSAwLjA2MiwwLjA0OCAwLjEsMC4wNjYgMC4wNDcsMC4wMjUgMC4wODUsMC4wNyAwLjEzNCwwLjA5MiAwLjAxOCwwLjAwOCAwLjAzNywwLjAxIDAuMDU1LDAuMDE4IDAuMjQxLDAuMDk2IDAuNDksMC4xNTEgMC43NDQsMC4xNTEgMC4yNTEsMCAwLjUwNC0wLjA1NSAwLjc0My0wLjE1MSAwLjAxOC0wLjAwOCAwLjAzNy0wLjAxIDAuMDU2LTAuMDE4IDAuMDQ5LTAuMDIxIDAuMDg2LTAuMDY1IDAuMTMxLTAuMDkgMC4wMzMtMC4wMTkgMC4wNTktMC4wNDQgMC4wOTEtMC4wNjIgMC4xMDktMC4wNjQgMC4yMjYtMC4xMDkgMC4zMjEtMC4xOTUgMC4wMTYtMC4wMTUgMC4wMi0wLjAzNCAwLjAzNS0wLjA0OSAwLjAzLTAuMDI4IDAuMDU4LTAuMDU4IDAuMDg3LTAuMDg4IDAuMDEyLTAuMDE0IDAuMDMxLTAuMDE4IDAuMDQzLTAuMDMybDcuNDg4LTguNTI3YzAuNzQtMC44NDMgMC42NjUtMi4xMzYtMC4xNjktMi44ODYtMC44MzUtMC43NTItMi4xMS0wLjY3NS0yLjg0OSwwLjE2OHoiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiM2Yzc1N2QiIGZpbGw9IiM2Yzc1N2QiLz4KICAgIDwvZz4KICA8L2c+PC9nPiA8L3N2Zz4K"

/***/ }),
/* 191 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIj8+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDQ0LjU2OSA0NC41NjkiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDQ0LjU2OSA0NC41Njk7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiIGNsYXNzPSIiPjxnPjxwYXRoIGQ9Ik0xMS42OTgsMS43MzljMy4xMTEsMCw1LjY1LDEuMDQ4LDcuNjAzLDIuODU3YzEuMjU0LDEuMTU5LDIuMjU0LDIuNjE5LDIuOTg0LDQuMzE3ICBjMC43My0xLjY5OCwxLjczLTMuMTU5LDIuOTY4LTQuMzE3YzEuOTUyLTEuODA5LDQuNTA4LTIuODU3LDcuNjE5LTIuODU3YzMuMjM4LDAsNi4xNTgsMS4zMTcsOC4yNjksMy40MjggIGMyLjEyNywyLjEyNywzLjQyOCw1LjA0NywzLjQyOCw4LjI3YzAsNS41NC02LjA2MywxMi4wMzEtMTAuMzY1LDE2LjYxOWMtMC43NzgsMC44MjUtMS40OTIsMS41ODctMi4xNDMsMi4zMTdsLTguOTM2LDEwLjA3OSAgYy0wLjQxMywwLjQ2LTEuMTExLDAuNTA4LTEuNTcxLDAuMDk1Yy0wLjA0OC0wLjAzMi0wLjA3OS0wLjA2My0wLjExMS0wLjA5NWwtOC45MzYtMTAuMDc5Yy0wLjU4Ny0wLjY1MS0xLjM0OS0xLjQ2LTIuMTc0LTIuMzQ5ICBDNi4wMzEsMjUuNDM3LDAsMTguOTkzLDAsMTMuNDM3YzAtMy4yMjIsMS4zMTctNi4xNDMsMy40MjgtOC4yN0M1LjU1NSwzLjA1Nyw4LjQ3NiwxLjczOSwxMS42OTgsMS43MzlMMTEuNjk4LDEuNzM5eiBNMTcuNzkzLDYuMjMxICBjLTEuNTQtMS40MjktMy41NzEtMi4yNTQtNi4wOTUtMi4yNTRjLTIuNjAzLDAtNC45NjgsMS4wNjMtNi42ODIsMi43NzhzLTIuNzc4LDQuMDc5LTIuNzc4LDYuNjgyYzAsNC42ODIsNS42ODIsMTAuNzQ2LDkuNzMsMTUuMDYzICBjMC43OTQsMC44NDEsMS41NCwxLjYzNSwyLjIwNiwyLjM5N2w4LjExMSw5LjE0M2w4LjExMS05LjE0M2MwLjYwMy0wLjY2NywxLjM2NS0xLjQ5MiwyLjE5LTIuMzY1ICBjNC4wMzItNC4zMTcsOS43NDYtMTAuNDEyLDkuNzQ2LTE1LjA5NWMwLTIuNjAzLTEuMDYzLTQuOTY4LTIuNzYyLTYuNjgyYy0xLjcxNC0xLjcxNC00LjA3OS0yLjc3OC02LjY5OC0yLjc3OCAgYy0yLjUyNCwwLTQuNTU1LDAuODI1LTYuMDk1LDIuMjU0Yy0xLjY4MiwxLjU1NS0yLjgyNSwzLjg1Ny0zLjM4MSw2LjYxOWMtMC4wNzksMC40NDQtMC40MTMsMC44MS0wLjg4OSwwLjkwNSAgYy0wLjYwMywwLjEyNy0xLjE5LTAuMjU0LTEuMzE3LTAuODczQzIwLjYzNCwxMC4xMiwxOS40OTEsNy44MDMsMTcuNzkzLDYuMjMxTDE3Ljc5Myw2LjIzMXoiIGRhdGEtb3JpZ2luYWw9IiMxRTIwMUQiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiM2Yzc1N2QiIGZpbGw9IiM2Yzc1N2QiLz48L2c+IDwvc3ZnPgo="

/***/ }),
/* 192 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIj8+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDE1NS4xMjMgMTU1LjEyMyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgMTU1LjEyMyAxNTUuMTIzOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgd2lkdGg9IjUxMnB4IiBoZWlnaHQ9IjUxMnB4IiBjbGFzcz0iIj48Zz48Zz4KCTxnPgoJCTxwYXRoIGQ9Ik0xNTAuNjY5LDg0LjA2OGM3Ljg1OC03LjgyMyw1LjQzLTIzLjY0Ny04LjE4MS0yMy42NDdsLTM1LjgxMywwLjAyNCAgICBjMS4zNi03LjU4NCwzLjMzLTIwLjE1NiwzLjI1Mi0yMS4zNDNjLTAuNzUyLTExLjI0Mi03LjkxOC0yNC45MjQtOC4yMjgtMjUuNDg0Yy0xLjMwNy0yLjQzNC03LjkwNi01LjczNC0xNC41NDctNC4zMiAgICBjLTguNTg2LDEuODM4LTkuNDYzLDcuMzE1LTkuNDI4LDguODI1YzAsMCwwLjM3LDE0Ljk4MywwLjQwNiwxOC45ODFjLTQuMTA1LDkuMDE2LTE4LjI1OSwzMi43MS0yMi41NDksMzQuNTM2ICAgIGMtMS4wMjYtMC42MjEtMi4xOS0wLjk1NS0zLjQwMS0wLjk1NUg2LjkzNEMzLjA5MSw3MC42ODUsMCw3My43OTMsMCw3Ny42MThsMC4wMDYsNjIuNTMzYzAuMjY5LDMuMzcxLDMuMTMzLDYuMDE1LDYuNTE2LDYuMDE1ICAgIGg0MC42NGMzLjYwNCwwLDYuNTM0LTIuOTMsNi41MzQtNi41MzR2LTIuMDc2YzAsMCwxLjUxLTAuMTEzLDIuMTk2LDAuMzI4YzIuNjEzLDEuNjU5LDUuODQyLDMuNzQ3LDEwLjA1NCwzLjc0N2g2MC42NDcgICAgYzIyLjY3NCwwLDIwLjI0LTIwLjEyNiwxOC4xNjktMjIuODcxYzMuODMxLTQuMTcxLDYuMi0xMS41MjgsMi45NjYtMTcuMzRDMTUwLjIxLDk4Ljc4OSwxNTQuNTc4LDkxLjU1NywxNTAuNjY5LDg0LjA2OHogICAgIE00NS43NjYsMTM5LjYySDYuNTFWNzcuMjEyaDM5LjI1NlYxMzkuNjJ6IE0xNDAuMDksODMuNTMxbC0wLjM3LDEuNTQ1YzEwLjQ0OCwyLjk3MSw0Ljg4NywxNS4wMTMtMi42MDgsMTUuNzk0bC0wLjM3LDEuNTQ1ICAgIGMxMC4wMTgsMi41NDgsNS4yMzksMTQuOTQ3LTIuNjA4LDE1Ljc5NGwtMC4zNywxLjUzOWM4LjE4MSwxLjM0Myw2LjIsMTUuMzA1LTYuMTk0LDE1LjMwNWwtNjEuNjg2LDAuMDI0ICAgIGMtNC4zNTYsMC04LjMyNC00Ljk2NC0xMS41MjgtNC45NjRINTEuNTZWODIuMDc1YzMuNDg1LTIuMTYsNy43NjktNC45NjQsMTAuMTUtNi45ODdjNC40OTktMy44MzcsMjIuOTEzLTMzLjU5MywyMi45MTMtMzcuMzE3ICAgIHMtMC40MDYtMTkuODM0LTAuNDA2LTE5LjgzNHMzLjYxLTQuNjU0LDExLjY3MS0xLjI1OWMwLDAsNi43ODQsMTIuNzIxLDcuNDc2LDIyLjg1OWMwLDAtMy4wNTUsMjAuODg0LTQuNjk2LDI3LjQzNmg0Mi43NjUgICAgQzE1MS45NCw2Ni45ODUsMTQ5LjkzNSw4MS45ODYsMTQwLjA5LDgzLjUzMXoiIGRhdGEtb3JpZ2luYWw9IiMwMTAwMDIiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiM2Yzc1N2QiIGZpbGw9IiM2Yzc1N2QiLz4KCTwvZz4KPC9nPjwvZz4gPC9zdmc+Cg=="

/***/ }),
/* 193 */
/***/ (function(module, exports) {

module.exports = "data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIj8+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDYwIDYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA2MCA2MDsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSI1MTJweCIgaGVpZ2h0PSI1MTJweCIgY2xhc3M9IiI+PGc+PHBhdGggZD0iTTU0LDJINkMyLjc0OCwyLDAsNC43NDgsMCw4djMzYzAsMy4yNTIsMi43NDgsNiw2LDZoMjguNTU4bDkuNzAzLDEwLjY3M0M0NC40NTQsNTcuODg1LDQ0LjcyNCw1OCw0NSw1OCAgYzAuMTIxLDAsMC4yNDMtMC4wMjIsMC4zNjEtMC4wNjdDNDUuNzQ2LDU3Ljc4NCw0Niw1Ny40MTMsNDYsNTdWNDdoOGMzLjI1MiwwLDYtMi43NDgsNi02VjhDNjAsNC43NDgsNTcuMjUyLDIsNTQsMnogTTU4LDQxICBjMCwyLjE2OC0xLjgzMiw0LTQsNGgtOWMtMC41NTIsMC0xLDAuNDQ4LTEsMXY4LjQxNGwtOC4yNDMtOS4wNjhsLTQuOTk4LTUuODExYy0wLjM2LTAuNDE4LTAuOTkxLTAuNDY2LTEuNDExLTAuMTA2ICBjLTAuNDE4LDAuMzYtMC40NjYsMC45OTItMC4xMDYsMS40MUwzMi44MjEsNDVINmMtMi4xNjgsMC00LTEuODMyLTQtNFY4YzAtMi4xNjgsMS44MzItNCw0LTRoNDhjMi4xNjgsMCw0LDEuODMyLDQsNFY0MXoiIGRhdGEtb3JpZ2luYWw9IiMwMDAwMDAiIGNsYXNzPSJhY3RpdmUtcGF0aCIgZGF0YS1vbGRfY29sb3I9IiM2Yzc1N2QiIGZpbGw9IiM2Yzc1N2QiLz48L2c+IDwvc3ZnPgo="

/***/ }),
/* 194 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('nav',{staticClass:"navbar navbar-dark fixed-top bg-atas"},[_c('a',{staticClass:"navbar-brand",attrs:{"href":"#"}},[_c('img',{attrs:{"src":_vm.logocoba}})])]),_vm._v(" "),_c('div',{staticClass:"container-fluid analitic-page"},[_c('mu-card',{staticClass:"card-analitic-tab card-rewards",staticStyle:{"margin":"0 auto"}},[_c('div',{staticClass:"reward-tab"},[_c('div',{staticClass:"top-leader"},[_c('mu-tabs',{attrs:{"value":_vm.active},on:{"update:value":function($event){_vm.active=$event}}},[_c('mu-tab',[_c('div',{staticClass:"social-share"},[_c('mu-row',{attrs:{"gutter":""}},[_c('mu-col',{attrs:{"span":"5"}},[_c('img',{attrs:{"src":_vm.facebook}}),_vm._v(" "),_c('p',[_vm._v("Facebook")])]),_vm._v(" "),_c('mu-col',{attrs:{"span":"7"}},[_c('div',{staticClass:"shares-bitch"},[_c('h1',[_vm._v(_vm._s(this.count.facebook))])])])],1)],1)]),_vm._v(" "),_c('mu-tab',[_c('div',{staticClass:"social-share"},[_c('mu-row',{attrs:{"gutter":""}},[_c('mu-col',{attrs:{"span":"5"}},[_c('img',{attrs:{"src":_vm.twitter}}),_vm._v(" "),_c('p',[_vm._v("Twitter")])]),_vm._v(" "),_c('mu-col',{attrs:{"span":"7"}},[_c('div',{staticClass:"shares-bitch"},[_c('h1',[_vm._v(_vm._s(this.count.twitter))])])])],1)],1)]),_vm._v(" "),_c('mu-tab',[_c('div',{staticClass:"social-share"},[_c('mu-row',{attrs:{"gutter":""}},[_c('mu-col',{attrs:{"span":"5"}},[_c('img',{attrs:{"src":_vm.linkedin}}),_vm._v(" "),_c('p',[_vm._v("Linkedin")])]),_vm._v(" "),_c('mu-col',{attrs:{"span":"7"}},[_c('div',{staticClass:"shares-bitch"},[_c('h1',[_vm._v(_vm._s(this.count.linkedin))])])])],1)],1)])],1)],1)])]),_vm._v(" "),(_vm.active === 0)?_c('div',{staticClass:"demo-text konten-reward"},[_c('page-analitic-facebook')],1):_vm._e(),_vm._v(" "),(_vm.active === 1)?_c('div',{staticClass:"demo-text konten-reward"},[_c('page-analitic-twiter')],1):_vm._e(),_vm._v(" "),(_vm.active === 2)?_c('div',{staticClass:"demo-text konten-reward"},[_c('page-analitic-linkedin')],1):_vm._e()],1)])}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 195 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageLeaderboard_vue__ = __webpack_require__(71);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_135eb7ca_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageLeaderboard_vue__ = __webpack_require__(197);
function injectStyle (ssrContext) {
  __webpack_require__(196)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-135eb7ca"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageLeaderboard_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_135eb7ca_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageLeaderboard_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 196 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 197 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"leaderboard"},[_c('nav',{staticClass:"navbar navbar-dark fixed-top bg-atas"},[_c('div',{staticClass:"navinih"},[_c('div',{staticClass:"back"},[_c('mu-ripple',{staticClass:"icon-back",attrs:{"color":"rgb(33, 150, 243, 0.3)","opacity":0.3},on:{"click":_vm.back}},[_c('md-icon',[_vm._v("arrow_back")])],1)],1),_vm._v(" "),_vm._m(0)])]),_vm._v(" "),_c('mu-card',{staticClass:"card-feed card-leaderboard",staticStyle:{"margin":"0 auto"}},_vm._l((_vm.posts),function(post){return _c('div',{key:post.id},[_c('div',{staticClass:"leader-list",class:post.class},[_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-1"},[_c('div',{staticClass:"no-lead"},[_c('p',[_vm._v(_vm._s(post.rank))])])]),_vm._v(" "),_c('div',{staticClass:"col-2"},[(post.image === null)?_c('div',{staticClass:"gmbr-prfl"},[_c('img',{attrs:{"src":"http://103.49.223.93/cms-advokasi/public/img_user/TG0E5B_default-profile.png"}})]):_c('div',{staticClass:"gmbr-prfl"},[_c('img',{attrs:{"src":post.image}})])]),_vm._v(" "),_c('div',{staticClass:"col-5"},[_c('div',{staticClass:"name-lead"},[_c('p',[_vm._v(_vm._s(post.nama))])])]),_vm._v(" "),_c('div',{staticClass:"col-4"},[_c('div',{staticClass:"point-lead"},[_c('p',[_vm._v(_vm._s(post.point)+" "),_c('span',{staticClass:"pointing"},[_c('i',{staticClass:"fa fa-star"}),_vm._v(" Points")])])])])])])])}),0)],1)}
var staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"headbang2"},[_c('h1',[_vm._v("Leaderboard")])])}]
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 198 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageActivity_vue__ = __webpack_require__(72);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_01535a38_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageActivity_vue__ = __webpack_require__(200);
function injectStyle (ssrContext) {
  __webpack_require__(199)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageActivity_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_01535a38_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageActivity_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 199 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 200 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"activity"},[_c('nav',{staticClass:"navbar navbar-dark fixed-top bg-atas"},[_c('div',{staticClass:"navinih"},[_c('div',{staticClass:"back"},[_c('mu-ripple',{staticClass:"icon-back",attrs:{"color":"rgb(33, 150, 243, 0.3)","opacity":0.3},on:{"click":_vm.back}},[_c('md-icon',[_vm._v("arrow_back")])],1)],1),_vm._v(" "),_vm._m(0)])]),_vm._v(" "),_c('mu-card',{staticClass:"card-feed card-analitic",staticStyle:{"margin":"0 auto"}},[(this.hitung === 0)?_c('div',[_c('not-found')],1):_c('div',[_c('div',{staticClass:"listing-history"},[_c('ul',{staticClass:"timeline"},_vm._l((_vm.posts),function(post){return _c('li',{key:post.id,staticClass:"hlisting"},[_c('link-prevue',{attrs:{"url":post.feed.url},scopedSlots:_vm._u([{key:"default",fn:function(props){return [_c('p',{staticClass:"hdate"},[_vm._v(_vm._s(_vm._f("moment")(post.created_at,"LL")))]),_vm._v(" "),_c('div',{staticClass:"row"},[_c('div',{staticClass:"col-2"},[_c('div',{staticClass:"gmbr-activity"},[_c('img',{attrs:{"src":props.img}})])]),_vm._v(" "),_c('div',{staticClass:"col-10"},[_c('h2',{staticClass:"title-history"},[_vm._v(_vm._s(props.title))]),_vm._v(" "),(post.id_sosmed === 1)?_c('p',{staticClass:"share-activity"},[_vm._v("Dibagikan ke Facebook")]):_vm._e(),_vm._v(" "),(post.id_sosmed === 2)?_c('p',{staticClass:"share-activity"},[_vm._v("Dibagikan ke Twitter")]):_vm._e(),_vm._v(" "),(post.id_sosmed === 3)?_c('p',{staticClass:"share-activity"},[_vm._v("Dibagikan ke Linkedin")]):_vm._e()])])]}}],null,true)},[_vm._v(" "),_c('template',{slot:"loading"},[_c('loading-activity')],1)],2)],1)}),0)])])])],1)}
var staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"headbang2"},[_c('h1',[_vm._v("Activity")])])}]
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 201 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageReedem_vue__ = __webpack_require__(73);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_093a9273_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageReedem_vue__ = __webpack_require__(203);
function injectStyle (ssrContext) {
  __webpack_require__(202)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-093a9273"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PageReedem_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_093a9273_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PageReedem_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 202 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 203 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"reedem"},[_c('nav',{staticClass:"navbar navbar-dark fixed-top bg-atas"},[_c('div',{staticClass:"navinih"},[_c('div',{staticClass:"back"},[_c('mu-ripple',{staticClass:"icon-back",attrs:{"color":"rgb(33, 150, 243, 0.3)","opacity":0.3},on:{"click":_vm.back}},[_c('md-icon',[_vm._v("arrow_back")])],1)],1),_vm._v(" "),_vm._m(0)])]),_vm._v(" "),_c('mu-card',{staticClass:"card-feed card-reedem",staticStyle:{"margin":"0 auto"}},[_c('mu-card-media',[_c('div',{staticClass:"vcr-media"},[_c('img',{attrs:{"src":this.posts.image_voucher}})])]),_vm._v(" "),_c('div',{staticClass:"vcr-detail"},[_c('h1',[_vm._v(_vm._s(this.posts.nama_voucher))])]),_vm._v(" "),_c('div',{staticClass:"vcr-poin"},[_c('i',{staticClass:"material-icons"},[_vm._v("stars")]),_vm._v(" "),_c('span',[_vm._v(_vm._s(this.posts.point_voucher)+" Poin")])]),_vm._v(" "),_c('div',{staticClass:"vcr-date"},[_c('p',{staticClass:"lft"},[_vm._v("Kadaluarsa")]),_vm._v(" "),_c('p',{staticClass:"rght"},[_vm._v(_vm._s(_vm._f("moment")(this.posts.expired_date,"LL")))])])],1),_vm._v(" "),_c('mu-card',{staticClass:"card-feed card-reedem2",staticStyle:{"margin":"0 auto"}},[_c('div',{staticClass:"reward-tab"},[_c('mu-tabs',{staticClass:"menyu",attrs:{"value":_vm.active},on:{"update:value":function($event){_vm.active=$event}}},[_c('mu-tab',[_vm._v("Deskripsi")]),_vm._v(" "),_c('mu-tab',[_vm._v("Cara Menggunakan")]),_vm._v(" "),_c('mu-tab',{staticClass:"tnk"},[_vm._v("Syarat dan Ketentuan")])],1),_vm._v(" "),(_vm.active === 0)?_c('div',{staticClass:"demo-text konten-vcr"},[_c('div',{domProps:{"innerHTML":_vm._s(this.posts.description)}})]):_vm._e(),_vm._v(" "),(_vm.active === 1)?_c('div',{staticClass:"demo-text konten-vcr"},[_c('div',{domProps:{"innerHTML":_vm._s(this.posts.cara_pakai)}})]):_vm._e(),_vm._v(" "),(_vm.active === 2)?_c('div',{staticClass:"demo-text konten-vcr"},[_c('div',{domProps:{"innerHTML":_vm._s(this.posts.syarat)}})]):_vm._e()],1)]),_vm._v(" "),_c('mu-card',{staticClass:"tombol-reedem",staticStyle:{"margin":"10px auto"}},[_c('div',{staticClass:"jar-btn-vcs"},[_c('a',{staticClass:"btn btn-primary btn-block",on:{"click":function($event){return _vm.reedemVoucher()}}},[_vm._v("Tukarkan")])])])],1)}
var staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"headbang2"},[_c('h1',[_vm._v("Voucher")])])}]
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 204 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PagePassword_vue__ = __webpack_require__(74);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_016c26b6_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PagePassword_vue__ = __webpack_require__(206);
function injectStyle (ssrContext) {
  __webpack_require__(205)
}
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = "data-v-016c26b6"
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_PagePassword_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_016c26b6_hasScoped_true_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_PagePassword_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 205 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 206 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',[_c('nav',{staticClass:"navbar navbar-dark fixed-top bg-atas"},[_c('div',{staticClass:"navinih"},[_c('div',{staticClass:"back"},[_c('mu-ripple',{staticClass:"icon-back",attrs:{"color":"rgb(33, 150, 243, 0.3)","opacity":0.3},on:{"click":_vm.back}},[_c('md-icon',[_vm._v("arrow_back")])],1)],1),_vm._v(" "),_vm._m(0)])]),_vm._v(" "),_c('div',{staticClass:"edit-profile"},[_c('mu-card',{staticClass:"card-feed card-edit",staticStyle:{"margin":"0 auto"}},[_c('div',{staticClass:"name-edit"},[_c('h2',[_vm._v("Password")]),_vm._v(" "),_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.password),expression:"password"}],staticClass:"form-control",attrs:{"type":"password"},domProps:{"value":(_vm.password)},on:{"input":function($event){if($event.target.composing){ return; }_vm.password=$event.target.value}}}),_vm._v(" "),_c('h2',[_vm._v("Konfirmasi Password")]),_vm._v(" "),_c('input',{directives:[{name:"model",rawName:"v-model",value:(_vm.confirm),expression:"confirm"}],staticClass:"form-control",attrs:{"type":"password"},domProps:{"value":(_vm.confirm)},on:{"input":function($event){if($event.target.composing){ return; }_vm.confirm=$event.target.value}}}),_vm._v(" "),_c('button',{staticClass:"btn btn-deep-purple btn-block",attrs:{"type":"button"},on:{"click":_vm.change}},[_vm._v("Save")])])])],1)])}
var staticRenderFns = [function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{staticClass:"headbang2"},[_c('h1',[_vm._v("Change Password")])])}]
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 207 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_LoginTwitter_vue__ = __webpack_require__(75);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_405c5030_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_LoginTwitter_vue__ = __webpack_require__(208);
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_LoginTwitter_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_405c5030_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_LoginTwitter_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 208 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c("div")}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 209 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_LoginLinkedin_vue__ = __webpack_require__(76);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_07b603f8_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_LoginLinkedin_vue__ = __webpack_require__(210);
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_node_modules_vue_loader_lib_selector_type_script_index_0_LoginLinkedin_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__node_modules_vue_loader_lib_template_compiler_index_id_data_v_07b603f8_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_node_modules_vue_loader_lib_selector_type_template_index_0_LoginLinkedin_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 210 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c("div")}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 211 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 212 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 213 */,
/* 214 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 215 */,
/* 216 */,
/* 217 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 218 */,
/* 219 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_vue_particles_vue__ = __webpack_require__(77);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_5ededfff_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_vue_loader_lib_selector_type_template_index_0_vue_particles_vue__ = __webpack_require__(221);
var normalizeComponent = __webpack_require__(0)
/* script */


/* template */

/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __WEBPACK_IMPORTED_MODULE_0__babel_loader_vue_loader_lib_selector_type_script_index_0_vue_particles_vue__["a" /* default */],
  __WEBPACK_IMPORTED_MODULE_1__vue_loader_lib_template_compiler_index_id_data_v_5ededfff_hasScoped_false_transformToRequire_video_src_poster_source_src_img_src_image_xlink_href_buble_transforms_vue_loader_lib_selector_type_template_index_0_vue_particles_vue__["a" /* default */],
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)

/* harmony default export */ __webpack_exports__["a"] = (Component.exports);


/***/ }),
/* 220 */,
/* 221 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
var render = function () {var _vm=this;var _h=_vm.$createElement;var _c=_vm._self._c||_h;return _c('div',{attrs:{"id":"particles-js","color":_vm.color,"particleOpacity":_vm.particleOpacity,"linesColor":_vm.linesColor,"particlesNumber":_vm.particlesNumber,"shapeType":_vm.shapeType,"particleSize":_vm.particleSize,"linesWidth":_vm.linesWidth,"lineLinked":_vm.lineLinked,"lineOpacity":_vm.lineOpacity,"linesDistance":_vm.linesDistance,"moveSpeed":_vm.moveSpeed,"hoverEffect":_vm.hoverEffect,"hoverMode":_vm.hoverMode,"clickEffect":_vm.clickEffect,"clickMode":_vm.clickMode}})}
var staticRenderFns = []
var esExports = { render: render, staticRenderFns: staticRenderFns }
/* harmony default export */ __webpack_exports__["a"] = (esExports);

/***/ }),
/* 222 */,
/* 223 */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {(function (global, factory) {
	 true ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(factory((global.vueMoment = {})));
}(this, (function (exports) { 'use strict';

var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

function commonjsRequire () {
	throw new Error('Dynamic requires are not currently supported by rollup-plugin-commonjs');
}



function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var moment = createCommonjsModule(function (module, exports) {
var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

//! moment.js
//! version : 2.19.1
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com

(function (global, factory) {
    (_typeof(exports)) === 'object' && 'object' !== 'undefined' ? module.exports = factory() :  false ? undefined(factory) : global.moment = factory();
})(commonjsGlobal, function () {
    var hookCallback;

    function hooks() {
        return hookCallback.apply(null, arguments);
    }

    // This is done to register the method called with moment()
    // without creating circular dependencies.
    function setHookCallback(callback) {
        hookCallback = callback;
    }

    function isArray(input) {
        return input instanceof Array || Object.prototype.toString.call(input) === '[object Array]';
    }

    function isObject(input) {
        // IE8 will treat undefined and null as object if it wasn't for
        // input != null
        return input != null && Object.prototype.toString.call(input) === '[object Object]';
    }

    function isObjectEmpty(obj) {
        if (Object.getOwnPropertyNames) {
            return Object.getOwnPropertyNames(obj).length === 0;
        } else {
            var k;
            for (k in obj) {
                if (obj.hasOwnProperty(k)) {
                    return false;
                }
            }
            return true;
        }
    }

    function isUndefined(input) {
        return input === void 0;
    }

    function isNumber(input) {
        return typeof input === 'number' || Object.prototype.toString.call(input) === '[object Number]';
    }

    function isDate(input) {
        return input instanceof Date || Object.prototype.toString.call(input) === '[object Date]';
    }

    function map(arr, fn) {
        var res = [],
            i;
        for (i = 0; i < arr.length; ++i) {
            res.push(fn(arr[i], i));
        }
        return res;
    }

    function hasOwnProp(a, b) {
        return Object.prototype.hasOwnProperty.call(a, b);
    }

    function extend(a, b) {
        for (var i in b) {
            if (hasOwnProp(b, i)) {
                a[i] = b[i];
            }
        }

        if (hasOwnProp(b, 'toString')) {
            a.toString = b.toString;
        }

        if (hasOwnProp(b, 'valueOf')) {
            a.valueOf = b.valueOf;
        }

        return a;
    }

    function createUTC(input, format, locale, strict) {
        return createLocalOrUTC(input, format, locale, strict, true).utc();
    }

    function defaultParsingFlags() {
        // We need to deep clone this object.
        return {
            empty: false,
            unusedTokens: [],
            unusedInput: [],
            overflow: -2,
            charsLeftOver: 0,
            nullInput: false,
            invalidMonth: null,
            invalidFormat: false,
            userInvalidated: false,
            iso: false,
            parsedDateParts: [],
            meridiem: null,
            rfc2822: false,
            weekdayMismatch: false
        };
    }

    function getParsingFlags(m) {
        if (m._pf == null) {
            m._pf = defaultParsingFlags();
        }
        return m._pf;
    }

    var some;
    if (Array.prototype.some) {
        some = Array.prototype.some;
    } else {
        some = function some(fun) {
            var t = Object(this);
            var len = t.length >>> 0;

            for (var i = 0; i < len; i++) {
                if (i in t && fun.call(this, t[i], i, t)) {
                    return true;
                }
            }

            return false;
        };
    }

    function isValid(m) {
        if (m._isValid == null) {
            var flags = getParsingFlags(m);
            var parsedParts = some.call(flags.parsedDateParts, function (i) {
                return i != null;
            });
            var isNowValid = !isNaN(m._d.getTime()) && flags.overflow < 0 && !flags.empty && !flags.invalidMonth && !flags.invalidWeekday && !flags.weekdayMismatch && !flags.nullInput && !flags.invalidFormat && !flags.userInvalidated && (!flags.meridiem || flags.meridiem && parsedParts);

            if (m._strict) {
                isNowValid = isNowValid && flags.charsLeftOver === 0 && flags.unusedTokens.length === 0 && flags.bigHour === undefined;
            }

            if (Object.isFrozen == null || !Object.isFrozen(m)) {
                m._isValid = isNowValid;
            } else {
                return isNowValid;
            }
        }
        return m._isValid;
    }

    function createInvalid(flags) {
        var m = createUTC(NaN);
        if (flags != null) {
            extend(getParsingFlags(m), flags);
        } else {
            getParsingFlags(m).userInvalidated = true;
        }

        return m;
    }

    // Plugins that add properties should also add the key here (null value),
    // so we can properly clone ourselves.
    var momentProperties = hooks.momentProperties = [];

    function copyConfig(to, from) {
        var i, prop, val;

        if (!isUndefined(from._isAMomentObject)) {
            to._isAMomentObject = from._isAMomentObject;
        }
        if (!isUndefined(from._i)) {
            to._i = from._i;
        }
        if (!isUndefined(from._f)) {
            to._f = from._f;
        }
        if (!isUndefined(from._l)) {
            to._l = from._l;
        }
        if (!isUndefined(from._strict)) {
            to._strict = from._strict;
        }
        if (!isUndefined(from._tzm)) {
            to._tzm = from._tzm;
        }
        if (!isUndefined(from._isUTC)) {
            to._isUTC = from._isUTC;
        }
        if (!isUndefined(from._offset)) {
            to._offset = from._offset;
        }
        if (!isUndefined(from._pf)) {
            to._pf = getParsingFlags(from);
        }
        if (!isUndefined(from._locale)) {
            to._locale = from._locale;
        }

        if (momentProperties.length > 0) {
            for (i = 0; i < momentProperties.length; i++) {
                prop = momentProperties[i];
                val = from[prop];
                if (!isUndefined(val)) {
                    to[prop] = val;
                }
            }
        }

        return to;
    }

    var updateInProgress = false;

    // Moment prototype object
    function Moment(config) {
        copyConfig(this, config);
        this._d = new Date(config._d != null ? config._d.getTime() : NaN);
        if (!this.isValid()) {
            this._d = new Date(NaN);
        }
        // Prevent infinite loop in case updateOffset creates new moment
        // objects.
        if (updateInProgress === false) {
            updateInProgress = true;
            hooks.updateOffset(this);
            updateInProgress = false;
        }
    }

    function isMoment(obj) {
        return obj instanceof Moment || obj != null && obj._isAMomentObject != null;
    }

    function absFloor(number) {
        if (number < 0) {
            // -0 -> 0
            return Math.ceil(number) || 0;
        } else {
            return Math.floor(number);
        }
    }

    function toInt(argumentForCoercion) {
        var coercedNumber = +argumentForCoercion,
            value = 0;

        if (coercedNumber !== 0 && isFinite(coercedNumber)) {
            value = absFloor(coercedNumber);
        }

        return value;
    }

    // compare two arrays, return the number of differences
    function compareArrays(array1, array2, dontConvert) {
        var len = Math.min(array1.length, array2.length),
            lengthDiff = Math.abs(array1.length - array2.length),
            diffs = 0,
            i;
        for (i = 0; i < len; i++) {
            if (dontConvert && array1[i] !== array2[i] || !dontConvert && toInt(array1[i]) !== toInt(array2[i])) {
                diffs++;
            }
        }
        return diffs + lengthDiff;
    }

    function warn(msg) {
        if (hooks.suppressDeprecationWarnings === false && typeof console !== 'undefined' && console.warn) {
            console.warn('Deprecation warning: ' + msg);
        }
    }

    function deprecate(msg, fn) {
        var firstTime = true;

        return extend(function () {
            if (hooks.deprecationHandler != null) {
                hooks.deprecationHandler(null, msg);
            }
            if (firstTime) {
                var args = [];
                var arg;
                for (var i = 0; i < arguments.length; i++) {
                    arg = '';
                    if (_typeof(arguments[i]) === 'object') {
                        arg += '\n[' + i + '] ';
                        for (var key in arguments[0]) {
                            arg += key + ': ' + arguments[0][key] + ', ';
                        }
                        arg = arg.slice(0, -2); // Remove trailing comma and space
                    } else {
                        arg = arguments[i];
                    }
                    args.push(arg);
                }
                warn(msg + '\nArguments: ' + Array.prototype.slice.call(args).join('') + '\n' + new Error().stack);
                firstTime = false;
            }
            return fn.apply(this, arguments);
        }, fn);
    }

    var deprecations = {};

    function deprecateSimple(name, msg) {
        if (hooks.deprecationHandler != null) {
            hooks.deprecationHandler(name, msg);
        }
        if (!deprecations[name]) {
            warn(msg);
            deprecations[name] = true;
        }
    }

    hooks.suppressDeprecationWarnings = false;
    hooks.deprecationHandler = null;

    function isFunction(input) {
        return input instanceof Function || Object.prototype.toString.call(input) === '[object Function]';
    }

    function set(config) {
        var prop, i;
        for (i in config) {
            prop = config[i];
            if (isFunction(prop)) {
                this[i] = prop;
            } else {
                this['_' + i] = prop;
            }
        }
        this._config = config;
        // Lenient ordinal parsing accepts just a number in addition to
        // number + (possibly) stuff coming from _dayOfMonthOrdinalParse.
        // TODO: Remove "ordinalParse" fallback in next major release.
        this._dayOfMonthOrdinalParseLenient = new RegExp((this._dayOfMonthOrdinalParse.source || this._ordinalParse.source) + '|' + /\d{1,2}/.source);
    }

    function mergeConfigs(parentConfig, childConfig) {
        var res = extend({}, parentConfig),
            prop;
        for (prop in childConfig) {
            if (hasOwnProp(childConfig, prop)) {
                if (isObject(parentConfig[prop]) && isObject(childConfig[prop])) {
                    res[prop] = {};
                    extend(res[prop], parentConfig[prop]);
                    extend(res[prop], childConfig[prop]);
                } else if (childConfig[prop] != null) {
                    res[prop] = childConfig[prop];
                } else {
                    delete res[prop];
                }
            }
        }
        for (prop in parentConfig) {
            if (hasOwnProp(parentConfig, prop) && !hasOwnProp(childConfig, prop) && isObject(parentConfig[prop])) {
                // make sure changes to properties don't modify parent config
                res[prop] = extend({}, res[prop]);
            }
        }
        return res;
    }

    function Locale(config) {
        if (config != null) {
            this.set(config);
        }
    }

    var keys;

    if (Object.keys) {
        keys = Object.keys;
    } else {
        keys = function keys(obj) {
            var i,
                res = [];
            for (i in obj) {
                if (hasOwnProp(obj, i)) {
                    res.push(i);
                }
            }
            return res;
        };
    }

    var defaultCalendar = {
        sameDay: '[Today at] LT',
        nextDay: '[Tomorrow at] LT',
        nextWeek: 'dddd [at] LT',
        lastDay: '[Yesterday at] LT',
        lastWeek: '[Last] dddd [at] LT',
        sameElse: 'L'
    };

    function calendar(key, mom, now) {
        var output = this._calendar[key] || this._calendar['sameElse'];
        return isFunction(output) ? output.call(mom, now) : output;
    }

    var defaultLongDateFormat = {
        LTS: 'h:mm:ss A',
        LT: 'h:mm A',
        L: 'MM/DD/YYYY',
        LL: 'MMMM D, YYYY',
        LLL: 'MMMM D, YYYY h:mm A',
        LLLL: 'dddd, MMMM D, YYYY h:mm A'
    };

    function longDateFormat(key) {
        var format = this._longDateFormat[key],
            formatUpper = this._longDateFormat[key.toUpperCase()];

        if (format || !formatUpper) {
            return format;
        }

        this._longDateFormat[key] = formatUpper.replace(/MMMM|MM|DD|dddd/g, function (val) {
            return val.slice(1);
        });

        return this._longDateFormat[key];
    }

    var defaultInvalidDate = 'Invalid date';

    function invalidDate() {
        return this._invalidDate;
    }

    var defaultOrdinal = '%d';
    var defaultDayOfMonthOrdinalParse = /\d{1,2}/;

    function ordinal(number) {
        return this._ordinal.replace('%d', number);
    }

    var defaultRelativeTime = {
        future: 'in %s',
        past: '%s ago',
        s: 'a few seconds',
        ss: '%d seconds',
        m: 'a minute',
        mm: '%d minutes',
        h: 'an hour',
        hh: '%d hours',
        d: 'a day',
        dd: '%d days',
        M: 'a month',
        MM: '%d months',
        y: 'a year',
        yy: '%d years'
    };

    function relativeTime(number, withoutSuffix, string, isFuture) {
        var output = this._relativeTime[string];
        return isFunction(output) ? output(number, withoutSuffix, string, isFuture) : output.replace(/%d/i, number);
    }

    function pastFuture(diff, output) {
        var format = this._relativeTime[diff > 0 ? 'future' : 'past'];
        return isFunction(format) ? format(output) : format.replace(/%s/i, output);
    }

    var aliases = {};

    function addUnitAlias(unit, shorthand) {
        var lowerCase = unit.toLowerCase();
        aliases[lowerCase] = aliases[lowerCase + 's'] = aliases[shorthand] = unit;
    }

    function normalizeUnits(units) {
        return typeof units === 'string' ? aliases[units] || aliases[units.toLowerCase()] : undefined;
    }

    function normalizeObjectUnits(inputObject) {
        var normalizedInput = {},
            normalizedProp,
            prop;

        for (prop in inputObject) {
            if (hasOwnProp(inputObject, prop)) {
                normalizedProp = normalizeUnits(prop);
                if (normalizedProp) {
                    normalizedInput[normalizedProp] = inputObject[prop];
                }
            }
        }

        return normalizedInput;
    }

    var priorities = {};

    function addUnitPriority(unit, priority) {
        priorities[unit] = priority;
    }

    function getPrioritizedUnits(unitsObj) {
        var units = [];
        for (var u in unitsObj) {
            units.push({ unit: u, priority: priorities[u] });
        }
        units.sort(function (a, b) {
            return a.priority - b.priority;
        });
        return units;
    }

    function zeroFill(number, targetLength, forceSign) {
        var absNumber = '' + Math.abs(number),
            zerosToFill = targetLength - absNumber.length,
            sign = number >= 0;
        return (sign ? forceSign ? '+' : '' : '-') + Math.pow(10, Math.max(0, zerosToFill)).toString().substr(1) + absNumber;
    }

    var formattingTokens = /(\[[^\[]*\])|(\\)?([Hh]mm(ss)?|Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Qo?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|kk?|mm?|ss?|S{1,9}|x|X|zz?|ZZ?|.)/g;

    var localFormattingTokens = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g;

    var formatFunctions = {};

    var formatTokenFunctions = {};

    // token:    'M'
    // padded:   ['MM', 2]
    // ordinal:  'Mo'
    // callback: function () { this.month() + 1 }
    function addFormatToken(token, padded, ordinal, callback) {
        var func = callback;
        if (typeof callback === 'string') {
            func = function func() {
                return this[callback]();
            };
        }
        if (token) {
            formatTokenFunctions[token] = func;
        }
        if (padded) {
            formatTokenFunctions[padded[0]] = function () {
                return zeroFill(func.apply(this, arguments), padded[1], padded[2]);
            };
        }
        if (ordinal) {
            formatTokenFunctions[ordinal] = function () {
                return this.localeData().ordinal(func.apply(this, arguments), token);
            };
        }
    }

    function removeFormattingTokens(input) {
        if (input.match(/\[[\s\S]/)) {
            return input.replace(/^\[|\]$/g, '');
        }
        return input.replace(/\\/g, '');
    }

    function makeFormatFunction(format) {
        var array = format.match(formattingTokens),
            i,
            length;

        for (i = 0, length = array.length; i < length; i++) {
            if (formatTokenFunctions[array[i]]) {
                array[i] = formatTokenFunctions[array[i]];
            } else {
                array[i] = removeFormattingTokens(array[i]);
            }
        }

        return function (mom) {
            var output = '',
                i;
            for (i = 0; i < length; i++) {
                output += isFunction(array[i]) ? array[i].call(mom, format) : array[i];
            }
            return output;
        };
    }

    // format date using native date object
    function formatMoment(m, format) {
        if (!m.isValid()) {
            return m.localeData().invalidDate();
        }

        format = expandFormat(format, m.localeData());
        formatFunctions[format] = formatFunctions[format] || makeFormatFunction(format);

        return formatFunctions[format](m);
    }

    function expandFormat(format, locale) {
        var i = 5;

        function replaceLongDateFormatTokens(input) {
            return locale.longDateFormat(input) || input;
        }

        localFormattingTokens.lastIndex = 0;
        while (i >= 0 && localFormattingTokens.test(format)) {
            format = format.replace(localFormattingTokens, replaceLongDateFormatTokens);
            localFormattingTokens.lastIndex = 0;
            i -= 1;
        }

        return format;
    }

    var match1 = /\d/; //       0 - 9
    var match2 = /\d\d/; //      00 - 99
    var match3 = /\d{3}/; //     000 - 999
    var match4 = /\d{4}/; //    0000 - 9999
    var match6 = /[+-]?\d{6}/; // -999999 - 999999
    var match1to2 = /\d\d?/; //       0 - 99
    var match3to4 = /\d\d\d\d?/; //     999 - 9999
    var match5to6 = /\d\d\d\d\d\d?/; //   99999 - 999999
    var match1to3 = /\d{1,3}/; //       0 - 999
    var match1to4 = /\d{1,4}/; //       0 - 9999
    var match1to6 = /[+-]?\d{1,6}/; // -999999 - 999999

    var matchUnsigned = /\d+/; //       0 - inf
    var matchSigned = /[+-]?\d+/; //    -inf - inf

    var matchOffset = /Z|[+-]\d\d:?\d\d/gi; // +00:00 -00:00 +0000 -0000 or Z
    var matchShortOffset = /Z|[+-]\d\d(?::?\d\d)?/gi; // +00 -00 +00:00 -00:00 +0000 -0000 or Z

    var matchTimestamp = /[+-]?\d+(\.\d{1,3})?/; // 123456789 123456789.123

    // any word (or two) characters or numbers including two/three word month in arabic.
    // includes scottish gaelic two word and hyphenated months
    var matchWord = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i;

    var regexes = {};

    function addRegexToken(token, regex, strictRegex) {
        regexes[token] = isFunction(regex) ? regex : function (isStrict, localeData) {
            return isStrict && strictRegex ? strictRegex : regex;
        };
    }

    function getParseRegexForToken(token, config) {
        if (!hasOwnProp(regexes, token)) {
            return new RegExp(unescapeFormat(token));
        }

        return regexes[token](config._strict, config._locale);
    }

    // Code from http://stackoverflow.com/questions/3561493/is-there-a-regexp-escape-function-in-javascript
    function unescapeFormat(s) {
        return regexEscape(s.replace('\\', '').replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function (matched, p1, p2, p3, p4) {
            return p1 || p2 || p3 || p4;
        }));
    }

    function regexEscape(s) {
        return s.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    }

    var tokens = {};

    function addParseToken(token, callback) {
        var i,
            func = callback;
        if (typeof token === 'string') {
            token = [token];
        }
        if (isNumber(callback)) {
            func = function func(input, array) {
                array[callback] = toInt(input);
            };
        }
        for (i = 0; i < token.length; i++) {
            tokens[token[i]] = func;
        }
    }

    function addWeekParseToken(token, callback) {
        addParseToken(token, function (input, array, config, token) {
            config._w = config._w || {};
            callback(input, config._w, config, token);
        });
    }

    function addTimeToArrayFromToken(token, input, config) {
        if (input != null && hasOwnProp(tokens, token)) {
            tokens[token](input, config._a, config, token);
        }
    }

    var YEAR = 0;
    var MONTH = 1;
    var DATE = 2;
    var HOUR = 3;
    var MINUTE = 4;
    var SECOND = 5;
    var MILLISECOND = 6;
    var WEEK = 7;
    var WEEKDAY = 8;

    // FORMATTING

    addFormatToken('Y', 0, 0, function () {
        var y = this.year();
        return y <= 9999 ? '' + y : '+' + y;
    });

    addFormatToken(0, ['YY', 2], 0, function () {
        return this.year() % 100;
    });

    addFormatToken(0, ['YYYY', 4], 0, 'year');
    addFormatToken(0, ['YYYYY', 5], 0, 'year');
    addFormatToken(0, ['YYYYYY', 6, true], 0, 'year');

    // ALIASES

    addUnitAlias('year', 'y');

    // PRIORITIES

    addUnitPriority('year', 1);

    // PARSING

    addRegexToken('Y', matchSigned);
    addRegexToken('YY', match1to2, match2);
    addRegexToken('YYYY', match1to4, match4);
    addRegexToken('YYYYY', match1to6, match6);
    addRegexToken('YYYYYY', match1to6, match6);

    addParseToken(['YYYYY', 'YYYYYY'], YEAR);
    addParseToken('YYYY', function (input, array) {
        array[YEAR] = input.length === 2 ? hooks.parseTwoDigitYear(input) : toInt(input);
    });
    addParseToken('YY', function (input, array) {
        array[YEAR] = hooks.parseTwoDigitYear(input);
    });
    addParseToken('Y', function (input, array) {
        array[YEAR] = parseInt(input, 10);
    });

    // HELPERS

    function daysInYear(year) {
        return isLeapYear(year) ? 366 : 365;
    }

    function isLeapYear(year) {
        return year % 4 === 0 && year % 100 !== 0 || year % 400 === 0;
    }

    // HOOKS

    hooks.parseTwoDigitYear = function (input) {
        return toInt(input) + (toInt(input) > 68 ? 1900 : 2000);
    };

    // MOMENTS

    var getSetYear = makeGetSet('FullYear', true);

    function getIsLeapYear() {
        return isLeapYear(this.year());
    }

    function makeGetSet(unit, keepTime) {
        return function (value) {
            if (value != null) {
                set$1(this, unit, value);
                hooks.updateOffset(this, keepTime);
                return this;
            } else {
                return get(this, unit);
            }
        };
    }

    function get(mom, unit) {
        return mom.isValid() ? mom._d['get' + (mom._isUTC ? 'UTC' : '') + unit]() : NaN;
    }

    function set$1(mom, unit, value) {
        if (mom.isValid() && !isNaN(value)) {
            if (unit === 'FullYear' && isLeapYear(mom.year())) {
                mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value, mom.month(), daysInMonth(value, mom.month()));
            } else {
                mom._d['set' + (mom._isUTC ? 'UTC' : '') + unit](value);
            }
        }
    }

    // MOMENTS

    function stringGet(units) {
        units = normalizeUnits(units);
        if (isFunction(this[units])) {
            return this[units]();
        }
        return this;
    }

    function stringSet(units, value) {
        if ((typeof units === 'undefined' ? 'undefined' : _typeof(units)) === 'object') {
            units = normalizeObjectUnits(units);
            var prioritized = getPrioritizedUnits(units);
            for (var i = 0; i < prioritized.length; i++) {
                this[prioritized[i].unit](units[prioritized[i].unit]);
            }
        } else {
            units = normalizeUnits(units);
            if (isFunction(this[units])) {
                return this[units](value);
            }
        }
        return this;
    }

    function mod(n, x) {
        return (n % x + x) % x;
    }

    var indexOf;

    if (Array.prototype.indexOf) {
        indexOf = Array.prototype.indexOf;
    } else {
        indexOf = function indexOf(o) {
            // I know
            var i;
            for (i = 0; i < this.length; ++i) {
                if (this[i] === o) {
                    return i;
                }
            }
            return -1;
        };
    }

    function daysInMonth(year, month) {
        if (isNaN(year) || isNaN(month)) {
            return NaN;
        }
        var modMonth = mod(month, 12);
        year += (month - modMonth) / 12;
        return modMonth === 1 ? isLeapYear(year) ? 29 : 28 : 31 - modMonth % 7 % 2;
    }

    // FORMATTING

    addFormatToken('M', ['MM', 2], 'Mo', function () {
        return this.month() + 1;
    });

    addFormatToken('MMM', 0, 0, function (format) {
        return this.localeData().monthsShort(this, format);
    });

    addFormatToken('MMMM', 0, 0, function (format) {
        return this.localeData().months(this, format);
    });

    // ALIASES

    addUnitAlias('month', 'M');

    // PRIORITY

    addUnitPriority('month', 8);

    // PARSING

    addRegexToken('M', match1to2);
    addRegexToken('MM', match1to2, match2);
    addRegexToken('MMM', function (isStrict, locale) {
        return locale.monthsShortRegex(isStrict);
    });
    addRegexToken('MMMM', function (isStrict, locale) {
        return locale.monthsRegex(isStrict);
    });

    addParseToken(['M', 'MM'], function (input, array) {
        array[MONTH] = toInt(input) - 1;
    });

    addParseToken(['MMM', 'MMMM'], function (input, array, config, token) {
        var month = config._locale.monthsParse(input, token, config._strict);
        // if we didn't find a month name, mark the date as invalid.
        if (month != null) {
            array[MONTH] = month;
        } else {
            getParsingFlags(config).invalidMonth = input;
        }
    });

    // LOCALES

    var MONTHS_IN_FORMAT = /D[oD]?(\[[^\[\]]*\]|\s)+MMMM?/;
    var defaultLocaleMonths = 'January_February_March_April_May_June_July_August_September_October_November_December'.split('_');
    function localeMonths(m, format) {
        if (!m) {
            return isArray(this._months) ? this._months : this._months['standalone'];
        }
        return isArray(this._months) ? this._months[m.month()] : this._months[(this._months.isFormat || MONTHS_IN_FORMAT).test(format) ? 'format' : 'standalone'][m.month()];
    }

    var defaultLocaleMonthsShort = 'Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec'.split('_');
    function localeMonthsShort(m, format) {
        if (!m) {
            return isArray(this._monthsShort) ? this._monthsShort : this._monthsShort['standalone'];
        }
        return isArray(this._monthsShort) ? this._monthsShort[m.month()] : this._monthsShort[MONTHS_IN_FORMAT.test(format) ? 'format' : 'standalone'][m.month()];
    }

    function handleStrictParse(monthName, format, strict) {
        var i,
            ii,
            mom,
            llc = monthName.toLocaleLowerCase();
        if (!this._monthsParse) {
            // this is not used
            this._monthsParse = [];
            this._longMonthsParse = [];
            this._shortMonthsParse = [];
            for (i = 0; i < 12; ++i) {
                mom = createUTC([2000, i]);
                this._shortMonthsParse[i] = this.monthsShort(mom, '').toLocaleLowerCase();
                this._longMonthsParse[i] = this.months(mom, '').toLocaleLowerCase();
            }
        }

        if (strict) {
            if (format === 'MMM') {
                ii = indexOf.call(this._shortMonthsParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._longMonthsParse, llc);
                return ii !== -1 ? ii : null;
            }
        } else {
            if (format === 'MMM') {
                ii = indexOf.call(this._shortMonthsParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._longMonthsParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._longMonthsParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._shortMonthsParse, llc);
                return ii !== -1 ? ii : null;
            }
        }
    }

    function localeMonthsParse(monthName, format, strict) {
        var i, mom, regex;

        if (this._monthsParseExact) {
            return handleStrictParse.call(this, monthName, format, strict);
        }

        if (!this._monthsParse) {
            this._monthsParse = [];
            this._longMonthsParse = [];
            this._shortMonthsParse = [];
        }

        // TODO: add sorting
        // Sorting makes sure if one month (or abbr) is a prefix of another
        // see sorting in computeMonthsParse
        for (i = 0; i < 12; i++) {
            // make the regex if we don't have it already
            mom = createUTC([2000, i]);
            if (strict && !this._longMonthsParse[i]) {
                this._longMonthsParse[i] = new RegExp('^' + this.months(mom, '').replace('.', '') + '$', 'i');
                this._shortMonthsParse[i] = new RegExp('^' + this.monthsShort(mom, '').replace('.', '') + '$', 'i');
            }
            if (!strict && !this._monthsParse[i]) {
                regex = '^' + this.months(mom, '') + '|^' + this.monthsShort(mom, '');
                this._monthsParse[i] = new RegExp(regex.replace('.', ''), 'i');
            }
            // test the regex
            if (strict && format === 'MMMM' && this._longMonthsParse[i].test(monthName)) {
                return i;
            } else if (strict && format === 'MMM' && this._shortMonthsParse[i].test(monthName)) {
                return i;
            } else if (!strict && this._monthsParse[i].test(monthName)) {
                return i;
            }
        }
    }

    // MOMENTS

    function setMonth(mom, value) {
        var dayOfMonth;

        if (!mom.isValid()) {
            // No op
            return mom;
        }

        if (typeof value === 'string') {
            if (/^\d+$/.test(value)) {
                value = toInt(value);
            } else {
                value = mom.localeData().monthsParse(value);
                // TODO: Another silent failure?
                if (!isNumber(value)) {
                    return mom;
                }
            }
        }

        dayOfMonth = Math.min(mom.date(), daysInMonth(mom.year(), value));
        mom._d['set' + (mom._isUTC ? 'UTC' : '') + 'Month'](value, dayOfMonth);
        return mom;
    }

    function getSetMonth(value) {
        if (value != null) {
            setMonth(this, value);
            hooks.updateOffset(this, true);
            return this;
        } else {
            return get(this, 'Month');
        }
    }

    function getDaysInMonth() {
        return daysInMonth(this.year(), this.month());
    }

    var defaultMonthsShortRegex = matchWord;
    function monthsShortRegex(isStrict) {
        if (this._monthsParseExact) {
            if (!hasOwnProp(this, '_monthsRegex')) {
                computeMonthsParse.call(this);
            }
            if (isStrict) {
                return this._monthsShortStrictRegex;
            } else {
                return this._monthsShortRegex;
            }
        } else {
            if (!hasOwnProp(this, '_monthsShortRegex')) {
                this._monthsShortRegex = defaultMonthsShortRegex;
            }
            return this._monthsShortStrictRegex && isStrict ? this._monthsShortStrictRegex : this._monthsShortRegex;
        }
    }

    var defaultMonthsRegex = matchWord;
    function monthsRegex(isStrict) {
        if (this._monthsParseExact) {
            if (!hasOwnProp(this, '_monthsRegex')) {
                computeMonthsParse.call(this);
            }
            if (isStrict) {
                return this._monthsStrictRegex;
            } else {
                return this._monthsRegex;
            }
        } else {
            if (!hasOwnProp(this, '_monthsRegex')) {
                this._monthsRegex = defaultMonthsRegex;
            }
            return this._monthsStrictRegex && isStrict ? this._monthsStrictRegex : this._monthsRegex;
        }
    }

    function computeMonthsParse() {
        function cmpLenRev(a, b) {
            return b.length - a.length;
        }

        var shortPieces = [],
            longPieces = [],
            mixedPieces = [],
            i,
            mom;
        for (i = 0; i < 12; i++) {
            // make the regex if we don't have it already
            mom = createUTC([2000, i]);
            shortPieces.push(this.monthsShort(mom, ''));
            longPieces.push(this.months(mom, ''));
            mixedPieces.push(this.months(mom, ''));
            mixedPieces.push(this.monthsShort(mom, ''));
        }
        // Sorting makes sure if one month (or abbr) is a prefix of another it
        // will match the longer piece.
        shortPieces.sort(cmpLenRev);
        longPieces.sort(cmpLenRev);
        mixedPieces.sort(cmpLenRev);
        for (i = 0; i < 12; i++) {
            shortPieces[i] = regexEscape(shortPieces[i]);
            longPieces[i] = regexEscape(longPieces[i]);
        }
        for (i = 0; i < 24; i++) {
            mixedPieces[i] = regexEscape(mixedPieces[i]);
        }

        this._monthsRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
        this._monthsShortRegex = this._monthsRegex;
        this._monthsStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
        this._monthsShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
    }

    function createDate(y, m, d, h, M, s, ms) {
        // can't just apply() to create a date:
        // https://stackoverflow.com/q/181348
        var date = new Date(y, m, d, h, M, s, ms);

        // the date constructor remaps years 0-99 to 1900-1999
        if (y < 100 && y >= 0 && isFinite(date.getFullYear())) {
            date.setFullYear(y);
        }
        return date;
    }

    function createUTCDate(y) {
        var date = new Date(Date.UTC.apply(null, arguments));

        // the Date.UTC function remaps years 0-99 to 1900-1999
        if (y < 100 && y >= 0 && isFinite(date.getUTCFullYear())) {
            date.setUTCFullYear(y);
        }
        return date;
    }

    // start-of-first-week - start-of-year
    function firstWeekOffset(year, dow, doy) {
        var // first-week day -- which january is always in the first week (4 for iso, 1 for other)
        fwd = 7 + dow - doy,

        // first-week day local weekday -- which local weekday is fwd
        fwdlw = (7 + createUTCDate(year, 0, fwd).getUTCDay() - dow) % 7;

        return -fwdlw + fwd - 1;
    }

    // https://en.wikipedia.org/wiki/ISO_week_date#Calculating_a_date_given_the_year.2C_week_number_and_weekday
    function dayOfYearFromWeeks(year, week, weekday, dow, doy) {
        var localWeekday = (7 + weekday - dow) % 7,
            weekOffset = firstWeekOffset(year, dow, doy),
            dayOfYear = 1 + 7 * (week - 1) + localWeekday + weekOffset,
            resYear,
            resDayOfYear;

        if (dayOfYear <= 0) {
            resYear = year - 1;
            resDayOfYear = daysInYear(resYear) + dayOfYear;
        } else if (dayOfYear > daysInYear(year)) {
            resYear = year + 1;
            resDayOfYear = dayOfYear - daysInYear(year);
        } else {
            resYear = year;
            resDayOfYear = dayOfYear;
        }

        return {
            year: resYear,
            dayOfYear: resDayOfYear
        };
    }

    function weekOfYear(mom, dow, doy) {
        var weekOffset = firstWeekOffset(mom.year(), dow, doy),
            week = Math.floor((mom.dayOfYear() - weekOffset - 1) / 7) + 1,
            resWeek,
            resYear;

        if (week < 1) {
            resYear = mom.year() - 1;
            resWeek = week + weeksInYear(resYear, dow, doy);
        } else if (week > weeksInYear(mom.year(), dow, doy)) {
            resWeek = week - weeksInYear(mom.year(), dow, doy);
            resYear = mom.year() + 1;
        } else {
            resYear = mom.year();
            resWeek = week;
        }

        return {
            week: resWeek,
            year: resYear
        };
    }

    function weeksInYear(year, dow, doy) {
        var weekOffset = firstWeekOffset(year, dow, doy),
            weekOffsetNext = firstWeekOffset(year + 1, dow, doy);
        return (daysInYear(year) - weekOffset + weekOffsetNext) / 7;
    }

    // FORMATTING

    addFormatToken('w', ['ww', 2], 'wo', 'week');
    addFormatToken('W', ['WW', 2], 'Wo', 'isoWeek');

    // ALIASES

    addUnitAlias('week', 'w');
    addUnitAlias('isoWeek', 'W');

    // PRIORITIES

    addUnitPriority('week', 5);
    addUnitPriority('isoWeek', 5);

    // PARSING

    addRegexToken('w', match1to2);
    addRegexToken('ww', match1to2, match2);
    addRegexToken('W', match1to2);
    addRegexToken('WW', match1to2, match2);

    addWeekParseToken(['w', 'ww', 'W', 'WW'], function (input, week, config, token) {
        week[token.substr(0, 1)] = toInt(input);
    });

    // HELPERS

    // LOCALES

    function localeWeek(mom) {
        return weekOfYear(mom, this._week.dow, this._week.doy).week;
    }

    var defaultLocaleWeek = {
        dow: 0, // Sunday is the first day of the week.
        doy: 6 // The week that contains Jan 1st is the first week of the year.
    };

    function localeFirstDayOfWeek() {
        return this._week.dow;
    }

    function localeFirstDayOfYear() {
        return this._week.doy;
    }

    // MOMENTS

    function getSetWeek(input) {
        var week = this.localeData().week(this);
        return input == null ? week : this.add((input - week) * 7, 'd');
    }

    function getSetISOWeek(input) {
        var week = weekOfYear(this, 1, 4).week;
        return input == null ? week : this.add((input - week) * 7, 'd');
    }

    // FORMATTING

    addFormatToken('d', 0, 'do', 'day');

    addFormatToken('dd', 0, 0, function (format) {
        return this.localeData().weekdaysMin(this, format);
    });

    addFormatToken('ddd', 0, 0, function (format) {
        return this.localeData().weekdaysShort(this, format);
    });

    addFormatToken('dddd', 0, 0, function (format) {
        return this.localeData().weekdays(this, format);
    });

    addFormatToken('e', 0, 0, 'weekday');
    addFormatToken('E', 0, 0, 'isoWeekday');

    // ALIASES

    addUnitAlias('day', 'd');
    addUnitAlias('weekday', 'e');
    addUnitAlias('isoWeekday', 'E');

    // PRIORITY
    addUnitPriority('day', 11);
    addUnitPriority('weekday', 11);
    addUnitPriority('isoWeekday', 11);

    // PARSING

    addRegexToken('d', match1to2);
    addRegexToken('e', match1to2);
    addRegexToken('E', match1to2);
    addRegexToken('dd', function (isStrict, locale) {
        return locale.weekdaysMinRegex(isStrict);
    });
    addRegexToken('ddd', function (isStrict, locale) {
        return locale.weekdaysShortRegex(isStrict);
    });
    addRegexToken('dddd', function (isStrict, locale) {
        return locale.weekdaysRegex(isStrict);
    });

    addWeekParseToken(['dd', 'ddd', 'dddd'], function (input, week, config, token) {
        var weekday = config._locale.weekdaysParse(input, token, config._strict);
        // if we didn't get a weekday name, mark the date as invalid
        if (weekday != null) {
            week.d = weekday;
        } else {
            getParsingFlags(config).invalidWeekday = input;
        }
    });

    addWeekParseToken(['d', 'e', 'E'], function (input, week, config, token) {
        week[token] = toInt(input);
    });

    // HELPERS

    function parseWeekday(input, locale) {
        if (typeof input !== 'string') {
            return input;
        }

        if (!isNaN(input)) {
            return parseInt(input, 10);
        }

        input = locale.weekdaysParse(input);
        if (typeof input === 'number') {
            return input;
        }

        return null;
    }

    function parseIsoWeekday(input, locale) {
        if (typeof input === 'string') {
            return locale.weekdaysParse(input) % 7 || 7;
        }
        return isNaN(input) ? null : input;
    }

    // LOCALES

    var defaultLocaleWeekdays = 'Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday'.split('_');
    function localeWeekdays(m, format) {
        if (!m) {
            return isArray(this._weekdays) ? this._weekdays : this._weekdays['standalone'];
        }
        return isArray(this._weekdays) ? this._weekdays[m.day()] : this._weekdays[this._weekdays.isFormat.test(format) ? 'format' : 'standalone'][m.day()];
    }

    var defaultLocaleWeekdaysShort = 'Sun_Mon_Tue_Wed_Thu_Fri_Sat'.split('_');
    function localeWeekdaysShort(m) {
        return m ? this._weekdaysShort[m.day()] : this._weekdaysShort;
    }

    var defaultLocaleWeekdaysMin = 'Su_Mo_Tu_We_Th_Fr_Sa'.split('_');
    function localeWeekdaysMin(m) {
        return m ? this._weekdaysMin[m.day()] : this._weekdaysMin;
    }

    function handleStrictParse$1(weekdayName, format, strict) {
        var i,
            ii,
            mom,
            llc = weekdayName.toLocaleLowerCase();
        if (!this._weekdaysParse) {
            this._weekdaysParse = [];
            this._shortWeekdaysParse = [];
            this._minWeekdaysParse = [];

            for (i = 0; i < 7; ++i) {
                mom = createUTC([2000, 1]).day(i);
                this._minWeekdaysParse[i] = this.weekdaysMin(mom, '').toLocaleLowerCase();
                this._shortWeekdaysParse[i] = this.weekdaysShort(mom, '').toLocaleLowerCase();
                this._weekdaysParse[i] = this.weekdays(mom, '').toLocaleLowerCase();
            }
        }

        if (strict) {
            if (format === 'dddd') {
                ii = indexOf.call(this._weekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else if (format === 'ddd') {
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._minWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            }
        } else {
            if (format === 'dddd') {
                ii = indexOf.call(this._weekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._minWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else if (format === 'ddd') {
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._weekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._minWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            } else {
                ii = indexOf.call(this._minWeekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._weekdaysParse, llc);
                if (ii !== -1) {
                    return ii;
                }
                ii = indexOf.call(this._shortWeekdaysParse, llc);
                return ii !== -1 ? ii : null;
            }
        }
    }

    function localeWeekdaysParse(weekdayName, format, strict) {
        var i, mom, regex;

        if (this._weekdaysParseExact) {
            return handleStrictParse$1.call(this, weekdayName, format, strict);
        }

        if (!this._weekdaysParse) {
            this._weekdaysParse = [];
            this._minWeekdaysParse = [];
            this._shortWeekdaysParse = [];
            this._fullWeekdaysParse = [];
        }

        for (i = 0; i < 7; i++) {
            // make the regex if we don't have it already

            mom = createUTC([2000, 1]).day(i);
            if (strict && !this._fullWeekdaysParse[i]) {
                this._fullWeekdaysParse[i] = new RegExp('^' + this.weekdays(mom, '').replace('.', '\.?') + '$', 'i');
                this._shortWeekdaysParse[i] = new RegExp('^' + this.weekdaysShort(mom, '').replace('.', '\.?') + '$', 'i');
                this._minWeekdaysParse[i] = new RegExp('^' + this.weekdaysMin(mom, '').replace('.', '\.?') + '$', 'i');
            }
            if (!this._weekdaysParse[i]) {
                regex = '^' + this.weekdays(mom, '') + '|^' + this.weekdaysShort(mom, '') + '|^' + this.weekdaysMin(mom, '');
                this._weekdaysParse[i] = new RegExp(regex.replace('.', ''), 'i');
            }
            // test the regex
            if (strict && format === 'dddd' && this._fullWeekdaysParse[i].test(weekdayName)) {
                return i;
            } else if (strict && format === 'ddd' && this._shortWeekdaysParse[i].test(weekdayName)) {
                return i;
            } else if (strict && format === 'dd' && this._minWeekdaysParse[i].test(weekdayName)) {
                return i;
            } else if (!strict && this._weekdaysParse[i].test(weekdayName)) {
                return i;
            }
        }
    }

    // MOMENTS

    function getSetDayOfWeek(input) {
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }
        var day = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
        if (input != null) {
            input = parseWeekday(input, this.localeData());
            return this.add(input - day, 'd');
        } else {
            return day;
        }
    }

    function getSetLocaleDayOfWeek(input) {
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }
        var weekday = (this.day() + 7 - this.localeData()._week.dow) % 7;
        return input == null ? weekday : this.add(input - weekday, 'd');
    }

    function getSetISODayOfWeek(input) {
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }

        // behaves the same as moment#day except
        // as a getter, returns 7 instead of 0 (1-7 range instead of 0-6)
        // as a setter, sunday should belong to the previous week.

        if (input != null) {
            var weekday = parseIsoWeekday(input, this.localeData());
            return this.day(this.day() % 7 ? weekday : weekday - 7);
        } else {
            return this.day() || 7;
        }
    }

    var defaultWeekdaysRegex = matchWord;
    function weekdaysRegex(isStrict) {
        if (this._weekdaysParseExact) {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                computeWeekdaysParse.call(this);
            }
            if (isStrict) {
                return this._weekdaysStrictRegex;
            } else {
                return this._weekdaysRegex;
            }
        } else {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                this._weekdaysRegex = defaultWeekdaysRegex;
            }
            return this._weekdaysStrictRegex && isStrict ? this._weekdaysStrictRegex : this._weekdaysRegex;
        }
    }

    var defaultWeekdaysShortRegex = matchWord;
    function weekdaysShortRegex(isStrict) {
        if (this._weekdaysParseExact) {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                computeWeekdaysParse.call(this);
            }
            if (isStrict) {
                return this._weekdaysShortStrictRegex;
            } else {
                return this._weekdaysShortRegex;
            }
        } else {
            if (!hasOwnProp(this, '_weekdaysShortRegex')) {
                this._weekdaysShortRegex = defaultWeekdaysShortRegex;
            }
            return this._weekdaysShortStrictRegex && isStrict ? this._weekdaysShortStrictRegex : this._weekdaysShortRegex;
        }
    }

    var defaultWeekdaysMinRegex = matchWord;
    function weekdaysMinRegex(isStrict) {
        if (this._weekdaysParseExact) {
            if (!hasOwnProp(this, '_weekdaysRegex')) {
                computeWeekdaysParse.call(this);
            }
            if (isStrict) {
                return this._weekdaysMinStrictRegex;
            } else {
                return this._weekdaysMinRegex;
            }
        } else {
            if (!hasOwnProp(this, '_weekdaysMinRegex')) {
                this._weekdaysMinRegex = defaultWeekdaysMinRegex;
            }
            return this._weekdaysMinStrictRegex && isStrict ? this._weekdaysMinStrictRegex : this._weekdaysMinRegex;
        }
    }

    function computeWeekdaysParse() {
        function cmpLenRev(a, b) {
            return b.length - a.length;
        }

        var minPieces = [],
            shortPieces = [],
            longPieces = [],
            mixedPieces = [],
            i,
            mom,
            minp,
            shortp,
            longp;
        for (i = 0; i < 7; i++) {
            // make the regex if we don't have it already
            mom = createUTC([2000, 1]).day(i);
            minp = this.weekdaysMin(mom, '');
            shortp = this.weekdaysShort(mom, '');
            longp = this.weekdays(mom, '');
            minPieces.push(minp);
            shortPieces.push(shortp);
            longPieces.push(longp);
            mixedPieces.push(minp);
            mixedPieces.push(shortp);
            mixedPieces.push(longp);
        }
        // Sorting makes sure if one weekday (or abbr) is a prefix of another it
        // will match the longer piece.
        minPieces.sort(cmpLenRev);
        shortPieces.sort(cmpLenRev);
        longPieces.sort(cmpLenRev);
        mixedPieces.sort(cmpLenRev);
        for (i = 0; i < 7; i++) {
            shortPieces[i] = regexEscape(shortPieces[i]);
            longPieces[i] = regexEscape(longPieces[i]);
            mixedPieces[i] = regexEscape(mixedPieces[i]);
        }

        this._weekdaysRegex = new RegExp('^(' + mixedPieces.join('|') + ')', 'i');
        this._weekdaysShortRegex = this._weekdaysRegex;
        this._weekdaysMinRegex = this._weekdaysRegex;

        this._weekdaysStrictRegex = new RegExp('^(' + longPieces.join('|') + ')', 'i');
        this._weekdaysShortStrictRegex = new RegExp('^(' + shortPieces.join('|') + ')', 'i');
        this._weekdaysMinStrictRegex = new RegExp('^(' + minPieces.join('|') + ')', 'i');
    }

    // FORMATTING

    function hFormat() {
        return this.hours() % 12 || 12;
    }

    function kFormat() {
        return this.hours() || 24;
    }

    addFormatToken('H', ['HH', 2], 0, 'hour');
    addFormatToken('h', ['hh', 2], 0, hFormat);
    addFormatToken('k', ['kk', 2], 0, kFormat);

    addFormatToken('hmm', 0, 0, function () {
        return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2);
    });

    addFormatToken('hmmss', 0, 0, function () {
        return '' + hFormat.apply(this) + zeroFill(this.minutes(), 2) + zeroFill(this.seconds(), 2);
    });

    addFormatToken('Hmm', 0, 0, function () {
        return '' + this.hours() + zeroFill(this.minutes(), 2);
    });

    addFormatToken('Hmmss', 0, 0, function () {
        return '' + this.hours() + zeroFill(this.minutes(), 2) + zeroFill(this.seconds(), 2);
    });

    function meridiem(token, lowercase) {
        addFormatToken(token, 0, 0, function () {
            return this.localeData().meridiem(this.hours(), this.minutes(), lowercase);
        });
    }

    meridiem('a', true);
    meridiem('A', false);

    // ALIASES

    addUnitAlias('hour', 'h');

    // PRIORITY
    addUnitPriority('hour', 13);

    // PARSING

    function matchMeridiem(isStrict, locale) {
        return locale._meridiemParse;
    }

    addRegexToken('a', matchMeridiem);
    addRegexToken('A', matchMeridiem);
    addRegexToken('H', match1to2);
    addRegexToken('h', match1to2);
    addRegexToken('k', match1to2);
    addRegexToken('HH', match1to2, match2);
    addRegexToken('hh', match1to2, match2);
    addRegexToken('kk', match1to2, match2);

    addRegexToken('hmm', match3to4);
    addRegexToken('hmmss', match5to6);
    addRegexToken('Hmm', match3to4);
    addRegexToken('Hmmss', match5to6);

    addParseToken(['H', 'HH'], HOUR);
    addParseToken(['k', 'kk'], function (input, array, config) {
        var kInput = toInt(input);
        array[HOUR] = kInput === 24 ? 0 : kInput;
    });
    addParseToken(['a', 'A'], function (input, array, config) {
        config._isPm = config._locale.isPM(input);
        config._meridiem = input;
    });
    addParseToken(['h', 'hh'], function (input, array, config) {
        array[HOUR] = toInt(input);
        getParsingFlags(config).bigHour = true;
    });
    addParseToken('hmm', function (input, array, config) {
        var pos = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos));
        array[MINUTE] = toInt(input.substr(pos));
        getParsingFlags(config).bigHour = true;
    });
    addParseToken('hmmss', function (input, array, config) {
        var pos1 = input.length - 4;
        var pos2 = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos1));
        array[MINUTE] = toInt(input.substr(pos1, 2));
        array[SECOND] = toInt(input.substr(pos2));
        getParsingFlags(config).bigHour = true;
    });
    addParseToken('Hmm', function (input, array, config) {
        var pos = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos));
        array[MINUTE] = toInt(input.substr(pos));
    });
    addParseToken('Hmmss', function (input, array, config) {
        var pos1 = input.length - 4;
        var pos2 = input.length - 2;
        array[HOUR] = toInt(input.substr(0, pos1));
        array[MINUTE] = toInt(input.substr(pos1, 2));
        array[SECOND] = toInt(input.substr(pos2));
    });

    // LOCALES

    function localeIsPM(input) {
        // IE8 Quirks Mode & IE7 Standards Mode do not allow accessing strings like arrays
        // Using charAt should be more compatible.
        return (input + '').toLowerCase().charAt(0) === 'p';
    }

    var defaultLocaleMeridiemParse = /[ap]\.?m?\.?/i;
    function localeMeridiem(hours, minutes, isLower) {
        if (hours > 11) {
            return isLower ? 'pm' : 'PM';
        } else {
            return isLower ? 'am' : 'AM';
        }
    }

    // MOMENTS

    // Setting the hour should keep the time, because the user explicitly
    // specified which hour he wants. So trying to maintain the same hour (in
    // a new timezone) makes sense. Adding/subtracting hours does not follow
    // this rule.
    var getSetHour = makeGetSet('Hours', true);

    // months
    // week
    // weekdays
    // meridiem
    var baseConfig = {
        calendar: defaultCalendar,
        longDateFormat: defaultLongDateFormat,
        invalidDate: defaultInvalidDate,
        ordinal: defaultOrdinal,
        dayOfMonthOrdinalParse: defaultDayOfMonthOrdinalParse,
        relativeTime: defaultRelativeTime,

        months: defaultLocaleMonths,
        monthsShort: defaultLocaleMonthsShort,

        week: defaultLocaleWeek,

        weekdays: defaultLocaleWeekdays,
        weekdaysMin: defaultLocaleWeekdaysMin,
        weekdaysShort: defaultLocaleWeekdaysShort,

        meridiemParse: defaultLocaleMeridiemParse
    };

    // internal storage for locale config files
    var locales = {};
    var localeFamilies = {};
    var globalLocale;

    function normalizeLocale(key) {
        return key ? key.toLowerCase().replace('_', '-') : key;
    }

    // pick the locale from the array
    // try ['en-au', 'en-gb'] as 'en-au', 'en-gb', 'en', as in move through the list trying each
    // substring from most specific to least, but move to the next array item if it's a more specific variant than the current root
    function chooseLocale(names) {
        var i = 0,
            j,
            next,
            locale,
            split;

        while (i < names.length) {
            split = normalizeLocale(names[i]).split('-');
            j = split.length;
            next = normalizeLocale(names[i + 1]);
            next = next ? next.split('-') : null;
            while (j > 0) {
                locale = loadLocale(split.slice(0, j).join('-'));
                if (locale) {
                    return locale;
                }
                if (next && next.length >= j && compareArrays(split, next, true) >= j - 1) {
                    //the next array item is better than a shallower substring of this one
                    break;
                }
                j--;
            }
            i++;
        }
        return null;
    }

    function loadLocale(name) {
        var oldLocale = null;
        // TODO: Find a better way to register and load all the locales in Node
        if (!locales[name] && 'object' !== 'undefined' && module && module.exports) {
            try {
                oldLocale = globalLocale._abbr;
                var aliasedRequire = commonjsRequire;
                aliasedRequire('./locale/' + name);
                getSetGlobalLocale(oldLocale);
            } catch (e) {}
        }
        return locales[name];
    }

    // This function will load locale and then set the global locale.  If
    // no arguments are passed in, it will simply return the current global
    // locale key.
    function getSetGlobalLocale(key, values) {
        var data;
        if (key) {
            if (isUndefined(values)) {
                data = getLocale(key);
            } else {
                data = defineLocale(key, values);
            }

            if (data) {
                // moment.duration._locale = moment._locale = data;
                globalLocale = data;
            }
        }

        return globalLocale._abbr;
    }

    function defineLocale(name, config) {
        if (config !== null) {
            var parentConfig = baseConfig;
            config.abbr = name;
            if (locales[name] != null) {
                deprecateSimple('defineLocaleOverride', 'use moment.updateLocale(localeName, config) to change ' + 'an existing locale. moment.defineLocale(localeName, ' + 'config) should only be used for creating a new locale ' + 'See http://momentjs.com/guides/#/warnings/define-locale/ for more info.');
                parentConfig = locales[name]._config;
            } else if (config.parentLocale != null) {
                if (locales[config.parentLocale] != null) {
                    parentConfig = locales[config.parentLocale]._config;
                } else {
                    if (!localeFamilies[config.parentLocale]) {
                        localeFamilies[config.parentLocale] = [];
                    }
                    localeFamilies[config.parentLocale].push({
                        name: name,
                        config: config
                    });
                    return null;
                }
            }
            locales[name] = new Locale(mergeConfigs(parentConfig, config));

            if (localeFamilies[name]) {
                localeFamilies[name].forEach(function (x) {
                    defineLocale(x.name, x.config);
                });
            }

            // backwards compat for now: also set the locale
            // make sure we set the locale AFTER all child locales have been
            // created, so we won't end up with the child locale set.
            getSetGlobalLocale(name);

            return locales[name];
        } else {
            // useful for testing
            delete locales[name];
            return null;
        }
    }

    function updateLocale(name, config) {
        if (config != null) {
            var locale,
                parentConfig = baseConfig;
            // MERGE
            if (locales[name] != null) {
                parentConfig = locales[name]._config;
            }
            config = mergeConfigs(parentConfig, config);
            locale = new Locale(config);
            locale.parentLocale = locales[name];
            locales[name] = locale;

            // backwards compat for now: also set the locale
            getSetGlobalLocale(name);
        } else {
            // pass null for config to unupdate, useful for tests
            if (locales[name] != null) {
                if (locales[name].parentLocale != null) {
                    locales[name] = locales[name].parentLocale;
                } else if (locales[name] != null) {
                    delete locales[name];
                }
            }
        }
        return locales[name];
    }

    // returns locale data
    function getLocale(key) {
        var locale;

        if (key && key._locale && key._locale._abbr) {
            key = key._locale._abbr;
        }

        if (!key) {
            return globalLocale;
        }

        if (!isArray(key)) {
            //short-circuit everything else
            locale = loadLocale(key);
            if (locale) {
                return locale;
            }
            key = [key];
        }

        return chooseLocale(key);
    }

    function listLocales() {
        return keys(locales);
    }

    function checkOverflow(m) {
        var overflow;
        var a = m._a;

        if (a && getParsingFlags(m).overflow === -2) {
            overflow = a[MONTH] < 0 || a[MONTH] > 11 ? MONTH : a[DATE] < 1 || a[DATE] > daysInMonth(a[YEAR], a[MONTH]) ? DATE : a[HOUR] < 0 || a[HOUR] > 24 || a[HOUR] === 24 && (a[MINUTE] !== 0 || a[SECOND] !== 0 || a[MILLISECOND] !== 0) ? HOUR : a[MINUTE] < 0 || a[MINUTE] > 59 ? MINUTE : a[SECOND] < 0 || a[SECOND] > 59 ? SECOND : a[MILLISECOND] < 0 || a[MILLISECOND] > 999 ? MILLISECOND : -1;

            if (getParsingFlags(m)._overflowDayOfYear && (overflow < YEAR || overflow > DATE)) {
                overflow = DATE;
            }
            if (getParsingFlags(m)._overflowWeeks && overflow === -1) {
                overflow = WEEK;
            }
            if (getParsingFlags(m)._overflowWeekday && overflow === -1) {
                overflow = WEEKDAY;
            }

            getParsingFlags(m).overflow = overflow;
        }

        return m;
    }

    // Pick the first defined of two or three arguments.
    function defaults(a, b, c) {
        if (a != null) {
            return a;
        }
        if (b != null) {
            return b;
        }
        return c;
    }

    function currentDateArray(config) {
        // hooks is actually the exported moment object
        var nowValue = new Date(hooks.now());
        if (config._useUTC) {
            return [nowValue.getUTCFullYear(), nowValue.getUTCMonth(), nowValue.getUTCDate()];
        }
        return [nowValue.getFullYear(), nowValue.getMonth(), nowValue.getDate()];
    }

    // convert an array to a date.
    // the array should mirror the parameters below
    // note: all values past the year are optional and will default to the lowest possible value.
    // [year, month, day , hour, minute, second, millisecond]
    function configFromArray(config) {
        var i,
            date,
            input = [],
            currentDate,
            yearToUse;

        if (config._d) {
            return;
        }

        currentDate = currentDateArray(config);

        //compute day of the year from weeks and weekdays
        if (config._w && config._a[DATE] == null && config._a[MONTH] == null) {
            dayOfYearFromWeekInfo(config);
        }

        //if the day of the year is set, figure out what it is
        if (config._dayOfYear != null) {
            yearToUse = defaults(config._a[YEAR], currentDate[YEAR]);

            if (config._dayOfYear > daysInYear(yearToUse) || config._dayOfYear === 0) {
                getParsingFlags(config)._overflowDayOfYear = true;
            }

            date = createUTCDate(yearToUse, 0, config._dayOfYear);
            config._a[MONTH] = date.getUTCMonth();
            config._a[DATE] = date.getUTCDate();
        }

        // Default to current date.
        // * if no year, month, day of month are given, default to today
        // * if day of month is given, default month and year
        // * if month is given, default only year
        // * if year is given, don't default anything
        for (i = 0; i < 3 && config._a[i] == null; ++i) {
            config._a[i] = input[i] = currentDate[i];
        }

        // Zero out whatever was not defaulted, including time
        for (; i < 7; i++) {
            config._a[i] = input[i] = config._a[i] == null ? i === 2 ? 1 : 0 : config._a[i];
        }

        // Check for 24:00:00.000
        if (config._a[HOUR] === 24 && config._a[MINUTE] === 0 && config._a[SECOND] === 0 && config._a[MILLISECOND] === 0) {
            config._nextDay = true;
            config._a[HOUR] = 0;
        }

        config._d = (config._useUTC ? createUTCDate : createDate).apply(null, input);
        // Apply timezone offset from input. The actual utcOffset can be changed
        // with parseZone.
        if (config._tzm != null) {
            config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);
        }

        if (config._nextDay) {
            config._a[HOUR] = 24;
        }

        // check for mismatching day of week
        if (config._w && typeof config._w.d !== 'undefined' && config._w.d !== config._d.getDay()) {
            getParsingFlags(config).weekdayMismatch = true;
        }
    }

    function dayOfYearFromWeekInfo(config) {
        var w, weekYear, week, weekday, dow, doy, temp, weekdayOverflow;

        w = config._w;
        if (w.GG != null || w.W != null || w.E != null) {
            dow = 1;
            doy = 4;

            // TODO: We need to take the current isoWeekYear, but that depends on
            // how we interpret now (local, utc, fixed offset). So create
            // a now version of current config (take local/utc/offset flags, and
            // create now).
            weekYear = defaults(w.GG, config._a[YEAR], weekOfYear(createLocal(), 1, 4).year);
            week = defaults(w.W, 1);
            weekday = defaults(w.E, 1);
            if (weekday < 1 || weekday > 7) {
                weekdayOverflow = true;
            }
        } else {
            dow = config._locale._week.dow;
            doy = config._locale._week.doy;

            var curWeek = weekOfYear(createLocal(), dow, doy);

            weekYear = defaults(w.gg, config._a[YEAR], curWeek.year);

            // Default to current week.
            week = defaults(w.w, curWeek.week);

            if (w.d != null) {
                // weekday -- low day numbers are considered next week
                weekday = w.d;
                if (weekday < 0 || weekday > 6) {
                    weekdayOverflow = true;
                }
            } else if (w.e != null) {
                // local weekday -- counting starts from begining of week
                weekday = w.e + dow;
                if (w.e < 0 || w.e > 6) {
                    weekdayOverflow = true;
                }
            } else {
                // default to begining of week
                weekday = dow;
            }
        }
        if (week < 1 || week > weeksInYear(weekYear, dow, doy)) {
            getParsingFlags(config)._overflowWeeks = true;
        } else if (weekdayOverflow != null) {
            getParsingFlags(config)._overflowWeekday = true;
        } else {
            temp = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy);
            config._a[YEAR] = temp.year;
            config._dayOfYear = temp.dayOfYear;
        }
    }

    // iso 8601 regex
    // 0000-00-00 0000-W00 or 0000-W00-0 + T + 00 or 00:00 or 00:00:00 or 00:00:00.000 + +00:00 or +0000 or +00)
    var extendedIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})-(?:\d\d-\d\d|W\d\d-\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?::\d\d(?::\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;
    var basicIsoRegex = /^\s*((?:[+-]\d{6}|\d{4})(?:\d\d\d\d|W\d\d\d|W\d\d|\d\d\d|\d\d))(?:(T| )(\d\d(?:\d\d(?:\d\d(?:[.,]\d+)?)?)?)([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/;

    var tzRegex = /Z|[+-]\d\d(?::?\d\d)?/;

    var isoDates = [['YYYYYY-MM-DD', /[+-]\d{6}-\d\d-\d\d/], ['YYYY-MM-DD', /\d{4}-\d\d-\d\d/], ['GGGG-[W]WW-E', /\d{4}-W\d\d-\d/], ['GGGG-[W]WW', /\d{4}-W\d\d/, false], ['YYYY-DDD', /\d{4}-\d{3}/], ['YYYY-MM', /\d{4}-\d\d/, false], ['YYYYYYMMDD', /[+-]\d{10}/], ['YYYYMMDD', /\d{8}/],
    // YYYYMM is NOT allowed by the standard
    ['GGGG[W]WWE', /\d{4}W\d{3}/], ['GGGG[W]WW', /\d{4}W\d{2}/, false], ['YYYYDDD', /\d{7}/]];

    // iso time formats and regexes
    var isoTimes = [['HH:mm:ss.SSSS', /\d\d:\d\d:\d\d\.\d+/], ['HH:mm:ss,SSSS', /\d\d:\d\d:\d\d,\d+/], ['HH:mm:ss', /\d\d:\d\d:\d\d/], ['HH:mm', /\d\d:\d\d/], ['HHmmss.SSSS', /\d\d\d\d\d\d\.\d+/], ['HHmmss,SSSS', /\d\d\d\d\d\d,\d+/], ['HHmmss', /\d\d\d\d\d\d/], ['HHmm', /\d\d\d\d/], ['HH', /\d\d/]];

    var aspNetJsonRegex = /^\/?Date\((\-?\d+)/i;

    // date from iso format
    function configFromISO(config) {
        var i,
            l,
            string = config._i,
            match = extendedIsoRegex.exec(string) || basicIsoRegex.exec(string),
            allowTime,
            dateFormat,
            timeFormat,
            tzFormat;

        if (match) {
            getParsingFlags(config).iso = true;

            for (i = 0, l = isoDates.length; i < l; i++) {
                if (isoDates[i][1].exec(match[1])) {
                    dateFormat = isoDates[i][0];
                    allowTime = isoDates[i][2] !== false;
                    break;
                }
            }
            if (dateFormat == null) {
                config._isValid = false;
                return;
            }
            if (match[3]) {
                for (i = 0, l = isoTimes.length; i < l; i++) {
                    if (isoTimes[i][1].exec(match[3])) {
                        // match[2] should be 'T' or space
                        timeFormat = (match[2] || ' ') + isoTimes[i][0];
                        break;
                    }
                }
                if (timeFormat == null) {
                    config._isValid = false;
                    return;
                }
            }
            if (!allowTime && timeFormat != null) {
                config._isValid = false;
                return;
            }
            if (match[4]) {
                if (tzRegex.exec(match[4])) {
                    tzFormat = 'Z';
                } else {
                    config._isValid = false;
                    return;
                }
            }
            config._f = dateFormat + (timeFormat || '') + (tzFormat || '');
            configFromStringAndFormat(config);
        } else {
            config._isValid = false;
        }
    }

    // RFC 2822 regex: For details see https://tools.ietf.org/html/rfc2822#section-3.3
    var rfc2822 = /^(?:(Mon|Tue|Wed|Thu|Fri|Sat|Sun),?\s)?(\d{1,2})\s(Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec)\s(\d{2,4})\s(\d\d):(\d\d)(?::(\d\d))?\s(?:(UT|GMT|[ECMP][SD]T)|([Zz])|([+-]\d{4}))$/;

    function extractFromRFC2822Strings(yearStr, monthStr, dayStr, hourStr, minuteStr, secondStr) {
        var result = [untruncateYear(yearStr), defaultLocaleMonthsShort.indexOf(monthStr), parseInt(dayStr, 10), parseInt(hourStr, 10), parseInt(minuteStr, 10)];

        if (secondStr) {
            result.push(parseInt(secondStr, 10));
        }

        return result;
    }

    function untruncateYear(yearStr) {
        var year = parseInt(yearStr, 10);
        if (year <= 49) {
            return 2000 + year;
        } else if (year <= 999) {
            return 1900 + year;
        }
        return year;
    }

    function preprocessRFC2822(s) {
        // Remove comments and folding whitespace and replace multiple-spaces with a single space
        return s.replace(/\([^)]*\)|[\n\t]/g, ' ').replace(/(\s\s+)/g, ' ').trim();
    }

    function checkWeekday(weekdayStr, parsedInput, config) {
        if (weekdayStr) {
            // TODO: Replace the vanilla JS Date object with an indepentent day-of-week check.
            var weekdayProvided = defaultLocaleWeekdaysShort.indexOf(weekdayStr),
                weekdayActual = new Date(parsedInput[0], parsedInput[1], parsedInput[2]).getDay();
            if (weekdayProvided !== weekdayActual) {
                getParsingFlags(config).weekdayMismatch = true;
                config._isValid = false;
                return false;
            }
        }
        return true;
    }

    var obsOffsets = {
        UT: 0,
        GMT: 0,
        EDT: -4 * 60,
        EST: -5 * 60,
        CDT: -5 * 60,
        CST: -6 * 60,
        MDT: -6 * 60,
        MST: -7 * 60,
        PDT: -7 * 60,
        PST: -8 * 60
    };

    function calculateOffset(obsOffset, militaryOffset, numOffset) {
        if (obsOffset) {
            return obsOffsets[obsOffset];
        } else if (militaryOffset) {
            // the only allowed military tz is Z
            return 0;
        } else {
            var hm = parseInt(numOffset, 10);
            var m = hm % 100,
                h = (hm - m) / 100;
            return h * 60 + m;
        }
    }

    // date and time from ref 2822 format
    function configFromRFC2822(config) {
        var match = rfc2822.exec(preprocessRFC2822(config._i));
        if (match) {
            var parsedArray = extractFromRFC2822Strings(match[4], match[3], match[2], match[5], match[6], match[7]);
            if (!checkWeekday(match[1], parsedArray, config)) {
                return;
            }

            config._a = parsedArray;
            config._tzm = calculateOffset(match[8], match[9], match[10]);

            config._d = createUTCDate.apply(null, config._a);
            config._d.setUTCMinutes(config._d.getUTCMinutes() - config._tzm);

            getParsingFlags(config).rfc2822 = true;
        } else {
            config._isValid = false;
        }
    }

    // date from iso format or fallback
    function configFromString(config) {
        var matched = aspNetJsonRegex.exec(config._i);

        if (matched !== null) {
            config._d = new Date(+matched[1]);
            return;
        }

        configFromISO(config);
        if (config._isValid === false) {
            delete config._isValid;
        } else {
            return;
        }

        configFromRFC2822(config);
        if (config._isValid === false) {
            delete config._isValid;
        } else {
            return;
        }

        // Final attempt, use Input Fallback
        hooks.createFromInputFallback(config);
    }

    hooks.createFromInputFallback = deprecate('value provided is not in a recognized RFC2822 or ISO format. moment construction falls back to js Date(), ' + 'which is not reliable across all browsers and versions. Non RFC2822/ISO date formats are ' + 'discouraged and will be removed in an upcoming major release. Please refer to ' + 'http://momentjs.com/guides/#/warnings/js-date/ for more info.', function (config) {
        config._d = new Date(config._i + (config._useUTC ? ' UTC' : ''));
    });

    // constant that refers to the ISO standard
    hooks.ISO_8601 = function () {};

    // constant that refers to the RFC 2822 form
    hooks.RFC_2822 = function () {};

    // date from string and format string
    function configFromStringAndFormat(config) {
        // TODO: Move this to another part of the creation flow to prevent circular deps
        if (config._f === hooks.ISO_8601) {
            configFromISO(config);
            return;
        }
        if (config._f === hooks.RFC_2822) {
            configFromRFC2822(config);
            return;
        }
        config._a = [];
        getParsingFlags(config).empty = true;

        // This array is used to make a Date, either with `new Date` or `Date.UTC`
        var string = '' + config._i,
            i,
            parsedInput,
            tokens,
            token,
            skipped,
            stringLength = string.length,
            totalParsedInputLength = 0;

        tokens = expandFormat(config._f, config._locale).match(formattingTokens) || [];

        for (i = 0; i < tokens.length; i++) {
            token = tokens[i];
            parsedInput = (string.match(getParseRegexForToken(token, config)) || [])[0];
            // console.log('token', token, 'parsedInput', parsedInput,
            //         'regex', getParseRegexForToken(token, config));
            if (parsedInput) {
                skipped = string.substr(0, string.indexOf(parsedInput));
                if (skipped.length > 0) {
                    getParsingFlags(config).unusedInput.push(skipped);
                }
                string = string.slice(string.indexOf(parsedInput) + parsedInput.length);
                totalParsedInputLength += parsedInput.length;
            }
            // don't parse if it's not a known token
            if (formatTokenFunctions[token]) {
                if (parsedInput) {
                    getParsingFlags(config).empty = false;
                } else {
                    getParsingFlags(config).unusedTokens.push(token);
                }
                addTimeToArrayFromToken(token, parsedInput, config);
            } else if (config._strict && !parsedInput) {
                getParsingFlags(config).unusedTokens.push(token);
            }
        }

        // add remaining unparsed input length to the string
        getParsingFlags(config).charsLeftOver = stringLength - totalParsedInputLength;
        if (string.length > 0) {
            getParsingFlags(config).unusedInput.push(string);
        }

        // clear _12h flag if hour is <= 12
        if (config._a[HOUR] <= 12 && getParsingFlags(config).bigHour === true && config._a[HOUR] > 0) {
            getParsingFlags(config).bigHour = undefined;
        }

        getParsingFlags(config).parsedDateParts = config._a.slice(0);
        getParsingFlags(config).meridiem = config._meridiem;
        // handle meridiem
        config._a[HOUR] = meridiemFixWrap(config._locale, config._a[HOUR], config._meridiem);

        configFromArray(config);
        checkOverflow(config);
    }

    function meridiemFixWrap(locale, hour, meridiem) {
        var isPm;

        if (meridiem == null) {
            // nothing to do
            return hour;
        }
        if (locale.meridiemHour != null) {
            return locale.meridiemHour(hour, meridiem);
        } else if (locale.isPM != null) {
            // Fallback
            isPm = locale.isPM(meridiem);
            if (isPm && hour < 12) {
                hour += 12;
            }
            if (!isPm && hour === 12) {
                hour = 0;
            }
            return hour;
        } else {
            // this is not supposed to happen
            return hour;
        }
    }

    // date from string and array of format strings
    function configFromStringAndArray(config) {
        var tempConfig, bestMoment, scoreToBeat, i, currentScore;

        if (config._f.length === 0) {
            getParsingFlags(config).invalidFormat = true;
            config._d = new Date(NaN);
            return;
        }

        for (i = 0; i < config._f.length; i++) {
            currentScore = 0;
            tempConfig = copyConfig({}, config);
            if (config._useUTC != null) {
                tempConfig._useUTC = config._useUTC;
            }
            tempConfig._f = config._f[i];
            configFromStringAndFormat(tempConfig);

            if (!isValid(tempConfig)) {
                continue;
            }

            // if there is any input that was not parsed add a penalty for that format
            currentScore += getParsingFlags(tempConfig).charsLeftOver;

            //or tokens
            currentScore += getParsingFlags(tempConfig).unusedTokens.length * 10;

            getParsingFlags(tempConfig).score = currentScore;

            if (scoreToBeat == null || currentScore < scoreToBeat) {
                scoreToBeat = currentScore;
                bestMoment = tempConfig;
            }
        }

        extend(config, bestMoment || tempConfig);
    }

    function configFromObject(config) {
        if (config._d) {
            return;
        }

        var i = normalizeObjectUnits(config._i);
        config._a = map([i.year, i.month, i.day || i.date, i.hour, i.minute, i.second, i.millisecond], function (obj) {
            return obj && parseInt(obj, 10);
        });

        configFromArray(config);
    }

    function createFromConfig(config) {
        var res = new Moment(checkOverflow(prepareConfig(config)));
        if (res._nextDay) {
            // Adding is smart enough around DST
            res.add(1, 'd');
            res._nextDay = undefined;
        }

        return res;
    }

    function prepareConfig(config) {
        var input = config._i,
            format = config._f;

        config._locale = config._locale || getLocale(config._l);

        if (input === null || format === undefined && input === '') {
            return createInvalid({ nullInput: true });
        }

        if (typeof input === 'string') {
            config._i = input = config._locale.preparse(input);
        }

        if (isMoment(input)) {
            return new Moment(checkOverflow(input));
        } else if (isDate(input)) {
            config._d = input;
        } else if (isArray(format)) {
            configFromStringAndArray(config);
        } else if (format) {
            configFromStringAndFormat(config);
        } else {
            configFromInput(config);
        }

        if (!isValid(config)) {
            config._d = null;
        }

        return config;
    }

    function configFromInput(config) {
        var input = config._i;
        if (isUndefined(input)) {
            config._d = new Date(hooks.now());
        } else if (isDate(input)) {
            config._d = new Date(input.valueOf());
        } else if (typeof input === 'string') {
            configFromString(config);
        } else if (isArray(input)) {
            config._a = map(input.slice(0), function (obj) {
                return parseInt(obj, 10);
            });
            configFromArray(config);
        } else if (isObject(input)) {
            configFromObject(config);
        } else if (isNumber(input)) {
            // from milliseconds
            config._d = new Date(input);
        } else {
            hooks.createFromInputFallback(config);
        }
    }

    function createLocalOrUTC(input, format, locale, strict, isUTC) {
        var c = {};

        if (locale === true || locale === false) {
            strict = locale;
            locale = undefined;
        }

        if (isObject(input) && isObjectEmpty(input) || isArray(input) && input.length === 0) {
            input = undefined;
        }
        // object construction must be done this way.
        // https://github.com/moment/moment/issues/1423
        c._isAMomentObject = true;
        c._useUTC = c._isUTC = isUTC;
        c._l = locale;
        c._i = input;
        c._f = format;
        c._strict = strict;

        return createFromConfig(c);
    }

    function createLocal(input, format, locale, strict) {
        return createLocalOrUTC(input, format, locale, strict, false);
    }

    var prototypeMin = deprecate('moment().min is deprecated, use moment.max instead. http://momentjs.com/guides/#/warnings/min-max/', function () {
        var other = createLocal.apply(null, arguments);
        if (this.isValid() && other.isValid()) {
            return other < this ? this : other;
        } else {
            return createInvalid();
        }
    });

    var prototypeMax = deprecate('moment().max is deprecated, use moment.min instead. http://momentjs.com/guides/#/warnings/min-max/', function () {
        var other = createLocal.apply(null, arguments);
        if (this.isValid() && other.isValid()) {
            return other > this ? this : other;
        } else {
            return createInvalid();
        }
    });

    // Pick a moment m from moments so that m[fn](other) is true for all
    // other. This relies on the function fn to be transitive.
    //
    // moments should either be an array of moment objects or an array, whose
    // first element is an array of moment objects.
    function pickBy(fn, moments) {
        var res, i;
        if (moments.length === 1 && isArray(moments[0])) {
            moments = moments[0];
        }
        if (!moments.length) {
            return createLocal();
        }
        res = moments[0];
        for (i = 1; i < moments.length; ++i) {
            if (!moments[i].isValid() || moments[i][fn](res)) {
                res = moments[i];
            }
        }
        return res;
    }

    // TODO: Use [].sort instead?
    function min() {
        var args = [].slice.call(arguments, 0);

        return pickBy('isBefore', args);
    }

    function max() {
        var args = [].slice.call(arguments, 0);

        return pickBy('isAfter', args);
    }

    var now = function now() {
        return Date.now ? Date.now() : +new Date();
    };

    var ordering = ['year', 'quarter', 'month', 'week', 'day', 'hour', 'minute', 'second', 'millisecond'];

    function isDurationValid(m) {
        for (var key in m) {
            if (!(indexOf.call(ordering, key) !== -1 && (m[key] == null || !isNaN(m[key])))) {
                return false;
            }
        }

        var unitHasDecimal = false;
        for (var i = 0; i < ordering.length; ++i) {
            if (m[ordering[i]]) {
                if (unitHasDecimal) {
                    return false; // only allow non-integers for smallest unit
                }
                if (parseFloat(m[ordering[i]]) !== toInt(m[ordering[i]])) {
                    unitHasDecimal = true;
                }
            }
        }

        return true;
    }

    function isValid$1() {
        return this._isValid;
    }

    function createInvalid$1() {
        return createDuration(NaN);
    }

    function Duration(duration) {
        var normalizedInput = normalizeObjectUnits(duration),
            years = normalizedInput.year || 0,
            quarters = normalizedInput.quarter || 0,
            months = normalizedInput.month || 0,
            weeks = normalizedInput.week || 0,
            days = normalizedInput.day || 0,
            hours = normalizedInput.hour || 0,
            minutes = normalizedInput.minute || 0,
            seconds = normalizedInput.second || 0,
            milliseconds = normalizedInput.millisecond || 0;

        this._isValid = isDurationValid(normalizedInput);

        // representation for dateAddRemove
        this._milliseconds = +milliseconds + seconds * 1e3 + // 1000
        minutes * 6e4 + // 1000 * 60
        hours * 1000 * 60 * 60; //using 1000 * 60 * 60 instead of 36e5 to avoid floating point rounding errors https://github.com/moment/moment/issues/2978
        // Because of dateAddRemove treats 24 hours as different from a
        // day when working around DST, we need to store them separately
        this._days = +days + weeks * 7;
        // It is impossible to translate months into days without knowing
        // which months you are are talking about, so we have to store
        // it separately.
        this._months = +months + quarters * 3 + years * 12;

        this._data = {};

        this._locale = getLocale();

        this._bubble();
    }

    function isDuration(obj) {
        return obj instanceof Duration;
    }

    function absRound(number) {
        if (number < 0) {
            return Math.round(-1 * number) * -1;
        } else {
            return Math.round(number);
        }
    }

    // FORMATTING

    function offset(token, separator) {
        addFormatToken(token, 0, 0, function () {
            var offset = this.utcOffset();
            var sign = '+';
            if (offset < 0) {
                offset = -offset;
                sign = '-';
            }
            return sign + zeroFill(~~(offset / 60), 2) + separator + zeroFill(~~offset % 60, 2);
        });
    }

    offset('Z', ':');
    offset('ZZ', '');

    // PARSING

    addRegexToken('Z', matchShortOffset);
    addRegexToken('ZZ', matchShortOffset);
    addParseToken(['Z', 'ZZ'], function (input, array, config) {
        config._useUTC = true;
        config._tzm = offsetFromString(matchShortOffset, input);
    });

    // HELPERS

    // timezone chunker
    // '+10:00' > ['10',  '00']
    // '-1530'  > ['-15', '30']
    var chunkOffset = /([\+\-]|\d\d)/gi;

    function offsetFromString(matcher, string) {
        var matches = (string || '').match(matcher);

        if (matches === null) {
            return null;
        }

        var chunk = matches[matches.length - 1] || [];
        var parts = (chunk + '').match(chunkOffset) || ['-', 0, 0];
        var minutes = +(parts[1] * 60) + toInt(parts[2]);

        return minutes === 0 ? 0 : parts[0] === '+' ? minutes : -minutes;
    }

    // Return a moment from input, that is local/utc/zone equivalent to model.
    function cloneWithOffset(input, model) {
        var res, diff;
        if (model._isUTC) {
            res = model.clone();
            diff = (isMoment(input) || isDate(input) ? input.valueOf() : createLocal(input).valueOf()) - res.valueOf();
            // Use low-level api, because this fn is low-level api.
            res._d.setTime(res._d.valueOf() + diff);
            hooks.updateOffset(res, false);
            return res;
        } else {
            return createLocal(input).local();
        }
    }

    function getDateOffset(m) {
        // On Firefox.24 Date#getTimezoneOffset returns a floating point.
        // https://github.com/moment/moment/pull/1871
        return -Math.round(m._d.getTimezoneOffset() / 15) * 15;
    }

    // HOOKS

    // This function will be called whenever a moment is mutated.
    // It is intended to keep the offset in sync with the timezone.
    hooks.updateOffset = function () {};

    // MOMENTS

    // keepLocalTime = true means only change the timezone, without
    // affecting the local hour. So 5:31:26 +0300 --[utcOffset(2, true)]-->
    // 5:31:26 +0200 It is possible that 5:31:26 doesn't exist with offset
    // +0200, so we adjust the time as needed, to be valid.
    //
    // Keeping the time actually adds/subtracts (one hour)
    // from the actual represented time. That is why we call updateOffset
    // a second time. In case it wants us to change the offset again
    // _changeInProgress == true case, then we have to adjust, because
    // there is no such time in the given timezone.
    function getSetOffset(input, keepLocalTime, keepMinutes) {
        var offset = this._offset || 0,
            localAdjust;
        if (!this.isValid()) {
            return input != null ? this : NaN;
        }
        if (input != null) {
            if (typeof input === 'string') {
                input = offsetFromString(matchShortOffset, input);
                if (input === null) {
                    return this;
                }
            } else if (Math.abs(input) < 16 && !keepMinutes) {
                input = input * 60;
            }
            if (!this._isUTC && keepLocalTime) {
                localAdjust = getDateOffset(this);
            }
            this._offset = input;
            this._isUTC = true;
            if (localAdjust != null) {
                this.add(localAdjust, 'm');
            }
            if (offset !== input) {
                if (!keepLocalTime || this._changeInProgress) {
                    addSubtract(this, createDuration(input - offset, 'm'), 1, false);
                } else if (!this._changeInProgress) {
                    this._changeInProgress = true;
                    hooks.updateOffset(this, true);
                    this._changeInProgress = null;
                }
            }
            return this;
        } else {
            return this._isUTC ? offset : getDateOffset(this);
        }
    }

    function getSetZone(input, keepLocalTime) {
        if (input != null) {
            if (typeof input !== 'string') {
                input = -input;
            }

            this.utcOffset(input, keepLocalTime);

            return this;
        } else {
            return -this.utcOffset();
        }
    }

    function setOffsetToUTC(keepLocalTime) {
        return this.utcOffset(0, keepLocalTime);
    }

    function setOffsetToLocal(keepLocalTime) {
        if (this._isUTC) {
            this.utcOffset(0, keepLocalTime);
            this._isUTC = false;

            if (keepLocalTime) {
                this.subtract(getDateOffset(this), 'm');
            }
        }
        return this;
    }

    function setOffsetToParsedOffset() {
        if (this._tzm != null) {
            this.utcOffset(this._tzm, false, true);
        } else if (typeof this._i === 'string') {
            var tZone = offsetFromString(matchOffset, this._i);
            if (tZone != null) {
                this.utcOffset(tZone);
            } else {
                this.utcOffset(0, true);
            }
        }
        return this;
    }

    function hasAlignedHourOffset(input) {
        if (!this.isValid()) {
            return false;
        }
        input = input ? createLocal(input).utcOffset() : 0;

        return (this.utcOffset() - input) % 60 === 0;
    }

    function isDaylightSavingTime() {
        return this.utcOffset() > this.clone().month(0).utcOffset() || this.utcOffset() > this.clone().month(5).utcOffset();
    }

    function isDaylightSavingTimeShifted() {
        if (!isUndefined(this._isDSTShifted)) {
            return this._isDSTShifted;
        }

        var c = {};

        copyConfig(c, this);
        c = prepareConfig(c);

        if (c._a) {
            var other = c._isUTC ? createUTC(c._a) : createLocal(c._a);
            this._isDSTShifted = this.isValid() && compareArrays(c._a, other.toArray()) > 0;
        } else {
            this._isDSTShifted = false;
        }

        return this._isDSTShifted;
    }

    function isLocal() {
        return this.isValid() ? !this._isUTC : false;
    }

    function isUtcOffset() {
        return this.isValid() ? this._isUTC : false;
    }

    function isUtc() {
        return this.isValid() ? this._isUTC && this._offset === 0 : false;
    }

    // ASP.NET json date format regex
    var aspNetRegex = /^(\-|\+)?(?:(\d*)[. ])?(\d+)\:(\d+)(?:\:(\d+)(\.\d*)?)?$/;

    // from http://docs.closure-library.googlecode.com/git/closure_goog_date_date.js.source.html
    // somewhat more in line with 4.4.3.2 2004 spec, but allows decimal anywhere
    // and further modified to allow for strings containing both week and day
    var isoRegex = /^(-|\+)?P(?:([-+]?[0-9,.]*)Y)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)W)?(?:([-+]?[0-9,.]*)D)?(?:T(?:([-+]?[0-9,.]*)H)?(?:([-+]?[0-9,.]*)M)?(?:([-+]?[0-9,.]*)S)?)?$/;

    function createDuration(input, key) {
        var duration = input,

        // matching against regexp is expensive, do it on demand
        match = null,
            sign,
            ret,
            diffRes;

        if (isDuration(input)) {
            duration = {
                ms: input._milliseconds,
                d: input._days,
                M: input._months
            };
        } else if (isNumber(input)) {
            duration = {};
            if (key) {
                duration[key] = input;
            } else {
                duration.milliseconds = input;
            }
        } else if (!!(match = aspNetRegex.exec(input))) {
            sign = match[1] === '-' ? -1 : 1;
            duration = {
                y: 0,
                d: toInt(match[DATE]) * sign,
                h: toInt(match[HOUR]) * sign,
                m: toInt(match[MINUTE]) * sign,
                s: toInt(match[SECOND]) * sign,
                ms: toInt(absRound(match[MILLISECOND] * 1000)) * sign // the millisecond decimal point is included in the match
            };
        } else if (!!(match = isoRegex.exec(input))) {
            sign = match[1] === '-' ? -1 : match[1] === '+' ? 1 : 1;
            duration = {
                y: parseIso(match[2], sign),
                M: parseIso(match[3], sign),
                w: parseIso(match[4], sign),
                d: parseIso(match[5], sign),
                h: parseIso(match[6], sign),
                m: parseIso(match[7], sign),
                s: parseIso(match[8], sign)
            };
        } else if (duration == null) {
            // checks for null or undefined
            duration = {};
        } else if ((typeof duration === 'undefined' ? 'undefined' : _typeof(duration)) === 'object' && ('from' in duration || 'to' in duration)) {
            diffRes = momentsDifference(createLocal(duration.from), createLocal(duration.to));

            duration = {};
            duration.ms = diffRes.milliseconds;
            duration.M = diffRes.months;
        }

        ret = new Duration(duration);

        if (isDuration(input) && hasOwnProp(input, '_locale')) {
            ret._locale = input._locale;
        }

        return ret;
    }

    createDuration.fn = Duration.prototype;
    createDuration.invalid = createInvalid$1;

    function parseIso(inp, sign) {
        // We'd normally use ~~inp for this, but unfortunately it also
        // converts floats to ints.
        // inp may be undefined, so careful calling replace on it.
        var res = inp && parseFloat(inp.replace(',', '.'));
        // apply sign while we're at it
        return (isNaN(res) ? 0 : res) * sign;
    }

    function positiveMomentsDifference(base, other) {
        var res = { milliseconds: 0, months: 0 };

        res.months = other.month() - base.month() + (other.year() - base.year()) * 12;
        if (base.clone().add(res.months, 'M').isAfter(other)) {
            --res.months;
        }

        res.milliseconds = +other - +base.clone().add(res.months, 'M');

        return res;
    }

    function momentsDifference(base, other) {
        var res;
        if (!(base.isValid() && other.isValid())) {
            return { milliseconds: 0, months: 0 };
        }

        other = cloneWithOffset(other, base);
        if (base.isBefore(other)) {
            res = positiveMomentsDifference(base, other);
        } else {
            res = positiveMomentsDifference(other, base);
            res.milliseconds = -res.milliseconds;
            res.months = -res.months;
        }

        return res;
    }

    // TODO: remove 'name' arg after deprecation is removed
    function createAdder(direction, name) {
        return function (val, period) {
            var dur, tmp;
            //invert the arguments, but complain about it
            if (period !== null && !isNaN(+period)) {
                deprecateSimple(name, 'moment().' + name + '(period, number) is deprecated. Please use moment().' + name + '(number, period). ' + 'See http://momentjs.com/guides/#/warnings/add-inverted-param/ for more info.');
                tmp = val;val = period;period = tmp;
            }

            val = typeof val === 'string' ? +val : val;
            dur = createDuration(val, period);
            addSubtract(this, dur, direction);
            return this;
        };
    }

    function addSubtract(mom, duration, isAdding, updateOffset) {
        var milliseconds = duration._milliseconds,
            days = absRound(duration._days),
            months = absRound(duration._months);

        if (!mom.isValid()) {
            // No op
            return;
        }

        updateOffset = updateOffset == null ? true : updateOffset;

        if (months) {
            setMonth(mom, get(mom, 'Month') + months * isAdding);
        }
        if (days) {
            set$1(mom, 'Date', get(mom, 'Date') + days * isAdding);
        }
        if (milliseconds) {
            mom._d.setTime(mom._d.valueOf() + milliseconds * isAdding);
        }
        if (updateOffset) {
            hooks.updateOffset(mom, days || months);
        }
    }

    var add = createAdder(1, 'add');
    var subtract = createAdder(-1, 'subtract');

    function getCalendarFormat(myMoment, now) {
        var diff = myMoment.diff(now, 'days', true);
        return diff < -6 ? 'sameElse' : diff < -1 ? 'lastWeek' : diff < 0 ? 'lastDay' : diff < 1 ? 'sameDay' : diff < 2 ? 'nextDay' : diff < 7 ? 'nextWeek' : 'sameElse';
    }

    function calendar$1(time, formats) {
        // We want to compare the start of today, vs this.
        // Getting start-of-today depends on whether we're local/utc/offset or not.
        var now = time || createLocal(),
            sod = cloneWithOffset(now, this).startOf('day'),
            format = hooks.calendarFormat(this, sod) || 'sameElse';

        var output = formats && (isFunction(formats[format]) ? formats[format].call(this, now) : formats[format]);

        return this.format(output || this.localeData().calendar(format, this, createLocal(now)));
    }

    function clone() {
        return new Moment(this);
    }

    function isAfter(input, units) {
        var localInput = isMoment(input) ? input : createLocal(input);
        if (!(this.isValid() && localInput.isValid())) {
            return false;
        }
        units = normalizeUnits(!isUndefined(units) ? units : 'millisecond');
        if (units === 'millisecond') {
            return this.valueOf() > localInput.valueOf();
        } else {
            return localInput.valueOf() < this.clone().startOf(units).valueOf();
        }
    }

    function isBefore(input, units) {
        var localInput = isMoment(input) ? input : createLocal(input);
        if (!(this.isValid() && localInput.isValid())) {
            return false;
        }
        units = normalizeUnits(!isUndefined(units) ? units : 'millisecond');
        if (units === 'millisecond') {
            return this.valueOf() < localInput.valueOf();
        } else {
            return this.clone().endOf(units).valueOf() < localInput.valueOf();
        }
    }

    function isBetween(from, to, units, inclusivity) {
        inclusivity = inclusivity || '()';
        return (inclusivity[0] === '(' ? this.isAfter(from, units) : !this.isBefore(from, units)) && (inclusivity[1] === ')' ? this.isBefore(to, units) : !this.isAfter(to, units));
    }

    function isSame(input, units) {
        var localInput = isMoment(input) ? input : createLocal(input),
            inputMs;
        if (!(this.isValid() && localInput.isValid())) {
            return false;
        }
        units = normalizeUnits(units || 'millisecond');
        if (units === 'millisecond') {
            return this.valueOf() === localInput.valueOf();
        } else {
            inputMs = localInput.valueOf();
            return this.clone().startOf(units).valueOf() <= inputMs && inputMs <= this.clone().endOf(units).valueOf();
        }
    }

    function isSameOrAfter(input, units) {
        return this.isSame(input, units) || this.isAfter(input, units);
    }

    function isSameOrBefore(input, units) {
        return this.isSame(input, units) || this.isBefore(input, units);
    }

    function diff(input, units, asFloat) {
        var that, zoneDelta, delta, output;

        if (!this.isValid()) {
            return NaN;
        }

        that = cloneWithOffset(input, this);

        if (!that.isValid()) {
            return NaN;
        }

        zoneDelta = (that.utcOffset() - this.utcOffset()) * 6e4;

        units = normalizeUnits(units);

        switch (units) {
            case 'year':
                output = monthDiff(this, that) / 12;break;
            case 'month':
                output = monthDiff(this, that);break;
            case 'quarter':
                output = monthDiff(this, that) / 3;break;
            case 'second':
                output = (this - that) / 1e3;break; // 1000
            case 'minute':
                output = (this - that) / 6e4;break; // 1000 * 60
            case 'hour':
                output = (this - that) / 36e5;break; // 1000 * 60 * 60
            case 'day':
                output = (this - that - zoneDelta) / 864e5;break; // 1000 * 60 * 60 * 24, negate dst
            case 'week':
                output = (this - that - zoneDelta) / 6048e5;break; // 1000 * 60 * 60 * 24 * 7, negate dst
            default:
                output = this - that;
        }

        return asFloat ? output : absFloor(output);
    }

    function monthDiff(a, b) {
        // difference in months
        var wholeMonthDiff = (b.year() - a.year()) * 12 + (b.month() - a.month()),

        // b is in (anchor - 1 month, anchor + 1 month)
        anchor = a.clone().add(wholeMonthDiff, 'months'),
            anchor2,
            adjust;

        if (b - anchor < 0) {
            anchor2 = a.clone().add(wholeMonthDiff - 1, 'months');
            // linear across the month
            adjust = (b - anchor) / (anchor - anchor2);
        } else {
            anchor2 = a.clone().add(wholeMonthDiff + 1, 'months');
            // linear across the month
            adjust = (b - anchor) / (anchor2 - anchor);
        }

        //check for negative zero, return zero if negative zero
        return -(wholeMonthDiff + adjust) || 0;
    }

    hooks.defaultFormat = 'YYYY-MM-DDTHH:mm:ssZ';
    hooks.defaultFormatUtc = 'YYYY-MM-DDTHH:mm:ss[Z]';

    function toString() {
        return this.clone().locale('en').format('ddd MMM DD YYYY HH:mm:ss [GMT]ZZ');
    }

    function toISOString() {
        if (!this.isValid()) {
            return null;
        }
        var m = this.clone().utc();
        if (m.year() < 0 || m.year() > 9999) {
            return formatMoment(m, 'YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
        }
        if (isFunction(Date.prototype.toISOString)) {
            // native implementation is ~50x faster, use it when we can
            return this.toDate().toISOString();
        }
        return formatMoment(m, 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
    }

    /**
     * Return a human readable representation of a moment that can
     * also be evaluated to get a new moment which is the same
     *
     * @link https://nodejs.org/dist/latest/docs/api/util.html#util_custom_inspect_function_on_objects
     */
    function inspect() {
        if (!this.isValid()) {
            return 'moment.invalid(/* ' + this._i + ' */)';
        }
        var func = 'moment';
        var zone = '';
        if (!this.isLocal()) {
            func = this.utcOffset() === 0 ? 'moment.utc' : 'moment.parseZone';
            zone = 'Z';
        }
        var prefix = '[' + func + '("]';
        var year = 0 <= this.year() && this.year() <= 9999 ? 'YYYY' : 'YYYYYY';
        var datetime = '-MM-DD[T]HH:mm:ss.SSS';
        var suffix = zone + '[")]';

        return this.format(prefix + year + datetime + suffix);
    }

    function format(inputString) {
        if (!inputString) {
            inputString = this.isUtc() ? hooks.defaultFormatUtc : hooks.defaultFormat;
        }
        var output = formatMoment(this, inputString);
        return this.localeData().postformat(output);
    }

    function from(time, withoutSuffix) {
        if (this.isValid() && (isMoment(time) && time.isValid() || createLocal(time).isValid())) {
            return createDuration({ to: this, from: time }).locale(this.locale()).humanize(!withoutSuffix);
        } else {
            return this.localeData().invalidDate();
        }
    }

    function fromNow(withoutSuffix) {
        return this.from(createLocal(), withoutSuffix);
    }

    function to(time, withoutSuffix) {
        if (this.isValid() && (isMoment(time) && time.isValid() || createLocal(time).isValid())) {
            return createDuration({ from: this, to: time }).locale(this.locale()).humanize(!withoutSuffix);
        } else {
            return this.localeData().invalidDate();
        }
    }

    function toNow(withoutSuffix) {
        return this.to(createLocal(), withoutSuffix);
    }

    // If passed a locale key, it will set the locale for this
    // instance.  Otherwise, it will return the locale configuration
    // variables for this instance.
    function locale(key) {
        var newLocaleData;

        if (key === undefined) {
            return this._locale._abbr;
        } else {
            newLocaleData = getLocale(key);
            if (newLocaleData != null) {
                this._locale = newLocaleData;
            }
            return this;
        }
    }

    var lang = deprecate('moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.', function (key) {
        if (key === undefined) {
            return this.localeData();
        } else {
            return this.locale(key);
        }
    });

    function localeData() {
        return this._locale;
    }

    function startOf(units) {
        units = normalizeUnits(units);
        // the following switch intentionally omits break keywords
        // to utilize falling through the cases.
        switch (units) {
            case 'year':
                this.month(0);
            /* falls through */
            case 'quarter':
            case 'month':
                this.date(1);
            /* falls through */
            case 'week':
            case 'isoWeek':
            case 'day':
            case 'date':
                this.hours(0);
            /* falls through */
            case 'hour':
                this.minutes(0);
            /* falls through */
            case 'minute':
                this.seconds(0);
            /* falls through */
            case 'second':
                this.milliseconds(0);
        }

        // weeks are a special case
        if (units === 'week') {
            this.weekday(0);
        }
        if (units === 'isoWeek') {
            this.isoWeekday(1);
        }

        // quarters are also special
        if (units === 'quarter') {
            this.month(Math.floor(this.month() / 3) * 3);
        }

        return this;
    }

    function endOf(units) {
        units = normalizeUnits(units);
        if (units === undefined || units === 'millisecond') {
            return this;
        }

        // 'date' is an alias for 'day', so it should be considered as such.
        if (units === 'date') {
            units = 'day';
        }

        return this.startOf(units).add(1, units === 'isoWeek' ? 'week' : units).subtract(1, 'ms');
    }

    function valueOf() {
        return this._d.valueOf() - (this._offset || 0) * 60000;
    }

    function unix() {
        return Math.floor(this.valueOf() / 1000);
    }

    function toDate() {
        return new Date(this.valueOf());
    }

    function toArray() {
        var m = this;
        return [m.year(), m.month(), m.date(), m.hour(), m.minute(), m.second(), m.millisecond()];
    }

    function toObject() {
        var m = this;
        return {
            years: m.year(),
            months: m.month(),
            date: m.date(),
            hours: m.hours(),
            minutes: m.minutes(),
            seconds: m.seconds(),
            milliseconds: m.milliseconds()
        };
    }

    function toJSON() {
        // new Date(NaN).toJSON() === null
        return this.isValid() ? this.toISOString() : null;
    }

    function isValid$2() {
        return isValid(this);
    }

    function parsingFlags() {
        return extend({}, getParsingFlags(this));
    }

    function invalidAt() {
        return getParsingFlags(this).overflow;
    }

    function creationData() {
        return {
            input: this._i,
            format: this._f,
            locale: this._locale,
            isUTC: this._isUTC,
            strict: this._strict
        };
    }

    // FORMATTING

    addFormatToken(0, ['gg', 2], 0, function () {
        return this.weekYear() % 100;
    });

    addFormatToken(0, ['GG', 2], 0, function () {
        return this.isoWeekYear() % 100;
    });

    function addWeekYearFormatToken(token, getter) {
        addFormatToken(0, [token, token.length], 0, getter);
    }

    addWeekYearFormatToken('gggg', 'weekYear');
    addWeekYearFormatToken('ggggg', 'weekYear');
    addWeekYearFormatToken('GGGG', 'isoWeekYear');
    addWeekYearFormatToken('GGGGG', 'isoWeekYear');

    // ALIASES

    addUnitAlias('weekYear', 'gg');
    addUnitAlias('isoWeekYear', 'GG');

    // PRIORITY

    addUnitPriority('weekYear', 1);
    addUnitPriority('isoWeekYear', 1);

    // PARSING

    addRegexToken('G', matchSigned);
    addRegexToken('g', matchSigned);
    addRegexToken('GG', match1to2, match2);
    addRegexToken('gg', match1to2, match2);
    addRegexToken('GGGG', match1to4, match4);
    addRegexToken('gggg', match1to4, match4);
    addRegexToken('GGGGG', match1to6, match6);
    addRegexToken('ggggg', match1to6, match6);

    addWeekParseToken(['gggg', 'ggggg', 'GGGG', 'GGGGG'], function (input, week, config, token) {
        week[token.substr(0, 2)] = toInt(input);
    });

    addWeekParseToken(['gg', 'GG'], function (input, week, config, token) {
        week[token] = hooks.parseTwoDigitYear(input);
    });

    // MOMENTS

    function getSetWeekYear(input) {
        return getSetWeekYearHelper.call(this, input, this.week(), this.weekday(), this.localeData()._week.dow, this.localeData()._week.doy);
    }

    function getSetISOWeekYear(input) {
        return getSetWeekYearHelper.call(this, input, this.isoWeek(), this.isoWeekday(), 1, 4);
    }

    function getISOWeeksInYear() {
        return weeksInYear(this.year(), 1, 4);
    }

    function getWeeksInYear() {
        var weekInfo = this.localeData()._week;
        return weeksInYear(this.year(), weekInfo.dow, weekInfo.doy);
    }

    function getSetWeekYearHelper(input, week, weekday, dow, doy) {
        var weeksTarget;
        if (input == null) {
            return weekOfYear(this, dow, doy).year;
        } else {
            weeksTarget = weeksInYear(input, dow, doy);
            if (week > weeksTarget) {
                week = weeksTarget;
            }
            return setWeekAll.call(this, input, week, weekday, dow, doy);
        }
    }

    function setWeekAll(weekYear, week, weekday, dow, doy) {
        var dayOfYearData = dayOfYearFromWeeks(weekYear, week, weekday, dow, doy),
            date = createUTCDate(dayOfYearData.year, 0, dayOfYearData.dayOfYear);

        this.year(date.getUTCFullYear());
        this.month(date.getUTCMonth());
        this.date(date.getUTCDate());
        return this;
    }

    // FORMATTING

    addFormatToken('Q', 0, 'Qo', 'quarter');

    // ALIASES

    addUnitAlias('quarter', 'Q');

    // PRIORITY

    addUnitPriority('quarter', 7);

    // PARSING

    addRegexToken('Q', match1);
    addParseToken('Q', function (input, array) {
        array[MONTH] = (toInt(input) - 1) * 3;
    });

    // MOMENTS

    function getSetQuarter(input) {
        return input == null ? Math.ceil((this.month() + 1) / 3) : this.month((input - 1) * 3 + this.month() % 3);
    }

    // FORMATTING

    addFormatToken('D', ['DD', 2], 'Do', 'date');

    // ALIASES

    addUnitAlias('date', 'D');

    // PRIOROITY
    addUnitPriority('date', 9);

    // PARSING

    addRegexToken('D', match1to2);
    addRegexToken('DD', match1to2, match2);
    addRegexToken('Do', function (isStrict, locale) {
        // TODO: Remove "ordinalParse" fallback in next major release.
        return isStrict ? locale._dayOfMonthOrdinalParse || locale._ordinalParse : locale._dayOfMonthOrdinalParseLenient;
    });

    addParseToken(['D', 'DD'], DATE);
    addParseToken('Do', function (input, array) {
        array[DATE] = toInt(input.match(match1to2)[0], 10);
    });

    // MOMENTS

    var getSetDayOfMonth = makeGetSet('Date', true);

    // FORMATTING

    addFormatToken('DDD', ['DDDD', 3], 'DDDo', 'dayOfYear');

    // ALIASES

    addUnitAlias('dayOfYear', 'DDD');

    // PRIORITY
    addUnitPriority('dayOfYear', 4);

    // PARSING

    addRegexToken('DDD', match1to3);
    addRegexToken('DDDD', match3);
    addParseToken(['DDD', 'DDDD'], function (input, array, config) {
        config._dayOfYear = toInt(input);
    });

    // HELPERS

    // MOMENTS

    function getSetDayOfYear(input) {
        var dayOfYear = Math.round((this.clone().startOf('day') - this.clone().startOf('year')) / 864e5) + 1;
        return input == null ? dayOfYear : this.add(input - dayOfYear, 'd');
    }

    // FORMATTING

    addFormatToken('m', ['mm', 2], 0, 'minute');

    // ALIASES

    addUnitAlias('minute', 'm');

    // PRIORITY

    addUnitPriority('minute', 14);

    // PARSING

    addRegexToken('m', match1to2);
    addRegexToken('mm', match1to2, match2);
    addParseToken(['m', 'mm'], MINUTE);

    // MOMENTS

    var getSetMinute = makeGetSet('Minutes', false);

    // FORMATTING

    addFormatToken('s', ['ss', 2], 0, 'second');

    // ALIASES

    addUnitAlias('second', 's');

    // PRIORITY

    addUnitPriority('second', 15);

    // PARSING

    addRegexToken('s', match1to2);
    addRegexToken('ss', match1to2, match2);
    addParseToken(['s', 'ss'], SECOND);

    // MOMENTS

    var getSetSecond = makeGetSet('Seconds', false);

    // FORMATTING

    addFormatToken('S', 0, 0, function () {
        return ~~(this.millisecond() / 100);
    });

    addFormatToken(0, ['SS', 2], 0, function () {
        return ~~(this.millisecond() / 10);
    });

    addFormatToken(0, ['SSS', 3], 0, 'millisecond');
    addFormatToken(0, ['SSSS', 4], 0, function () {
        return this.millisecond() * 10;
    });
    addFormatToken(0, ['SSSSS', 5], 0, function () {
        return this.millisecond() * 100;
    });
    addFormatToken(0, ['SSSSSS', 6], 0, function () {
        return this.millisecond() * 1000;
    });
    addFormatToken(0, ['SSSSSSS', 7], 0, function () {
        return this.millisecond() * 10000;
    });
    addFormatToken(0, ['SSSSSSSS', 8], 0, function () {
        return this.millisecond() * 100000;
    });
    addFormatToken(0, ['SSSSSSSSS', 9], 0, function () {
        return this.millisecond() * 1000000;
    });

    // ALIASES

    addUnitAlias('millisecond', 'ms');

    // PRIORITY

    addUnitPriority('millisecond', 16);

    // PARSING

    addRegexToken('S', match1to3, match1);
    addRegexToken('SS', match1to3, match2);
    addRegexToken('SSS', match1to3, match3);

    var token;
    for (token = 'SSSS'; token.length <= 9; token += 'S') {
        addRegexToken(token, matchUnsigned);
    }

    function parseMs(input, array) {
        array[MILLISECOND] = toInt(('0.' + input) * 1000);
    }

    for (token = 'S'; token.length <= 9; token += 'S') {
        addParseToken(token, parseMs);
    }
    // MOMENTS

    var getSetMillisecond = makeGetSet('Milliseconds', false);

    // FORMATTING

    addFormatToken('z', 0, 0, 'zoneAbbr');
    addFormatToken('zz', 0, 0, 'zoneName');

    // MOMENTS

    function getZoneAbbr() {
        return this._isUTC ? 'UTC' : '';
    }

    function getZoneName() {
        return this._isUTC ? 'Coordinated Universal Time' : '';
    }

    var proto = Moment.prototype;

    proto.add = add;
    proto.calendar = calendar$1;
    proto.clone = clone;
    proto.diff = diff;
    proto.endOf = endOf;
    proto.format = format;
    proto.from = from;
    proto.fromNow = fromNow;
    proto.to = to;
    proto.toNow = toNow;
    proto.get = stringGet;
    proto.invalidAt = invalidAt;
    proto.isAfter = isAfter;
    proto.isBefore = isBefore;
    proto.isBetween = isBetween;
    proto.isSame = isSame;
    proto.isSameOrAfter = isSameOrAfter;
    proto.isSameOrBefore = isSameOrBefore;
    proto.isValid = isValid$2;
    proto.lang = lang;
    proto.locale = locale;
    proto.localeData = localeData;
    proto.max = prototypeMax;
    proto.min = prototypeMin;
    proto.parsingFlags = parsingFlags;
    proto.set = stringSet;
    proto.startOf = startOf;
    proto.subtract = subtract;
    proto.toArray = toArray;
    proto.toObject = toObject;
    proto.toDate = toDate;
    proto.toISOString = toISOString;
    proto.inspect = inspect;
    proto.toJSON = toJSON;
    proto.toString = toString;
    proto.unix = unix;
    proto.valueOf = valueOf;
    proto.creationData = creationData;

    // Year
    proto.year = getSetYear;
    proto.isLeapYear = getIsLeapYear;

    // Week Year
    proto.weekYear = getSetWeekYear;
    proto.isoWeekYear = getSetISOWeekYear;

    // Quarter
    proto.quarter = proto.quarters = getSetQuarter;

    // Month
    proto.month = getSetMonth;
    proto.daysInMonth = getDaysInMonth;

    // Week
    proto.week = proto.weeks = getSetWeek;
    proto.isoWeek = proto.isoWeeks = getSetISOWeek;
    proto.weeksInYear = getWeeksInYear;
    proto.isoWeeksInYear = getISOWeeksInYear;

    // Day
    proto.date = getSetDayOfMonth;
    proto.day = proto.days = getSetDayOfWeek;
    proto.weekday = getSetLocaleDayOfWeek;
    proto.isoWeekday = getSetISODayOfWeek;
    proto.dayOfYear = getSetDayOfYear;

    // Hour
    proto.hour = proto.hours = getSetHour;

    // Minute
    proto.minute = proto.minutes = getSetMinute;

    // Second
    proto.second = proto.seconds = getSetSecond;

    // Millisecond
    proto.millisecond = proto.milliseconds = getSetMillisecond;

    // Offset
    proto.utcOffset = getSetOffset;
    proto.utc = setOffsetToUTC;
    proto.local = setOffsetToLocal;
    proto.parseZone = setOffsetToParsedOffset;
    proto.hasAlignedHourOffset = hasAlignedHourOffset;
    proto.isDST = isDaylightSavingTime;
    proto.isLocal = isLocal;
    proto.isUtcOffset = isUtcOffset;
    proto.isUtc = isUtc;
    proto.isUTC = isUtc;

    // Timezone
    proto.zoneAbbr = getZoneAbbr;
    proto.zoneName = getZoneName;

    // Deprecations
    proto.dates = deprecate('dates accessor is deprecated. Use date instead.', getSetDayOfMonth);
    proto.months = deprecate('months accessor is deprecated. Use month instead', getSetMonth);
    proto.years = deprecate('years accessor is deprecated. Use year instead', getSetYear);
    proto.zone = deprecate('moment().zone is deprecated, use moment().utcOffset instead. http://momentjs.com/guides/#/warnings/zone/', getSetZone);
    proto.isDSTShifted = deprecate('isDSTShifted is deprecated. See http://momentjs.com/guides/#/warnings/dst-shifted/ for more information', isDaylightSavingTimeShifted);

    function createUnix(input) {
        return createLocal(input * 1000);
    }

    function createInZone() {
        return createLocal.apply(null, arguments).parseZone();
    }

    function preParsePostFormat(string) {
        return string;
    }

    var proto$1 = Locale.prototype;

    proto$1.calendar = calendar;
    proto$1.longDateFormat = longDateFormat;
    proto$1.invalidDate = invalidDate;
    proto$1.ordinal = ordinal;
    proto$1.preparse = preParsePostFormat;
    proto$1.postformat = preParsePostFormat;
    proto$1.relativeTime = relativeTime;
    proto$1.pastFuture = pastFuture;
    proto$1.set = set;

    // Month
    proto$1.months = localeMonths;
    proto$1.monthsShort = localeMonthsShort;
    proto$1.monthsParse = localeMonthsParse;
    proto$1.monthsRegex = monthsRegex;
    proto$1.monthsShortRegex = monthsShortRegex;

    // Week
    proto$1.week = localeWeek;
    proto$1.firstDayOfYear = localeFirstDayOfYear;
    proto$1.firstDayOfWeek = localeFirstDayOfWeek;

    // Day of Week
    proto$1.weekdays = localeWeekdays;
    proto$1.weekdaysMin = localeWeekdaysMin;
    proto$1.weekdaysShort = localeWeekdaysShort;
    proto$1.weekdaysParse = localeWeekdaysParse;

    proto$1.weekdaysRegex = weekdaysRegex;
    proto$1.weekdaysShortRegex = weekdaysShortRegex;
    proto$1.weekdaysMinRegex = weekdaysMinRegex;

    // Hours
    proto$1.isPM = localeIsPM;
    proto$1.meridiem = localeMeridiem;

    function get$1(format, index, field, setter) {
        var locale = getLocale();
        var utc = createUTC().set(setter, index);
        return locale[field](utc, format);
    }

    function listMonthsImpl(format, index, field) {
        if (isNumber(format)) {
            index = format;
            format = undefined;
        }

        format = format || '';

        if (index != null) {
            return get$1(format, index, field, 'month');
        }

        var i;
        var out = [];
        for (i = 0; i < 12; i++) {
            out[i] = get$1(format, i, field, 'month');
        }
        return out;
    }

    // ()
    // (5)
    // (fmt, 5)
    // (fmt)
    // (true)
    // (true, 5)
    // (true, fmt, 5)
    // (true, fmt)
    function listWeekdaysImpl(localeSorted, format, index, field) {
        if (typeof localeSorted === 'boolean') {
            if (isNumber(format)) {
                index = format;
                format = undefined;
            }

            format = format || '';
        } else {
            format = localeSorted;
            index = format;
            localeSorted = false;

            if (isNumber(format)) {
                index = format;
                format = undefined;
            }

            format = format || '';
        }

        var locale = getLocale(),
            shift = localeSorted ? locale._week.dow : 0;

        if (index != null) {
            return get$1(format, (index + shift) % 7, field, 'day');
        }

        var i;
        var out = [];
        for (i = 0; i < 7; i++) {
            out[i] = get$1(format, (i + shift) % 7, field, 'day');
        }
        return out;
    }

    function listMonths(format, index) {
        return listMonthsImpl(format, index, 'months');
    }

    function listMonthsShort(format, index) {
        return listMonthsImpl(format, index, 'monthsShort');
    }

    function listWeekdays(localeSorted, format, index) {
        return listWeekdaysImpl(localeSorted, format, index, 'weekdays');
    }

    function listWeekdaysShort(localeSorted, format, index) {
        return listWeekdaysImpl(localeSorted, format, index, 'weekdaysShort');
    }

    function listWeekdaysMin(localeSorted, format, index) {
        return listWeekdaysImpl(localeSorted, format, index, 'weekdaysMin');
    }

    getSetGlobalLocale('en', {
        dayOfMonthOrdinalParse: /\d{1,2}(th|st|nd|rd)/,
        ordinal: function ordinal(number) {
            var b = number % 10,
                output = toInt(number % 100 / 10) === 1 ? 'th' : b === 1 ? 'st' : b === 2 ? 'nd' : b === 3 ? 'rd' : 'th';
            return number + output;
        }
    });

    // Side effect imports
    hooks.lang = deprecate('moment.lang is deprecated. Use moment.locale instead.', getSetGlobalLocale);
    hooks.langData = deprecate('moment.langData is deprecated. Use moment.localeData instead.', getLocale);

    var mathAbs = Math.abs;

    function abs() {
        var data = this._data;

        this._milliseconds = mathAbs(this._milliseconds);
        this._days = mathAbs(this._days);
        this._months = mathAbs(this._months);

        data.milliseconds = mathAbs(data.milliseconds);
        data.seconds = mathAbs(data.seconds);
        data.minutes = mathAbs(data.minutes);
        data.hours = mathAbs(data.hours);
        data.months = mathAbs(data.months);
        data.years = mathAbs(data.years);

        return this;
    }

    function addSubtract$1(duration, input, value, direction) {
        var other = createDuration(input, value);

        duration._milliseconds += direction * other._milliseconds;
        duration._days += direction * other._days;
        duration._months += direction * other._months;

        return duration._bubble();
    }

    // supports only 2.0-style add(1, 's') or add(duration)
    function add$1(input, value) {
        return addSubtract$1(this, input, value, 1);
    }

    // supports only 2.0-style subtract(1, 's') or subtract(duration)
    function subtract$1(input, value) {
        return addSubtract$1(this, input, value, -1);
    }

    function absCeil(number) {
        if (number < 0) {
            return Math.floor(number);
        } else {
            return Math.ceil(number);
        }
    }

    function bubble() {
        var milliseconds = this._milliseconds;
        var days = this._days;
        var months = this._months;
        var data = this._data;
        var seconds, minutes, hours, years, monthsFromDays;

        // if we have a mix of positive and negative values, bubble down first
        // check: https://github.com/moment/moment/issues/2166
        if (!(milliseconds >= 0 && days >= 0 && months >= 0 || milliseconds <= 0 && days <= 0 && months <= 0)) {
            milliseconds += absCeil(monthsToDays(months) + days) * 864e5;
            days = 0;
            months = 0;
        }

        // The following code bubbles up values, see the tests for
        // examples of what that means.
        data.milliseconds = milliseconds % 1000;

        seconds = absFloor(milliseconds / 1000);
        data.seconds = seconds % 60;

        minutes = absFloor(seconds / 60);
        data.minutes = minutes % 60;

        hours = absFloor(minutes / 60);
        data.hours = hours % 24;

        days += absFloor(hours / 24);

        // convert days to months
        monthsFromDays = absFloor(daysToMonths(days));
        months += monthsFromDays;
        days -= absCeil(monthsToDays(monthsFromDays));

        // 12 months -> 1 year
        years = absFloor(months / 12);
        months %= 12;

        data.days = days;
        data.months = months;
        data.years = years;

        return this;
    }

    function daysToMonths(days) {
        // 400 years have 146097 days (taking into account leap year rules)
        // 400 years have 12 months === 4800
        return days * 4800 / 146097;
    }

    function monthsToDays(months) {
        // the reverse of daysToMonths
        return months * 146097 / 4800;
    }

    function as(units) {
        if (!this.isValid()) {
            return NaN;
        }
        var days;
        var months;
        var milliseconds = this._milliseconds;

        units = normalizeUnits(units);

        if (units === 'month' || units === 'year') {
            days = this._days + milliseconds / 864e5;
            months = this._months + daysToMonths(days);
            return units === 'month' ? months : months / 12;
        } else {
            // handle milliseconds separately because of floating point math errors (issue #1867)
            days = this._days + Math.round(monthsToDays(this._months));
            switch (units) {
                case 'week':
                    return days / 7 + milliseconds / 6048e5;
                case 'day':
                    return days + milliseconds / 864e5;
                case 'hour':
                    return days * 24 + milliseconds / 36e5;
                case 'minute':
                    return days * 1440 + milliseconds / 6e4;
                case 'second':
                    return days * 86400 + milliseconds / 1000;
                // Math.floor prevents floating point math errors here
                case 'millisecond':
                    return Math.floor(days * 864e5) + milliseconds;
                default:
                    throw new Error('Unknown unit ' + units);
            }
        }
    }

    // TODO: Use this.as('ms')?
    function valueOf$1() {
        if (!this.isValid()) {
            return NaN;
        }
        return this._milliseconds + this._days * 864e5 + this._months % 12 * 2592e6 + toInt(this._months / 12) * 31536e6;
    }

    function makeAs(alias) {
        return function () {
            return this.as(alias);
        };
    }

    var asMilliseconds = makeAs('ms');
    var asSeconds = makeAs('s');
    var asMinutes = makeAs('m');
    var asHours = makeAs('h');
    var asDays = makeAs('d');
    var asWeeks = makeAs('w');
    var asMonths = makeAs('M');
    var asYears = makeAs('y');

    function clone$1() {
        return createDuration(this);
    }

    function get$2(units) {
        units = normalizeUnits(units);
        return this.isValid() ? this[units + 's']() : NaN;
    }

    function makeGetter(name) {
        return function () {
            return this.isValid() ? this._data[name] : NaN;
        };
    }

    var milliseconds = makeGetter('milliseconds');
    var seconds = makeGetter('seconds');
    var minutes = makeGetter('minutes');
    var hours = makeGetter('hours');
    var days = makeGetter('days');
    var months = makeGetter('months');
    var years = makeGetter('years');

    function weeks() {
        return absFloor(this.days() / 7);
    }

    var round = Math.round;
    var thresholds = {
        ss: 44, // a few seconds to seconds
        s: 45, // seconds to minute
        m: 45, // minutes to hour
        h: 22, // hours to day
        d: 26, // days to month
        M: 11 // months to year
    };

    // helper function for moment.fn.from, moment.fn.fromNow, and moment.duration.fn.humanize
    function substituteTimeAgo(string, number, withoutSuffix, isFuture, locale) {
        return locale.relativeTime(number || 1, !!withoutSuffix, string, isFuture);
    }

    function relativeTime$1(posNegDuration, withoutSuffix, locale) {
        var duration = createDuration(posNegDuration).abs();
        var seconds = round(duration.as('s'));
        var minutes = round(duration.as('m'));
        var hours = round(duration.as('h'));
        var days = round(duration.as('d'));
        var months = round(duration.as('M'));
        var years = round(duration.as('y'));

        var a = seconds <= thresholds.ss && ['s', seconds] || seconds < thresholds.s && ['ss', seconds] || minutes <= 1 && ['m'] || minutes < thresholds.m && ['mm', minutes] || hours <= 1 && ['h'] || hours < thresholds.h && ['hh', hours] || days <= 1 && ['d'] || days < thresholds.d && ['dd', days] || months <= 1 && ['M'] || months < thresholds.M && ['MM', months] || years <= 1 && ['y'] || ['yy', years];

        a[2] = withoutSuffix;
        a[3] = +posNegDuration > 0;
        a[4] = locale;
        return substituteTimeAgo.apply(null, a);
    }

    // This function allows you to set the rounding function for relative time strings
    function getSetRelativeTimeRounding(roundingFunction) {
        if (roundingFunction === undefined) {
            return round;
        }
        if (typeof roundingFunction === 'function') {
            round = roundingFunction;
            return true;
        }
        return false;
    }

    // This function allows you to set a threshold for relative time strings
    function getSetRelativeTimeThreshold(threshold, limit) {
        if (thresholds[threshold] === undefined) {
            return false;
        }
        if (limit === undefined) {
            return thresholds[threshold];
        }
        thresholds[threshold] = limit;
        if (threshold === 's') {
            thresholds.ss = limit - 1;
        }
        return true;
    }

    function humanize(withSuffix) {
        if (!this.isValid()) {
            return this.localeData().invalidDate();
        }

        var locale = this.localeData();
        var output = relativeTime$1(this, !withSuffix, locale);

        if (withSuffix) {
            output = locale.pastFuture(+this, output);
        }

        return locale.postformat(output);
    }

    var abs$1 = Math.abs;

    function sign(x) {
        return (x > 0) - (x < 0) || +x;
    }

    function toISOString$1() {
        // for ISO strings we do not use the normal bubbling rules:
        //  * milliseconds bubble up until they become hours
        //  * days do not bubble at all
        //  * months bubble up until they become years
        // This is because there is no context-free conversion between hours and days
        // (think of clock changes)
        // and also not between days and months (28-31 days per month)
        if (!this.isValid()) {
            return this.localeData().invalidDate();
        }

        var seconds = abs$1(this._milliseconds) / 1000;
        var days = abs$1(this._days);
        var months = abs$1(this._months);
        var minutes, hours, years;

        // 3600 seconds -> 60 minutes -> 1 hour
        minutes = absFloor(seconds / 60);
        hours = absFloor(minutes / 60);
        seconds %= 60;
        minutes %= 60;

        // 12 months -> 1 year
        years = absFloor(months / 12);
        months %= 12;

        // inspired by https://github.com/dordille/moment-isoduration/blob/master/moment.isoduration.js
        var Y = years;
        var M = months;
        var D = days;
        var h = hours;
        var m = minutes;
        var s = seconds ? seconds.toFixed(3).replace(/\.?0+$/, '') : '';
        var total = this.asSeconds();

        if (!total) {
            // this is the same as C#'s (Noda) and python (isodate)...
            // but not other JS (goog.date)
            return 'P0D';
        }

        var totalSign = total < 0 ? '-' : '';
        var ymSign = sign(this._months) !== sign(total) ? '-' : '';
        var daysSign = sign(this._days) !== sign(total) ? '-' : '';
        var hmsSign = sign(this._milliseconds) !== sign(total) ? '-' : '';

        return totalSign + 'P' + (Y ? ymSign + Y + 'Y' : '') + (M ? ymSign + M + 'M' : '') + (D ? daysSign + D + 'D' : '') + (h || m || s ? 'T' : '') + (h ? hmsSign + h + 'H' : '') + (m ? hmsSign + m + 'M' : '') + (s ? hmsSign + s + 'S' : '');
    }

    var proto$2 = Duration.prototype;

    proto$2.isValid = isValid$1;
    proto$2.abs = abs;
    proto$2.add = add$1;
    proto$2.subtract = subtract$1;
    proto$2.as = as;
    proto$2.asMilliseconds = asMilliseconds;
    proto$2.asSeconds = asSeconds;
    proto$2.asMinutes = asMinutes;
    proto$2.asHours = asHours;
    proto$2.asDays = asDays;
    proto$2.asWeeks = asWeeks;
    proto$2.asMonths = asMonths;
    proto$2.asYears = asYears;
    proto$2.valueOf = valueOf$1;
    proto$2._bubble = bubble;
    proto$2.clone = clone$1;
    proto$2.get = get$2;
    proto$2.milliseconds = milliseconds;
    proto$2.seconds = seconds;
    proto$2.minutes = minutes;
    proto$2.hours = hours;
    proto$2.days = days;
    proto$2.weeks = weeks;
    proto$2.months = months;
    proto$2.years = years;
    proto$2.humanize = humanize;
    proto$2.toISOString = toISOString$1;
    proto$2.toString = toISOString$1;
    proto$2.toJSON = toISOString$1;
    proto$2.locale = locale;
    proto$2.localeData = localeData;

    // Deprecations
    proto$2.toIsoString = deprecate('toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)', toISOString$1);
    proto$2.lang = lang;

    // Side effect imports

    // FORMATTING

    addFormatToken('X', 0, 0, 'unix');
    addFormatToken('x', 0, 0, 'valueOf');

    // PARSING

    addRegexToken('x', matchSigned);
    addRegexToken('X', matchTimestamp);
    addParseToken('X', function (input, array, config) {
        config._d = new Date(parseFloat(input, 10) * 1000);
    });
    addParseToken('x', function (input, array, config) {
        config._d = new Date(toInt(input));
    });

    // Side effect imports


    hooks.version = '2.19.1';

    setHookCallback(createLocal);

    hooks.fn = proto;
    hooks.min = min;
    hooks.max = max;
    hooks.now = now;
    hooks.utc = createUTC;
    hooks.unix = createUnix;
    hooks.months = listMonths;
    hooks.isDate = isDate;
    hooks.locale = getSetGlobalLocale;
    hooks.invalid = createInvalid;
    hooks.duration = createDuration;
    hooks.isMoment = isMoment;
    hooks.weekdays = listWeekdays;
    hooks.parseZone = createInZone;
    hooks.localeData = getLocale;
    hooks.isDuration = isDuration;
    hooks.monthsShort = listMonthsShort;
    hooks.weekdaysMin = listWeekdaysMin;
    hooks.defineLocale = defineLocale;
    hooks.updateLocale = updateLocale;
    hooks.locales = listLocales;
    hooks.weekdaysShort = listWeekdaysShort;
    hooks.normalizeUnits = normalizeUnits;
    hooks.relativeTimeRounding = getSetRelativeTimeRounding;
    hooks.relativeTimeThreshold = getSetRelativeTimeThreshold;
    hooks.calendarFormat = getCalendarFormat;
    hooks.prototype = proto;

    return hooks;
});
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
  return typeof obj;
} : function (obj) {
  return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
};

function _toConsumableArray(arr) {
  if (Array.isArray(arr)) {
    for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
      arr2[i] = arr[i];
    }return arr2;
  } else {
    return Array.from(arr);
  }
}

var vueMoment = {
  install: function install(Vue, options) {
    var moment$$1 = options && options.moment ? options.moment : moment;

    Object.defineProperties(Vue.prototype, {
      $moment: {
        get: function get() {
          return moment$$1;
        }
      }
    });

    Vue.moment = moment$$1;

    Vue.filter('moment', function () {
      var arguments$1 = arguments;

      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments$1[_key];
      }

      args = Array.prototype.slice.call(args);
      var input = args.shift();
      var date = void 0;

      if (Array.isArray(input) && typeof input[0] === 'string') {
        // If input is array, assume we're being passed a format pattern to parse against.
        // Format pattern will accept an array of potential formats to parse against.
        // Date string should be at [0], format pattern(s) should be at [1]
        date = moment$$1(input[0], input[1], true);
      } else if (typeof input === 'number') {
        if (input.toString().length < 12) {
          // If input is an integer with fewer than 12 digits, assume Unix seconds...
          date = moment$$1.unix(input);
        } else {
          // ..otherwise, assume milliseconds.
          date = moment$$1(input);
        }
      } else {
        // Otherwise, throw the input at moment and see what happens...
        date = moment$$1(input);
      }

      if (!input || !date.isValid()) {
        // Log a warning if moment couldn't reconcile the input. Better than throwing an error?
        console.warn('Could not build a valid `moment` object from input.');
        return input;
      }

      function parse() {
        var arguments$1 = arguments;

        for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
          args[_key2] = arguments$1[_key2];
        }

        args = Array.prototype.slice.call(args);
        var method = args.shift();

        switch (method) {
          case 'add':
            {
              /*
              * Mutates the original moment by adding time.
              * http://momentjs.com/docs/#/manipulating/add/
              */

              var addends = args.shift().split(',').map(Function.prototype.call, String.prototype.trim);
              var obj = {};

              for (var n = 0; n < addends.length; n++) {
                var addend = addends[n].split(' ');
                obj[addend[1]] = addend[0];
              }
              date.add(obj);
              break;
            }

          case 'subtract':
            {
              /*
              * Mutates the original moment by subtracting time.
              * http://momentjs.com/docs/#/manipulating/subtract/
              */

              var subtrahends = args.shift().split(',').map(Function.prototype.call, String.prototype.trim);
              var _obj = {};

              for (var _n = 0; _n < subtrahends.length; _n++) {
                var subtrahend = subtrahends[_n].split(' ');
                _obj[subtrahend[1]] = subtrahend[0];
              }
              date.subtract(_obj);
              break;
            }

          case 'from':
            {
              /*
              * Display a moment in relative time, either from now or from a specified date.
              * http://momentjs.com/docs/#/displaying/fromnow/
              */

              var from = 'now';
              var removeSuffix = false;

              if (args[0] === 'now') { args.shift(); }
              // If valid, assume it is a date we want the output computed against.
              if (moment$$1(args[0]).isValid()) { from = moment$$1(args.shift()); }

              if (args[0] === true) {
                args.shift();
                removeSuffix = true;
              }

              if (from !== 'now') {
                date = date.from(from, removeSuffix);
              } else {
                date = date.fromNow(removeSuffix);
              }
              break;
            }

          case 'diff':
            {
              /*
              * Mutates the original moment by doing a difference with another date.
              * http://momentjs.com/docs/#/displaying/difference/
              */

              var referenceTime = moment$$1();
              var units = '';
              var float = false;

              if (moment$$1(args[0]).isValid()) {
                // If valid, assume it is a date we want the output computed against.
                referenceTime = moment$$1(args.shift());
              } else if (args[0] === null || args[0] === 'now') {
                // If null or 'now', remove argument and proceed with default referenceTime.
                args.shift();
              }

              if (args[0]) { units = args.shift(); }

              if (args[0] === true) { float = args.shift(); }

              date = date.diff(referenceTime, units, float);
              break;
            }

          case 'calendar':
            {
              /*
              * Formats a date with different strings depending on how close
              * to a certain date (today by default) the date is.
              * http://momentjs.com/docs/#/displaying/calendar-time/
              */

              var _referenceTime = moment$$1();
              var formats = {};

              if (moment$$1(args[0]).isValid()) {
                // If valid, assume it is a date we want the output computed against.
                _referenceTime = moment$$1(args.shift());
              } else if (args[0] === null || args[0] === 'now') {
                // If null or 'now', remove argument and proceed with default referenceTime.
                args.shift();
              }

              if (_typeof(args[0]) === 'object') { formats = args.shift(); }

              date = date.calendar(_referenceTime, formats);
              break;
            }

          case 'utc':
            {
              /*
              * Mutates the original moment by converting to UTC
              * https://momentjs.com/docs/#/manipulating/utc/
              */
              date.utc();
              break;
            }

          case 'timezone':
            {
              /*
              * Mutates the original moment by converting to a new timezone.
              * https://momentjs.com/timezone/docs/#/using-timezones/converting-to-zone/
              */
              date.tz(args.shift());
              break;
            }

          default:
            {
              /*
              * Formats a date by taking a string of tokens and replacing
              * them with their corresponding values.
              * http://momentjs.com/docs/#/displaying/format/
              */

              var format = method;
              date = date.format(format);
            }
        }

        if (args.length) { parse.apply(parse, args); }
      }

      parse.apply(parse, args);

      return date;
    });

    Vue.filter('duration', function () {
      var arguments$1 = arguments;

      for (var _len3 = arguments.length, args = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
        args[_key3] = arguments$1[_key3];
      }

      /*
      * Basic pass-through filter for leveraging moment.js's ability
      * to manipulate and display durations.
      * https://momentjs.com/docs/#/durations/
      */
      args = Array.prototype.slice.call(args);
      var input = args.shift();
      var method = args.shift();

      function createDuration(time) {
        if (!Array.isArray(time)) { time = [time]; }
        var result = moment$$1.duration.apply(moment$$1, _toConsumableArray(time));
        if (!result.isValid()) { console.warn('Could not build a valid `duration` object from input.'); }
        return result;
      }
      var duration = createDuration(input);

      if (method === 'add' || method === 'subtract') {
        // Generates a duration object and either adds or subtracts it
        // from our original duration.
        var durationChange = createDuration(args);
        duration[method](durationChange);
      } else if (duration && duration[method]) {
        var _duration;

        // This gives a full proxy to moment.duration functions.
        duration = (_duration = duration)[method].apply(_duration, _toConsumableArray(args));
      }

      return duration;
    });
  }
};

var vueMoment_1 = vueMoment.install;

exports['default'] = vueMoment;
exports.install = vueMoment_1;

Object.defineProperty(exports, '__esModule', { value: true });

})));

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(2)))

/***/ })
],[78]);
//# sourceMappingURL=app.9fcbe71d398d723fd588.js.map